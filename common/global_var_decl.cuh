#ifndef __GLOBAL_VAR_DECL_CUH__
#define __GLOBAL_VAR_DECL_CUH__

#include "mesh_type.h"
#include "gpu_helper.h"

GType::type *GType2_dev, *GType1_dev, *GTypeT_dev;
int  *IIndex_dev, *JIndex_dev, *BNI_dev, *ONI_dev;

CoordMember *Cordx_dev, *Cordy_dev, *Cordz_dev;

//int    *Nb_Points_dev;
//double *Csvd_dev;
MeshlessMember* C2M_List_dev = NULL;
MeshlessMember* Cld_List_dev = NULL;

double *P_dev, *U_dev, *V_dev, *W_dev;
double *So_dev, *So_U_dev, *So_V_dev, *So_W_dev;
//double *Src_U_dev, *Src_V_dev, *Src_W_dev;
double *TMP1_dev, *TMP2_dev, *TMP3_dev, *TMP4_dev;
//double *Ustar_dev, *Vstar_dev, *Wstar_dev;

double *U_Ale_dev, *V_Ale_dev, *W_Ale_dev;
double *ACCX_Ale_dev, *ACCY_Ale_dev, *ACCZ_Ale_dev;

//double *X_dev, *Y_dev, *Z_dev;
double *X_cld_dev, *Y_cld_dev, *Z_cld_dev;
double *MNx_dev, *MNy_dev, *MNz_dev;


void mallocGlobal()
{
    int ico, iv;
	

    Point_Car=IPoint*JPoint*KPoint;
    Point_All=Point_Car+Point_Meshless;

    GType1 = new char [Point_All];
    GType2 = new char [Point_All];
    GTypeT = new char [Point_All];
    IIndex = new int [IPoint];
    JIndex = new int [JPoint];

	for(ico=0;ico<3;ico++) MeshAll_Cord[ico] = new double [Point_All];
    X = MeshAll_Cord[0];
    Y = MeshAll_Cord[1];
    Z = MeshAll_Cord[2];
	Fcx = new double [IPoint];
    Fcy = new double [JPoint];
	Fcz = new double [KPoint];
	X_Meshless = & MeshAll_Cord[0][Point_Car];
	Y_Meshless = & MeshAll_Cord[1][Point_Car];
	Z_Meshless = & MeshAll_Cord[2][Point_Car];
    
    Cord[0] = new CoordMember [IPoint];
    Cord[1] = new CoordMember [JPoint];
    Cord[2] = new CoordMember [KPoint];
    
    MNx = new double [Point_Meshless]; 
	MNy = new double [Point_Meshless];
	MNz = new double [Point_Meshless];
	
	ACCX_Ale=new double [Point_Meshless];
	ACCY_Ale=new double [Point_Meshless];
	ACCZ_Ale=new double [Point_Meshless];
	
	ACCX_Ale_Old=new double [Point_Meshless];
	ACCY_Ale_Old=new double [Point_Meshless];
	ACCZ_Ale_Old=new double [Point_Meshless];
	
	PointCategory = new int [Point_Meshless];
	
	showindex=new double [Point_Meshless];
	
    Cld_List = new MeshlessMember [Point_Meshless];
//    Cld_List_Old = new MeshlessMember [Point_Meshless];
	
	for(iv=0;iv<NF_MAX;iv++)
	{
        F0[iv] = new double [Point_All];
		F1[iv] = new double [Point_All];
		F2[iv] = new double [Point_All];
	}	
	
	U=F0[0];
	V=F0[1];
	W=F0[2];
	P=F0[3];
	Ustar=F1[0];
    Vstar=F1[1];	
    Wstar=F1[2];	
    So=F1[3];
	U_Old=F2[0];
    V_Old=F2[1];	
    W_Old=F2[2];	
    P_Old=F2[3];
    
    So_U = new double [Point_All];
	So_V = new double [Point_All];
	So_W = new double [Point_All];
	
	if(ALE == 1)
	{
		X_Meshless_Old = new double [Point_Meshless];
        Y_Meshless_Old = new double [Point_Meshless];
        Z_Meshless_Old = new double [Point_Meshless];
		U_Ale = new double [Point_Meshless];
		V_Ale = new double [Point_Meshless];
		W_Ale = new double [Point_Meshless];
		U_Ale_Old = new double [Point_Meshless];
		V_Ale_Old = new double [Point_Meshless];
		W_Ale_Old = new double [Point_Meshless];
	}
	
    if (Rigid_Object_Number>0)
	{
		Rigid_Offset = new int [Rigid_Object_Number];
		Body_Offset  = new int [Rigid_Object_Number];
		Outer_Offset = new int [Rigid_Object_Number];
	}
}

void mfreeGlobal()
{
    int ico, iv;
 
    delete []GType1;
	delete []GType2;
	delete []GTypeT;
	delete []IIndex;
	delete []JIndex;

	for(ico=0;ico<3;ico++) delete []MeshAll_Cord[ico];
	for(ico=0;ico<3;ico++) delete []Cord[ico];
	
    delete []MNx; 
	delete []MNy; 
	delete []MNz; 
	delete []Fcx;
	delete []Fcy;
	delete []Fcz;

	for(iv=0;iv<NF_MAX;iv++)
	{
        delete []F0[iv];
		delete []F1[iv];
		delete []F2[iv];
	}
	
	delete []Cld_List;
//    delete []Cld_List_Old;
	
	delete []PointCategory;

	if(ALE == 1)
	{
		delete []X_Meshless_Old;
        delete []Y_Meshless_Old;
        delete []Z_Meshless_Old;
		delete []U_Ale;
		delete []V_Ale;
		delete []W_Ale;
		delete []U_Ale_Old;
		delete []V_Ale_Old;
		delete []W_Ale_Old;
	}
}

void mallocGlobal_GPU()
{
    cudaMalloc((void**)&GType2_dev, Point_All*sizeof(GType::type));
    cudaMalloc((void**)&GType1_dev, Point_All*sizeof(GType::type));
    cudaMalloc((void**)&GTypeT_dev, Point_All*sizeof(GType::type));
    
//    cudaMalloc((void**)&count_mark_dev, Point_All*sizeof(int));
    cudaMalloc((void**)&IIndex_dev, IPoint*sizeof(int));
    cudaMalloc((void**)&JIndex_dev, JPoint*sizeof(int));
    cudaMalloc((void**)&ONI_dev, Total_Outer_Nodes*sizeof(int));
    cudaMalloc((void**)&BNI_dev, Total_Body_Nodes*sizeof(int));
    
    cudaMalloc((void**)&Cordx_dev, IPoint*sizeof(CoordMember));
    cudaMalloc((void**)&Cordy_dev, JPoint*sizeof(CoordMember));
    cudaMalloc((void**)&Cordz_dev, KPoint*sizeof(CoordMember));
    
    cudaMemcpy(IIndex_dev, IIndex, IPoint*sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(JIndex_dev, JIndex, JPoint*sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(ONI_dev, ONI, Total_Outer_Nodes*sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(BNI_dev, BNI, Total_Body_Nodes*sizeof(int), cudaMemcpyHostToDevice);
    
    cudaMemcpy(Cordx_dev, Cord[0], IPoint*sizeof(CoordMember), cudaMemcpyHostToDevice);
    cudaMemcpy(Cordy_dev, Cord[1], JPoint*sizeof(CoordMember), cudaMemcpyHostToDevice);
    cudaMemcpy(Cordz_dev, Cord[2], KPoint*sizeof(CoordMember), cudaMemcpyHostToDevice);
    
    cudaMalloc((void**)&Cld_List_dev, Point_Meshless*sizeof(MeshlessMember));
//    cudaMalloc((void**)&C2M_List_dev, C2M_List.size()*sizeof(MeshlessMember));
    if (C2M_List_dev != NULL)
    {
        cudaFree(C2M_List_dev);
        C2M_List_dev = NULL;
    }
    
    cudaMalloc((void**)&P_dev, Point_All*sizeof(double));
    cudaMalloc((void**)&U_dev, Point_All*sizeof(double));
    cudaMalloc((void**)&V_dev, Point_All*sizeof(double));
    cudaMalloc((void**)&W_dev, Point_All*sizeof(double));

    cudaMalloc((void**)&So_dev,   Point_All*sizeof(double));
    cudaMalloc((void**)&So_U_dev, Point_All*sizeof(double));
    cudaMalloc((void**)&So_V_dev, Point_All*sizeof(double));
    cudaMalloc((void**)&So_W_dev, Point_All*sizeof(double));

//    cudaMalloc((void**)&Src_U_dev, Point_All*sizeof(double));
//    cudaMalloc((void**)&Src_V_dev, Point_All*sizeof(double));
//    cudaMalloc((void**)&Src_W_dev, Point_All*sizeof(double));
    
    cudaMalloc((void**)&TMP1_dev, Point_All*sizeof(double));
    cudaMalloc((void**)&TMP2_dev, Point_All*sizeof(double));
    cudaMalloc((void**)&TMP3_dev, Point_All*sizeof(double));
    cudaMalloc((void**)&TMP4_dev, Point_All*sizeof(double));
    
//    cudaMalloc((void**)&Ustar_dev, Point_All*sizeof(double));
//    cudaMalloc((void**)&Vstar_dev, Point_All*sizeof(double));
//    cudaMalloc((void**)&Wstar_dev, Point_All*sizeof(double));
    
    cudaMalloc((void**)&U_Ale_dev, Point_Meshless*sizeof(double));
    cudaMalloc((void**)&V_Ale_dev, Point_Meshless*sizeof(double));
    cudaMalloc((void**)&W_Ale_dev, Point_Meshless*sizeof(double));

    cudaMalloc((void**)&ACCX_Ale_dev, Point_Meshless*sizeof(double));
    cudaMalloc((void**)&ACCY_Ale_dev, Point_Meshless*sizeof(double));
    cudaMalloc((void**)&ACCZ_Ale_dev, Point_Meshless*sizeof(double));
    
//    cudaMalloc((void**)&X_dev, Point_All*sizeof(double));
//    cudaMalloc((void**)&Y_dev, Point_All*sizeof(double));
//    cudaMalloc((void**)&Z_dev, Point_All*sizeof(double)); 
    
    cudaMalloc((void**)&X_cld_dev, Point_Meshless*sizeof(double));
    cudaMalloc((void**)&Y_cld_dev, Point_Meshless*sizeof(double));
    cudaMalloc((void**)&Z_cld_dev, Point_Meshless*sizeof(double)); 
    
    cudaMalloc((void**)&MNx_dev,Point_Meshless*sizeof(double));
    cudaMalloc((void**)&MNy_dev,Point_Meshless*sizeof(double));
    cudaMalloc((void**)&MNz_dev,Point_Meshless*sizeof(double));
    
    cudaDeviceSynchronize(); getLastCudaError("mallocGlobal_GPU");
}

void mfreeGlobal_GPU()
{
    cudaFree(GType2_dev); GType2_dev = NULL;
    cudaFree(GType1_dev); GType1_dev = NULL;
    cudaFree(GTypeT_dev); GTypeT_dev = NULL;
    
    cudaFree(IIndex_dev); IIndex_dev = NULL;
    cudaFree(JIndex_dev); JIndex_dev = NULL;
    cudaFree(BNI_dev);    BNI_dev = NULL;
    cudaFree(ONI_dev);    ONI_dev = NULL;
    
    cudaFree(Cordx_dev); Cordx_dev = NULL;
    cudaFree(Cordy_dev); Cordy_dev = NULL;
    cudaFree(Cordz_dev); Cordz_dev = NULL;
    
    cudaFree(Cld_List_dev); Cld_List_dev = NULL;
    if (C2M_List_dev != NULL)
    {
        cudaFree(C2M_List_dev);
        C2M_List_dev = NULL;
    }
    
    cudaFree(P_dev); P_dev = NULL;
    cudaFree(U_dev); U_dev = NULL;
    cudaFree(V_dev); V_dev = NULL;
    cudaFree(W_dev); W_dev = NULL;
    
    cudaFree(So_dev); So_dev = NULL;
    cudaFree(So_U_dev); So_U_dev = NULL;
    cudaFree(So_V_dev); So_V_dev = NULL;
    cudaFree(So_W_dev); So_W_dev = NULL;
    
//    cudaFree(Src_U_dev); Src_U_dev = NULL;
//    cudaFree(Src_V_dev); Src_V_dev = NULL;
//    cudaFree(Src_W_dev); Src_W_dev = NULL;
    
    cudaFree(TMP1_dev); TMP1_dev = NULL;
    cudaFree(TMP2_dev); TMP2_dev = NULL;
    cudaFree(TMP3_dev); TMP3_dev = NULL;
    cudaFree(TMP4_dev); TMP4_dev = NULL;
    
//    cudaFree(Ustar_dev); Ustar_dev = NULL;
//    cudaFree(Vstar_dev); Vstar_dev = NULL;
//    cudaFree(Wstar_dev); Wstar_dev = NULL;
    
    cudaFree(U_Ale_dev); U_Ale_dev = NULL;
    cudaFree(V_Ale_dev); V_Ale_dev = NULL;
    cudaFree(W_Ale_dev); W_Ale_dev = NULL;
    
    cudaFree(ACCX_Ale_dev); ACCX_Ale_dev = NULL;
    cudaFree(ACCY_Ale_dev); ACCY_Ale_dev = NULL;
    cudaFree(ACCZ_Ale_dev); ACCZ_Ale_dev = NULL;
    
//    cudaFree(X_dev); X_dev = NULL;
//    cudaFree(Y_dev); Y_dev = NULL;
//    cudaFree(Z_dev); Z_dev = NULL;
    
    cudaFree(X_cld_dev); X_cld_dev = NULL;
    cudaFree(Y_cld_dev); Y_cld_dev = NULL;
    cudaFree(Z_cld_dev); Z_cld_dev = NULL;
    
    cudaFree(MNx_dev); MNx_dev = NULL;
    cudaFree(MNy_dev); MNy_dev = NULL;
    cudaFree(MNz_dev); MNz_dev = NULL;
    
    cudaDeviceSynchronize(); getLastCudaError("mfreeGlobal_GPU");
}

#endif /* __GLOBAL_VAR_DECL_CUH__ */

