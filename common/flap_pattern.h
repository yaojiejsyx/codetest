#ifndef FLAP_PATTERN_H
#define FLAP_PATTERN_H

#include <fstream>

#include "control_module.h"

class FlapPattern 
{
  public:
    /* ***** Controlling Angles ***** */
    double Angle_Current_Phi, Angle_Previous_Phi, Angle_Next_Phi;
    double Angle_Current_Theta, Angle_Previous_Theta, Angle_Next_Theta;
    double Angle_Current_Psi, Angle_Previous_Psi, Angle_Next_Psi;

    double Angle_Current_Stroke_Delta, Angle_Previous_Stroke_Delta, Angle_Next_Stroke_Delta;
    double Angle_Current_AoA_Delta, Angle_Previous_AoA_Delta, Angle_Next_AoA_Delta;
    double Angle_Current_Psi_Delta, Angle_Previous_Psi_Delta, Angle_Next_Psi_Delta;
    
    double Angle_Current_Ele0, Angle_Previous_Ele0, Angle_Next_Ele0;
    /* ****************************** */

    PID_Control_Module X_controller, Y_controller, Z_controller, V_controller;
    PID_Control_Module Orientation_controller, Roll_controller, Yaw_controller;
	
    int check_intvl_Re;
    int check_intvl_Angle;
    //char *modify_mark;
    
    double total_cyc_time, freq, freq_global;

    double wing1_X(double stime, int i);
    double wing1_Y(double stime, int i);
    double wing1_Z(double stime, int i);
    double wing2_X(double stime, int i);
    double wing2_Y(double stime, int i);
    double wing2_Z(double stime, int i);

    double wingplane1_X(double stime, int i);
    double wingplane1_Y(double stime, int i);
    double wingplane1_Z(double stime, int i);
    double wingplane2_X(double stime, int i);
    double wingplane2_Y(double stime, int i);
    double wingplane2_Z(double stime, int i);

    double wing1_Phi(double stime, int i);
    double wing1_Theta(double stime, int i);
    double wing1_Psi(double stime, int i);
    double wing2_Phi(double stime, int i);
    double wing2_Theta(double stime, int i);
    double wing2_Psi(double stime, int i);

    double wingplane1_Phi(double stime, int i);
    double wingplane1_Theta(double stime, int i);
    double wingplane1_Psi(double stime, int i);
    double wingplane2_Phi(double stime, int i);
    double wingplane2_Theta(double stime, int i);
    double wingplane2_Psi(double stime, int i);

    void   Set_Flap_Pattern(double _freq, double root[3], double body_centre[3]);
    void   Update_Flap_Pattern(double stime, int it, double body_centre[3], double body_angle[3], double body_velocity[3], double angular_velocity[3]);
    void   Update_Flap_Kinematics(void);
    void   Get_Current_Controlling_Angles(double stime, double wingplane_angle[3], double wing1_angle[3], double wing2_angle[3]);

    int    Record_Flap_Pattern(ofstream &file);
    int    Load_Flap_Pattern(ifstream &file);
    int    Record_Flap_Pattern_History(double stime,
				          /*for test*/ double wp_a[3], double w1_a[3], double w2_a[3], double freq);

    FlapPattern() 
    {
        cyc_counter = 0;
        //modify_mark = new char [1000];
        //flap_history.open("flap_history.dat");
    }
    ~FlapPattern() 
    { 
        //delete[] modify_mark;
        //flap_history.close();
    }
	
  private:
    double freq_old;
    double stime_start, stime_end; // start and end solver time of current wing cycle
    double cyc_time;
    int    cyc_counter;
    
    double wingplane_root[3];
    double stroke_amplitude;
    double mid_aoa_ini[2], mid_aoa[2][2];
    double rot_tp[2], rot_tp_ini, rot_ft_h, rot_ts;
    
    // wing plane motion parameters
    double cc_wingplane_phi[6];
    double cc_wingplane_theta[6];
    double cc_wingplane_psi[6];

    // wing motion parameters
//    double cc_wing_phi_ini[7];
    double cc_wing_phi[2][8];
//    double cc_wing_psi_ini[7];
    double cc_wing_psi_a1[8];
    double cc_wing_psi_a2[8];
    double cc_wing_psi_b1[9];
    double cc_wing_psi_b2[9];
    double cc_wing_psi_c1[9];
    double cc_wing_psi_c2[9];
    double cc_wing_theta[2][8];
    
    double lastbodycentre[3];

    ofstream flap_history;

    void   Init_Flap_Kinematics(void);
    void   Init_Flap_Controllers(void);
    void   Update_Flap_Controllers(double body_centre[3], double body_angle[3], double body_centre_old[3], double body_velocity[3], double angular_velocity[3], double pitch_predict);
    
    void   Update_Flap_Kinematics_WP(void); // update wingplane kinematics
    void   Update_Flap_Kinematics_Phi(void); // update wing sweep kinematics, i.e. wing angle phi
    void   Update_Flap_Kinematics_Psi(void); // update wing rotation kinematics, i.e. wing angle psi
    void   Update_Flap_Kinematics_Theta(void); // update wing elevation kinematics, i.e. wing angle theta
};

#endif /* FLAP_PATTERN_H */
