#ifndef WRITE_RESULT_H
#define WRITE_RESULT_H


//write_result.cpp
void Write_Result_PLT(int it);
void Write_Result_DAT(int it);
int Write_Result_PLT_Cartesian(int iter, int freq);
int Write_Result_PLT_Meshless(int iter, int freq);
int Write_Result_PLT_RealSurface(int iter, int freq);
void Write_Result_DIV(int iter);

void Frame_tranformation(int it);


#endif /* GLOBAL_H */