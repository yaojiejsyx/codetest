#ifndef GLOBAL_H
#define GLOBAL_H

#define MAX(a,b) ((a)>(b)?(a):(b))
#define MIN(a,b) ((a)<(b)?(a):(b))
#define ABS(a,b) ((a)>0?(a):-(a))

#include "basis.h"
#include "control_module.h"
#include "flap_pattern.h"
#include "frame_structure.h"
#include "insect_parameters.h"
#include "mesh_type.h"
#include "pm.h"

vector<MeshlessMember> C2M_List;
MeshlessMember* Cld_List;
//vector<MeshlessMember> C2M_List_Old;
//MeshlessMember* Cld_List_Old;
RectBox sq_box;

int    Isq, Jsq, Ksq, NumIsq, NumJsq, NumKsq;
double Xsq, Ysq, Zsq, XLsq, YLsq, ZLsq;
int    Drawer_Number;
int    Isq_Dr, Jsq_Dr, Ksq_Dr;
double Xsq_Dr, Ysq_Dr, Zsq_Dr;

int    IJK_Pressure_Ref;

int    FSI=1,FSI_Iter,FSI_Max=5;
double FSI_Eps=1e-6;
double FSI_Eps_Position,FSI_Eps_Angle,FSI_Eps_Momentum,FSI_Eps_Angular_Momentum;

int    Solver_Choice;
int    RECORD;
ofstream outforce;
ofstream outpv;
ofstream trajectory;

//Name for the simulation
char   Name[30], Open_Name[50], number[15], Name_Grd[20];
double Ch_Num;

//Domain information   
int    Point_All;
double *X, *Y, *Z, *MeshAll_Cord[3];
double *Fcx, *Fcy, *Fcz;
double XL, YL, ZL, Deltx, Deltx2;
GType::type   *GType1, *GType2, *GTypeT;

double Obj_XYZ[3],Obj_P[3],Obj_L[3],Obj_Orien[3][3];
double Car_Ale[3],Car_Ale_Old[3];
double bodycentre_old[3]={8,8,8};
double bodycentre[3]={8,8,8};
double bodycentre_tmp[3]={8,8,8};
double Car_ACC[3],Car_ACC_Old[3];

double RF[4]={1,1,1,1},RF_Old[4]={1,1,1,1},RF_Max=1.0;
double RF_Residual[4][3],RF_Residual_Old[4][3];

double Total_Center_B[3], Total_Center_B_Old[3];     // total center in body frame
double Total_Center_G[3], Total_Center_G_Old[3];     // total center in global frame
double Total_Center_Trajectory[3];
double Total_Center_Velocity[3], Total_Center_Acce[3];   // total center velocity and acceleration
double LW_center_UVW[3], RW_center_UVW[3];              // wing center velocity
double LW_center_ACCE[3], RW_center_ACCE[3];              // wing center acceleration
double body_inertia[3][3];                           // body moment of inertia about total center
double I_rw[3][3], I_lw[3][3], I_rw_B[3][3], I_lw_B[3][3];                       //    wing moment of inertia in global/body frame
double body_inertia_effective[3][3];                          // effective body inertia
double AM_BMI[3], d_AM_BMI[3];                                             // body motion independent angular momentum and d(that)/dt
double R_lw_inB[3], R_rw_inB[3];                              // coordinates of wing centers in body frame
double R_rw[3], R_lw[3], R_rw_B[3], R_lw_B[3];                //    vector from wing root to wing center represented in global/body frame
double R_PtoC_rw[3], R_PtoC_lw[3], R_PtoC_rw_B[3], R_PtoC_lw_B[3];                            //    vector from total center to wing root represented in global/body frame
double R_BtoC[3];                                             //    vector from total center to body center represented in global frame.
double omega_rw[3], omega_rwp[3], omega_lw[3], omega_lwp[3];      // angular velocity of wing/wing plane represented in global frame
double omega_rw_B[3], omega_lw_B[3];
double alpha_rw[3], alpha_rwp[3], alpha_lw[3], alpha_lwp[3];      // angular acceleration of wing/wing plane represented in global frame
double Rw_op_Rpc_r[3][3], Rpc_op_Rw_r[3][3], Rpc_op_Rpc_r[3][3];   // defined operation matrix for right wing
double Rw_op_Rpc_l[3][3], Rpc_op_Rw_l[3][3], Rpc_op_Rpc_l[3][3];   // defined operation matrix for right wing

//Cartesian Coordinates
int    IPoint, JPoint, KPoint, Point_Car;
int    *IIndex, *JIndex;
int    NType0=0;  //No. of Type 0 Nodes in Cartesian Nodes
int    NC2M=0;    //No. of Type 3 Nodes in Cartesian Nodes
int    NType4=0;  //No. of Type 4 Nodes in Cartesian Nodes
double *MeshCar_Cord[3];
CoordMember* Cord[3];

//Meshless Grid Points: global view
int    Point_Meshless;
double *X_Meshless_Old, *Y_Meshless_Old, *Z_Meshless_Old, 
       *X_Meshless, *Y_Meshless, *Z_Meshless;	   
//double  ***Csvd, ***Csvd_Old;
//int **Nb_Points, **Nb_Points_Old;


//from the view of No. of Objectives
int    Rigid_Object_Number=3;   
int    *Rigid_Offset, *Body_Offset,	*Outer_Offset;
int    *PointCategory;


int    *BNI, *ONI;
double *Surf_Ele_Area;

double *MNx, *MNy, *MNz;
//double *MNx_Orig, *MNy_Orig, *MNz_Orig;
int    Total_Body_Nodes, Total_Outer_Nodes;


//Aux Parameters
int GlobalInt;
double Aux_Phi0, Aux_Theta0, Aux_Psi0;

//Artificial Viscosities
int	AD;
int	CSVD, CPM, CLD, COP;
int	NSS;

//Solver
double *U, *V, *W, *P, *Ustar, *Vstar, *Wstar, *So, *U_Old, *V_Old, *W_Old, *P_Old;
double *So_U, *So_V, *So_W;
// double *DUX_s,*DUY_s,*DUZ_s;
//double *DPX, *DPY, *DPZ, *DUX, *DUY, *DUZ;
int    *Drawer_Count, **Drawer;
int    Drawer_Member=8;

//int    NF_MAX=4;
double *F0[NF_MAX], *F1[NF_MAX], *F2[NF_MAX], Res[NF_MAX];
int    IU=0, IV=1, IW=2, IP=3;

double *U_Ale, *V_Ale, *W_Ale;
double *U_Ale_Old, *V_Ale_Old, *W_Ale_Old;

double *ACCX_Ale,*ACCY_Ale,*ACCZ_Ale;
double *ACCX_Ale_Old,*ACCY_Ale_Old,*ACCZ_Ale_Old;

//ALE flag
int ALE;

//time step
int    Time_Current, Time_Start, Time_End, Time_Step, Time_Scheme;
int    Time_Begin_Save, Time_Save_Freq, Time_Plain_Test;
int	   Car_Save_Freq, Square_Save_Freq, Meshless_Save_Freq, Body_Save_Freq;
int	   CheckPoint_Freq;
double Time=0, Dt;

// Dimensionless Number
//double Re, Frequency, Gravity;
double Re;

string Processor;

double CoM[3],CoM_Global[3], Wing_Root[3];
double bodyforce[3]={0}, bodytorque[3]={0};
double wing1force[3]={0}, wing1torque[3]={0};
double wing2force[3]={0}, wing2torque[3]={0};

Ref_Frame lab,insect,wing_1,wing_2, wingplane_1, wingplane_2;
Insect_Module insect_parameters;

FlapPattern my_flap_pattern;

////Pressure piossion equation
double Res_PP;
int    Max_N_PP;
double Fp = 1.0;

//Projection Method
int PM_Iter;
int ifs;


double Eff_Rad=1.4;
double  GFactor=0.18;
int Test_Depth=3;

int Test =0;

int  output_redir=1;
char Dirs[50];


double KinematicsPattern;
//General Kinematic Parameters for Symmetrical flapping
double DKS_Tr;
double StrokeAngle, PitchAngle;

ifstream readp;
int No_Cpu;
int No_N;
// Output and Read Control Parameters
int TestMode;
int LPlot, LWrite, LRead;
//LPlot: '1'  plot on screen, '0' donot plot on screen
//LWrite: '1' write to data file, '0' not write to data file
//LRead:  '1' Read previous results, '0' new simulation
ofstream Info, Conv;
double *showindex;


//linklist.cpp
void Fresh_Node();
//void Update_UT();

//node_search.cpp
void updateMeshSystem(const Ref_Frame& lab, int FSI_Iter, const string& Processor, bool is_new = true);



//check_point.cpp
int Write_Check_Point(int it);
int Read_Check_Point(char dataname[], char infoname[]);

bool Init(char chpname[]);
void User(const char auxp[]);
void Set_Para(void);
//void Memory_Allocate(void);
//void Memory_Free();
void Grid(void);
void Get_1DCoord_Info(CoordMember* coord, const int dim, const int offset, const double* Xs);
void Get_Meshless(void);

void Force_Estimate();
void Initial_Guess();
void Solve_Structure_Problem();
void Update_Structrue_value();
double RF_Recursion(double lastrf, double *rold, double *rnew);
void Compute_Relaxation_Factor(int FSI_Iter);
int Check_FSI_REL();
int Check_FSI_ABS();

void Cal_CdCl(int, double);
void Motion(double,int,int);


//GPU related variables
#include "global_var_decl.cuh"


#endif /* GLOBAL_H */

