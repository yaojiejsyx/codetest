#ifndef FRAME_STRUCTURE_H
#define FRAME_STRUCTURE_H

#include "flap_pattern.h"

struct Rigid_Obj
{
	int           	IDENTITY;
	int				LEVEL;
	bool            POINTMODE;
	
	int    			POINT_NUMBER;
	double 			*XYZ[3];
	double 			*UVW[3];
	double          *ACC[3];
	double        	*OUTER_NORMAL_VECTOR[3];
	double        	*AREA;
	
	int             TETRAHEDRON_NUMBER;
	int   			TRIANGLE_NUMBER;
	int             *TETRAHEDRON[4];
	int 			*TRIANGLE[3];
	
	int             *INNERMARK;
	int             *OUTERMARK;
	
	int             INNER_POINT_NUMBER;
	int             OUTER_POINT_NUMBER;
	
};

// A functor to update relative XYZ and angles from wing kinematics
// use template for this?
class AttitudeEvaluator 
{	
  public:
	AttitudeEvaluator(FlapPattern* _pt2object, double (FlapPattern::*_pt2func)(double Time, int i))
	{
		pt2object = _pt2object;  pt2func = _pt2func;
	}
	AttitudeEvaluator() {}
	
	double operator()(double Time, int i) // call using operator
	{
		return (*pt2object.*pt2func)(Time, i);
	}
	
  private:
	double (FlapPattern::*pt2func)(double Time, int i);   // pointer to member function
	FlapPattern *pt2object;                  // pointer to object
};

class Ref_Frame
{
  public:
	double        Time;
  
	int           level;
	Ref_Frame     *super;
	int           subframe_number;
	Ref_Frame     **sub;
	double        local_inertia[3][3];
	double        wing_inertia[3][3];
	
	double        position[3];            //X
	double        wingcenter[3];          // CoM of the wing
	double        wingcenterUVW[3];       // velocity of wing CoM
	double        wingcenterACCE[3];       // acceleration of wing CoM
	double        velocity[3];            //V
	double        acceleration[3];
	double        angle[3];
	double        angular_velocity[3];    //omega
	double        angular_acceleration[3];
	double        orientation[3][3];      //Rc
	double        force[3];               //F
	double        torque[3];              //T
	double        momentum[3];            // Mv
	double        inertia[3][3];         //I
	double        angular_momentum[3];    //L
	
	double        power;
	double        energy;
	
	double        position_old[3];            //X
	double        wingcenter_old[3];
	double        velocity_old[3];            //V
	double        acceleration_old[3];
	double        angle_old[3];
	double        angular_velocity_old[3];    //omega
	double        angular_acceleration_old[3];
	double        orientation_old[3][3];      //Rc
	double        force_old[3];               //F
	double        torque_old[3];              //T
	double        momentum_old[3];            // Mv
	double        inertia_old[3][3];         //I
	double        angular_momentum_old[3];    //L
	
	double        position_tmp[3];            //X
	double        velocity_tmp[3];            //V
	double        acceleration_tmp[3];
	double        angle_tmp[3];
	double        angular_velocity_tmp[3];    //omega
	double        angular_acceleration_tmp[3];
	double        orientation_tmp[3][3];      //Rc
	double        force_tmp[3];               //F
	double        torque_tmp[3];              //T
	double        momentum_tmp[3];            // Mv
	double        inertia_tmp[3][3];         //I
	double        angular_momentum_tmp[3];    //L
	
	int           obj_number;
	Rigid_Obj     **rigid_body;

	Rigid_Obj     *get_rigid_body_from_file(char *);//input the file, allocate memory.
	Rigid_Obj     *get_rigid_body_from_subframe(int,int);//input from other object, allocate memory.
	void          change_ref_frame();//modify the attributes of a body into the current frame;
	void          compute_orientation();
	void          compute_orientation_tmp();
	void          copy_new2old();
	void          get_wing_mass_from_subframe(int sub_id);  // get the wing properties (wing center location, inertia) from subframe.
	void          get_wing_mass_from_subframe_tmp(int sub_id);  // get the wing properties (wing center location, inertia) from subframe.
	
	AttitudeEvaluator compute_x; // functor version of -- double (*compute_x)(double Time, int i);
	AttitudeEvaluator compute_y;
	AttitudeEvaluator compute_z;
	AttitudeEvaluator compute_phi;
	AttitudeEvaluator compute_theta;
	AttitudeEvaluator compute_psi;
	
	void          update_all();
	
  private:
	void          memory_allocate_for_rigid_body(Rigid_Obj*);
	double        compute_angular_velocity1(double Time);
	double        compute_angular_velocity2(double Time);
	double        compute_angular_velocity3(double Time);
	double        compute_angular_acceleration1(double Time);
	double        compute_angular_acceleration2(double Time);
	double        compute_angular_acceleration3(double Time);
	void          generate_cloud(Rigid_Obj*,double);
	void          random_perturbation(Rigid_Obj*,double,double);
	
};

void inverse_change_ref_frame(Ref_Frame *sour,double *sour_X,double *sour_Y,double *sour_Z,double *sour_U,double *sour_V,double *sour_W,
			      Ref_Frame *dest,double *dest_X,double *dest_Y,double *dest_Z,double *dest_U,double *dest_V,double *dest_W);

#endif /*FRAME_STRUCTURE_H*/
