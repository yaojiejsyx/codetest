#pragma once

#include <cuda.h>
#include <cuda_runtime.h>

#include "timer.h"

#define getLastCudaError(msg)      __getLastCudaError (msg, __FILE__, __LINE__)
inline void __getLastCudaError(const char *errorMessage, const char *file, const int line)
{
    cudaError_t err = cudaGetLastError();

    if (cudaSuccess != err)
    {
        fprintf(stderr, "%s(%i) : Last CUDA error : %s : (%d) %s.\n",
                file, line, errorMessage, (int)err, cudaGetErrorString(err));
		cudaDeviceReset();
        exit(EXIT_FAILURE);
    }
}

// A simple timer class

class GpuTimer
{
  private:
    cudaEvent_t start;
    cudaEvent_t stop;

  public:
    GpuTimer()
    {
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
    }

    ~GpuTimer()
    {
        cudaEventDestroy(start);
        cudaEventDestroy(stop);
    }

    void Start()
    {
        cudaEventRecord(start, 0);
    }

    void Stop()
    {
        cudaEventRecord(stop, 0);
    }

    float Elapsed()
    {
        float elapsed;
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&elapsed, start, stop);
        return elapsed;
    }
};

inline unsigned int iDivUp(unsigned int dividend, unsigned int divisor)
{
    return ((dividend % divisor) == 0) ? (dividend / divisor) : (dividend / divisor + 1);
}

template<typename Type>
unsigned int compareArray(const Type* gpu_res, const Type* cpu_res, size_t num)
{
    printf("\n ...comparing the results\n");
    int localFlag = -1;
//    float err;
//    int errCnt = 0;
    
    for (uint iter = 0; iter < num; iter++)
    {
        if (fabs(gpu_res[iter] - cpu_res[iter]) > 1e-12f)
        {
            localFlag = iter;
//            errCnt++;
//            err = fabs(cpu_res[iter]) > 1e-12f ? fabs(cpu_res[iter]) : fabs(gpu_res[iter]);
//            err = fabs(float(gpu_res[iter] - cpu_res[iter])) / err;
            break;
        }
    }
    
    if (localFlag == -1) {
        printf(" ...Results Match\n\n");
        return 0;
    }
    else {
        printf(" ...Results DON'T Match !!! Array value at pos %d: GPU--%f, CPU--%f\n\n", 
               localFlag, float(gpu_res[localFlag]), float(cpu_res[localFlag]));
//        printf(" ...Results DON'T Match !!! %d/%d not match\n\n", 
//               errCnt, num);
        return 1;
    }
}

