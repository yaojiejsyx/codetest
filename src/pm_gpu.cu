#include <iostream>
#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <vector>

#include <cublas.h>
#include <cusparse.h>
#include <cuda_runtime.h>
#include <thrust/device_ptr.h>
//#include <thrust/reduce.h>
#include <thrust/scan.h>

#include "../common/mesh_type.h"
#include "../common/gpu_helper.h"
#include "../common/pm.h"

#include "../common/global_var_list.h"
#include "../common/write_result.h"

using namespace std;

struct SolverInfo
{
    double reynolds, dt, deltax;
    
//    double ad_alpha, ad_beta;
//    unsigned int if_ad;
    
    unsigned int dim[3];
    unsigned int point_all, point_car, point_meshless, point_mobilized;
    double vel[3], acc[3];
} *info_dev;

__constant__ int NB_DEV = NB;

template <size_t DIM>
__host__ __device__
void computeConvectionTerm(double res[DIM], double dudx[][DIM], double U_loc[DIM])
{
	// convection part
	// (U[i]-U_Ale[ip]-Car_Ale[0])*dudx + (V[i]-U_Ale[ip]-Car_Ale[1])*dudy + (W[i]-U_Ale[ip]-Car_Ale[2])*dudz);
	for (int dim=0; dim<DIM; dim++) 
	{
	    res[dim] = 0;
        for (int dim_j=0; dim_j<DIM; dim_j++) 
        {
            res[dim] += U_loc[dim_j] * dudx[dim][dim_j];
        }
    }
}

template <size_t DIM>
__host__ __device__
void computeDiffusionTerm
(
    double res[DIM], double dudx2[][DIM], double dpdx[DIM], double dudx[][DIM],
    double Re, double Deltx, int type
)
{
    // diffusion part
    
    double tmpx[3] = {0};
    double nu_sgs = 0;
    
    // Using global variables: Re, AD, AD_alfa, AD_Beta, Deltx
    
    // Compute artificial diffusion and LES viscousity
#   ifdef USE_ART_DIFF
    for (int dim=0; dim<DIM; dim++) 
    {
        tmpx[dim] = AD_ALPHA*fabs(dpdx[dim]);
        if (tmpx[dim] > AD_BETA) tmpx[dim] = AD_BETA;
        tmpx[dim] = tmpx[dim]*Deltx*Deltx;
    }
#   endif //USE_ART_DIFF
            
#   ifdef USE_LES
    double S_mag = 0;
	
    for (int ii = 0; ii < DIM; ii++)
    for (int jj = 0; jj < DIM; jj++)
        S_mag += (dudx[ii][jj] + dudx[jj][ii])
	           * (dudx[ii][jj] + dudx[jj][ii]);
    
    S_mag *= 0.5;
    
    if (type == 1)
        nu_sgs = (LES_Cs*Deltx)*(LES_Cs*Deltx) * sqrt(S_mag);
    else
        nu_sgs = 0;
#   endif //USE_LES

	// (dudx2+dudy2+dudz2)*(1/Re+tmpx+nu_sgs)
	for (int dim=0; dim<DIM; dim++) 
	{
        res[dim] = 0;
        for (int dim_j=0; dim_j<DIM; dim_j++) 
        {
            res[dim] += dudx2[dim][dim_j];
        }
        res[dim] *= (1.0/Re + tmpx[dim] + nu_sgs);
    }
}

__global__
void initInfo_kernel
(
    SolverInfo* info_d,
    double re, double dt, double deltax,
    int IPoint, int JPoint, int KPoint,
    unsigned int point_all, unsigned int point_car, unsigned int point_meshless, unsigned int point_mobilized,
    double Car_Alex, double Car_Aley, double Car_Alez,
    double Car_ACCx, double Car_ACCy, double Car_ACCz
)
{
    info_d->reynolds = re;
    info_d->dt = dt;
    
    info_d->deltax = deltax;
    
    info_d->point_all = point_all;
    info_d->point_car = point_car;
    info_d->point_meshless = point_meshless;
    info_d->point_mobilized = point_mobilized;
    
    info_d->dim[0] = IPoint;
    info_d->dim[1] = JPoint;
    info_d->dim[2] = KPoint;
    
    info_d->vel[0] = Car_Alex;
    info_d->vel[1] = Car_Aley;
    info_d->vel[2] = Car_Alez;
    
    info_d->acc[0] = Car_ACCx;
    info_d->acc[1] = Car_ACCy;
    info_d->acc[2] = Car_ACCz;
}

void initSolver()
{
    if (C2M_List_dev != NULL)
    {
        cudaFree(C2M_List_dev);
        C2M_List_dev = NULL;
    }
    cudaMalloc((void**)&C2M_List_dev, C2M_List.size()*sizeof(MeshlessMember));
//    cudaMalloc((void**)&Cld_List_dev, Point_Meshless*sizeof(MeshlessMember));
    
//    GpuTimer tt_timer; tt_timer.Start();
    
//    cudaMemcpy(GType2_dev, GType2, Point_All*sizeof(char), cudaMemcpyHostToDevice);
	
//    cudaMemcpy(IIndex_dev, IIndex, IPoint*sizeof(int), cudaMemcpyHostToDevice);
//    cudaMemcpy(JIndex_dev, JIndex, JPoint*sizeof(int), cudaMemcpyHostToDevice);
//    cudaMemcpy(BNI_dev, BNI, Total_Body_Nodes*sizeof(int), cudaMemcpyHostToDevice);
//    
//    cudaMemcpy(Cordx_dev, Cord[0], IPoint*sizeof(CoordMember), cudaMemcpyHostToDevice);
//    cudaMemcpy(Cordy_dev, Cord[1], JPoint*sizeof(CoordMember), cudaMemcpyHostToDevice);
//    cudaMemcpy(Cordz_dev, Cord[2], KPoint*sizeof(CoordMember), cudaMemcpyHostToDevice);
	
	cudaMemcpy(MNx_dev, MNx, Point_Meshless*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(MNy_dev, MNy, Point_Meshless*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(MNz_dev, MNz, Point_Meshless*sizeof(double), cudaMemcpyHostToDevice);
	
    cudaMemcpy(Cld_List_dev, Cld_List, Point_Meshless*sizeof(MeshlessMember), cudaMemcpyHostToDevice);
    cudaMemcpy(C2M_List_dev, &C2M_List[0], C2M_List.size()*sizeof(MeshlessMember), cudaMemcpyHostToDevice);
    
    cudaMemcpy(P_dev, P, Point_All*sizeof(double), cudaMemcpyHostToDevice);
    
    cudaMemcpy(U_dev, U, Point_All*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(V_dev, V, Point_All*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(W_dev, W, Point_All*sizeof(double), cudaMemcpyHostToDevice);
    
    cudaMemcpy(So_U_dev, So_U, Point_All*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(So_V_dev, So_V, Point_All*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(So_W_dev, So_W, Point_All*sizeof(double), cudaMemcpyHostToDevice);
    
    cudaMemcpy(U_Ale_dev, U_Ale, Point_Meshless*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(V_Ale_dev, V_Ale, Point_Meshless*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(W_Ale_dev, W_Ale, Point_Meshless*sizeof(double), cudaMemcpyHostToDevice);
    
    cudaMemcpy(ACCX_Ale_dev, ACCX_Ale, Point_Meshless*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(ACCY_Ale_dev, ACCY_Ale, Point_Meshless*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(ACCZ_Ale_dev, ACCZ_Ale, Point_Meshless*sizeof(double), cudaMemcpyHostToDevice);
    
//    tt_timer.Stop(); cout<<"memcpy time: "<<tt_timer.Elapsed()<<"ms | "<<endl;
    
    cudaMalloc((void**)&info_dev, sizeof(SolverInfo));
    initInfo_kernel <<< 1, 1 >>>
    (
        info_dev,
        Re, Dt, Deltx,
        IPoint, JPoint, KPoint,
        Point_All, Point_Car, Point_Meshless, C2M_List.size(),
        Car_Ale[0], Car_Ale[1], Car_Ale[2],
        Car_ACC[0], Car_ACC[1], Car_ACC[2]
    );
    
	cudaDeviceSynchronize(); getLastCudaError("initSolver");
}

void terminateSolver()
{
    cudaMemcpy(U, U_dev, Point_All*sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(V, V_dev, Point_All*sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(W, W_dev, Point_All*sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(P, P_dev, Point_All*sizeof(double), cudaMemcpyDeviceToHost);
    
    cudaFree(C2M_List_dev); C2M_List_dev = NULL;
//    cudaFree(Cld_List_dev); Cld_List_dev = NULL;
    
    cudaFree(info_dev); info_dev = NULL;
    
	cudaDeviceSynchronize(); getLastCudaError("terminateSolver");
}

//dim3 grid1(IPoint, JPoint, 1);
//dim3 block1(KPoint, 1, 1);
__global__
void calNewCart_kernel
(
    double* Src_U_d, double* Src_V_d, double* Src_W_d,
    const double* P_d, const double* U_d, const double* V_d, const double* W_d,
    const char* GType2_d, CoordMember* const Cordx_d, CoordMember* const Cordy_d, CoordMember* const Cordz_d,
    const SolverInfo* const info
)
{
    // index range: 1 ~ IPoint-2
    unsigned int i = blockIdx.x; 
    unsigned int j = blockIdx.y; 
    unsigned int k = threadIdx.x;
    if (i>=info->dim[0] || j>=info->dim[1] || k>=info->dim[2]) return;
    
    int ijk = i*info->dim[1]*info->dim[2] + j*info->dim[2] + k;
    if (GType2_d[ijk] != 1) return;
    
//        double dudxv[3][3] = { { dudx, dudy, dudz }, 
//                               { dvdx, dvdy, dvdz },
//                               { dwdx, dwdy, dwdz } };
    double dpdx[3]     = {0};
    double dudx[3][3]  = {0};
    double dudx2[3][3] = {0};
    double conv[3], diff[3];
    double coef_w, coef_p, coef_e;
    
    int id;
    int ijk_w, ijk_e;
    CoordMember* current_coord = NULL;
    
    for (int dim=0; dim<3; dim++)
    {
        switch (dim)
        {
          case 0:
            id = i; 
            current_coord = Cordx_d;
            break;
          case 1:
            id = j; 
            current_coord = Cordy_d;
            break;
          case 2:
            id = k; 
            current_coord = Cordz_d;
            break;
          default:
#           if __CUDA_ARCH__ >= 200
            printf("dimension error!\n");
#           endif
            continue;
        }
        
        ijk_w = ijk - current_coord[id].offset[0] + current_coord[id].offset[1];
        ijk_e = ijk - current_coord[id].offset[0] + current_coord[id].offset[2];
        
        // 1st order difference coefficient
        coef_w = current_coord[id].coef[1][1];
        coef_p = current_coord[id].coef[1][0];
        coef_e = current_coord[id].coef[1][2];
        
        dpdx[dim] = P_d[ijk_w]*coef_w + P_d[ijk]*coef_p + P_d[ijk_e]*coef_e;
        
        dudx[0][dim] = U_d[ijk_w]*coef_w + U_d[ijk]*coef_p + U_d[ijk_e]*coef_e;
        dudx[1][dim] = V_d[ijk_w]*coef_w + V_d[ijk]*coef_p + V_d[ijk_e]*coef_e;
        dudx[2][dim] = W_d[ijk_w]*coef_w + W_d[ijk]*coef_p + W_d[ijk_e]*coef_e;
        
        // 2st order difference coefficient
        coef_w = current_coord[id].coef[2][1];
        coef_p = current_coord[id].coef[2][0];
        coef_e = current_coord[id].coef[2][2];
        
        dudx2[0][dim] = U_d[ijk_w]*coef_w + U_d[ijk]*coef_p + U_d[ijk_e]*coef_e;
        dudx2[1][dim] = V_d[ijk_w]*coef_w + V_d[ijk]*coef_p + V_d[ijk_e]*coef_e;
        dudx2[2][dim] = W_d[ijk_w]*coef_w + W_d[ijk]*coef_p + W_d[ijk_e]*coef_e;
    }
    
//#ifdef UPWIND
//    double vel = 0, coef_uw[3] = {0};
//    int ijk_uw[3] = {0};
//    if ((i==1)||(j==1)||(k==1)||(i==(IPoint-2))||j==((JPoint-2))||k==((KPoint-2)))
//    {}
//    else
//    {
//    for (int dim=0; dim<3; dim++)
//    {
//        switch (dim)
//        {
//          case 0:
//            id = i; vel = U_d[ijk]; 
//            current_coord = Cordx_d;
//            break;
//          case 1:
//            id = j; vel = V_d[ijk]; 
//            current_coord = Cordy_d;
//            break;
//          case 2:
//            id = k; vel = W_d[ijk]; 
//            current_coord = Cordy_d;
//            break;
//          default:
//#           if __CUDA_ARCH__ >= 200
//            printf("dimension error!\n");
//#           endif
//            continue;
//        }
//        
//        if (vel >= 0)
//        {
//            for (int i_nb = 0; i_nb<3; i_nb++)
//            {
//                ijk_uw[i_nb] = ijk - current_coord[id].offset_upwind[0][0] + current_coord[id].offset_upwind[0][i_nb];
//                coef_uw[i_nb] = current_coord[id].coef_upwind[0][i_nb];
//            }
//        }
//        else
//        {
//            for (int i_nb = 0; i_nb<3; i_nb++)
//            {
//                ijk_uw[i_nb] = ijk - current_coord[id].offset_upwind[1][0] + current_coord[id].offset_upwind[1][i_nb];
//                coef_uw[i_nb] = current_coordid].coef_upwind[1][i_nb];
//            }
//        }
//        
//        dudx[0][dim] = U_d[ijk_uw[0]]*coef_uw[0] + U_d[ijk_uw[1]]*coef_uw[1] + U_d[ijk_uw[2]]*coef_uw[2];
//        dudx[1][dim] = V_d[ijk_uw[0]]*coef_uw[0] + V_d[ijk_uw[1]]*coef_uw[1] + V_d[ijk_uw[2]]*coef_uw[2];
//        dudx[2][dim] = W_d[ijk_uw[0]]*coef_uw[0] + W_d[ijk_uw[1]]*coef_uw[1] + W_d[ijk_uw[2]]*coef_uw[2];
//    }
//    }
//#endif
    
	// currently ALE = TRUE
	double U_loc[3] = { U_d[ijk] - info->vel[0],
	                    V_d[ijk] - info->vel[1],
	                    W_d[ijk] - info->vel[2] };
    computeConvectionTerm <3> (conv, dudx, U_loc);
	
    computeDiffusionTerm  <3> (diff, dudx2, dpdx, dudx, info->reynolds, info->deltax, 1);

    Src_U_d[ijk] = 0.5*info->dt*(diff[0] - conv[0]);
    Src_V_d[ijk] = 0.5*info->dt*(diff[1] - conv[1]);
    Src_W_d[ijk] = 0.5*info->dt*(diff[2] - conv[2]);
}

//block.x = 256; block.y = 1; block.z = 1;
//grid.x  = iDivUp(C2M_List.size(), block.x); grid.y = 1; grid.z = 1;
__global__
void calNewML_kernel
(
    double* Src_U_d, double* Src_V_d, double* Src_W_d,
    const double* P_d, const double* U_d, const double* V_d, const double* W_d,
    const char* GType2_d, const MeshlessMember* Mls_List_d,
    const double* U_Ale_d, const double* V_Ale_d, const double* W_Ale_d, 
    const SolverInfo* const info, int mls_type
)
{
    unsigned int ip = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int size = 0;
    switch (mls_type)
    {
      case 1:
        size = info->point_meshless;
        break;
      case 2:
        size = info->point_mobilized;
        break;
    }
    if (ip >= size) return;
    
    unsigned int i = Mls_List_d[ip].Meshless_Ind, j;
    if (GType2_d[i] != 3) return;
    
//        double dudxv[3][3] = { { dudx, dudy, dudz }, 
//                               { dvdx, dvdy, dvdz },
//                               { dwdx, dwdy, dwdz } };
    double dpdx[3]     = {0};
    double dudx[3][3]  = {0};
    double dudx2[3][3] = {0};
    double conv[3], diff[3];
    
    for (int dim=0; dim<3; dim++)
    {
	    for (int io=0; io<NB; io++) 
	    {
            j = Mls_List_d[ip].Nb_Points[io];
            
            dpdx[dim] = dpdx[dim] + Mls_List_d[ip].Csvd[dim][io] * (P_d[j] - P_d[i]);
            
            dudx[0][dim] = dudx[0][dim] + Mls_List_d[ip].Csvd[dim][io] * (U_d[j] - U_d[i]);
            dudx[1][dim] = dudx[1][dim] + Mls_List_d[ip].Csvd[dim][io] * (V_d[j] - V_d[i]);
            dudx[2][dim] = dudx[2][dim] + Mls_List_d[ip].Csvd[dim][io] * (W_d[j] - W_d[i]);
            
            dudx2[0][dim] = dudx2[0][dim] + Mls_List_d[ip].Csvd[dim+3][io] * (U_d[j]-U_d[i]);
            dudx2[1][dim] = dudx2[1][dim] + Mls_List_d[ip].Csvd[dim+3][io] * (V_d[j]-V_d[i]);
            dudx2[2][dim] = dudx2[2][dim] + Mls_List_d[ip].Csvd[dim+3][io] * (W_d[j]-W_d[i]);
        }
	}
	
	// currently ALE = TRUE
	double U_loc[3] = {0};
    switch (mls_type)
    {
      case 1:
        U_loc[0] = U_d[i] - U_Ale_d[ip] - info->vel[0];
        U_loc[1] = V_d[i] - V_Ale_d[ip] - info->vel[1];
        U_loc[2] = W_d[i] - W_Ale_d[ip] - info->vel[2];
        break;
      case 2:
        U_loc[0] = U_d[i] - info->vel[0];
        U_loc[1] = V_d[i] - info->vel[1];
        U_loc[2] = W_d[i] - info->vel[2];
        break;
    }
    computeConvectionTerm <3> (conv, dudx, U_loc);
	
    computeDiffusionTerm  <3> (diff, dudx2, dpdx, dudx, info->reynolds, info->deltax, 1);

    Src_U_d[i] = 0.5*info->dt*(diff[0] - conv[0]);
    Src_V_d[i] = 0.5*info->dt*(diff[1] - conv[1]);
    Src_W_d[i] = 0.5*info->dt*(diff[2] - conv[2]);
}

//block.x = 256; block.y = 1; block.z = 1;
//grid.x  = iDivUp(Point_All, block.x); grid.y = 1; grid.z = 1;
__global__
void calUstar_kernel
(
    double* Ustar, double* Vstar, double* Wstar,
    const double* U_d, const double* V_d, const double* W_d,
    const double* Src_U_d, const double* Src_V_d, const double* Src_W_d,
    const double* So_U_d, const double* So_V_d, const double* So_W_d,
    const double* U_Ale_d, const double* V_Ale_d, const double* W_Ale_d, 
    const double* ACCX_Ale_d, const double* ACCY_Ale_d, const double* ACCZ_Ale_d, 
    const char* grid_type2,
    const SolverInfo* const info
)
{
    unsigned int ijk = blockIdx.x * blockDim.x + threadIdx.x;
    if (ijk >= info->point_all) return;
    
    if (grid_type2[ijk] == GType::_7POINT || grid_type2[ijk] == GType::_GFDSVD)
    {
        Ustar[ijk] = Src_U_d[ijk] + So_U_d[ijk];
        Vstar[ijk] = Src_V_d[ijk] + So_V_d[ijk];
        Wstar[ijk] = Src_W_d[ijk] + So_W_d[ijk];
    }
    else if (grid_type2[ijk] == GType::OUTFLOW)
    {
        Ustar[ijk] = U_d[ijk];
        Vstar[ijk] = V_d[ijk];
        Wstar[ijk] = W_d[ijk];
    }
    else if (grid_type2[ijk] == GType::WALL)
    {
        unsigned int ip = ijk - info->point_car;
        
        Ustar[ijk] = U_Ale_d[ip] + info->vel[0] - 0.5*info->dt*(ACCX_Ale_d[ip] + info->acc[0]);
		Vstar[ijk] = V_Ale_d[ip] + info->vel[1] - 0.5*info->dt*(ACCY_Ale_d[ip] + info->acc[1]);
		Wstar[ijk] = W_Ale_d[ip] + info->vel[2] - 0.5*info->dt*(ACCZ_Ale_d[ip] + info->acc[2]);
    }
    else
    {
        return;
    }
}

void computeSourceContributionNew_GPU()
{
    dim3 grid, block;
    
    grid.x  = IPoint; grid.y  = JPoint; grid.z  = 1;
    block.x = KPoint; block.y = 1;      block.z = 1;
    calNewCart_kernel <<< grid, block >>>
    (
//        Src_U_dev, Src_V_dev, Src_W_dev,
        TMP1_dev, TMP2_dev, TMP3_dev,
        P_dev, U_dev, V_dev, W_dev,
        GType2_dev, Cordx_dev, Cordy_dev, Cordz_dev,
        info_dev
    );
    cudaDeviceSynchronize(); getLastCudaError("calCartNew");
    
    block.x = 256; block.y = 1; block.z = 1;
    grid.x  = iDivUp(Point_Meshless, block.x); grid.y = 1; grid.z = 1;
    calNewML_kernel <<< grid, block >>>
    (
//        Src_U_dev, Src_V_dev, Src_W_dev,
        TMP1_dev, TMP2_dev, TMP3_dev,
        P_dev, U_dev, V_dev, W_dev,
        GType2_dev, Cld_List_dev,
        U_Ale_dev, V_Ale_dev, W_Ale_dev,
        info_dev, 1
    );
    cudaDeviceSynchronize(); getLastCudaError("calMLNew1");
    
    block.x = 256; block.y = 1; block.z = 1;
    grid.x  = iDivUp(C2M_List.size(), block.x); grid.y = 1; grid.z = 1;
    calNewML_kernel <<< grid, block >>>
    (
//        Src_U_dev, Src_V_dev, Src_W_dev, 
        TMP1_dev, TMP2_dev, TMP3_dev,
        P_dev, U_dev, V_dev, W_dev,
        GType2_dev, C2M_List_dev,
        U_Ale_dev, V_Ale_dev, W_Ale_dev,
        info_dev, 2
    );
    cudaDeviceSynchronize(); getLastCudaError("calMLNew2");
    
    block.x = 256; block.y = 1; block.z = 1;
    grid.x  = iDivUp(Point_All, block.x); grid.y = 1; grid.z = 1;
    calUstar_kernel <<< grid, block >>>
    (
//        Ustar_dev, Vstar_dev, Wstar_dev,
        U_dev, V_dev, W_dev,
        U_dev, V_dev, W_dev,
//        Src_U_dev, Src_V_dev, Src_W_dev,
        TMP1_dev, TMP2_dev, TMP3_dev,
        So_U_dev, So_V_dev, So_W_dev, 
        U_Ale_dev, V_Ale_dev, W_Ale_dev,
        ACCX_Ale_dev, ACCY_Ale_dev, ACCZ_Ale_dev,
        GType2_dev, info_dev
    );
    cudaDeviceSynchronize(); getLastCudaError("calUstar");
}

//dim3 grid1(IPoint, JPoint, 1);
//dim3 block1(KPoint, 1, 1);
__global__
void calSrcCart_kernel
(
    double* So,
    const double* Ustar, const double* Vstar, const double* Wstar,
    const char* GType2, CoordMember* const Cordx, CoordMember* const Cordy, CoordMember* const Cordz,
    const SolverInfo* const info
)
{
    // index range: 1 ~ IPoint-2
    unsigned int i = blockIdx.x; 
    unsigned int j = blockIdx.y; 
    unsigned int k = threadIdx.x;
    if (i>=info->dim[0] || j>=info->dim[1] || k>=info->dim[2]) return;
    
    unsigned int ijk = i*info->dim[1]*info->dim[2] + j*info->dim[2] + k;
    if (GType2[ijk] != GType::_7POINT)
    {
        So[ijk] = 0;
        return;
    }
    
    unsigned int ijk_w, ijk_e;
    So[ijk] = 0;
    
    ijk_w = ijk - Cordx[i].offset[0] + Cordx[i].offset[1];
    ijk_e = ijk - Cordx[i].offset[0] + Cordx[i].offset[2];
    So[ijk] += Ustar[ijk_w] * Cordx[i].coef[1][1]
             + Ustar[ijk]   * Cordx[i].coef[1][0]
             + Ustar[ijk_e] * Cordx[i].coef[1][2];
    
    ijk_w = ijk - Cordy[j].offset[0] + Cordy[j].offset[1];
    ijk_e = ijk - Cordy[j].offset[0] + Cordy[j].offset[2];
    So[ijk] += Vstar[ijk_w] * Cordy[j].coef[1][1]
             + Vstar[ijk]   * Cordy[j].coef[1][0]
             + Vstar[ijk_e] * Cordy[j].coef[1][2];
    
    ijk_w = ijk - Cordz[k].offset[0] + Cordz[k].offset[1];
    ijk_e = ijk - Cordz[k].offset[0] + Cordz[k].offset[2];
    So[ijk] += Wstar[ijk_w] * Cordz[k].coef[1][1]
             + Wstar[ijk]   * Cordz[k].coef[1][0]
             + Wstar[ijk_e] * Cordz[k].coef[1][2];
    
    So[ijk] = So[ijk] * (2.0/info->dt);
}

__global__
void calSrcML_kernel
(
    double* So,
    const double* Ustar, const double* Vstar, const double* Wstar,
    const char* GType2, const MeshlessMember* Mls_List,
    const double* MNx, const double* MNy, const double* MNz,
    const double* ACCX_Ale, const double* ACCY_Ale, const double* ACCZ_Ale, 
    const SolverInfo* const info, int mls_type
)
{
    unsigned int ip = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int size = 0;
    switch (mls_type)
    {
      case 1:
        size = info->point_meshless;
        break;
      case 2:
        size = info->point_mobilized;
        break;
    }
    if (ip >= size) return;
    
    unsigned int i = Mls_List[ip].Meshless_Ind;
    
	if (GType2[i] == GType::_GFDSVD || GType2[i] == GType::MOBILIZED)
	{
        So[i] = 0;
        for (int io=0; io<NB_DEV; io++)
        {
            int j = Mls_List[ip].Nb_Points[io];
            
            So[i] += Mls_List[ip].Csvd[0][io] * (Ustar[j] - Ustar[i])
                   + Mls_List[ip].Csvd[1][io] * (Vstar[j] - Vstar[i])
                   + Mls_List[ip].Csvd[2][io] * (Wstar[j] - Wstar[i]);
        }
        
        So[i] = So[i] * (2.0/info->dt);
    }
	else if (GType2[i] == GType::WALL)
	{
        So[i] = -( MNx[ip] * (ACCX_Ale[ip] + info->acc[0])
                 + MNy[ip] * (ACCY_Ale[ip] + info->acc[1])
                 + MNz[ip] * (ACCZ_Ale[ip] + info->acc[2]) );
	}
	else
	{
	    So[i] = 0;
	    return;
	}
}

void computeSource_GPU()
{
    dim3 grid, block;
    
    grid.x  = IPoint; grid.y  = JPoint; grid.z  = 1;
    block.x = KPoint; block.y = 1;      block.z = 1;
    calSrcCart_kernel <<< grid, block >>>
    (
        So_dev,
//        Ustar_dev, Vstar_dev, Wstar_dev,
        U_dev, V_dev, W_dev,
        GType2_dev, Cordx_dev, Cordy_dev, Cordz_dev,
        info_dev
    );
    cudaDeviceSynchronize(); getLastCudaError("calCartSrc");
    
    block.x = 256; block.y = 1; block.z = 1;
    grid.x  = iDivUp(Point_Meshless, block.x); grid.y = 1; grid.z = 1;
    calSrcML_kernel <<< grid, block >>>
    (
        So_dev,
//        Ustar_dev, Vstar_dev, Wstar_dev,
        U_dev, V_dev, W_dev,
        GType2_dev, Cld_List_dev,
        MNx_dev, MNy_dev, MNz_dev,
        ACCX_Ale_dev, ACCY_Ale_dev, ACCZ_Ale_dev,
        info_dev, 1
    );
    cudaDeviceSynchronize(); getLastCudaError("calMLSrc1");
    
    block.x = 256; block.y = 1; block.z = 1;
    grid.x  = iDivUp(C2M_List.size(), block.x); grid.y = 1; grid.z = 1;
    calSrcML_kernel <<< grid, block >>>
    (
        So_dev,
//        Ustar_dev, Vstar_dev, Wstar_dev,
        U_dev, V_dev, W_dev,
        GType2_dev, C2M_List_dev,
        MNx_dev, MNy_dev, MNz_dev,
        ACCX_Ale_dev, ACCY_Ale_dev, ACCZ_Ale_dev,
        info_dev, 2
    );
    cudaDeviceSynchronize(); getLastCudaError("calMLSrc2");
}

//dim3 grid1(IPoint, JPoint, 1);
//dim3 block1(KPoint, 1, 1);
__global__
void updateVelCart_kernel
(
    double* U, double* V, double* W,
    double* P, const double* Ustar, const double* Vstar, const double* Wstar,
    const char* GType2, CoordMember* const Cordx, CoordMember* const Cordy, CoordMember* const Cordz,
    const SolverInfo* const info
)
{
    unsigned int i = blockIdx.x; 
    unsigned int j = blockIdx.y; 
    unsigned int k = threadIdx.x;
    if (i>=info->dim[0] || j>=info->dim[1] || k>=info->dim[2]) return;
    
    unsigned int ijk = i*info->dim[1]*info->dim[2] + j*info->dim[2] + k;
    
    if (GType2[ijk] == GType::INACTIVE) 
    {
        U[ijk] = V[ijk] = W[ijk] = P[ijk] = 0;
        return;
    }
    if (GType2[ijk] != GType::_7POINT) return;
    
    double dpdx[3] = {0};
    int id, ijk_w, ijk_e;
    double coef_p, coef_w, coef_e;
    CoordMember* current_coord;
    
    for (int dim=0; dim<3; dim++)
    {
        switch (dim)
        {
          case 0:
            id = i; 
            current_coord = Cordx;
            break;
          case 1:
            id = j; 
            current_coord = Cordy;
            break;
          case 2:
            id = k; 
            current_coord = Cordz;
            break;
          default:
#           if __CUDA_ARCH__ >= 200
            printf("dimension error!\n");
#           endif
            continue;
        }
        
        ijk_w = ijk - current_coord[id].offset[0] + current_coord[id].offset[1];
        ijk_e = ijk - current_coord[id].offset[0] + current_coord[id].offset[2];
        
        // 1st order difference coefficient
        coef_p = current_coord[id].coef[1][0];
        coef_w = current_coord[id].coef[1][1];
        coef_e = current_coord[id].coef[1][2];
        
        dpdx[dim] = P[ijk_w]*coef_w + P[ijk]*coef_p + P[ijk_e]*coef_e;
    }
    
    U[ijk] = Ustar[ijk] - 0.5*info->dt*dpdx[0];
    V[ijk] = Vstar[ijk] - 0.5*info->dt*dpdx[1];
    W[ijk] = Wstar[ijk] - 0.5*info->dt*dpdx[2];
}

//block.x = 256; block.y = 1; block.z = 1;
//grid.x  = iDivUp(C2M_List.size(), block.x); grid.y = 1; grid.z = 1;
__global__
void updateVelML_kernel
(
    double* U, double* V, double* W,
    double* P, const double* Ustar, const double* Vstar, const double* Wstar,
    const char* GType2, const MeshlessMember* Mls_List,
    const double* U_Ale, const double* V_Ale, const double* W_Ale, 
    const SolverInfo* const info, int mls_type
)
{
    unsigned int ip = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int size = 0;
    switch (mls_type)
    {
      case 1:
        size = info->point_meshless;
        break;
      case 2:
        size = info->point_mobilized;
        break;
    }
    if (ip >= size) return;
    
    unsigned int i = Mls_List[ip].Meshless_Ind, j;
    
    if (GType2[i] == GType::INACTIVE) 
    {
        U[i] = V[i] = W[i] = P[i] = 0;
        return;
    }
    
    double dpdx, dpdy, dpdz, dps;
	
	if (GType2[i] == GType::_GFDSVD || GType2[i] == GType::MOBILIZED)
	{
        dpdx=0;
        dpdy=0;
        dpdz=0;
        		
        for (int io=0; io<NB_DEV; io++)
        {
            j = Mls_List[ip].Nb_Points[io];
            
            dps = (P[j] - P[i]);
            dpdx += Mls_List[ip].Csvd[0][io]*dps;
            dpdy += Mls_List[ip].Csvd[1][io]*dps;
            dpdz += Mls_List[ip].Csvd[2][io]*dps;
        }
        
        U[i] = Ustar[i] - 0.5*info->dt*dpdx;
        V[i] = Vstar[i] - 0.5*info->dt*dpdy;
        W[i] = Wstar[i] - 0.5*info->dt*dpdz;
    }
	else if (GType2[i] == GType::WALL)
	{
        U[i] = U_Ale[ip] + info->vel[0];
        V[i] = V_Ale[ip] + info->vel[1];
        W[i] = W_Ale[ip] + info->vel[2];
	}
	else
	{
	    return;
	}
}

//grid.x = IPoint; block.x = JPoint;
__global__
void updateVelBoundary_kernel
(
    double* U, double* V, double* W,
    const char* GType2, CoordMember* const current_coord,
    const unsigned int dim, const SolverInfo* const info
)
{
    unsigned int i, j, k;
    unsigned int ijk_1, ijk_2, ijk_a, ijk_b;
    
    switch (dim)
    {
      case 0:
        i = 0; 
        j = blockIdx.x;
        k = threadIdx.x;
        ijk_a = 0*info->dim[1]*info->dim[2] + j*info->dim[2] + k;
        ijk_b = (info->dim[0]-1)*info->dim[1]*info->dim[2] + j*info->dim[2] + k;
        break;
      case 1:
        i = threadIdx.x; 
        j = 0;
        k = blockIdx.x;
        ijk_a = i*info->dim[1]*info->dim[2] + 0*info->dim[2] + k;
        ijk_b = i*info->dim[1]*info->dim[2] + (info->dim[1]-1)*info->dim[2] + k;
        break;
      case 2:
        i = blockIdx.x; 
        j = threadIdx.x;
        k = 0;
        ijk_a = i*info->dim[1]*info->dim[2] + j*info->dim[2] + 0;
        ijk_b = i*info->dim[1]*info->dim[2] + j*info->dim[2] + info->dim[2]-1;
        break;
      default:
#       if __CUDA_ARCH__ >= 200
        printf("dimension error!\n");
#       endif
        return;
    }
    
    if (i>=info->dim[0] || j>=info->dim[1] || k>=info->dim[2]) return;
    
    if (GType2[ijk_a] == GType::OUTFLOW)
    {
        ijk_1 = ijk_a - current_coord[0].offset[0] + current_coord[0].offset[1];
        ijk_2 = ijk_a - current_coord[0].offset[0] + current_coord[0].offset[2];
        U[ijk_a] = current_coord[0].coef[0][1] * U[ijk_1]
                 + current_coord[0].coef[0][2] * U[ijk_2];
        V[ijk_a] = current_coord[0].coef[0][1] * V[ijk_1]
                 + current_coord[0].coef[0][2] * V[ijk_2];
        W[ijk_a] = current_coord[0].coef[0][1] * W[ijk_1]
                 + current_coord[0].coef[0][2] * W[ijk_2];
    }
    
    if (GType2[ijk_b] == GType::OUTFLOW)
    {
        int end_idx = info->dim[dim]-1;
        ijk_1 = ijk_b - current_coord[end_idx].offset[0] + current_coord[end_idx].offset[1];
        ijk_2 = ijk_b - current_coord[end_idx].offset[0] + current_coord[end_idx].offset[2];
        U[ijk_b] = current_coord[end_idx].coef[0][1] * U[ijk_1]
                 + current_coord[end_idx].coef[0][2] * U[ijk_2];
        V[ijk_b] = current_coord[end_idx].coef[0][1] * V[ijk_1]
                 + current_coord[end_idx].coef[0][2] * V[ijk_2];
        W[ijk_b] = current_coord[end_idx].coef[0][1] * W[ijk_1]
                 + current_coord[end_idx].coef[0][2] * W[ijk_2];
    }
}

void updateVelocity_GPU()
{
    dim3 grid, block;
    
    cudaStream_t s1;
    cudaStreamCreate(&s1);
    
    grid.x  = IPoint; grid.y  = JPoint; grid.z  = 1;
    block.x = KPoint; block.y = 1;      block.z = 1;
    updateVelCart_kernel <<< grid, block, 0, s1 >>>
    (
        U_dev, V_dev, W_dev,
//        P_dev, Ustar_dev, Vstar_dev, Wstar_dev,
        P_dev, U_dev, V_dev, W_dev,
        GType2_dev, Cordx_dev, Cordy_dev, Cordz_dev,
        info_dev
    );
    cudaDeviceSynchronize(); getLastCudaError("updateVelCart");
    
    block.x = 256; block.y = 1; block.z = 1;
    grid.x  = iDivUp(Point_Meshless, block.x); grid.y = 1; grid.z = 1;
    updateVelML_kernel <<< grid, block >>>
    (
        U_dev, V_dev, W_dev,
//        P_dev, Ustar_dev, Vstar_dev, Wstar_dev,
        P_dev, U_dev, V_dev, W_dev,
        GType2_dev, Cld_List_dev,
        U_Ale_dev, V_Ale_dev, W_Ale_dev,
        info_dev, 1
    );
    cudaDeviceSynchronize(); getLastCudaError("updateVelML1");
    
    block.x = 256; block.y = 1; block.z = 1;
    grid.x  = iDivUp(C2M_List.size(), block.x); grid.y = 1; grid.z = 1;
    updateVelML_kernel <<< grid, block >>>
    (
        U_dev, V_dev, W_dev,
//        P_dev, Ustar_dev, Vstar_dev, Wstar_dev,
        P_dev, U_dev, V_dev, W_dev,
        GType2_dev, C2M_List_dev,
        U_Ale_dev, V_Ale_dev, W_Ale_dev,
        info_dev, 2
    );
    cudaDeviceSynchronize(); getLastCudaError("updateVelML2");
    
    grid.x = JPoint; block.x = KPoint;
    updateVelBoundary_kernel <<< grid.x, block.x, 0, s1 >>>
    (
        U_dev, V_dev, W_dev,
        GType2_dev, Cordx_dev,
        0, info_dev
    );
    
    grid.x = KPoint; block.x = IPoint;
    updateVelBoundary_kernel <<< grid.x, block.x, 0, s1 >>>
    (
        U_dev, V_dev, W_dev,
        GType2_dev, Cordy_dev,
        1, info_dev
    );
    
    grid.x = IPoint; block.x = JPoint;
    updateVelBoundary_kernel <<< grid.x, block.x, 0, s1 >>>
    (
        U_dev, V_dev, W_dev,
        GType2_dev, Cordz_dev,
        2, info_dev
    );
    cudaDeviceSynchronize(); getLastCudaError("updateVelBoundary");
    
    cudaStreamDestroy(s1);
}

int countNNZ_GPU(int* count_mark_dev);

void prepareCooMatrix_GPU(double* coo_value_dev, int* coo_row_idx_dev, int* coo_col_idx_dev, const int* count_mark_dev);

// void doProjectionMethod0_GPU(int it, const int PM_Iter)
// {
    // initSolver();
	
    // int nnz = 0;
    // int*    count_mark_dev = NULL;
    // int*    coo_row_idx_dev = NULL;
	// int*    coo_col_idx_dev = NULL;
	// double* coo_value_dev = NULL;
	
	// cudaMalloc((void**)&count_mark_dev, Point_All*sizeof(int));
	
    // nnz = countNNZ_GPU(count_mark_dev);
    
    // cudaMalloc((void**)&coo_row_idx_dev, nnz*sizeof(int));
    // cudaMalloc((void**)&coo_col_idx_dev, nnz*sizeof(int));
    // cudaMalloc((void**)&coo_value_dev,   nnz*sizeof(double));
    
    // prepareCooMatrix_GPU(coo_value_dev, coo_row_idx_dev, coo_col_idx_dev, count_mark_dev);
    
    // for (int ifs=0; ifs<PM_Iter; ifs++)
    // {
        // GpuTimer pm_timer;
        // pm_timer.Start();

        // computeSourceContributionNew_GPU();
        
        // computeSource_GPU();
        
        // GpuTimer bicg_timer;
        // bicg_timer.Start();
        // double tol=10;
        // int    maxit=5;
        // int    info=0;
        // Res[IP] = bicgstab_solver_GPU(Point_All, nnz, coo_row_idx_dev, coo_col_idx_dev, coo_value_dev,
                                      // P_dev, So_dev, maxit, tol, info);
        // bicg_timer.Stop(); cout<<"bicgstab time: "<<bicg_timer.Elapsed()<<"ms | ";

        // updateVelocity_GPU();
        
        // pm_timer.Stop();
        // cout<<"PM time: "<<pm_timer.Elapsed()<<"ms | ";
        // cout<<"Res_P: "<<Res[IP]/sqrt(Point_All)<<" "<<endl;

        // if (Res[IP] > 1e16)
        // {
            // cout << "Divergence!!! Code is terminated" << endl;
            // cudaDeviceReset();
            // exit(EXIT_FAILURE);
        // }
    // }
    
    // cudaFree(count_mark_dev);
    // cudaFree(coo_row_idx_dev);
    // cudaFree(coo_col_idx_dev);
    // cudaFree(coo_value_dev);
    
    // terminateSolver();
// }

void doProjectionMethod_GPU(int it, const int PM_Iter)
{
    void cmpPoissonEq_GPU(int n, double* xi_d, double* Axi_d);
    
    initSolver();
	
    for (int ifs=0; ifs<PM_Iter; ifs++)
    {
        GpuTimer pm_timer;
        pm_timer.Start();

        computeSourceContributionNew_GPU();
        
        computeSource_GPU();
        
        GpuTimer bicg_timer;
        bicg_timer.Start();
        double tol=10;
        int    maxit=5;
        int    info=0;
        Res[IP] = bicgstab_solver2_GPU(Point_All, &cmpPoissonEq_GPU, P_dev, So_dev, maxit, tol, info,
                                        TMP1_dev, TMP2_dev, TMP3_dev, TMP4_dev);
        bicg_timer.Stop(); cout<<"bicgstab time: "<<bicg_timer.Elapsed()<<"ms | ";

        updateVelocity_GPU();
        
        pm_timer.Stop();
        cout<<"PM time: "<<pm_timer.Elapsed()<<"ms | ";
        cout<<"Res_P: "<<Res[IP]/sqrt(Point_All)<<" "<<endl;

        if (Res[IP]/sqrt(Point_All) > 100000)
        {
            Write_Result_DIV(it);

            cout << "Divergence!!! Code is terminated" << endl;
            cudaDeviceReset();
            exit(EXIT_FAILURE);
        }
    }
    
    terminateSolver();
}

__global__ 
void setCountMark_kernel
(
    int *count_mark_d, const char *GType2_d, int Point_All
)
{
    int ijk;
    
    ijk = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (ijk >= Point_All) return;
    
    
	if (GType2_d[ijk] == 1) 
	{
	    count_mark_d[ijk] = 7;
	}
	else if (GType2_d[ijk] == 2) 
	{  
		count_mark_d[ijk] = 3;
	}
	else if (GType2_d[ijk] == 3 || GType2_d[ijk] == 5) 
	{
	    count_mark_d[ijk] = NB_DEV + 1;
	}
	else if (GType2_d[ijk] == 0 || GType2_d[ijk] == 4 || GType2_d[ijk] == 6) 
	{
		count_mark_d[ijk] = 1;
	}
	else 
	{
//		cout<<"Mistake in function Bigstab_prepare."<<endl;
#       if __CUDA_ARCH__ >= 200
        printf("Mistake in function Bigstab_prepare. Thread number %d.\n", ijk);
#       endif
		return;
	}
}

int countNNZ_GPU(int* count_mark_dev)
{
    int nnz;
    
    cudaMemset(count_mark_dev, 0, Point_All*sizeof(int));
    
    int blocks = 512; int grids = int(Point_All/blocks) + 1;
    setCountMark_kernel <<< grids, blocks >>> (count_mark_dev, GType2_dev, Point_All);
    
    thrust::device_ptr<int> count_mark_dev_ptr(count_mark_dev);
    nnz = count_mark_dev_ptr[Point_All-1];
    thrust::exclusive_scan(count_mark_dev_ptr, count_mark_dev_ptr + Point_All, count_mark_dev_ptr);
    nnz += count_mark_dev_ptr[Point_All-1];
    
    cudaDeviceSynchronize(); getLastCudaError("countNNZ");
    
    return nnz;
}

//dim3 grid1(IPoint, JPoint, 1);
//dim3 block1(KPoint, 1, 1);
__global__ 
void setCooMatrixForCartNodes_kernel
(
    double* coo_value, int* coo_row_idx, int* coo_col_idx, 
    const int IPoint, const int JPoint, const int KPoint,
    const char* GType2_d, const int* count_mark_d,
    const int* IIndex_d, const int* JIndex_d,
    CoordMember* Cordx_d, CoordMember* Cordy_d, CoordMember* Cordz_d
)
{
    int i = blockIdx.x; 
    int j = blockIdx.y; 
    int k = threadIdx.x;
    if ( i>=IPoint || j>=JPoint || k>=KPoint ) return;
    
    int ijk = IIndex_d[i] + JIndex_d[j] + k;
    int tp = count_mark_d[ijk];
    
    if (GType2_d[ijk] == 1) 
    {
        // central point
	    coo_row_idx[tp] = ijk;
	    coo_col_idx[tp] = ijk;
	    coo_value  [tp] = Cordx_d[i].coef[2][0] 
	                    + Cordy_d[j].coef[2][0] 
	                    + Cordz_d[k].coef[2][0];
	    
	    // west point
	    coo_row_idx[tp + 1] = ijk;
	    coo_col_idx[tp + 1] = IIndex_d[i-1] + JIndex_d[j] + k;
	    coo_value  [tp + 1] = Cordx_d[i].coef[2][1]; 
	    
	    // east point
	    coo_row_idx[tp + 2] = ijk;
	    coo_col_idx[tp + 2] = IIndex_d[i+1] + JIndex_d[j] + k;
	    coo_value  [tp + 2] = Cordx_d[i].coef[2][2]; 
	    
	    // south point
	    coo_row_idx[tp + 3] = ijk;
	    coo_col_idx[tp + 3] = IIndex_d[i] + JIndex_d[j-1] + k;
	    coo_value  [tp + 3] = Cordy_d[j].coef[2][1];
	    
	    // north point
	    coo_row_idx[tp + 4] = ijk;
	    coo_col_idx[tp + 4] = IIndex_d[i] + JIndex_d[j+1] + k;
	    coo_value  [tp + 4] = Cordy_d[j].coef[2][2]; 
	    
	    // lower point
	    coo_row_idx[tp + 5] = ijk;
	    coo_col_idx[tp + 5] = IIndex_d[i] + JIndex_d[j] + k-1;
	    coo_value  [tp + 5] = Cordz_d[k].coef[2][1]; 
	    
	    // upper point
	    coo_row_idx[tp + 6] = ijk;
	    coo_col_idx[tp + 6] = IIndex_d[i] + JIndex_d[j] + k+1;
	    coo_value  [tp + 6] = Cordz_d[k].coef[2][2]; 
	}
	else if (GType2_d[ijk] == 0 || GType2_d[ijk] == 4 || GType2_d[ijk] == 6) 
	{
	    coo_row_idx[tp] = ijk;
	    coo_col_idx[tp] = ijk;
	    coo_value  [tp] = 1.0;
	}
	else if (GType2_d[ijk] == 2)
	{
	    int id;
	    double norm;
	    CoordMember* current_coord = NULL;
	    
        if ((i == 0) || (i == IPoint-1))
        {
            id = i;
            current_coord = Cordx_d;
        }
        else if ((j == 0) || (j == JPoint-1))
        {
            id = j;
            current_coord = Cordy_d;
        }
        else if ((k == 0) || (k == KPoint-1))
        {
            id = k;
            current_coord = Cordz_d;
        }
        else
        {
#           if __CUDA_ARCH__ >= 200
            printf("Mistake in function setCooMatrixForCartNodes.\n");
#           endif
            return;
        }
        
        if (id == 0)
        {
            norm = 1.0;
        }
        else
        {
            norm = -1.0;
        }
        
        // central point
        coo_row_idx[tp] = ijk;
        coo_col_idx[tp] = ijk;
        coo_value  [tp] = norm * current_coord[id].coef[1][0];
        
        // first neighbour point
        coo_row_idx[tp + 1] = ijk;
        coo_col_idx[tp + 1] = ijk - current_coord[id].offset[0] + current_coord[id].offset[1];
        coo_value  [tp + 1] = norm * current_coord[id].coef[1][1];
        
        // second neighbour point
        coo_row_idx[tp + 2] = ijk;
        coo_col_idx[tp + 2] = ijk - current_coord[id].offset[0] + current_coord[id].offset[2];
        coo_value  [tp + 2] = norm * current_coord[id].coef[1][2];
	}
	else 
	{
	    return;
	}
}

__global__ 
void setCooMatrixForMLNodes_kernel
(
    double* coo_value, int* coo_row_idx, int* coo_col_idx,
    const MeshlessMember* Mls_List_d, const int size,
    const double* MNx_d, const double* MNy_d, const double* MNz_d,
    const char* GType2_d, const int* count_mark_d
)
{
	int id, ijk, tp, io, j;
    double dpx, aps;
	
	id = blockIdx.x * blockDim.x + threadIdx.x;
    if (id >= size) return;
    
    ijk = Mls_List_d[id].Meshless_Ind;
    
    if (GType2_d[ijk] == GType::_GFDSVD || GType2_d[ijk] == GType::MOBILIZED)
    {
        tp = count_mark_d[ijk];
        aps = 0;
        
        for (io=0; io<NB; io++) 
        {
	        j = Mls_List_d[id].Nb_Points[io];
	        dpx = Mls_List_d[id].Csvd[3][io]
	            + Mls_List_d[id].Csvd[4][io]
	            + Mls_List_d[id].Csvd[5][io];
		
		    aps += dpx;
		    
		    coo_row_idx[tp] = ijk;
            coo_col_idx[tp] = j;
            coo_value  [tp] = dpx;
		    
		    tp++;
	    }
	    
	    coo_row_idx[tp] = ijk;
        coo_col_idx[tp] = ijk;
        coo_value  [tp] = -aps;
    }
    else if (GType2_d[ijk] == GType::WALL)
    {
        aps = 0;
	    tp = count_mark_d[ijk];
	    
        for (io=0; io<NB_DEV; io++)
        { 
            j = Mls_List_d[id].Nb_Points[io];
            
            dpx = MNx_d[id]*Mls_List_d[id].Csvd[0][io]
                + MNy_d[id]*Mls_List_d[id].Csvd[1][io]
                + MNz_d[id]*Mls_List_d[id].Csvd[2][io];
            
            aps += dpx;
            
	        coo_row_idx[tp] = ijk;
            coo_col_idx[tp] = j;
            coo_value  [tp] = dpx;
            
            tp++;
        }
        
        coo_row_idx[tp] = ijk;
        coo_col_idx[tp] = ijk;
        coo_value  [tp] = -aps;
    }
}

void prepareCooMatrix_GPU(double* coo_value_dev, int* coo_row_idx_dev, int* coo_col_idx_dev, const int* count_mark_dev)
{
    dim3 grid1(IPoint, JPoint, 1);
    dim3 block1(KPoint, 1, 1);
    setCooMatrixForCartNodes_kernel <<< grid1, block1 >>> 
    (
        coo_value_dev, coo_row_idx_dev, coo_col_idx_dev, 
        IPoint, JPoint, KPoint,
        GType2_dev, count_mark_dev,
        IIndex_dev, JIndex_dev,
        Cordx_dev, Cordy_dev, Cordz_dev
    );
//    cudaDeviceSynchronize(); getLastCudaError("setCooMatrixForCartNodes");
    
    dim3 block2(256, 1, 1);
    dim3 grid2(iDivUp(Point_Meshless, block2.x), 1, 1);
    setCooMatrixForMLNodes_kernel <<< grid2, block2 >>> 
    (
        coo_value_dev, coo_row_idx_dev, coo_col_idx_dev, 
        Cld_List_dev, Point_Meshless,
        MNx_dev, MNy_dev, MNz_dev,
        GType2_dev, count_mark_dev
    );
    
    dim3 block3(256, 1, 1);
    dim3 grid3(iDivUp(C2M_List.size(), block3.x), 1, 1);
    setCooMatrixForMLNodes_kernel <<< grid3, block3 >>> 
    (
        coo_value_dev, coo_row_idx_dev, coo_col_idx_dev, 
        C2M_List_dev, C2M_List.size(),
        MNx_dev, MNy_dev, MNz_dev,
        GType2_dev, count_mark_dev
    );
    
    cudaDeviceSynchronize(); getLastCudaError("matrix generation");
}

__global__ 
void cmpPoissonEqForCartNodes_kernel
(
    double* Axi_d, const double* xi_d, 
    const int IPoint, const int JPoint, const int KPoint,
    const char* GType2_d, const int* IIndex_d, const int* JIndex_d,
    CoordMember* Cordx_d, CoordMember* Cordy_d, CoordMember* Cordz_d
)
{
    int i = blockIdx.x; 
    int j = blockIdx.y; 
    int k = threadIdx.x;
    if (i>=IPoint || j>=JPoint || k>=KPoint) return;
    
    int ijk = IIndex_d[i] + JIndex_d[j] + k;
    if (GType2_d[ijk] == 1)
    {
        int ijk_w = IIndex_d[i-1] + JIndex_d[j] + k;
        int ijk_e = IIndex_d[i+1] + JIndex_d[j] + k;
        int ijk_s = IIndex_d[i] + JIndex_d[j-1] + k;
        int ijk_n = IIndex_d[i] + JIndex_d[j+1] + k;
        
        Axi_d[ijk] = (Cordx_d[i].coef[2][0] + Cordy_d[j].coef[2][0] + Cordz_d[k].coef[2][0]) * xi_d[ijk]
                    + Cordx_d[i].coef[2][1] * xi_d[ijk_w] // west point
                    + Cordx_d[i].coef[2][2] * xi_d[ijk_e] // east point
                    + Cordy_d[j].coef[2][1] * xi_d[ijk_s] // south point
                    + Cordy_d[j].coef[2][2] * xi_d[ijk_n] // north point
                    + Cordz_d[k].coef[2][1] * xi_d[ijk-1] // lower point
                    + Cordz_d[k].coef[2][2] * xi_d[ijk+1]; // upper point
                
    }
    else if (GType2_d[ijk]==0 || GType2_d[ijk]==4 || GType2_d[ijk]==6)
    {
        Axi_d[ijk] = xi_d[ijk];
    }
    else if (GType2_d[ijk] == 2)
    {
	    int id;
	    double norm;
	    CoordMember* current_coord = NULL;
	    
        if ((i == 0) || (i == IPoint-1))
        {
            id = i;
            current_coord = Cordx_d;
        }
        else if ((j == 0) || (j == JPoint-1))
        {
            id = j;
            current_coord = Cordy_d;
        }
        else if ((k == 0) || (k == KPoint-1))
        {
            id = k;
            current_coord = Cordz_d;
        }
        else
        {
#           if __CUDA_ARCH__ >= 200
            printf("Mistake in function cmpPoissonEqForCartNodes.\n");
#           endif
            return;
        }
        
        if (id == 0)
        {
            norm = 1.0;
        }
        else
        {
            norm = -1.0;
        }
        
        int ijk_1 = ijk - current_coord[id].offset[0] + current_coord[id].offset[1];
        int ijk_2 = ijk - current_coord[id].offset[0] + current_coord[id].offset[2];
        Axi_d[ijk] = norm * current_coord[id].coef[1][0] * xi_d[ijk]
                   + norm * current_coord[id].coef[1][1] * xi_d[ijk_1]
                   + norm * current_coord[id].coef[1][2] * xi_d[ijk_2];
    }
    else 
    {
        return;
    }
}

__global__ 
void cmpPoissonEqForMLNodes_kernel
(
    double* Axi_d, const double* xi_d, 
    const MeshlessMember* Mls_List_d, const int size,
    const double* MNx_d, const double* MNy_d, const double* MNz_d,
    const char* GType2_d
)
{
	int id, ijk, j, io;
    double dpx, apx, dps;
	
	id = blockIdx.x * blockDim.x + threadIdx.x;
    if (id >= size) return;
    
    ijk = Mls_List_d[id].Meshless_Ind;
	
    if (GType2_d[ijk] == GType::_GFDSVD || GType2_d[ijk] == GType::MOBILIZED)
    {
        apx = 0;
        dps = 0;
        
        for (io=0; io<NB_DEV; io++) 
        {
	        j = Mls_List_d[id].Nb_Points[io];
	        dpx = Mls_List_d[id].Csvd[3][io]
	            + Mls_List_d[id].Csvd[4][io]
	            + Mls_List_d[id].Csvd[5][io];
		
		    apx += dpx;
		    
		    dps += dpx * xi_d[j];
	    }
	    
        Axi_d[ijk] = -apx*xi_d[ijk] + dps;
    }
    else if (GType2_d[ijk] == GType::WALL)
    {
        apx = 0;
        dps = 0;
        
        for (io=0; io<NB_DEV; io++)
        { 
            j = Mls_List_d[id].Nb_Points[io];
            
            dpx = MNx_d[id]*Mls_List_d[id].Csvd[0][io]
                + MNy_d[id]*Mls_List_d[id].Csvd[1][io]
                + MNz_d[id]*Mls_List_d[id].Csvd[2][io];
            
            apx += dpx;
            
            dps += dpx * xi_d[j];
        }
        
        Axi_d[ijk] = -apx*xi_d[ijk] + dps;
    }
    else if (GType2_d[ijk]==GType::INACTIVE || GType2_d[ijk]==GType::NEWBIE)
    {
        Axi_d[ijk] = xi_d[ijk];
    }
}

void cmpPoissonEq_GPU(int n, double* xi_d, double* Axi_d)
{
    dim3 grid1(IPoint, JPoint, 1);
    dim3 block1(KPoint, 1, 1);
    cmpPoissonEqForCartNodes_kernel <<< grid1, block1 >>> 
    (
        Axi_d, xi_d, 
        IPoint, JPoint, KPoint,
        GType2_dev, IIndex_dev, JIndex_dev,
        Cordx_dev, Cordy_dev, Cordz_dev
    );
    cudaDeviceSynchronize(); getLastCudaError("cmpPoissonEqForCartNodes");
    
    dim3 block2(256, 1, 1);
    dim3 grid2(iDivUp(Point_Meshless, block2.x), 1, 1);
    cmpPoissonEqForMLNodes_kernel <<< grid2, block2 >>> 
    (
        Axi_d, xi_d, 
        Cld_List_dev, Point_Meshless,
        MNx_dev, MNy_dev, MNz_dev,
        GType2_dev
    );
    
    dim3 block3(256, 1, 1);
    dim3 grid3(iDivUp(C2M_List.size(), block3.x), 1, 1);
    cmpPoissonEqForMLNodes_kernel <<< grid3, block3 >>> 
    (
        Axi_d, xi_d, 
        C2M_List_dev, C2M_List.size(),
        MNx_dev, MNy_dev, MNz_dev,
        GType2_dev
    );
    
    cudaDeviceSynchronize(); getLastCudaError("cmpPoissonEq");
}


