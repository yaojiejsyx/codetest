#include <cstring>
#include <iostream>
#include <omp.h>
#include <stdio.h>
#include <vector>
#include <numeric>
#include <parallel/numeric>

#include "../common/basis.h"
#include "../common/frame_structure.h"
#include "../common/mesh_type.h"
#include "../common/gpu_helper.h"
#include "../common/timer.h"

#define  ASB        4000
#define  SAFE_D     0.1
#define  TEST_DEPTH 3
#define  G_FACTOR   0.5
#define  BREAK_DEEP 1

using std::cout;
using std::vector;

#define NORM3D(v) sqrt((v)[0]*(v)[0] + (v)[1]*(v)[1] + (v)[2]*(v)[2])
#define DOT3D(v1,v2) ((v1)[0]*(v2)[0] + (v1)[1]*(v2)[1] + (v1)[2]*(v2)[2])
//get the vector x2-x1
#define GETVECX2X1(v,x1,x2) {(v)[0]=(x2)[0]-(x1)[0]; (v)[1]=(x2)[1]-(x1)[1]; (v)[2]=(x2)[2]-(x1)[2];}

extern char   *GType1, *GType2, *GTypeT;;
extern double *X, *Y, *Z, *X_Meshless, *Y_Meshless, *Z_Meshless;
extern double *MNx, *MNy, *MNz;
extern int    *IIndex, *JIndex, *ONI, *BNI;
extern int    *Rigid_Offset, *Body_Offset, *Outer_Offset;
extern int    IPoint, JPoint, KPoint, Point_Car, Point_All;
extern int    Point_Meshless, Total_Body_Nodes, Total_Outer_Nodes, Rigid_Object_Number;

extern vector<MeshlessMember> C2M_List;
extern MeshlessMember* Cld_List;
extern CoordMember* Cord[3];

extern int    Drawer_Member;

extern Ref_Frame lab;

extern RectBox sq_box;

void Calculate_SVD(int wi);
void Update_UT();

bool getCoordBox(unsigned int start[3], unsigned int end[3], const double min[3], const double max[3], double safe_d);
unsigned searchCandidateNodes(unsigned* cand_mark, const unsigned start[3], const unsigned end[3], const bool is_new = true);
unsigned searchInactiveNodes(unsigned* cand_list, const unsigned n_cand, const unsigned* cand_mark, const bool is_new = true);
unsigned searchMobilizedNodes(unsigned* mob_mark, const unsigned* cand_list, const unsigned n_cand, const unsigned start[3], const unsigned end[3], const bool is_new = true);
void findSupportingNodes(const unsigned int start[3], const unsigned int end[3], const unsigned int size_limit);

void updateGridType_GPU(const Ref_Frame& lab, const int *outer_offset, const int *inner_offset, const unsigned start[3], const unsigned end[3], int FSI_Iter, const bool is_new /*= true*/);

void updateMeshSystem(const Ref_Frame& lab, int FSI_Iter, const string& Processor, bool is_new /*= true*/)
{
    unsigned int id, idx_start[3], idx_end[3];
    double max_xyz[3], min_xyz[3], *pos[3];
    int point_inactive, point_mobilized, point_newbie;
    
    pos[0] = X; pos[1] = Y; pos[2] = Z;
    
	id = ONI[0];
	for (int i=0; i<3; i++)
	{
	    max_xyz[i] = min_xyz[i] = pos[i][id];
	}
    
	for (int io=1; io<Total_Outer_Nodes; io++)
	{
		id = ONI[io];
		for (int i=0; i<3; i++)
	    {
	        max_xyz[i] = (pos[i][id] > max_xyz[i]) ? pos[i][id] : max_xyz[i];
	        min_xyz[i] = (pos[i][id] < min_xyz[i]) ? pos[i][id] : min_xyz[i];
	    }
	}
	
	if (!getCoordBox(idx_start, idx_end, min_xyz, max_xyz, SAFE_D))
	{
        cout<<"Meshless points are out of range! Need bigger box! Code terminate!"<<endl;
        exit(EXIT_FAILURE);
	}
	
	if (Processor == "GPU")
	{
	    updateGridType_GPU(lab, Outer_Offset, Body_Offset, idx_start, idx_end, FSI_Iter, is_new);
	}
	else
	{
	
        if (is_new)
        {
#           pragma omp parallel for
            for (int ijk=0; ijk<Point_Car; ijk++) 
            {
                GTypeT[ijk] = GType2[ijk];
                if (FSI_Iter == 1)
                {
                    if (GType2[ijk]==4) GTypeT[ijk] = 3;
                }
                // reset all cartesian nodes to GType::_7POINT, except boundaries
                if (GType2[ijk]!=2 && GType2[ijk]!=6) GType2[ijk] = 1;
            }
        }
        
        unsigned int *mark, *cand_list;
        unsigned int n_cand;

        mark = new unsigned int [Point_Car];
        n_cand = searchCandidateNodes(mark, idx_start, idx_end, is_new);

        cand_list = new unsigned int [n_cand];
        point_inactive = searchInactiveNodes(cand_list, n_cand, mark, is_new);

        point_mobilized = searchMobilizedNodes(mark, cand_list, n_cand, idx_start, idx_end, is_new);

        delete[] mark;
        delete[] cand_list;
	}
	
    findSupportingNodes(idx_start, idx_end, Drawer_Member/*DRAWER_LIMIT*/);
    
    Calculate_SVD(0);
    
    Update_UT();
}

bool getCoordBox(unsigned int start[3], unsigned int end[3], const double min[3], const double max[3], double safe_d)
{
    bool is_got = true;
    
    for (int i=0; i<3; i++)
    {
        if (min[i] <= (sq_box.pos[i] + safe_d) || max[i] >= (sq_box.pos[i] + sq_box.len[i] - safe_d))
        {
            is_got = false;
            return is_got;
        }
        //get the indices
        start[i] = sq_box.idx[i] + int((min[i] - sq_box.pos[i])/sq_box.delta);
        end[i] = sq_box.idx[i] + int((max[i] - sq_box.pos[i])/sq_box.delta) + 1;
    }

    return is_got;
}

unsigned searchCandidateNodes(unsigned* cand_mark, const unsigned start[3], const unsigned end[3], const bool is_new /*= true*/)
{
    unsigned int idx_start[3], idx_end[3];
    unsigned int n_err = 0;
    unsigned int dim[3] = {IPoint, JPoint, KPoint};
    
    memset(cand_mark, 0, Point_Car*sizeof(unsigned int));
    
    if (is_new)
    {
#       pragma omp parallel for
        for (int ijk=0; ijk<Point_Car; ijk++)
        {
            if (GType1[ijk] == GType::INACTIVE || GTypeT[ijk] == GType::INACTIVE || (GType1[ijk] == 3 && GTypeT[ijk] == 4))
            {
                GType2[ijk] = GType::CAND_4;
                cand_mark[ijk] = 1;
            }
        }
    }
    
	for (int i=0; i<3; i++)
	{
	    idx_start[i] = start[i] - 1;
	    if (idx_start[i] < 0) idx_start[i]=0;
	    
	    idx_end[i] = end[i] + 1;
	    if (idx_end[i] >= dim[i]) idx_end[i] = dim[i] - 1;
	}
	
#   pragma omp parallel for reduction(+: n_err) 
    for (int i=idx_start[0]; i<idx_end[0]+1; i++)
    {
    for (int j=idx_start[1]; j<idx_end[1]+1; j++)
    {
    for (int k=idx_start[2]; k<idx_end[2]+1; k++)
    {
        int ijk = IIndex[i] + JIndex[j] + k;
        
        if (GType2[ijk] == GType::MUTE || GType2[ijk] == GType::OUTFLOW) 
        {
            n_err++;
            continue;
        }
        
        unsigned int id_nearest;
        double dist, dist_min;
        
        for (int id=0; id<lab.obj_number; id++)
        {
            dist = dist_min = 1.0e6;
            for (int im=0; im<lab.rigid_body[id]->OUTER_POINT_NUMBER; im++)
            {
                unsigned int iter = ONI[im + Outer_Offset[id]];
                dist = (X[ijk] - X[iter]) * (X[ijk] - X[iter])
                    + (Y[ijk] - Y[iter]) * (Y[ijk] - Y[iter])
                    + (Z[ijk] - Z[iter]) * (Z[ijk] - Z[iter]);
                
                if (dist < dist_min)
                {
                    dist_min = dist;
                    id_nearest = iter - Point_Car;
                }
            }
            
            dist = MNx[id_nearest]*(X[ijk] - X_Meshless[id_nearest])
                + MNy[id_nearest]*(Y[ijk] - Y_Meshless[id_nearest])
                + MNz[id_nearest]*(Z[ijk] - Z_Meshless[id_nearest]);
            
            if (dist < 0)
            {
                if (GType2[ijk] == GType::CAND_4)
                    GType2[ijk] = GType::OVLP_4;
                else
                    GType2[ijk] = GType::OVLP_3;
                
                cand_mark[ijk] = 1;
                break;
            }
        }
    }
    }
    }
    if (n_err > 0)
    {
        cout << "*** Cloud is too close to the boundary! " << n_err << " boundary points involved. ***" << endl;
    }
    
//    for (int i=1; i<Point_Car; i++)
//    {
//        cand_mark[i] += cand_mark[i-1];
//    }
    __gnu_parallel::partial_sum(cand_mark, cand_mark+Point_Car, cand_mark);
    
    return cand_mark[Point_Car-1];
}

unsigned searchInactiveNodes(unsigned* cand_list, const unsigned n_cand, const unsigned* cand_mark, const bool is_new /*= true*/)
{
    bool end_search = false;
    unsigned int break_iter = 0;
    unsigned int n_inactive = 0, n_newbie = 0, n_fast = 0;
    
#   pragma omp parallel for
    for (int i=1; i<Point_Car; i++)
    {
        if (cand_mark[i] != cand_mark[i-1])
        {
            int list_id = cand_mark[i] - 1;
            cand_list[list_id] = i;
        }
    }
    
//    Timer t1; t1.Start();
    while (!end_search && break_iter<BREAK_DEEP)
    {
#       pragma omp parallel for
        for (int it=0; it<n_cand; it++)
        {
            unsigned int ijk = cand_list[it];
            
            if (GType2[ijk]!=GType::OVLP_3 && GType2[ijk]!=GType::OVLP_4) continue;
            
            int ij = int(ijk/KPoint);
            int k = ijk%KPoint;
            int i = ij/JPoint;
            int j = ij%JPoint;
            
            if ((IIndex[i] + JIndex[j] + k) != ijk)
            {
                cout << "*** calculation error. ***" << endl;
                exit(EXIT_FAILURE);
            }
            
            unsigned int ijk_w = IIndex[i-1] + JIndex[j] + k;
            unsigned int ijk_e = IIndex[i+1] + JIndex[j] + k;
            unsigned int ijk_s = IIndex[i] + JIndex[j-1] + k;
            unsigned int ijk_n = IIndex[i] + JIndex[j+1] + k;
            
            if (GType2[ijk-1]==GType::_7POINT || GType2[ijk+1]==GType::_7POINT
                || GType2[ijk_w]==GType::_7POINT || GType2[ijk_e]==GType::_7POINT
                || GType2[ijk_s]==GType::_7POINT || GType2[ijk_n]==GType::_7POINT
                || GType2[ijk-1]==GType::MOBILIZED || GType2[ijk+1]==GType::MOBILIZED
                || GType2[ijk_w]==GType::MOBILIZED || GType2[ijk_e]==GType::MOBILIZED
                || GType2[ijk_s]==GType::MOBILIZED || GType2[ijk_n]==GType::MOBILIZED)
		    {
                if (GType2[ijk] == GType::OVLP_4)
                    GType2[ijk] = GType::CAND_4;
                else if (GType2[ijk] == GType::OVLP_3)
                    GType2[ijk] = GType::CAND_3;
		    }
            else
            {
                continue;
            }
            
            unsigned int id_nearest;
            double dist, dist_min;
            
            for (int id=0; id<lab.obj_number; id++)
            {
                dist = dist_min = 1.0e6;
                for (int im=0; im<lab.rigid_body[id]->INNER_POINT_NUMBER; im++)
                {
                    unsigned int iter = BNI[im + Body_Offset[id]];
                    dist = (X[ijk] - X[iter]) * (X[ijk] - X[iter])
                        + (Y[ijk] - Y[iter]) * (Y[ijk] - Y[iter])
                        + (Z[ijk] - Z[iter]) * (Z[ijk] - Z[iter]);
                    
                    if (dist < dist_min)
                    {
                        dist_min = dist;
                        id_nearest = iter - Point_Car;
                    }
                }
                
                dist = MNx[id_nearest]*(X[ijk] - X_Meshless[id_nearest])
                    + MNy[id_nearest]*(Y[ijk] - Y_Meshless[id_nearest])
                    + MNz[id_nearest]*(Z[ijk] - Z_Meshless[id_nearest]);
                
                if (dist < G_FACTOR*sq_box.delta)
                {
                    GType2[ijk] = GType::INACTIVE;
                    end_search = true;
                    break;
                }
            }
        }
        
#       pragma omp parallel for
        for (int it=0; it<n_cand; it++)
        {
            unsigned int ijk = cand_list[it];
            if (GType2[ijk] == GType::CAND_3)
                GType2[ijk] = GType::MOBILIZED;
        }
        
        break_iter++;
    }
//    t1.Stop(); cout<<"\tTime Used for " << break_iter << " break iter is: "<<t1.Elapsed()<<" ms"<<endl;
    
#   pragma omp parallel for reduction(+: n_inactive, n_newbie, n_fast) 
    for (int i=0; i<n_cand; i++)
    {
        unsigned int ijk = cand_list[i];
        
        if (GType2[ijk] == GType::OVLP_3 || GType2[ijk] == GType::OVLP_4 || GType2[ijk] == GType::INACTIVE)
        {
            GType2[ijk] = GType::INACTIVE;
            n_inactive++;
            if (GType1[ijk] == GType::_7POINT && is_new)
            {
                n_fast++;
            }
        }
        else if (GType2[ijk] == GType::CAND_4)
        {
            GType2[ijk] = GType::NEWBIE;
            n_newbie++;
        }
    }
    if (n_fast > 0)
    {
        cout << "*** Cloud moves too fast! " << n_fast << " _7POINT points overlaped. " 
             << n_newbie << " NEWBIE points involved. ***" << endl;
    }
    
    return n_inactive;
}

unsigned searchMobilizedNodes(unsigned* mob_mark, const unsigned* cand_list, const unsigned n_cand, const unsigned start[3], const unsigned end[3], const bool is_new /*= true*/)
{
    unsigned int id, idx_start[3], idx_end[3];
    unsigned int n_mobilized = 0, n_err = 0;
	unsigned int dim[3] = {IPoint, JPoint, KPoint};
	
	memset(mob_mark, 0, Point_Car*sizeof(unsigned int));
	
#   pragma omp parallel for reduction(+: n_err) 
    for (int i=0; i<n_cand; i++)
    {
        unsigned int ijk = cand_list[i];
        if (GType2[ijk] == GType::NEWBIE || GType2[ijk] == GType::MOBILIZED)
        {
            mob_mark[ijk] = 1;
        }
        else if (GType2[ijk] != GType::INACTIVE)
        {
            n_err++;
            GType2[ijk] = GType::INACTIVE;
        }
    }
    if (n_err > 0)
    {
        cout << "*** GTYPE error! " << n_err << " points' type were not converted. ***" << endl;
    }
    
	for (int i=0; i<3; i++)
	{
	    idx_start[i] = start[i] - 2;
	    if (idx_start[i] < 0) idx_start[i]=0;
	    
	    idx_end[i] = end[i] + 2;
	    if (idx_end[i] >= dim[i]) idx_end[i] = dim[i] - 1;
	}
	
#   pragma omp parallel for
    for (int i=idx_start[0]; i<idx_end[0]+1; i++)
    {
    for (int j=idx_start[1]; j<idx_end[1]+1; j++)
    {
    for (int k=idx_start[2]; k<idx_end[2]+1; k++)
    {
        unsigned int ijk = IIndex[i] + JIndex[j] + k;
        
        if (GType2[ijk] == GType::_7POINT)
        {
            unsigned int ijk_w = IIndex[i-1] + JIndex[j] + k;
            unsigned int ijk_e = IIndex[i+1] + JIndex[j] + k;
            unsigned int ijk_s = IIndex[i] + JIndex[j-1] + k;
            unsigned int ijk_n = IIndex[i] + JIndex[j+1] + k;
            
#ifdef  UPWIND
            unsigned int ijk_ww = IIndex[i-2] + JIndex[j] + k;
            unsigned int ijk_ee = IIndex[i+2] + JIndex[j] + k;
            unsigned int ijk_ss = IIndex[i] + JIndex[j-2] + k;
            unsigned int ijk_nn = IIndex[i] + JIndex[j+2] + k;
            
            if (GType2[ijk-1] == GType::INACTIVE || GType2[ijk-1] == GType::NEWBIE 
                || GType2[ijk+1] == GType::INACTIVE || GType2[ijk+1] == GType::NEWBIE 
                || GType2[ijk_e] == GType::INACTIVE || GType2[ijk_e] == GType::NEWBIE 
                || GType2[ijk_w] == GType::INACTIVE || GType2[ijk_w] == GType::NEWBIE 
                || GType2[ijk_s] == GType::INACTIVE || GType2[ijk_s] == GType::NEWBIE 
                || GType2[ijk_n] == GType::INACTIVE || GType2[ijk_n] == GType::NEWBIE
                || GType2[ijk-2] == GType::INACTIVE || GType2[ijk-2] == GType::NEWBIE 
                || GType2[ijk+2] == GType::INACTIVE || GType2[ijk+2] == GType::NEWBIE 
                || GType2[ijk_ee] == GType::INACTIVE || GType2[ijk_ee] == GType::NEWBIE 
                || GType2[ijk_ww] == GType::INACTIVE || GType2[ijk_ww] == GType::NEWBIE 
                || GType2[ijk_ss] == GType::INACTIVE || GType2[ijk_ss] == GType::NEWBIE 
                || GType2[ijk_nn] == GType::INACTIVE || GType2[ijk_nn] == GType::NEWBIE) 
#else
            if (GType2[ijk-1] == GType::INACTIVE || GType2[ijk-1] == GType::NEWBIE 
                || GType2[ijk+1] == GType::INACTIVE || GType2[ijk+1] == GType::NEWBIE 
                || GType2[ijk_e] == GType::INACTIVE || GType2[ijk_e] == GType::NEWBIE 
                || GType2[ijk_w] == GType::INACTIVE || GType2[ijk_w] == GType::NEWBIE 
                || GType2[ijk_s] == GType::INACTIVE || GType2[ijk_s] == GType::NEWBIE 
                || GType2[ijk_n] == GType::INACTIVE || GType2[ijk_n] == GType::NEWBIE) 
#endif /*UPWIND*/
//            if (GType2[ijk-1] == GType::INACTIVE || GType2[ijk+1] == GType::INACTIVE 
//                || GType2[ijk_e] == GType::INACTIVE || GType2[ijk_w] == GType::INACTIVE 
//                || GType2[ijk_s] == GType::INACTIVE || GType2[ijk_n] == GType::INACTIVE
//                || GType2[ijk-1] == GType::NEWBIE || GType2[ijk+1] == GType::NEWBIE 
//                || GType2[ijk_e] == GType::NEWBIE || GType2[ijk_w] == GType::NEWBIE 
//                || GType2[ijk_s] == GType::NEWBIE || GType2[ijk_n] == GType::NEWBIE) 
            {
                GType2[ijk] = GType::MOBILIZED;
                mob_mark[ijk] = 1;
            }
        }
    }
    }
    }
    
//    for (int i=1; i<Point_Car; i++)
//    {
//        mob_mark[i] += mob_mark[i-1];
//    }
    __gnu_parallel::partial_sum(mob_mark, mob_mark+Point_Car, mob_mark);
    
    n_mobilized = mob_mark[Point_Car-1];
    
    C2M_List.resize(n_mobilized);
    
#   pragma omp parallel for
    for (int i=1; i<Point_Car; i++)
    {
        if (mob_mark[i] != mob_mark[i-1])
        {
            int list_id = mob_mark[i] - 1;
            C2M_List[list_id].Meshless_Ind = i;
        }
    }
    
    return n_mobilized;
}

bool creatDrawer
(
    unsigned int** drawer, unsigned int* drawer_size, 
    const RectBox& drawer_box, const unsigned int idx_start[3], const unsigned int idx_end[3],
    unsigned int drawer_num, unsigned int size_limit
)
{
    memset(drawer_size, 0, drawer_num*sizeof(unsigned int));
    
    bool is_created = true;
    
#   pragma omp parallel for
    for (int it=0; it<Point_Meshless; it++)
    {
        int kp = int((Z_Meshless[it] - drawer_box.pos[2])/drawer_box.delta);
        int jp = int((Y_Meshless[it] - drawer_box.pos[1])/drawer_box.delta);
        int ip = int((X_Meshless[it] - drawer_box.pos[0])/drawer_box.delta);
        
        int ijk_dr = ip*drawer_box.dim[2]*drawer_box.dim[1] + jp*drawer_box.dim[2] + kp;
        
#       pragma omp critical
        {
            drawer[ijk_dr][drawer_size[ijk_dr]] = it + Point_Car;
            drawer_size[ijk_dr]++;
            if (drawer_size[ijk_dr] >= size_limit) 
            {
                is_created = false;
            }
        }
    }
    if (!is_created) return is_created;
    
#   pragma omp parallel for
    for(int i=idx_start[0]; i<idx_end[0]+1; i++)
    {
    for(int j=idx_start[1]; j<idx_end[1]+1; j++)
    {
    for(int k=idx_start[2]; k<idx_end[2]+1; k++)
    {
        unsigned int ijk = IIndex[i] + JIndex[j] + k;
        
        if (GType2[ijk]==GType::_7POINT || GType2[ijk]==GType::MOBILIZED)
        {
            int kp = int((Z[ijk] - drawer_box.pos[2])/drawer_box.delta);
            int jp = int((Y[ijk] - drawer_box.pos[1])/drawer_box.delta);
            int ip = int((X[ijk] - drawer_box.pos[0])/drawer_box.delta);
            
            int ijk_dr = ip*drawer_box.dim[2]*drawer_box.dim[1] + jp*drawer_box.dim[2] + kp;
            
            if (ijk_dr>=0 && ijk_dr<drawer_num)
            {
#           pragma omp critical
            {
                drawer[ijk_dr][drawer_size[ijk_dr]] = ijk;
                drawer_size[ijk_dr]++;
                if (drawer_size[ijk_dr] >= size_limit)
                {
                    is_created = false;
//                    cout << ip << '\t' << jp << '\t' << kp << '\t' << drawer_size[ijk_dr] << endl;
                }
            }
            }
        }
    }
    }
    }
    
    return is_created;
    
}

//check node validity by using dot product
int checkNodeValidityCone(int ijk, int target, const int* node_group, int nnodes, double tol)
{
    int i;
    int ni;
    double v1[3], v2[3];
    double v1mag, v2mag;
    double* xyz[3] = {X, Y, Z};
    
    for (int id=0; id<3; id++)
    {
        v1[id] = xyz[id][target] - xyz[id][ijk];
    }
    v1mag = sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
    
    for (i=0; i<nnodes; i++) 
    {
        ni = node_group[i];
        
        for (int id=0; id<3; id++)
        {
            v2[id] = xyz[id][ni] - xyz[id][ijk];
        }
        v2mag = sqrt(v2[0]*v2[0] + v2[1]*v2[1] + v2[2]*v2[2]);

        if (ni==target || (v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2])/(v1mag*v2mag) > tol)
            return 0;
    }

    return 1;
}

//check node validity by using sphere based selection zone
int checkNodeValiditySphere(int ijk, int target, const int* node_group, int nnodes, double tol)
{
    int i;
    int ni;
    double v1[3], v2[3];
    double v1mag, v2mag;
    double* xyz[3] = {X, Y, Z};
    
    for (i=0; i<nnodes; i++)
    {
        ni=node_group[i];
        
        for (int id=0; id<3; id++)
        {
            v1[id] = xyz[id][target] - xyz[id][ni];
            v2[id] = xyz[id][ni] - xyz[id][ijk];
        }
        v1mag = sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
        v2mag = sqrt(v2[0]*v2[0] + v2[1]*v2[1] + v2[2]*v2[2]);
        
        if (ni==target || v1mag < v2mag*tol)
            return 0;
    }
    return 1;
}

void selectNodes(MeshlessMember* ml_member, int *node_pool, int nn, double *dist, int *rank, double tol)
{
    int    ijk; //, ij_p;
    double x0, y0, z0; //, dr;
    double x1, y1, z1;
    
    ijk = ml_member->Meshless_Ind;
    
    GType::type node_t = GType2[ijk];
    
    int NPoint_Sph = NB + NB/2;
    if (node_t == GType::WALL) NPoint_Sph *= 2;
    
    double dot_tol = sqrt(1 - 4.0/NPoint_Sph);
    int bnode = 0;

    x0 = X[ijk];
    y0 = Y[ijk];
    z0 = Z[ijk];
    
    for(int it=0; it<nn; it++)
    {
        int ijk1 = node_pool[it];
        
        x1 = X[ijk1];
        y1 = Y[ijk1];
        z1 = Z[ijk1];
        
        dist[it] = (x0-x1)*(x0-x1) + (y0-y1)*(y0-y1) + (z0-z1)*(z0-z1);
        rank[it] = ijk1;
    }

    QuickSort(dist, rank, 0, nn-1);

    int count = 0;
    int itst = 0;
    
    int mark[nn];
    memset(mark, 0, nn*sizeof(int));

    int npoints_old = NB;

    //accepted[0] = im;
    double vec[3], nrm[3], dot, mag1, mag2;

    for (itst=0; itst<2; itst++)
    {
        //strict selection
        for (int i=1; i<nn && count < npoints_old; i++)
        {
            if (mark[i] == 1) continue;
            
            if (count!=0 && (itst==0 && checkNodeValidityCone(ijk, rank[i], ml_member->Nb_Points, count, dot_tol)==0))
                continue;
//            if(count!=0 && (itst==0 && checkNodeValiditySphere(ijk, rank[i], ml_member->Nb_Points, count, tol)==0))
//                continue;
            
            x1 = X[rank[i]];
            y1 = Y[rank[i]];
            z1 = Z[rank[i]];
            
            if (node_t == GType::WALL)
            {
                vec[0] = x1 - x0;
                vec[1] = y1 - y0;
                vec[2] = z1 - z0;
                nrm[0] = MNx[ijk-Point_Car];
                nrm[1] = MNy[ijk-Point_Car];
                nrm[2] = MNz[ijk-Point_Car];

                dot = DOT3D(vec, nrm);
                mag1 = NORM3D(vec);
                mag2 = NORM3D(nrm);

                if (mag1 < -0.2) continue; //point too close

                dot /= mag1*mag2;

                if(dot<0) continue; //all points in direction of normal

                if (GType2[rank[i]] == GType::WALL)
                {
                    bnode++;
                    if (bnode > 0) continue;
                }
            }
            else if (node_t == GType::_GFDSVD)
            {
                vec[0] = x1 - x0;
                vec[1] = y1 - y0;
                vec[2] = z1 - z0;
                
                mag1 = NORM3D(vec);
                if(mag1<5e-4) continue; //point too close
                
                if (GType2[rank[i]] == GType::WALL)
                {
                    bnode++;
                    if (bnode > 5) continue;
                }
            }
            
            ml_member->Nb_Points[count] = rank[i];
            mark[i] = 1;
            count++;
        }
    }

    if (count<19 || count!=npoints_old) 
    {
        cout << "im= " << ijk << " count= " << count << endl;
    }

    return;
}

void selectNodes_YY(MeshlessMember* ml_member, int *idx, int nn, double *dist, int *rank);

void collectNodes(MeshlessMember* ml_member, const unsigned* const* dr, const unsigned* dr_size, const RectBox& dr_box, int nb)
{
    int idx[ASB], rank[ASB];
    double dist[ASB];
    int count;
    int* accepted;
    
    int i, j, k, ijk_dr;
    int ist, ied, jst, jed, kst, ked;
    
    int ijk = ml_member->Meshless_Ind;
    
    int kp = int((Z[ijk] - dr_box.pos[2])/dr_box.delta);
    int jp = int((Y[ijk] - dr_box.pos[1])/dr_box.delta);
    int ip = int((X[ijk] - dr_box.pos[0])/dr_box.delta);
    
    count = 0;
    ist = ip - nb;
    ied = ip + nb;
    jst = jp - nb;
    jed = jp + nb;
    kst = kp - nb;
    ked = kp + nb;
    
    if (ist < 0 || ied >= dr_box.dim[0] || jst < 0 || jed >= dr_box.dim[1]
        || kst < 0 || ked >= dr_box.dim[2])
    {
        cout << "Drawer id exceeds range" << endl;
        cout << X[ijk] << '\t' << Y[ijk] << '\t' << Z[ijk] << endl;
        cout << dr_box.pos[0] << '\t' << dr_box.pos[1] << '\t' << dr_box.pos[2] << endl;
    }
    
    //collect surrounding meshless points, the central node is also included
    for (i=ist; i<ied+1; i++)
    {
        for (j=jst; j<jed+1; j++)
        {
            for (k=kst; k<ked+1; k++)
            {    
                ijk_dr = i*dr_box.dim[2]*dr_box.dim[1] + j*dr_box.dim[2] + k;
                
                for (int i_dr=0; i_dr<dr_size[ijk_dr]; i_dr++)
                {
                    idx[count] = dr[ijk_dr][i_dr];
                    count++;
                }
            }
        }
    }
    if (count >= ASB) cout << "ERROR: too many points for meshless node " << ml_member->Meshless_Ind << endl;
    
    accepted = ml_member->Nb_Points;
    
//    Template_Node_Select(ml_member, idx, count, dist, rank, 0.5);
//    selectNodes(ml_member, idx, count, dist, rank, 0.5);
    selectNodes_YY(ml_member, idx, count, dist, rank);
}

void findSupportingNodes(const unsigned int start[3], const unsigned int end[3], const unsigned int size_limit)
{
    unsigned int **drawer, *drawer_size, drawer_num;
    RectBox drawer_box;
    bool is_created;
    
    //-------------------------------------------------------------------------- 
    // set drawer region
    unsigned int depth = TEST_DEPTH + 3;  // Test_Depth usually 3
    unsigned int idx_start[3], idx_end[3];
    unsigned int dim[3] = {IPoint, JPoint, KPoint};
    
	for (int i=0; i<3; i++)
	{
	    idx_start[i] = start[i] - depth;
	    if (idx_start[i] < 0) idx_start[i]=0;
	    
	    idx_end[i] = end[i] + depth;
	    if (idx_end[i] >= dim[i]) idx_end[i] = dim[i] - 1;
	}
	
	for (int i=0; i<3; i++)
	{
	    drawer_box.idx[i] = 0;
	    drawer_box.delta = sq_box.delta;
	    
	    drawer_box.pos[i] = Cord[i][idx_start[i]].pos - 0.5*drawer_box.delta;
	    drawer_box.len[i] = Cord[i][idx_end[i]].pos - Cord[i][idx_start[i]].pos + drawer_box.delta;
	    
	    drawer_box.dim[i] = (unsigned int)(drawer_box.len[i]/drawer_box.delta) + 1;
	}
	drawer_num = drawer_box.dim[0] * drawer_box.dim[1] * drawer_box.dim[2];
	
	drawer_size = new unsigned int [drawer_num];
    drawer = new unsigned int* [drawer_num];
    for(int i=0; i<drawer_num; i++) drawer[i] = new unsigned int [size_limit];
	
	is_created = creatDrawer(drawer, drawer_size, drawer_box, idx_start, idx_end, drawer_num, size_limit);
	
    if (!is_created)
    {
        cout << "ERROR: Failed to created drawer. we need a big drawer!" << endl;
        for(int i=0; i<drawer_num; i++) delete[] drawer[i];
        delete[] drawer; drawer = NULL;
        delete[] drawer_size; drawer_size = NULL;
        exit(EXIT_FAILURE);
    }
    //-------------------------------------------------------------------------- 
    
    unsigned int nb = TEST_DEPTH; // Test_Depth usually 3

//    cout<<"   Find Supporting Points for Meshless Points"<<endl;
#   pragma omp parallel for
    for (int it=0; it<Point_Meshless; it++)
    {
        collectNodes(&Cld_List[it], drawer, drawer_size, drawer_box, nb);
    }
    
#   pragma omp parallel for
    for (int it=0; it<C2M_List.size(); it++)
    {
        collectNodes(&C2M_List[it], drawer, drawer_size, drawer_box, nb);
    }
    
    for(int it=0; it<drawer_num; it++) delete[] drawer[it];
    delete[] drawer; drawer = NULL;
    delete[] drawer_size; drawer_size = NULL;
}

//check node validity by using dot product
bool checkValidity_YY
(
    double* mag, int ijk, int target, const int* nodes, const int nnodes, 
    const double grid_delta, const double dist_lim, const double tol
)
{
    int i;
    int ni;
	int nearest1idx, nearest2idx;
    double v1[3], v2[3], v3[3], v_near1[3], v_near2[3];
    double v1mag, v2mag, v3mag, r_eff;
	double nearest1 = 1.0;
	double nearest2 = 1.0;
    double* xyz[3] = {X, Y, Z};
    
    double r_cos = sqrt(1-0.25*tol*tol);
    
    for (int id=0; id<3; id++)
    {
        v1[id] = xyz[id][target] - xyz[id][ijk];
    }
    v1mag = sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]); 				// distance between the target point and the center point
    mag[NB] = v1mag;
    
    if(v1mag < dist_lim) return false; 								//point too close to central node
    
    for (i=0; i<nnodes; i++)     								// 'nnodes' is the number of already selected nodes
    {
        ni = nodes[i];
        if (ni==target) return false; 										// point already inside nodes group
        
        for (int id=0; id<3; id++)
        {
            v2[id] = xyz[id][ni] - xyz[id][ijk];
            v3[id] = xyz[id][ni] - xyz[id][target];
        }
//        v2mag = sqrt(v2[0]*v2[0] + v2[1]*v2[1] + v2[2]*v2[2]);
        v2mag = mag[i];                                                        // distance between the ith node and the center node
        v3mag = sqrt(v3[0]*v3[0] + v3[1]*v3[1] + v3[2]*v3[2]);          	    // distance between the ith node and the target node
        if (v3mag < dist_lim) return false;                        				 //point too close to exist node
        
        r_eff = (v2mag>2*grid_delta) ? 0.5*v2mag : grid_delta;
        r_eff *= tol;
        
        if (v3mag<r_eff)                                                    //point too close to exist node
        {
            if (v2mag>2*grid_delta)
            {
                return false;
            }
            else
            {
                if ((v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2])/(v1mag*v2mag) > r_cos) 
                    return false;                                                      // angle too small
            }
        }
		
		if (i==0)
		{
			nearest1 = v3mag;
			nearest1idx = ni;			        
		}
		if (i==1)
		{
			if (nearest1>v3mag)
			{
				nearest2 = nearest1;
				nearest2idx = nearest1idx;
				nearest1 = v3mag;
				nearest1idx = ni;
			}
			else
			{
				nearest2 = v3mag;
				nearest2idx = ni;
			}			
		}
		if (nnodes>2 && i>1)
		{
			if (nearest1>v3mag)
			{
				nearest2 = nearest1;
				nearest2idx = nearest1idx;
				nearest1 = v3mag;
				nearest1idx = ni;
			}
			else if (nearest2>v3mag)
			{
				nearest2 = v3mag;
				nearest2idx = ni;
			}
		}
    }
	
	if (nnodes>NB/2) 
	{
		for (int id=0; id<3; id++)
		{
			v_near1[id] = xyz[id][nearest1idx] - xyz[id][target];
			v_near2[id] = xyz[id][nearest2idx] - xyz[id][target];
		}
		double near_cos = cos(15.0/180.0*3.14159);

		if (abs(v_near1[0]*v_near2[0] + v_near1[1]*v_near2[1] + v_near1[2]*v_near2[2])/(nearest1*nearest2) > near_cos)
		{
			return false;    													// neighbouring nodes form sharp triangle			
		}
		 						
	}
			
    return true;
}

void selectNodes_YY(MeshlessMember* ml_member, int *idx, int nn, double *dist, int *rank)
{
    int    ijk; //, ij_p;
    double x0, y0, z0; //, dr;
    double x1, y1, z1;
    
    ijk = ml_member->Meshless_Ind;
    
    GType::type node_t = GType2[ijk];
    x0 = X[ijk];
    y0 = Y[ijk];
    z0 = Z[ijk];
    
    for(int it=0; it<nn; it++)
    {
        int ijk1 = idx[it];
        
        x1 = X[ijk1];
        y1 = Y[ijk1];
        z1 = Z[ijk1];
        
        dist[it] = (x0-x1)*(x0-x1) + (y0-y1)*(y0-y1) + (z0-z1)*(z0-z1);
        rank[it] = ijk1;
    }

    QuickSort(dist, rank, 0, nn-1);

    unsigned count = 0;
    unsigned itst = 0;
    unsigned bnode = 0;
    
    int mark[nn];
    memset(mark, 0, nn*sizeof(int));
    
    double vec[3], vec_nearbywall[3], nrm[3], nrm_nearbywall[3];
	double dot, dot_nearbywall, mag1, mag2, dis_towall, dis_towall_tmp, mag_nearbywall, mag_t=0;
    double mag[NB+1] = {0};
	int nearest_wall_idx;
//    double* mag = new double [NB+1];
    bool is_found = false;
    
    double min_dist_lim = 0.1*sq_box.delta, tol=0.8;
//    if (node_t == GType::WALL) tol = 0.8;

	// find the nearest solid boundary node on the same solid surface side for non-boundary node.
	dis_towall = 1.0;
	if (node_t == GType::_GFDSVD || node_t == GType::MOBILIZED)
	{
		for (int i=1; i<nn; i++)
			{
				if (GType2[rank[i]] == GType::WALL && rank[i]!=ijk)
                {
					vec_nearbywall[0] = x0 - X[rank[i]];
					vec_nearbywall[1] = y0 - Y[rank[i]];
					vec_nearbywall[2] = z0 - Z[rank[i]];
					nrm_nearbywall[0] = MNx[rank[i]-Point_Car];
					nrm_nearbywall[1] = MNy[rank[i]-Point_Car];
					nrm_nearbywall[2] = MNz[rank[i]-Point_Car];
					dis_towall_tmp = sqrt(vec_nearbywall[0]*vec_nearbywall[0] + vec_nearbywall[1]*vec_nearbywall[1] + vec_nearbywall[2]*vec_nearbywall[2]);
					mag_nearbywall = sqrt(nrm_nearbywall[0]*nrm_nearbywall[0] + nrm_nearbywall[1]*nrm_nearbywall[1] + nrm_nearbywall[2]*nrm_nearbywall[2]);
					dot_nearbywall = DOT3D(vec_nearbywall, nrm_nearbywall)/(dis_towall_tmp*mag_nearbywall);
					if (dot_nearbywall > 0)
					{
						if (dis_towall > dis_towall_tmp)
						{
							nearest_wall_idx = rank[i];
							dis_towall = dis_towall_tmp;
							// printf("dis_towall: %f, mag_nearbywall: %f, dot product: %f.\n", dis_towall_tmp, mag_nearbywall, dot_nearbywall);
						}
					}
                }
			}
	}
	if (dis_towall<0.02)     // some nodes do not have nearby bounday nodes
	{
		nrm_nearbywall[0] = MNx[nearest_wall_idx-Point_Car];
		nrm_nearbywall[1] = MNy[nearest_wall_idx-Point_Car];
		nrm_nearbywall[2] = MNz[nearest_wall_idx-Point_Car];	
		mag_nearbywall = sqrt(nrm_nearbywall[0]*nrm_nearbywall[0] + nrm_nearbywall[1]*nrm_nearbywall[1] + nrm_nearbywall[2]*nrm_nearbywall[2]);
	}
		
    while (!is_found && itst<2)
    {
        for (int i=1; i<nn; i++)
        {
            if (mark[i] == 1) continue;
            
            bool is_ok = true;
//            if (count>0) 
            is_ok = checkValidity_YY(mag, ijk, rank[i], ml_member->Nb_Points, count, 
                                         sq_box.delta, min_dist_lim, tol);
            if (!is_ok) continue;
            
            vec[0] = X[rank[i]] - x0;
            vec[1] = Y[rank[i]] - y0;
            vec[2] = Z[rank[i]] - z0;
            mag1 = mag[NB];
           
            if (node_t == GType::WALL)
            {
                
                nrm[0] = MNx[ijk-Point_Car];
                nrm[1] = MNy[ijk-Point_Car];
                nrm[2] = MNz[ijk-Point_Car];
                mag2 = NORM3D(nrm);
                
                dot = DOT3D(vec, nrm)/(mag1*mag2);
				// printf("mag_vec: %f, mag_norm: %f, dot product: %f.\n", mag1, mag2, dot);
                if(dot<0) continue; //all points in direction of normal
                
                if (GType2[rank[i]] == GType::WALL)
                {
                    bnode++;
                    if (bnode > 0) continue;
                }
            }
			else if (node_t == GType::_GFDSVD)
            {
                if (dis_towall<0.02 && GType2[rank[i]] != GType::WALL)
				{
					vec_nearbywall[0] = X[rank[i]] - X[nearest_wall_idx];
					vec_nearbywall[1] = Y[rank[i]] - Y[nearest_wall_idx];
					vec_nearbywall[2] = Z[rank[i]] - Z[nearest_wall_idx];
					dis_towall_tmp = sqrt(vec_nearbywall[0]*vec_nearbywall[0] + vec_nearbywall[1]*vec_nearbywall[1] + vec_nearbywall[2]*vec_nearbywall[2]);
					dot_nearbywall=DOT3D(vec_nearbywall, nrm_nearbywall)/(dis_towall_tmp*mag_nearbywall);
					if (dot_nearbywall < -0.7 && sqrt(dist[i])>0.02) 
					{
						// printf("dis_towall: %f, dis_towall_tmp: %f, dot product: %f, node: %d, dist: %f.\n", dis_towall, dis_towall_tmp, dot_nearbywall, ijk, sqrt(dist[i]));
						continue;      // solid surface is between the current point and the central point
				
					}
				
				}
								
				if (GType2[rank[i]] == GType::WALL)
                {
                    bnode++;
                    if (bnode > 6) continue;
                }				
            }
            
            ml_member->Nb_Points[count] = rank[i];
            mark[i] = 1;
            mag_t += mag1; mag[count] = mag1;
            
            count++;
            if (count==NB) 
            {
                is_found=true; break;
            }
        }
        
        tol = tol*0.9;
        itst++;
    }
    mag_t = mag_t/count;
    
    if (count<19 || count!=NB /*|| itst>1/*|| ijk==500+Point_Car*/) 
    {
        printf("node: %d, count: %d, search iteration: %d, type: %d.\n", ijk, count, itst, node_t); //NO Enough Nodes! 
    }
    
    return;
}


