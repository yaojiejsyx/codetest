#include <iostream>
#include <math.h>
#include <cmath>

using namespace std;

#define  PI 3.1415926535897932

#include "../common/basis.h"
#include "../common/flap_pattern.h"

void Cal_Poly_Connection(double x1, int order1, double x2, int order2, double x_ctrl, int ctrl_number, double *right);

// set flap pattern parameters at beginning of a new simulation
void FlapPattern::Set_Flap_Pattern(double _freq, double root[3], double body_centre[3])
{
	freq        = _freq;
	freq_old    = _freq;
	freq_global = _freq;
	stime_start = 0;
	stime_end   = stime_start + 1.0;
	
	mid_aoa_ini[0] = 45.0/180.0*PI;  // downstroke
	mid_aoa_ini[1] = 45.0/180.0*PI;  // upstroke
	stroke_amplitude = 1.0;
	
	// rotation kinematics shape control parameters
	rot_tp_ini = 0.25;  // peak time
	rot_ft_h = 1/2.0;     // f(t) at t = 0.5
	rot_ts = 0;
	
	for(int i=0;i<3;i++) {
		wingplane_root[i] = root[i];
		lastbodycentre[i] = body_centre[i];
	}
	
	Init_Flap_Kinematics();
	
	check_intvl_Re    = 500;
	check_intvl_Angle = 500;
	
	Init_Flap_Controllers();
	
	Angle_Current_Phi    = wingplane1_Phi(0, 0);
	Angle_Previous_Phi   = wingplane1_Phi(0, 0);
	Angle_Next_Phi       = wingplane1_Phi(0, 0);
	
	Angle_Current_Theta  = wingplane1_Theta(0, 0);
	Angle_Previous_Theta = wingplane1_Theta(0, 0);
	Angle_Next_Theta     = wingplane1_Theta(0, 0);
	
	Angle_Current_Psi    = wingplane1_Psi(0, 0);
	Angle_Previous_Psi   = wingplane1_Psi(0, 0);
	Angle_Next_Psi       = wingplane1_Psi(0, 0);
	
	Angle_Current_Stroke_Delta  = 0;
	Angle_Previous_Stroke_Delta = 0;
	Angle_Next_Stroke_Delta     = 0;
	
	Angle_Current_AoA_Delta     = 0;
	Angle_Previous_AoA_Delta    = 0;
	Angle_Next_AoA_Delta        = 0;
	
	Angle_Current_Psi_Delta     = 0;
	Angle_Previous_Psi_Delta    = 0;
	Angle_Next_Psi_Delta        = 0;
	
	Angle_Current_Ele0  = 0;
	Angle_Previous_Ele0 = 0;
	Angle_Next_Ele0     = 0;
	
	rot_tp[0] = rot_tp_ini - rot_ts;
	rot_tp[1] = rot_tp_ini + rot_ts;
	
	mid_aoa[0][0] = mid_aoa_ini[0];  // downstroke
	mid_aoa[0][1] = mid_aoa_ini[1];  // upstroke
	mid_aoa[1][0] = mid_aoa_ini[0];  // downstroke
	mid_aoa[1][1] = mid_aoa_ini[1];  // upstroke
}

double FlapPattern::wing1_X(double stime, int i) 
{
	if(i==0)
		return 0;
	else if(i==1)
		return 0;
	else if(i==2)
		return 0;
	else {
		cout<<"Mistake in function wing1_X!"<<endl;
		return 0;
	}
}
double FlapPattern::wing1_Y(double stime, int i) 
{
	return 0;
}
double FlapPattern::wing1_Z(double stime, int i) 
{
	return 0;
}
double FlapPattern::wing2_X(double stime, int i) 
{
	if(i==0)
		return 0;
	else if(i==1)
		return 0;
	else if(i==2)
		return 0;
	else {
		cout<<"Mistake in function wing2_X!"<<endl;
		return 0;
	}
}
double FlapPattern::wing2_Y(double stime, int i) 
{
	return 0;
}
double FlapPattern::wing2_Z(double stime, int i) 
{
	return 0;
}

double FlapPattern::wingplane1_X(double stime, int i) 
{
	if(i==0)
		return wingplane_root[0]+0.35;
	else if(i==1)
		return 0;
	else if(i==2)
		return 0;
	else {
		cout<<"Mistake in function wingplane1_X!"<<endl;
		return 0;
	}
}
double FlapPattern::wingplane1_Y(double stime, int i) 
{
    if(i==0)
		return wingplane_root[1];
	else if(i==1)
		return 0;
	else if(i==2)
		return 0;
	else {
		cout<<"Mistake in function wingplane1_Y!"<<endl;
		return 0;
	}
}
double FlapPattern::wingplane1_Z(double stime, int i) 
{
    if(i==0)
		return wingplane_root[2];
	else if(i==1)
		return 0;
	else if(i==2)
		return 0;
	else {
		cout<<"Mistake in function wingplane1_Z!"<<endl;
		return 0;
	}
}
double FlapPattern::wingplane2_X(double stime, int i) 
{
	if(i==0)
		return -wingplane_root[0]-0.35;
	else if(i==1)
		return 0;
	else if(i==2)
		return 0;
	else {
		cout<<"Mistake in function wingplane2_X!"<<endl;
		return 0;
	}
}
double FlapPattern::wingplane2_Y(double stime, int i) 
{
    if(i==0)
		return wingplane_root[1];
	else if(i==1)
		return 0;
	else if(i==2)
		return 0;
	else {
		cout<<"Mistake in function wingplane2_Y!"<<endl;
		return 0;
	}
}
double FlapPattern::wingplane2_Z(double stime, int i) 
{
    if(i==0)
		return wingplane_root[2];
	else if(i==1)
		return 0;
	else if(i==2)
		return 0;
	else {
		cout<<"Mistake in function wingplane2_Z!"<<endl;
		return 0;
	}
}

double FlapPattern::wing1_Phi(double stime, int i) 
{
	double tmp_wing_phi;
	int order;
	
	double Time = stime - stime_start;
	double f0 = freq/freq_global;
	
	tmp_wing_phi = 0;
	order = (sizeof(cc_wing_phi[0]) / sizeof(cc_wing_phi[0][0]));
	
	if(i==0) {
	    if ( stime<0.25 || (Time*f0 < 0.5 && stime>0.9999) ) {
			for (int k=0; k<order; k++) 
				tmp_wing_phi += cc_wing_phi[0][k]*pow(Time,k);
			return tmp_wing_phi;
	    }
	    else {
		    return -(stroke_amplitude - Angle_Next_Stroke_Delta)*cos(2*PI*f0*Time);
		}
	}
	else if(i==1) {
	    if ( stime<0.25 || (Time*f0 < 0.5 && stime>0.9999) ) {
			for (int k=1; k<order; k++) 
				tmp_wing_phi += k*cc_wing_phi[0][k]*pow(Time,k-1);
			return tmp_wing_phi;
	    }
	    else {
		    return (stroke_amplitude - Angle_Next_Stroke_Delta)*2*PI*f0*sin(2*PI*f0*Time);
		}
	}
	else if(i==2) {
	    if ( stime<0.25 || (Time*f0 < 0.5 && stime>0.9999) ) {
	        for (int k=2; k<order; k++) 
				tmp_wing_phi += k*(k-1)*cc_wing_phi[0][k]*pow(Time,k-2);
	        return tmp_wing_phi;
	    }
	    else {
		    return (stroke_amplitude - Angle_Next_Stroke_Delta)*2*PI*f0*2*PI*f0*cos(2*PI*f0*Time);
		}
	}
	else {
		cout<<"Mistake in function wing1_Phi!"<<endl;
		return 0;
	}
}


//double FlapPattern :: wing1_Theta(double stime, int i) 
//{
//	if(i==0)
//		return 0.0*PI/180.0;
//	else if(i==1)
//		return 0;
//	else if(i==2)
//		return 0;
//	else {
//		cout<<"Mistake in function wingplane1_X!"<<endl;
//		return 0;
//	}
//}

double FlapPattern::wing1_Theta(double stime, int i) 
{
	double tmp_wing_theta;
	int order;
	
	double Time = stime - stime_start;
	double f0 = freq/freq_global;
	
	tmp_wing_theta = 0;
	order = (sizeof(cc_wing_theta[0]) / sizeof(cc_wing_theta[0][0]));
	
//	double cc_wing_theta[2][8] = {0};
	double ampU_theta = 0.0/180.0*PI, tilt_theta = 0/180.0*PI;
	double amp0_theta = 0.0/180.0*PI, amp8_theta = 0/180.0*PI;
	amp0_theta = Angle_Current_Ele0;
	
	double a_theta[2][3] = { {ampU_theta, tilt_theta, ampU_theta},
	                         {0,          0,           0} };
	double b_theta[2][3] = { {0,          amp0_theta, amp8_theta},
	                         {0,          0,           0} };
    int n_theta = 3;
    
	if(i==0) 
	{
	    if (Time*f0<0.25) 
	    {
			for (int k=0; k<order; k++) 
				tmp_wing_theta += cc_wing_theta[0][k]*pow(Time,k);
			return tmp_wing_theta;
	    }
	    else
	    {
			for (int k=0; k<n_theta; k++) 
				tmp_wing_theta += a_theta[0][k]*cos(2*PI*k*f0*Time)
				                + b_theta[0][k]*sin(2*PI*k*f0*Time);
			return tmp_wing_theta;
		}
	}
	else if(i==1) 
	{
	    if (Time*f0<0.25)
	    {
			for (int k=1; k<order; k++) 
				tmp_wing_theta += k*cc_wing_theta[0][k]*pow(Time,k-1);
			return tmp_wing_theta;
	    }
	    else 
	    {
			for (int k=1; k<n_theta; k++) 
				tmp_wing_theta += -2*PI*k*f0*a_theta[0][k]*sin(2*PI*k*f0*Time)
				                 + 2*PI*k*f0*b_theta[0][k]*cos(2*PI*k*f0*Time);
			return tmp_wing_theta;
		}
	}
	else if(i==2)
	{
	    if (Time*f0<0.25)
	    {
	        for (int k=2; k<order; k++) 
				tmp_wing_theta += k*(k-1)*cc_wing_theta[0][k]*pow(Time,k-2);
	        return tmp_wing_theta;
	    }
	    else 
	    {
			for (int k=1; k<n_theta; k++) 
				tmp_wing_theta += -2*PI*k*f0*2*PI*k*f0*a_theta[0][k]*cos(2*PI*k*f0*Time)
				                 - 2*PI*k*f0*2*PI*k*f0*b_theta[0][k]*sin(2*PI*k*f0*Time);
			return tmp_wing_theta;
		}
	}
	else {
		cout<<"Mistake in function wing1_Theta!"<<endl;
		return 0;
	}
}

double FlapPattern::wing1_Psi(double stime, int i) 
{
	double temp;
	int psi_a_order, psi_b_order, psi_c_order;
	
	double Time = stime - stime_start;
	double f0 = freq/freq_global;
	
	if (stime < 0.25) {
		temp = 0;
		psi_a_order = (sizeof(cc_wing_psi_a1) / sizeof(cc_wing_psi_a1[0]));
		if(i==0) {
			for(int k=0;k<psi_a_order;k++)
				temp += cc_wing_psi_a1[k]*pow(Time,k);
			return temp;
			//return psi_a*pow(Time,6)+psi_b*pow(Time,5)+psi_c*pow(Time,4)+psi_d*pow(Time,3)+psi_e*pow(Time,2)+psi_f*Time+psi_g;
	    }
		else if(i==1) {
			for(int k=1;k<psi_a_order;k++)
				temp += k*cc_wing_psi_a1[k]*pow(Time,k-1);
			return temp;
			//return 6*psi_a*pow(Time,5)+5*psi_b*pow(Time,4)+4*psi_c*pow(Time,3)+3*psi_d*pow(Time,2)+2*psi_e*Time+psi_f;
	    }
		else if(i==2) {
			for(int k=2;k<psi_a_order;k++)
				temp += k*(k-1)*cc_wing_psi_a1[k]*pow(Time,k-2);
			return temp;
			//return 30*psi_a*pow(Time,4)+20*psi_b*pow(Time,3)+12*psi_c*pow(Time,2)+6*psi_d*Time+2*psi_e;
	    }
		else {
			cout<<"Mistake in function wing_Psi!"<<endl;
			return 0;
		}	
	}
	
//	double Time_p = Time-floor(Time);
//	if(Time_p>0.9999) Time_p -= 1.0;
	
	temp = 0;
	psi_a_order = (sizeof(cc_wing_psi_a1) / sizeof(cc_wing_psi_a1[0]));
	psi_b_order = (sizeof(cc_wing_psi_b1) / sizeof(cc_wing_psi_b1[0]));
	psi_c_order = (sizeof(cc_wing_psi_c1) / sizeof(cc_wing_psi_c1[0]));
	
    // wing rotation kinematics:
    //    0   ~ 1st ctrl point -- start of downstroke, polynominal connection
    //    1st ~ 2nd ctrl point -- in middle of downstroke, harmonic motion
    //    2nd ~ 3rd ctrl point -- transition from downstroke to upstroke, polynominal connection
    //    3st ~ 4th ctrl point -- in middle of upstroke, harmonic motion
    //    4th ~ 5th ctrl point -- transition to next wing cycle, polynominal connection
//	double x_ctrl[6] = {0, 0.1, 0.4, 0.6, 0.9, 1.1};
	
//    rot_tp[0] = rot_tp_ini - rot_ts;
//    rot_tp[1] = rot_tp_ini + rot_ts;
	
    double omega_k[2], omega_b[2];
    omega_k[0] = (rot_ft_h-0.25)/(0.5-rot_tp[0]);
    omega_k[1] = (rot_ft_h-0.25)/(0.5-rot_tp[1]);
    
    omega_b[0] = 0.25 - omega_k[0]*rot_tp[0];
    omega_b[1] = 0.75 - omega_k[1]*(0.5+rot_tp[1]);
    
    for (int id=0; id<2; id++)
    {
        omega_k[id] = omega_k[id]*2*PI;
        omega_b[id] = omega_b[id]*2*PI;
    }
    
    double x_ctrl[6] = {0, rot_tp[0], 0.4, 0.5+rot_tp[1], 0.9, 1.0+rot_tp[0]}, x_mid;
	
	if(i==0) {
		// return PI/2.0-Angle_mid_aoa*sin(2*PI*Time);
		double theta = 0;
		if ( Time*f0 <= x_ctrl[1] ) {
			for (int k=0; k<psi_a_order; k++) 
				theta += cc_wing_psi_a1[k]*pow(Time,k);
	    }
		else if ( x_ctrl[1]<Time*f0 && Time*f0<=x_ctrl[2] ) {
		    theta = mid_aoa[0][0]*sin(omega_k[0]*f0*Time+omega_b[0]) 
		            + PI/2.0*(1-sin(omega_k[0]*f0*Time+omega_b[0]));
	    }
	    else if ( x_ctrl[2]<Time*f0 && Time*f0<=x_ctrl[3] ) {
			for(int k=0;k<psi_b_order;k++) 
				theta += cc_wing_psi_b1[k]*pow(Time,k);
	    }  
	    else if ( x_ctrl[3]<Time*f0 && Time*f0<=x_ctrl[4]) {
			theta = mid_aoa[0][1]*sin(omega_k[1]*f0*Time+omega_b[1]) 
			        + PI/2.0*(1-sin(omega_k[1]*f0*Time+omega_b[1]));
	    }
	    else {
			for (int k=0; k<psi_c_order; k++) 
				theta += cc_wing_psi_c1[k]*pow(Time,k);
	    }
	    return theta;
	}
	else if(i==1) {
		// return -2*PI*Angle_mid_aoa*cos(2*PI*Time);
		double omega = 0;
		if( Time*f0 <= x_ctrl[1] ) {
			for (int k=1; k<psi_a_order; k++) 
				omega += k*cc_wing_psi_a1[k]*pow(Time,k-1);
	    }
		else if ( x_ctrl[1]<Time*f0 && Time*f0<=x_ctrl[2] ) {
		    omega = omega_k[0]*f0*mid_aoa[0][0]*cos(omega_k[0]*f0*Time+omega_b[0]) 
		            - PI/2.0*omega_k[0]*f0*cos(omega_k[0]*f0*Time+omega_b[0]);
	    }
	    else if ( x_ctrl[2]<Time*f0 && Time*f0<=x_ctrl[3] ) {
			for(int k=1; k<psi_b_order; k++) 
				omega += k*cc_wing_psi_b1[k]*pow(Time,k-1);
	    }  
	    else if ( x_ctrl[3]<Time*f0 && Time*f0<=x_ctrl[4]) {
			omega = omega_k[1]*f0*mid_aoa[0][1]*cos(omega_k[1]*f0*Time+omega_b[1]) 
			        - PI/2.0*omega_k[1]*f0*cos(omega_k[1]*f0*Time+omega_b[1]);
	    }
	    else {
			for (int k=1; k<psi_c_order; k++) 
				omega += k*cc_wing_psi_c1[k]*pow(Time,k-1);
	    }
	    return omega;
	}
	else if(i==2) {
		// return 4*PI*PI*Angle_mid_aoa*sin(2*PI*Time);
		double alpha = 0;
		if( Time*f0 <= x_ctrl[1] ) {
			for (int k=2; k<psi_a_order; k++) 
				alpha += k*(k-1)*cc_wing_psi_a1[k]*pow(Time,k-2);
	    }
		else if ( x_ctrl[1]<Time*f0 && Time*f0<=x_ctrl[2] ) {
		    alpha = -omega_k[0]*omega_k[0]*f0*f0*mid_aoa[0][0]*sin(omega_k[0]*f0*Time+omega_b[0]) 
		            + PI/2.0*omega_k[0]*omega_k[0]*f0*f0*sin(omega_k[0]*f0*Time+omega_b[0]);
	    }
	    else if ( x_ctrl[2]<Time*f0 && Time*f0<=x_ctrl[3] ) {
			for (int k=2; k<psi_b_order; k++) 
				alpha += k*(k-1)*cc_wing_psi_b1[k]*pow(Time,k-2);
	    }  
	    else if ( x_ctrl[3]<Time*f0 && Time*f0<=x_ctrl[4]) {
			alpha = -omega_k[1]*omega_k[1]*f0*f0*mid_aoa[0][1]*sin(omega_k[1]*f0*Time+omega_b[1]) 
			        + PI/2.0*omega_k[1]*omega_k[1]*f0*f0*sin(omega_k[1]*f0*Time+omega_b[1]);
		}
	    else {
			for (int k=2; k<psi_c_order; k++) 
				alpha += k*(k-1)*cc_wing_psi_c1[k]*pow(Time,k-2);
		}
		return alpha;
	}
	else {
		cout<<"Mistake in function wing1_Psi!"<<endl;
		return 0;
	}
}

double FlapPattern::wing2_Phi(double stime, int i)
{
	double tmp_wing_phi;
	int order;
	
	double Time = stime - stime_start;
	double f0 = freq/freq_global;
	
	tmp_wing_phi = 0;
	order = (sizeof(cc_wing_phi[1]) / sizeof(cc_wing_phi[1][0]));
	
	if(i==0) {
	    if ( stime<0.25 || (Time*f0 < 0.5 && stime>0.9999) ) {
			for (int k=0; k<order; k++) 
				tmp_wing_phi += cc_wing_phi[1][k]*pow(Time,k);
			return tmp_wing_phi;
	    }
	    else {
		    return (stroke_amplitude + Angle_Next_Stroke_Delta)*cos(2*PI*f0*Time);
		}
	}
	else if(i==1) {
	    if ( stime<0.25 || (Time*f0 < 0.5 && stime>0.9999) ) {
			for (int k=1; k<order; k++) 
				tmp_wing_phi += k*cc_wing_phi[1][k]*pow(Time,k-1);
			return tmp_wing_phi;
	    }
	    else {
		    return -(stroke_amplitude + Angle_Next_Stroke_Delta)*2*PI*f0*sin(2*PI*f0*Time);
		}
	}
	else if(i==2) {
	    if ( stime<0.25 || (Time*f0 < 0.5 && stime>0.9999) ) {
	        for (int k=2; k<order; k++) 
				tmp_wing_phi += k*(k-1)*cc_wing_phi[1][k]*pow(Time,k-2);
	        return tmp_wing_phi;
	    }
	    else {
		    return -(stroke_amplitude + Angle_Next_Stroke_Delta)*2*PI*f0*2*PI*f0*cos(2*PI*f0*Time);
		}
	}
	else {
		cout<<"Mistake in function wing1_Phi!"<<endl;
		return 0;
	}
}

double FlapPattern::wing2_Theta(double stime, int i)
{
	return -wing1_Theta(stime,i);
}

double FlapPattern::wing2_Psi(double stime, int i)
{	
	if (stime < 0.9999)
		return wing1_Psi(stime,i);
	
	double Time = stime - stime_start;
	double f0 = freq/freq_global;
	
	int psi_a_order = (sizeof(cc_wing_psi_a2) / sizeof(cc_wing_psi_a2[0]));
	int psi_b_order = (sizeof(cc_wing_psi_b2) / sizeof(cc_wing_psi_b2[0]));
	int psi_c_order = (sizeof(cc_wing_psi_c2) / sizeof(cc_wing_psi_c2[0]));
	
    // wing rotation kinematics:
    //    0   ~ 1st ctrl point -- start of downstroke, polynominal connection
    //    1st ~ 2nd ctrl point -- in middle of downstroke, harmonic motion
    //    2nd ~ 3rd ctrl point -- transition from downstroke to upstroke, polynominal connection
    //    3st ~ 4th ctrl point -- in middle of upstroke, harmonic motion
    //    4th ~ 5th ctrl point -- transition to next wing cycle, polynominal connection
//	double x_ctrl[6] = {0, 0.1, 0.4, 0.6, 0.9, 1.1};
	
//    rot_tp[0] = rot_tp_ini - rot_ts;
//    rot_tp[1] = rot_tp_ini + rot_ts;
	
    double omega_k[2], omega_b[2];
    omega_k[0] = (rot_ft_h-0.25)/(0.5-rot_tp[0]);
    omega_k[1] = (rot_ft_h-0.25)/(0.5-rot_tp[1]);
    
    omega_b[0] = 0.25 - omega_k[0]*rot_tp[0];
    omega_b[1] = 0.75 - omega_k[1]*(0.5+rot_tp[1]);
    
    for (int id=0; id<2; id++)
    {
        omega_k[id] = omega_k[id]*2*PI;
        omega_b[id] = omega_b[id]*2*PI;
    }
    
    double x_ctrl[6] = {0, rot_tp[0], 0.4, 0.5+rot_tp[1], 0.9, 1.0+rot_tp[0]}, x_mid;
	
	if(i==0) {
		// return PI/2.0-Angle_mid_aoa*sin(2*PI*Time);
		double theta = 0;
		if ( Time*f0 <= x_ctrl[1] ) {
			for (int k=0; k<psi_a_order; k++) 
				theta += cc_wing_psi_a2[k]*pow(Time,k);
	    }
		else if ( x_ctrl[1]<Time*f0 && Time*f0<=x_ctrl[2] ) {
		    theta = mid_aoa[1][0]*sin(omega_k[0]*f0*Time+omega_b[0]) 
		            + PI/2.0*(1-sin(omega_k[0]*f0*Time+omega_b[0]));
	    }
	    else if ( x_ctrl[2]<Time*f0 && Time*f0<=x_ctrl[3] ) {
			for (int k=0; k<psi_b_order; k++) 
				theta += cc_wing_psi_b2[k]*pow(Time,k);
	    }  
	    else if ( x_ctrl[3]<Time*f0 && Time*f0<=x_ctrl[4] ) {
			theta = mid_aoa[1][1]*sin(omega_k[1]*f0*Time+omega_b[1]) 
			        + PI/2.0*(1-sin(omega_k[1]*f0*Time+omega_b[1]));
	    }
	    else {
			for (int k=0; k<psi_c_order; k++) 
				theta += cc_wing_psi_c2[k]*pow(Time,k);
	    }
	    return theta;
	}
	else if(i==1) {
		// return -2*PI*Angle_mid_aoa*cos(2*PI*Time);
		double omega = 0;
		if ( Time*f0 <= x_ctrl[1] ) {
			for (int k=1; k<psi_a_order; k++) 
				omega += k*cc_wing_psi_a2[k]*pow(Time,k-1);
	    }
		else if ( x_ctrl[1]<Time*f0 && Time*f0<=x_ctrl[2] ) {
		    omega = omega_k[0]*f0*mid_aoa[1][0]*cos(omega_k[0]*f0*Time+omega_b[0]) 
		            - PI/2.0*omega_k[0]*f0*cos(omega_k[0]*f0*Time+omega_b[0]);
	    }
	    else if ( x_ctrl[2]<Time*f0 && Time*f0<=x_ctrl[3] ) {
			for (int k=1; k<psi_b_order; k++) 
				omega += k*cc_wing_psi_b2[k]*pow(Time,k-1);
	    }  
	    else if ( x_ctrl[3]<Time*f0 && Time*f0<=x_ctrl[4] ) {
			omega = omega_k[1]*f0*mid_aoa[1][1]*cos(omega_k[1]*f0*Time+omega_b[1]) 
			        - PI/2.0*omega_k[1]*f0*cos(omega_k[1]*f0*Time+omega_b[1]);
	    }
	    else {
			for (int k=1; k<psi_c_order; k++) 
				omega += k*cc_wing_psi_c2[k]*pow(Time,k-1);
	    }
	    return omega;
	}
	else if(i==2) {
		// return 4*PI*PI*Angle_mid_aoa*sin(2*PI*Time);
		double alpha = 0;
		if ( Time*f0 <= x_ctrl[1] ) {
			for (int k=2; k<psi_a_order; k++) 
				alpha += k*(k-1)*cc_wing_psi_a2[k]*pow(Time,k-2);
	    }
		else if ( x_ctrl[1]<Time*f0 && Time*f0<=x_ctrl[2] ) {
		    alpha = -omega_k[0]*omega_k[0]*f0*f0*mid_aoa[1][0]*sin(omega_k[0]*f0*Time+omega_b[0]) 
		            + PI/2.0*omega_k[0]*omega_k[0]*f0*f0*sin(omega_k[0]*f0*Time+omega_b[0]);
	    }
	    else if ( x_ctrl[2]<Time*f0 && Time*f0<=x_ctrl[3] ) {
			for (int k=2; k<psi_b_order; k++) 
				alpha += k*(k-1)*cc_wing_psi_b2[k]*pow(Time,k-2);
	    }
	    else if ( x_ctrl[3]<Time*f0 && Time*f0<=x_ctrl[4] ) {
			alpha = -omega_k[1]*omega_k[1]*f0*f0*mid_aoa[1][1]*sin(omega_k[1]*f0*Time+omega_b[1]) 
			        + PI/2.0*omega_k[1]*omega_k[1]*f0*f0*sin(omega_k[1]*f0*Time+omega_b[1]);
		}
	    else {
			for (int k=2; k<psi_c_order; k++) 
				alpha += k*(k-1)*cc_wing_psi_c2[k]*pow(Time,k-2);
		}
		return alpha;
	}
	else {
		cout<<"Mistake in function wing2_Psi!"<<endl;
		return 0;
	}
}

double FlapPattern::wingplane1_Phi(double stime, int i) {
	int order = (sizeof(cc_wingplane_phi) / sizeof(cc_wingplane_phi[0]));
	double temp=0;
	
	double Time = stime - stime_start; // using solver time directly
	
	if(stime<=1.0) {
	    if(i==0) {
	        return -7.0*PI/180.0;
	    }
	    else {
	        return 0;
	    }
	}
	
	if(i==0) {
		for(int k=0;k<order;k++) 
			temp += cc_wingplane_phi[k]*pow(Time,k);
		return temp;
    }
    else if(i==1) {
		for(int k=1;k<order;k++) 
			temp += k*cc_wingplane_phi[k]*pow(Time,k-1);
		return temp;
    }
    else if(i==2) {
		for(int k=2;k<order;k++) 
			temp += k*(k-1)*cc_wingplane_phi[k]*pow(Time,k-2);
		return temp;
    }
    else {
		cout<<"Mistake takes place in Function wingplane_Psi."<<endl;
		return 0;
    }
}
double FlapPattern::wingplane1_Theta(double stime, int i) {
	int order = (sizeof(cc_wingplane_theta) / sizeof(cc_wingplane_theta[0]));
	double temp=0;
	
	double Time = stime - stime_start; // using solver time directly
	
	if(stime<=1.0) {
	    if(i==0) {
	        return 0.0*PI/180.0;
	    }
	    else {
	        return 0;
	    }
	}
	
	if(i==0) {
		for(int k=0;k<order;k++) 
			temp += cc_wingplane_theta[k]*pow(Time,k);
		return temp;
	}
	else if(i==1) {
		for(int k=1;k<order;k++) 
			temp += k*cc_wingplane_theta[k]*pow(Time,k-1);
		return temp;
	}
	else if(i==2) {
		for(int k=2;k<order;k++) 
			temp += k*(k-1)*cc_wingplane_theta[k]*pow(Time,k-2);
		return temp;
	}
	else {
		cout<<"Mistake takes place in Function wingplane_Theta."<<endl;
		return 0;
	}
}
double FlapPattern::wingplane1_Psi(double stime, int i) {
	int order = (sizeof(cc_wingplane_psi) / sizeof(cc_wingplane_psi[0]));
	double temp=0;
	
	double Time = stime - stime_start; // using solver time directly
	
	if(stime<=1.0) {
	    if(i==0) {
	        return 0.5*PI/180.0;
	    }
	    else {
	        return 0;
	    }
	}
	
	if(i==0) {
		for(int k=0;k<order;k++) 
			temp += cc_wingplane_psi[k]*pow(Time,k);
		return temp;
	}
	else if(i==1) {
		for(int k=1;k<order;k++) 
			temp += k*cc_wingplane_psi[k]*pow(Time,k-1);
		return temp;
	}
	else if(i==2) {
		for(int k=2;k<order;k++) 
			temp += k*(k-1)*cc_wingplane_psi[k]*pow(Time,k-2);
		return temp;
	}
	else{
		cout<<"Mistake takes place in Function wingplane_Psi."<<endl;
		return 0;
	}
}

double FlapPattern::wingplane2_Phi(double stime, int i) {
	return -wingplane1_Phi(stime,i);
}
double FlapPattern::wingplane2_Theta(double stime, int i) {
	return wingplane1_Theta(stime,i);
}
double FlapPattern::wingplane2_Psi(double stime, int i) {
    return wingplane1_Psi(stime,i);
}

void FlapPattern::Init_Flap_Kinematics(void) 
{
	// using solver time directly in first cycle initialization
	
	double x1, x2;
	
	// Wing_Phi Initialization
	x1=0; x2=0.25;
	
	cc_wing_phi[0][0] = -stroke_amplitude;
	cc_wing_phi[0][1] = 0;
	cc_wing_phi[0][2] = 0;
	cc_wing_phi[0][3] = 0;
	cc_wing_phi[0][4] = -stroke_amplitude * cos(2*PI*x2);                       // -A*cos(2*pi*t) = A, t=n+0.5n
	cc_wing_phi[0][5] = 2*PI * stroke_amplitude * sin(2*PI*x2);                 // 2*pi*A*sin(2*pi*t) = 0, t=n+0.5n
	cc_wing_phi[0][6] = 4*PI*PI * stroke_amplitude * cos(2*PI*x2);              // 4*pi*pi*A*cos(2*pi*t) = -4*pi*pi*A, t=n+0.5n
	cc_wing_phi[0][7] = -8*PI*PI*PI * stroke_amplitude * sin(2*PI*x2);
	Cal_Poly_Connection(x1, 4, x2, 4, 0, 0, cc_wing_phi[0]);
	for (int i=0; i<8; i++) cc_wing_phi[1][i] = -cc_wing_phi[0][i];
	
	// Wing_Psi Initialization
    rot_tp[0] = rot_tp_ini - rot_ts;
    rot_tp[1] = rot_tp_ini + rot_ts;
    
	mid_aoa[0][0] = mid_aoa_ini[0] - Angle_Next_Psi_Delta + Angle_Next_AoA_Delta;  // downstroke
	mid_aoa[0][1] = mid_aoa_ini[1] + Angle_Next_Psi_Delta - Angle_Next_AoA_Delta;  // upstroke
	mid_aoa[1][0] = mid_aoa_ini[0] - Angle_Next_Psi_Delta - Angle_Next_AoA_Delta;  // downstroke
	mid_aoa[1][1] = mid_aoa_ini[1] + Angle_Next_Psi_Delta + Angle_Next_AoA_Delta;  // upstroke
    
    double f0 = freq/freq_global; // freq = 1;
//    double x_ctrl[6] = {0, 0.25, 0.4, 0.6, 0.9, 1.1}, x_mid;
    double x_ctrl[6] = {0, 0.25, 0.4, 0.5+rot_tp[1], 0.9, 1.0+rot_tp[0]}, x_mid;
    for (int i=0; i<6; i++) {
        x_ctrl[i] = x_ctrl[i] * (stime_end - stime_start);
    }
    double aoa_x_ctrl[6];    // required aoa at each control point
    double psi_x_ctrl[6][4]; // psi angle and higher derivatives at each control point
	
    double omega_k[6], omega_b[6];
    omega_k[0] = 0;
    omega_k[1] = (rot_ft_h-0.25)/(0.5-rot_tp[0]);
    omega_k[2] = omega_k[1];
    omega_k[3] = (rot_ft_h-0.25)/(0.5-rot_tp[1]);
    omega_k[4] = omega_k[3];
    omega_k[5] = omega_k[1];
    
    omega_b[0] = 0;
    omega_b[1] = 0.25 - omega_k[1]*rot_tp[0];
    omega_b[2] = omega_b[1];
    omega_b[3] = 0.75 - omega_k[3]*(0.5+rot_tp[1]);
    omega_b[4] = omega_b[3];
    omega_b[5] = 1.25 - omega_k[1]*(1.0+rot_tp[0]);
    
    for (int i=1; i<6; i++)
    {
        omega_k[i] = omega_k[i]*2*PI;
        omega_b[i] = omega_b[i]*2*PI;
    }
	
    // wing1 -- right wing
    aoa_x_ctrl[0] = 0; // x_ctrl = 0
    aoa_x_ctrl[1] = mid_aoa[0][0]; // downstroke, x_ctrl = 0.1
    aoa_x_ctrl[2] = mid_aoa[0][0]; // downstroke, x_ctrl = 0.4
    aoa_x_ctrl[3] = mid_aoa[0][1]; // upstroke, x_ctrl = 0.6
    aoa_x_ctrl[4] = mid_aoa[0][1]; // upstroke, x_ctrl = 0.9
    aoa_x_ctrl[5] = mid_aoa[0][0]; // downstroke, x_ctrl = 1.1
	
	psi_x_ctrl[0][0] = PI/2.0; 
	psi_x_ctrl[0][1] = 0;
	psi_x_ctrl[0][2] = 0;
	psi_x_ctrl[0][3] = 0;
    for (int i=1; i<6; i++) {
        psi_x_ctrl[i][0] = PI/2.0 - (PI/2.0 - aoa_x_ctrl[i]) * sin(omega_k[i]*f0*x_ctrl[i]+omega_b[i]);
        psi_x_ctrl[i][1] = -(PI/2.0 - aoa_x_ctrl[i]) * omega_k[i]*f0 * cos(omega_k[i]*f0*x_ctrl[i]+omega_b[i]);
        psi_x_ctrl[i][2] = (PI/2.0 - aoa_x_ctrl[i]) * std::pow(omega_k[i]*f0, 2) * sin(omega_k[i]*f0*x_ctrl[i]+omega_b[i]);
        psi_x_ctrl[i][3] = (PI/2.0 - aoa_x_ctrl[i]) * std::pow(omega_k[i]*f0, 3) * cos(omega_k[i]*f0*x_ctrl[i]+omega_b[i]);
    }
    
	for (int i=0; i<4; i++) {
	    // start section, cc_wing_psi_a1 size is 8
	    cc_wing_psi_a1[i]   = psi_x_ctrl[0][i];
	    cc_wing_psi_a1[i+4] = psi_x_ctrl[1][i];
        // middle section (downstroke to upstroke)
	    cc_wing_psi_b1[i]   = psi_x_ctrl[2][i];
	    cc_wing_psi_b1[i+4] = psi_x_ctrl[3][i];
        // end section (upstroke to downstroke)
	    cc_wing_psi_c1[i]   = psi_x_ctrl[4][i];
	    cc_wing_psi_c1[i+4] = psi_x_ctrl[5][i];
	}
	Cal_Poly_Connection(x_ctrl[0], 4, x_ctrl[1], 4, 0, 0, cc_wing_psi_a1);
    for(int i=0;i<8;i++) cc_wing_psi_a2[i]=cc_wing_psi_a1[i];
    
    x_mid = 0.5 * (stime_end - stime_start); cc_wing_psi_b1[8] = PI/2.0; // let curve pass (t=0.5, AoA=PI/2)
	Cal_Poly_Connection(x_ctrl[2], 4, x_ctrl[3], 4, x_mid, 1, cc_wing_psi_b1);
	for(int i=0;i<9;i++) cc_wing_psi_b2[i]=cc_wing_psi_b1[i];
	
    x_mid = 1.0 * (stime_end - stime_start); cc_wing_psi_c1[8] = PI/2.0; // let curve pass (t=1.0, AoA=PI/2)
	Cal_Poly_Connection(x_ctrl[4], 4, x_ctrl[5], 4, x_mid, 1, cc_wing_psi_c1);
	for(int i=0;i<9;i++) cc_wing_psi_c2[i]=cc_wing_psi_c1[i];
}

void FlapPattern::Update_Flap_Kinematics_WP(void)
{
	double x1,x2;
	double f0 = freq/freq_global;
	
    // update wingplane kinematics, using solver time directly
	x1 = 0; x2 = stime_end - stime_start;
	// wingplane phi
	cc_wingplane_phi[0]=Angle_Previous_Phi;
	cc_wingplane_phi[1]=0;
	cc_wingplane_phi[2]=0;
	cc_wingplane_phi[3]=Angle_Next_Phi;
	cc_wingplane_phi[4]=0;
	cc_wingplane_phi[5]=0;
	Cal_Poly_Connection(x1, 3, x2, 3, 0, 0, cc_wingplane_phi);
	//c5=right[5];c4=right[4];c3=right[3];c2=right[2];c1=right[1];c0=right[0];
	// wingplane theta
	cc_wingplane_theta[0]=Angle_Previous_Theta;
	cc_wingplane_theta[1]=0;
	cc_wingplane_theta[2]=0;
	cc_wingplane_theta[3]=Angle_Next_Theta;
	cc_wingplane_theta[4]=0;
	cc_wingplane_theta[5]=0;
	Cal_Poly_Connection(x1, 3, x2, 3, 0, 0, cc_wingplane_theta);
	//a5=right[5];a4=right[4];a3=right[3];a2=right[2];a1=right[1];a0=right[0];
	// wingplane psi
	cc_wingplane_psi[0]=Angle_Previous_Psi;
	cc_wingplane_psi[1]=0;
	cc_wingplane_psi[2]=0;
	cc_wingplane_psi[3]=Angle_Next_Psi;
	cc_wingplane_psi[4]=0;
	cc_wingplane_psi[5]=0;
	Cal_Poly_Connection(x1, 3, x2, 3, 0, 0, cc_wingplane_psi);
	//b5=right[5];b4=right[4];b3=right[3];b2=right[2];b1=right[1];b0=right[0];
}

void FlapPattern::Update_Flap_Kinematics_Phi(void)
{
	double x1,x2;
	double f0 = freq/freq_global;
	
	// Stroke amplitude control
	x1=0.0*(stime_end - stime_start); x2=0.5*(stime_end - stime_start);
	// wing1 -- right wing
	cc_wing_phi[0][0] = -(stroke_amplitude - Angle_Previous_Stroke_Delta);    // -A*cos(2*pi*f0*t) = -A, t=n
	cc_wing_phi[0][1] = 0;                                                   // 2*pi*f0*A*sin(2*pi*f0*t) = 0, t=n
	cc_wing_phi[0][2] = std::pow(2.0*PI*freq_old/freq_global, 2) 
                        * (stroke_amplitude - Angle_Previous_Stroke_Delta);   // (2pi*f0)^2*A*cos(2*pi*f0*t) = (2pi*f0)^2*A, t=n
    cc_wing_phi[0][3] = 0;                                                   // -(2pi*f0)^3*A*sin(2*pi*f0*t) = 0, t=n
	cc_wing_phi[0][4] = stroke_amplitude - Angle_Next_Stroke_Delta;          // -A*cos(2*pi*f0*t) = A, t=n+0.5n
	cc_wing_phi[0][5] = 0;                                                   // 2*pi*f0*A*sin(2*pi*f0*t) = 0 t=n+0.5n
	cc_wing_phi[0][6] = -std::pow(2.0*PI*freq/freq_global, 2) 
	                    * (stroke_amplitude - Angle_Next_Stroke_Delta);      // (2pi*f0)^2*A*cos(2*pi*f0*t) = -(2pi*f0)^2*A, t=n+0.5n
    cc_wing_phi[0][7] = 0;                                                   // -(2pi*f0)^3*A*sin(2*pi*f0*t) = 0, t=n+0.5n
	Cal_Poly_Connection(x1, 4, x2, 4, 0, 0, cc_wing_phi[0]);
	// wing2 -- left wing
	cc_wing_phi[1][0] = stroke_amplitude + Angle_Previous_Stroke_Delta;       // A*cos(2*pi*f0*t) = A, t=n
	cc_wing_phi[1][1] = 0;                                                   // -2*pi*f0*A*sin(2*pi*f0*t) = 0, t=n
	cc_wing_phi[1][2] = -std::pow(2.0*PI*freq_old/freq_global, 2) 
                        * (stroke_amplitude + Angle_Previous_Stroke_Delta);   // -(2pi*f0)^2*A*cos(2*pi*f0*t) = -(2pi*f0)^2*A, t=n
    cc_wing_phi[1][3] = 0;                                                   // (2pi*f0)^3*A*sin(2*pi*f0*t) = 0, t=n
	cc_wing_phi[1][4] = -(stroke_amplitude + Angle_Next_Stroke_Delta);       // A*cos(2*pi*f0*t) = -A, t=n+0.5n
	cc_wing_phi[1][5] = 0;                                                   // -2*pi*f0*A*sin(2*pi*f0*t) = 0 t=n+0.5n
	cc_wing_phi[1][6] = std::pow(2.0*PI*freq/freq_global, 2) 
	                    * (stroke_amplitude + Angle_Next_Stroke_Delta);      // -(2pi*f0)^2*A*cos(2*pi*f0*t) = (2pi*f0)^2*A, t=n+0.5n
    cc_wing_phi[1][7] = 0;                                                   // (2pi*f0)^3*A*sin(2*pi*f0*t) = 0, t=n
	Cal_Poly_Connection(x1, 4, x2, 4, 0, 0, cc_wing_phi[1]);
    	
}

void FlapPattern::Update_Flap_Kinematics_Psi(void)
{
	double f0 = freq/freq_global;
	
	// AoA control
    // 		start section 
    //		using current psi_c, psi_c will change in next section
    rot_tp[0] = rot_tp_ini - rot_ts;
    rot_tp[1] = rot_tp_ini + rot_ts;
    
	mid_aoa[0][0] = mid_aoa_ini[0] - Angle_Next_Psi_Delta + Angle_Next_AoA_Delta;  // downstroke
	mid_aoa[0][1] = mid_aoa_ini[1] + Angle_Next_Psi_Delta - Angle_Next_AoA_Delta;  // upstroke
	mid_aoa[1][0] = mid_aoa_ini[0] - Angle_Next_Psi_Delta - Angle_Next_AoA_Delta;  // downstroke
	mid_aoa[1][1] = mid_aoa_ini[1] + Angle_Next_Psi_Delta + Angle_Next_AoA_Delta;  // upstroke
    
    double x_ctrl[6] = {0, rot_tp[0], 0.4, rot_tp[1]+0.5, 0.9, rot_tp[0]+1}, x_mid;
    for (int i=0; i<6; i++) {
        x_ctrl[i] = x_ctrl[i] * (stime_end - stime_start);
    }
    double aoa_x_ctrl[6];    // required aoa at each control point
    double psi_x_ctrl[6][4]; // psi angle and higher derivatives at each control point
	double previous_end = freq_global/freq_old;
	
    double omega_k[6], omega_b[6];
    omega_k[0] = 0;
    omega_k[1] = (rot_ft_h-0.25)/(0.5-rot_tp[0]);
    omega_k[2] = omega_k[1];
    omega_k[3] = (rot_ft_h-0.25)/(0.5-rot_tp[1]);
    omega_k[4] = omega_k[3];
    omega_k[5] = omega_k[1];
    
    omega_b[0] = 0;
    omega_b[1] = 0.25 - omega_k[1]*rot_tp[0];
    omega_b[2] = omega_b[1];
    omega_b[3] = 0.75 - omega_k[3]*(0.5+rot_tp[1]);
    omega_b[4] = omega_b[3];
    omega_b[5] = 1.25 - omega_k[1]*(1.0+rot_tp[0]);
    
    for (int i=1; i<6; i++)
    {
        omega_k[i] = omega_k[i]*2*PI;
        omega_b[i] = omega_b[i]*2*PI;
    }
	
    // wing1 -- right wing
    aoa_x_ctrl[0] = 0; // x_ctrl = 0
    aoa_x_ctrl[1] = mid_aoa[0][0]; // downstroke, x_ctrl = 0.1
    aoa_x_ctrl[2] = mid_aoa[0][0]; // downstroke, x_ctrl = 0.4
    aoa_x_ctrl[3] = mid_aoa[0][1]; // upstroke, x_ctrl = 0.6
    aoa_x_ctrl[4] = mid_aoa[0][1]; // upstroke, x_ctrl = 0.9
    aoa_x_ctrl[5] = mid_aoa[0][0]; // downstroke, x_ctrl = 1.1
	
	psi_x_ctrl[0][0] = 0; for(int i=0;i<9;i++) psi_x_ctrl[0][0] += cc_wing_psi_c1[i]*pow(previous_end,i);  // or: right[0] = PI/2.0;
	psi_x_ctrl[0][1] = 0; for(int i=1;i<9;i++) psi_x_ctrl[0][1] += i*cc_wing_psi_c1[i]*pow(previous_end,i-1);
	psi_x_ctrl[0][2] = 0; for(int i=2;i<9;i++) psi_x_ctrl[0][2] += i*(i-1)*cc_wing_psi_c1[i]*pow(previous_end,i-2);
	psi_x_ctrl[0][3] = 0; for(int i=3;i<9;i++) psi_x_ctrl[0][3] += i*(i-1)*(i-2)*cc_wing_psi_c1[i]*pow(previous_end,i-3);
    for (int i=1; i<6; i++) {
        psi_x_ctrl[i][0] = PI/2.0 - (PI/2.0 - aoa_x_ctrl[i]) * sin(omega_k[i]*f0*x_ctrl[i]+omega_b[i]);
        psi_x_ctrl[i][1] = -(PI/2.0 - aoa_x_ctrl[i]) * omega_k[i]*f0 * cos(omega_k[i]*f0*x_ctrl[i]+omega_b[i]);
        psi_x_ctrl[i][2] = (PI/2.0 - aoa_x_ctrl[i]) * std::pow(omega_k[i]*f0, 2) * sin(omega_k[i]*f0*x_ctrl[i]+omega_b[i]);
        psi_x_ctrl[i][3] = (PI/2.0 - aoa_x_ctrl[i]) * std::pow(omega_k[i]*f0, 3) * cos(omega_k[i]*f0*x_ctrl[i]+omega_b[i]);
    }
    
	for (int i=0; i<4; i++) {
	    // start section, cc_wing_psi_a1 size is 8
	    cc_wing_psi_a1[i]   = psi_x_ctrl[0][i];
	    cc_wing_psi_a1[i+4] = psi_x_ctrl[1][i];
        // middle section (downstroke to upstroke)
	    cc_wing_psi_b1[i]   = psi_x_ctrl[2][i];
	    cc_wing_psi_b1[i+4] = psi_x_ctrl[3][i];
        // end section (upstroke to downstroke)
	    cc_wing_psi_c1[i]   = psi_x_ctrl[4][i];
	    cc_wing_psi_c1[i+4] = psi_x_ctrl[5][i];
	}
	Cal_Poly_Connection(x_ctrl[0], 4, x_ctrl[1], 4, 0, 0, cc_wing_psi_a1);
    
    x_mid = 0.5 * (stime_end - stime_start); cc_wing_psi_b1[8] = PI/2.0; // let curve pass (t=0.5, AoA=PI/2)
	Cal_Poly_Connection(x_ctrl[2], 4, x_ctrl[3], 4, x_mid, 1, cc_wing_psi_b1);
	
    x_mid = 1.0 * (stime_end - stime_start); cc_wing_psi_c1[8] = PI/2.0; // let curve pass (t=1.0, AoA=PI/2)
	Cal_Poly_Connection(x_ctrl[4], 4, x_ctrl[5], 4, x_mid, 1, cc_wing_psi_c1);
	
    // wing2 -- left wing
    aoa_x_ctrl[0] = 0; // x_ctrl = 0
    aoa_x_ctrl[1] = mid_aoa[1][0]; // downstroke, x_ctrl = 0.1
    aoa_x_ctrl[2] = mid_aoa[1][0]; // downstroke, x_ctrl = 0.4
    aoa_x_ctrl[3] = mid_aoa[1][1]; // upstroke, x_ctrl = 0.6
    aoa_x_ctrl[4] = mid_aoa[1][1]; // upstroke, x_ctrl = 0.9
    aoa_x_ctrl[5] = mid_aoa[1][0]; // downstroke, x_ctrl = 1.1
	
	psi_x_ctrl[0][0] = 0; for(int i=0;i<9;i++) psi_x_ctrl[0][0] += cc_wing_psi_c2[i]*pow(previous_end,i);  // or: right[0] = PI/2.0;
	psi_x_ctrl[0][1] = 0; for(int i=1;i<9;i++) psi_x_ctrl[0][1] += i*cc_wing_psi_c2[i]*pow(previous_end,i-1);
	psi_x_ctrl[0][2] = 0; for(int i=2;i<9;i++) psi_x_ctrl[0][2] += i*(i-1)*cc_wing_psi_c2[i]*pow(previous_end,i-2);
	psi_x_ctrl[0][3] = 0; for(int i=3;i<9;i++) psi_x_ctrl[0][3] += i*(i-1)*(i-2)*cc_wing_psi_c2[i]*pow(previous_end,i-3);
    for (int i=1; i<6; i++) {
        psi_x_ctrl[i][0] = PI/2.0 - (PI/2.0 - aoa_x_ctrl[i]) * sin(omega_k[i]*f0*x_ctrl[i]+omega_b[i]);
        psi_x_ctrl[i][1] = -(PI/2.0 - aoa_x_ctrl[i]) * omega_k[i]*f0 * cos(omega_k[i]*f0*x_ctrl[i]+omega_b[i]);
        psi_x_ctrl[i][2] = (PI/2.0 - aoa_x_ctrl[i]) * std::pow(omega_k[i]*f0, 2) * sin(omega_k[i]*f0*x_ctrl[i]+omega_b[i]);
        psi_x_ctrl[i][3] = (PI/2.0 - aoa_x_ctrl[i]) * std::pow(omega_k[i]*f0, 3) * cos(omega_k[i]*f0*x_ctrl[i]+omega_b[i]);
    }
    
	for (int i=0; i<4; i++) {
	    // start section, cc_wing_psi_a2 size is 8
	    cc_wing_psi_a2[i]   = psi_x_ctrl[0][i];
	    cc_wing_psi_a2[i+4] = psi_x_ctrl[1][i];
	    // middle section (downstroke to upstroke)
	    cc_wing_psi_b2[i]   = psi_x_ctrl[2][i];
	    cc_wing_psi_b2[i+4] = psi_x_ctrl[3][i];
	    // end section (upstroke to downstroke)
	    cc_wing_psi_c2[i]   = psi_x_ctrl[4][i];
	    cc_wing_psi_c2[i+4] = psi_x_ctrl[5][i];
	}
	Cal_Poly_Connection(x_ctrl[0], 4, x_ctrl[1], 4, 0, 0, cc_wing_psi_a2);
    
    x_mid = 0.5 * (stime_end - stime_start); cc_wing_psi_b2[8] = PI/2.0; // let curve pass (t=0.5, AoA=PI/2)
	Cal_Poly_Connection(x_ctrl[2], 4, x_ctrl[3], 4, x_mid, 1, cc_wing_psi_b2);
    
    x_mid = 1.0 * (stime_end - stime_start); cc_wing_psi_c2[8] = PI/2.0; // let curve pass (t=1.0, AoA=PI/2)
	Cal_Poly_Connection(x_ctrl[4], 4, x_ctrl[5], 4, x_mid, 1, cc_wing_psi_c2);
}

void FlapPattern::Update_Flap_Kinematics_Theta(void)
{
	double x1,x2;
	double f0 = freq/freq_global, f0_old = freq_old/freq_global;
	
	double b_theta1;
	
	// Elevation control
	x1=0.0*(stime_end - stime_start); x2=0.25*(stime_end - stime_start);
	
	// wing1 -- right wing
	b_theta1 = Angle_Previous_Ele0;
	cc_wing_theta[0][0] = b_theta1*sin(2*PI*1.0); // f0_old*x1
	cc_wing_theta[0][1] = b_theta1*2*PI*f0_old*cos(2*PI*1.0);
	cc_wing_theta[0][2] = -b_theta1*std::pow(2*PI*f0_old, 2)*sin(2*PI*1.0);
	cc_wing_theta[0][3] = -b_theta1*std::pow(2*PI*f0_old, 3)*cos(2*PI*1.0);
	
	b_theta1 = Angle_Next_Ele0;
	cc_wing_theta[0][4] = b_theta1*sin(2*PI*f0*x2); 
	cc_wing_theta[0][5] = b_theta1*2*PI*f0*cos(2*PI*f0*x2);
	cc_wing_theta[0][6] = -b_theta1*std::pow(2*PI*f0, 2)*sin(2*PI*f0*x2);
	cc_wing_theta[0][7] = -b_theta1*std::pow(2*PI*f0, 3)*cos(2*PI*f0*x2);
	
	Cal_Poly_Connection(x1, 4, x2, 4, 0, 0, cc_wing_theta[0]);
}

void FlapPattern::Update_Flap_Kinematics(void) 
{
	double x1,x2;
	double f0 = freq/freq_global;
	
	Update_Flap_Kinematics_WP();
	
	Update_Flap_Kinematics_Phi();
	
	Update_Flap_Kinematics_Psi();
	
	Update_Flap_Kinematics_Theta();
}


void Cal_Poly_Connection(double x1, int order1, double x2, int order2, double x_ctrl, int ctrl_number, double *right) {
	/*
	Using a polynomial to connect motion functions
		Only one middle control point can be handled, and derivatives of it are not controlled
		The order of continuous derivatives at both end need to be the same
	*/
	int      i, j, k, order;
	double** temp;
	
	order = order1 + order2 + ctrl_number;
	temp = new double* [order]; 
	for (i=0; i<order; i++) temp[i] = new double [order];

	// x1
	for(i=0;i<order1;i++) 
	{
		for(j=0;j<order;j++) 
		{	
			if(j<i) 
			{
				temp[i][j]=0;
			}
			else 
			{
				temp[i][j]=pow(x1,j-i);
				for(k=j;k>j-i;k--) 
				{
					temp[i][j] *= k;
				}
			}
		}
	}
	// x2
	for(i=0;i<order2;i++) 
	{
		for(j=0;j<order;j++) 
		{	
			if(j<i) 
			{
				temp[i+order1][j]=0;
			}
			else 
			{
				temp[i+order1][j]=pow(x2,j-i);
				for(k=j;k>j-i;k--) 
				{
					temp[i+order1][j] *= k;
				}
			}
		}
	}
	// control point
	if(ctrl_number>0) 
	{
		for(i=order-ctrl_number;i<order;i++)
			for(j=0;j<order;j++)
				temp[i][j]=pow(x_ctrl,j);
	}

	gauss(temp, right, order); // using 'right' to store the results
	// c5=right[5];c4=right[4];c3=right[3];c2=right[2];c1=right[1];c0=right[0];
	for(i=0;i<order;i++) delete[] temp[i];
	delete[] temp;
}


