#include <omp.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <vector>

#include <cuda_runtime.h>
#include <cublas.h>

using namespace std;

#define  PI 3.1415926535897932
#define  NF_MAX 4
#define  OrderL 9

#include "../common/global.h"
#include "../common/gpu_helper.h"
#include "../common/write_result.h"

void Initialize_GPU()
{
    cublasInit();
    mallocGlobal_GPU();
}

void Finalize_GPU()
{
    cublasShutdown();
    mfreeGlobal_GPU();
}



void total_CoM(int tmp_or_not)
{
	// double R_lw_inB[3], R_rw_inB[3];
	double mass_ratio;

	wingplane_1.get_wing_mass_from_subframe(0);
	wingplane_2.get_wing_mass_from_subframe(0);
	// cout<<"Wing:  "<<setprecision(16)<<wing_1.wing_inertia[0][0]<<"  "<<wing_2.wing_inertia[0][0]<<"  "<<endl;
	// cout<<"Wing plane:  "<<setprecision(16)<<wingplane_1.wing_inertia[0][0]<<"  "<<wingplane_2.wing_inertia[0][0]<<"  "<<endl;

	insect.get_wing_mass_from_subframe(0);
	for (int i=0; i<3; i++) 
		{R_rw_inB[i]=insect.wingcenter[i];}
	for (int i=0; i<3; i++)
	{
		for (int j=0; j<3; j++) 
			{I_rw_B[i][j]=insect.wing_inertia[i][j];}
	}
		
	insect.get_wing_mass_from_subframe(1);
	for (int i=0; i<3; i++) 
		{R_lw_inB[i]=insect.wingcenter[i];}
	for (int i=0; i<3; i++)
	{
		for (int j=0; j<3; j++) 
			{I_lw_B[i][j]=insect.wing_inertia[i][j];}
	}

	for (int i=0; i<3; i++) 
	{
		mass_ratio = insect_parameters.Nondimensional_One_Wing_Mass/insect_parameters.Nondimensional_Total_Mass;
		// cout<<"mass_ratio:  "<<mass_ratio<<endl;
		Total_Center_B[i] = mass_ratio*(R_rw_inB[i]+R_lw_inB[i]);
	}

	switch (tmp_or_not)
	{
		case 1:
			for (int i=0; i<3; i++) 
				{
					Total_Center_G[i] = insect.orientation[i][0]*Total_Center_B[0]+insect.orientation[i][1]*Total_Center_B[1]
					                    +insect.orientation[i][2]*Total_Center_B[2]+insect.position[i];
				}
			break;
		case 2:
			for (int i=0; i<3; i++) 
				{
					Total_Center_G[i] = insect.orientation_tmp[i][0]*Total_Center_B[0]+insect.orientation_tmp[i][1]*Total_Center_B[1]
					                    +insect.orientation_tmp[i][2]*Total_Center_B[2]+insect.position[i];
				}
			break;
	}
		
}

void go_superframe(double orientation[3][3], double to_transform[3], double transformed[3])
{
	for (int i=0; i<3; i++) 
	{
		transformed[i] = orientation[i][0]*to_transform[0]+orientation[i][1]*to_transform[1]
		                    +orientation[i][2]*to_transform[2];
	}
}

void inertia_in_global(int tmp_or_not)
{
	double O_tran[3][3],temp[3][3];

	// for (int i=0; i<3; i++) {R_BtoC[i]=insect.position[i]-Total_Center_G[i];}

	switch (tmp_or_not)
	{
		case 1:
			go_superframe(insect.orientation, Total_Center_B, R_BtoC);
			for (int i=0; i<3; i++) {R_BtoC[i]=-R_BtoC[i];}
			Transpose(insect.orientation,O_tran);
			Multiply(insect.orientation,insect.local_inertia,temp);
			Multiply(temp,O_tran,insect.inertia);
			break;
		case 2:
			go_superframe(insect.orientation_tmp, Total_Center_B, R_BtoC);
			for (int i=0; i<3; i++) {R_BtoC[i]=-R_BtoC[i];}
			Transpose(insect.orientation_tmp,O_tran);
			Multiply(insect.orientation_tmp,insect.local_inertia,temp);
			Multiply(temp,O_tran,insect.inertia);
			break;
	}

	body_inertia[0][0] = insect.inertia[0][0]+insect_parameters.Nondimensional_Mass*(R_BtoC[1]*R_BtoC[1]+R_BtoC[2]*R_BtoC[2]);
	body_inertia[0][1] = insect.inertia[0][1]-insect_parameters.Nondimensional_Mass*(R_BtoC[0]*R_BtoC[1]);
	body_inertia[0][2] = insect.inertia[0][2]-insect_parameters.Nondimensional_Mass*(R_BtoC[0]*R_BtoC[2]);
	body_inertia[1][0] = insect.inertia[1][0]-insect_parameters.Nondimensional_Mass*(R_BtoC[1]*R_BtoC[0]);
	body_inertia[1][1] = insect.inertia[1][1]+insect_parameters.Nondimensional_Mass*(R_BtoC[0]*R_BtoC[0]+R_BtoC[2]*R_BtoC[2]);
	body_inertia[1][2] = insect.inertia[1][2]-insect_parameters.Nondimensional_Mass*(R_BtoC[1]*R_BtoC[2]);
	body_inertia[2][0] = insect.inertia[2][0]-insect_parameters.Nondimensional_Mass*(R_BtoC[2]*R_BtoC[0]);
	body_inertia[2][1] = insect.inertia[2][1]-insect_parameters.Nondimensional_Mass*(R_BtoC[2]*R_BtoC[1]);
	body_inertia[2][2] = insect.inertia[2][2]+insect_parameters.Nondimensional_Mass*(R_BtoC[0]*R_BtoC[0]+R_BtoC[1]*R_BtoC[1]);

	switch (tmp_or_not)
	{
		case 1:
			Multiply(insect.orientation,I_rw_B,temp);
			break;
		case 2:
			Multiply(insect.orientation_tmp,I_rw_B,temp);
			break;
	}
	Multiply(temp,O_tran,I_rw);

	switch (tmp_or_not)
	{
		case 1:
			Multiply(insect.orientation,I_lw_B,temp);
			break;
		case 2:
			Multiply(insect.orientation_tmp,I_lw_B,temp);
			break;
	}
	Multiply(temp,O_tran,I_lw);
}

void vector_in_global(int tmp_or_not)
{
	double temp[3];

	go_superframe(wing_1.orientation, wing_1.wingcenter, temp);
	go_superframe(wingplane_1.orientation, temp, R_rw_B);
	switch (tmp_or_not)
	{
		case 1:
			go_superframe(insect.orientation, R_rw_B, R_rw);
			break;
		case 2:
			go_superframe(insect.orientation_tmp, R_rw_B, R_rw);
			break;
	}	

	go_superframe(wing_2.orientation, wing_2.wingcenter, temp);
	go_superframe(wingplane_2.orientation, temp, R_lw_B);
	switch (tmp_or_not)
	{
		case 1:
			go_superframe(insect.orientation, R_lw_B, R_lw);
			break;
		case 2:
			go_superframe(insect.orientation_tmp, R_lw_B, R_lw);
			break;
	}
	// for (int i=0; i<3; i++)
	// {
	// 	cout<<"R_w is:  "<<R_rw[i]<<"  "<<R_lw[i]<<endl;
	// }
	
	for (int i=0; i<3; i++)
	{
		R_PtoC_rw_B[i] = wingplane_1.position[i]-Total_Center_B[i];
		R_PtoC_lw_B[i] = wingplane_2.position[i]-Total_Center_B[i];

		// cout<<"R_PtoC is:  "<<R_PtoC_rw_B[i]<<"  "<<R_PtoC_lw_B[i]<<endl;
	}
	switch (tmp_or_not)
	{
		case 1:
			go_superframe(insect.orientation, R_PtoC_rw_B, R_PtoC_rw);
			go_superframe(insect.orientation, R_PtoC_lw_B, R_PtoC_lw);
			break;
		case 2:
			go_superframe(insect.orientation_tmp, R_PtoC_rw_B, R_PtoC_rw);
			go_superframe(insect.orientation_tmp, R_PtoC_lw_B, R_PtoC_lw);
			break;
	}

	go_superframe(wingplane_1.orientation, wing_1.angular_velocity, omega_rw_B);  
	switch (tmp_or_not)
	{
		case 1:
			go_superframe(insect.orientation, omega_rw_B, omega_rw);
			break;
		case 2:
			go_superframe(insect.orientation_tmp, omega_rw_B, omega_rw);
			break;
	}	
	
	go_superframe(wingplane_2.orientation, wing_2.angular_velocity, omega_lw_B); 
	switch (tmp_or_not)
	{
		case 1:
			go_superframe(insect.orientation, omega_lw_B, omega_lw);
			break;
		case 2:
			go_superframe(insect.orientation_tmp, omega_lw_B, omega_lw);
			break;
	} 
	// for (int i=0; i<3; i++)
	// {
		// cout<<"Omega_w is:  "<<omega_rw[i]<<"  "<<omega_lw[i]<<endl;
	// }
	
	switch (tmp_or_not)
	{
		case 1:
			go_superframe(insect.orientation, wingplane_1.angular_velocity, omega_rwp);
			break;
		case 2:
			go_superframe(insect.orientation_tmp, wingplane_1.angular_velocity, omega_rwp);
			break;
	}
	
	switch (tmp_or_not)
	{
		case 1:
			go_superframe(insect.orientation, wingplane_2.angular_velocity, omega_lwp);
			break;
		case 2:
			go_superframe(insect.orientation_tmp, wingplane_2.angular_velocity, omega_lwp);
			break;
	}
	// for (int i=0; i<3; i++)
	// {
		// cout<<"Omega_wp is:  "<<omega_rwp[i]<<"  "<<omega_lwp[i]<<endl;
	// }

	go_superframe(wingplane_1.orientation, wing_1.angular_acceleration, temp);  
	switch (tmp_or_not)
	{
		case 1:
			go_superframe(insect.orientation, temp, alpha_rw);
			break;
		case 2:
			go_superframe(insect.orientation_tmp, temp, alpha_rw);
			break;
	}

	go_superframe(wingplane_2.orientation, wing_2.angular_acceleration, temp);  
	switch (tmp_or_not)
	{
		case 1:
			go_superframe(insect.orientation, temp, alpha_lw);
			break;
		case 2:
			go_superframe(insect.orientation_tmp, temp, alpha_lw);
			break;
	}
	
	switch (tmp_or_not)
	{
		case 1:
			go_superframe(insect.orientation, wingplane_1.angular_acceleration, alpha_rwp);
			break;
		case 2:
			go_superframe(insect.orientation_tmp, wingplane_1.angular_acceleration, alpha_rwp);
			break;
	}

	switch (tmp_or_not)
	{
		case 1:
			go_superframe(insect.orientation, wingplane_2.angular_acceleration, alpha_lwp);
			break;
		case 2:
			go_superframe(insect.orientation_tmp, wingplane_2.angular_acceleration, alpha_lwp);
			break;
	}	
}

void defined_operation(double R[3], double r[3], double R_op_r[3][3])
{
	R_op_r[0][0] = R[1]*r[1]+R[2]*r[2];
	R_op_r[0][1] = -R[1]*r[0];
	R_op_r[0][2] = -R[2]*r[0];
	R_op_r[1][0] = -R[0]*r[1];
	R_op_r[1][1] = R[0]*r[0]+R[2]*r[2];
	R_op_r[1][2] = -R[2]*r[1];
	R_op_r[2][0] = -R[0]*r[2];
	R_op_r[2][1] = -R[1]*r[2];
	R_op_r[2][2] = R[0]*r[0]+R[1]*r[1];
}

void effective_total_inertia()
{
	defined_operation(R_rw, R_PtoC_rw, Rw_op_Rpc_r);
	defined_operation(R_PtoC_rw, R_rw, Rpc_op_Rw_r);
	defined_operation(R_PtoC_rw, R_PtoC_rw, Rpc_op_Rpc_r);        // right wing
	defined_operation(R_lw, R_PtoC_lw, Rw_op_Rpc_l);
	defined_operation(R_PtoC_lw, R_lw, Rpc_op_Rw_l);
	defined_operation(R_PtoC_lw, R_PtoC_lw, Rpc_op_Rpc_l);        // left wing

	// cout<<"Rpc_op_Rpc_r"<<Rpc_op_Rpc_r[0][0]<<Rpc_op_Rpc_l[0][0]<<endl;

	for (int i=0; i<3; i++)
	{
		for (int j=0; j<3; j++) 
			{
				body_inertia_effective[i][j] = body_inertia[i][j]
											 + I_rw[i][j]+I_lw[i][j]
				                             + insect_parameters.Nondimensional_One_Wing_Mass*(Rw_op_Rpc_r[i][j]+Rpc_op_Rw_r[i][j]+Rpc_op_Rpc_r[i][j])
				                             + insect_parameters.Nondimensional_One_Wing_Mass*(Rw_op_Rpc_l[i][j]+Rpc_op_Rw_l[i][j]+Rpc_op_Rpc_l[i][j]);
			}
	}
}

void body_motion_independent_angular_momentum()
{
	double omega_sum_rw[3], omega_sum_lw[3], temp_rw1[3], temp_lw1[3], temp_rw2[3], temp_lw2[3];
	double Rw_plus_Rpc[3], Lw_plus_Rpc[3], temp_rr[3], temp_lr[3], temp_rl[3], temp_ll[3], mass_ratio;
	double body_induce_l[3], body_induce_r[3];
	mass_ratio = insect_parameters.Nondimensional_One_Wing_Mass/insect_parameters.Nondimensional_Total_Mass;
	// cout<<"mass_ratio:  "<<mass_ratio<<endl;

	for (int i=0; i<3; i++)
	{
		omega_sum_rw[i] = omega_rw[i]+omega_rwp[i];
		omega_sum_lw[i] = omega_lw[i]+omega_lwp[i];
		Rw_plus_Rpc[i] = R_rw[i]+R_PtoC_rw[i];
		Lw_plus_Rpc[i] = R_lw[i]+R_PtoC_lw[i];
	}

	cross_product(omega_sum_rw, R_rw, temp_rw1);    // temp_rw1 = (omega_rw + omega_rwp) x R_rw
	cross_product(R_PtoC_rw, temp_rw1, temp_rw2);   // temp_rw2 = R_PtoC_rw x temp_rw1
	cross_product(omega_sum_lw, R_lw, temp_lw1);    // temp_lw1 = (omega_lw + omega_lwp) x R_lw
	cross_product(R_PtoC_lw, temp_lw1, temp_lw2);   // temp_lw2 = R_PtoC_lw x temp_lw1
	cross_product(Rw_plus_Rpc, temp_rw1, temp_rr);  // temp_rr = (R_rw + R_PtoC_rw) x [(omega_rw + omega_rwp) x R_rw]
	cross_product(Lw_plus_Rpc, temp_lw1, temp_ll);  // temp_ll = (R_lw + R_PtoC_lw) x [(omega_lw + omega_lwp) x R_lw]
	cross_product(Rw_plus_Rpc, temp_lw1, temp_rl);  // temp_rl = (R_rw + R_PtoC_rw) x [(omega_lw + omega_lwp) x R_lw]
	cross_product(Lw_plus_Rpc, temp_rw1, temp_lr);  // temp_lr = (R_lw + R_PtoC_lw) x [(omega_rw + omega_rwp) x R_rw]
	cross_product(R_BtoC, temp_rw1, body_induce_r); // body_induce_r = R_BtoC x [(omega_rw + omega_rwp) x R_rw]
	cross_product(R_BtoC, temp_lw1, body_induce_l); // body_induce_l = R_BtoC x [(omega_lw + omega_lwp) x R_lw]
	
	

	for (int i=0; i<3; i++)
	{
		// temp_rr[i] = 0;
		// temp_rl[i] = 0;
		// temp_lr[i] = 0;
		// temp_ll[i] = 0;
		// body_induce_r[i] = 0;
		// body_induce_l[i] = 0;
		
		AM_BMI[i] = I_rw[i][0]*omega_sum_rw[0]+I_rw[i][1]*omega_sum_rw[1]+I_rw[i][2]*omega_sum_rw[2]
				  + I_lw[i][0]*omega_sum_lw[0]+I_lw[i][1]*omega_sum_lw[1]+I_lw[i][2]*omega_sum_lw[2]
				  + insect_parameters.Nondimensional_One_Wing_Mass*temp_rw2[i]
				  + insect_parameters.Nondimensional_One_Wing_Mass*temp_lw2[i]
				  - insect_parameters.Nondimensional_One_Wing_Mass*mass_ratio*(temp_rr[i]+temp_rl[i]+temp_lr[i]+temp_ll[i])
				  - insect_parameters.Nondimensional_Mass*mass_ratio*(body_induce_r[i]+body_induce_l[i]);
	}

}

void body_motion_independent_d_angular_momentum()
{
	double omega_sum_rw[3], omega_sum_lw[3], alpha_sum_rw[3], alpha_sum_lw[3], temp_rw1[3], temp_lw1[3], temp_rw2[3], temp_lw2[3];
	double Rw_plus_Rpc[3], Lw_plus_Rpc[3], temp_rr[3], temp_lr[3], temp_rl[3], temp_ll[3], mass_ratio, I_omega[3], omegaBxIomega[3];
	double body_induce_l[3], body_induce_r[3];
	mass_ratio = insect_parameters.Nondimensional_One_Wing_Mass/insect_parameters.Nondimensional_Total_Mass;

	for (int i=0; i<3; i++)
	{
		omega_sum_rw[i] = omega_rw[i]+omega_rwp[i];
		omega_sum_lw[i] = omega_lw[i]+omega_lwp[i];
		alpha_sum_rw[i] = alpha_rw[i]+alpha_rwp[i];
		alpha_sum_lw[i] = alpha_lw[i]+alpha_lwp[i];
		Rw_plus_Rpc[i] = R_rw[i]+R_PtoC_rw[i];
		Lw_plus_Rpc[i] = R_lw[i]+R_PtoC_lw[i];
	}

	for (int i=0; i<3; i++)
	{
		I_omega[i] = I_rw[i][0]*omega_sum_rw[0]+I_rw[i][1]*omega_sum_rw[1]+I_rw[i][2]*omega_sum_rw[2]
				   + I_lw[i][0]*omega_sum_lw[0]+I_lw[i][1]*omega_sum_lw[1]+I_lw[i][2]*omega_sum_lw[2];
	}
	cross_product(insect.angular_velocity, I_omega, omegaBxIomega);
	
	cross_product(alpha_sum_rw, R_rw, temp_rw1);
	cross_product(R_PtoC_rw, temp_rw1, temp_rw2);
	cross_product(alpha_sum_lw, R_lw, temp_lw1);
	cross_product(R_PtoC_lw, temp_lw1, temp_lw2);
	cross_product(Rw_plus_Rpc, temp_rw1, temp_rr);
	cross_product(Lw_plus_Rpc, temp_lw1, temp_ll);
	cross_product(Rw_plus_Rpc, temp_lw1, temp_rl);
	cross_product(Lw_plus_Rpc, temp_rw1, temp_lr);
	cross_product(R_BtoC, temp_rw1, body_induce_r);
	cross_product(R_BtoC, temp_lw1, body_induce_l);

	for (int i=0; i<3; i++)
	{
		d_AM_BMI[i] = I_rw[i][0]*alpha_sum_rw[0]+I_rw[i][1]*alpha_sum_rw[1]+I_rw[i][2]*alpha_sum_rw[2]
				    + I_lw[i][0]*alpha_sum_lw[0]+I_lw[i][1]*alpha_sum_lw[1]+I_lw[i][2]*alpha_sum_lw[2]
				    + omegaBxIomega[i]
				    + insect_parameters.Nondimensional_One_Wing_Mass*temp_rw2[i]
				    + insect_parameters.Nondimensional_One_Wing_Mass*temp_lw2[i]
				    - insect_parameters.Nondimensional_One_Wing_Mass*mass_ratio*(temp_rr[i]+temp_rl[i]+temp_lr[i]+temp_ll[i])
				    - insect_parameters.Nondimensional_Mass*mass_ratio*(body_induce_r[i]+body_induce_l[i]);
	}

}

void wing_center_UVW_ACCE(int tmp_or_not)
{
	wingplane_1.get_wing_mass_from_subframe(0);
	wingplane_2.get_wing_mass_from_subframe(0);

	switch (tmp_or_not)
	{
		case 1:
			insect.get_wing_mass_from_subframe(0);
			lab.get_wing_mass_from_subframe(0);
			for (int i=0; i<3; i++) 
				{
					RW_center_UVW[i]=lab.wingcenterUVW[i];
					RW_center_ACCE[i]=lab.wingcenterACCE[i];			
				}
				// cout<<"RW_center_UVW is:  "<<RW_center_UVW[2]<<"  "<<RW_center_ACCE[2]<<endl;
		
			insect.get_wing_mass_from_subframe(1);
			lab.get_wing_mass_from_subframe(0);
			for (int i=0; i<3; i++) 
				{
					LW_center_UVW[i]=lab.wingcenterUVW[i];
					LW_center_ACCE[i]=lab.wingcenterACCE[i];			
				}
				// cout<<"LW_center_UVW is:  "<<LW_center_UVW[2]<<"  "<<LW_center_ACCE[2]<<endl;
			break;
		case 2:
			insect.get_wing_mass_from_subframe(0);
			lab.get_wing_mass_from_subframe_tmp(0);
			for (int i=0; i<3; i++) 
				{
					RW_center_UVW[i]=lab.wingcenterUVW[i];
					RW_center_ACCE[i]=lab.wingcenterACCE[i];			
				}
				// cout<<"RW_center_UVW is:  "<<RW_center_UVW[2]<<"  "<<RW_center_ACCE[2]<<endl;
		
			insect.get_wing_mass_from_subframe(1);
			lab.get_wing_mass_from_subframe_tmp(0);
			for (int i=0; i<3; i++) 
				{
					LW_center_UVW[i]=lab.wingcenterUVW[i];
					LW_center_ACCE[i]=lab.wingcenterACCE[i];			
				}
				// cout<<"LW_center_UVW is:  "<<LW_center_UVW[2]<<"  "<<LW_center_ACCE[2]<<endl;
			break;
	}
	
}

void load_wing_mass_module(int tmp_or_not)
{
	total_CoM(tmp_or_not);
	inertia_in_global(tmp_or_not);
	vector_in_global(tmp_or_not);
	effective_total_inertia();
	body_motion_independent_angular_momentum();
	body_motion_independent_d_angular_momentum();
}

void Initialize_Ref_Frame(Ref_Frame* lab,Ref_Frame* insect,Ref_Frame* wingplane_1,Ref_Frame* wingplane_2,Ref_Frame* wing_1,Ref_Frame* wing_2)
{
	lab->level=0;
	insect->level=1;
	wingplane_1->level=2;
	wingplane_2->level=2;
	wing_1->level=3;
	wing_2->level=3;
	
	lab->super=NULL;
	insect->super=lab;
	wingplane_1->super=insect;
	wingplane_2->super=insect;
	wing_1->super=wingplane_1;
	wing_2->super=wingplane_2;
	
	lab->subframe_number=1;
	insect->subframe_number=2;
	wingplane_1->subframe_number=1;
	wingplane_2->subframe_number=1;
	wing_1->subframe_number=0;
	wing_2->subframe_number=0;
	
	lab->sub = new  Ref_Frame* [lab->subframe_number];
	insect->sub = new Ref_Frame* [insect->subframe_number];
	wingplane_1->sub = new Ref_Frame* [wingplane_1->subframe_number];
	wingplane_2->sub = new Ref_Frame* [wingplane_2->subframe_number];
	wing_1->sub = NULL;
	wing_2->sub = NULL;
	lab->sub[0]=insect;
	insect->sub[0]=wingplane_1;
	insect->sub[1]=wingplane_2;
	wingplane_1->sub[0]=wing_1;
	wingplane_2->sub[0]=wing_2;
	
	lab->position[0]=0;lab->position[1]=0;lab->position[2]=0;
	lab->velocity[0]=0;lab->velocity[1]=0;lab->velocity[2]=0;
	lab->angle[0]=0;lab->angle[1]=0;lab->angle[2]=0;
	lab->angular_velocity[0]=0;lab->angular_velocity[1]=0;lab->angular_velocity[2]=0;
	
	insect->position[0]=8;insect->position[1]=8;insect->position[2]=8;  
	insect->velocity[0]=0;insect->velocity[1]=0;insect->velocity[2]=0;
	insect->acceleration[0]=0;insect->acceleration[1]=0;insect->acceleration[2]=0;     //need to be reconsidered
	insect->angle[0]=0;insect->angle[1]=0;insect->angle[2]=0;
	insect->angular_velocity[0]=0;insect->angular_velocity[1]=0;insect->angular_velocity[2]=0;   
	insect->compute_orientation();
	insect->angular_acceleration[0]=0;insect->angular_acceleration[1]=0;insect->angular_acceleration[2]=0;
	
	
	insect_parameters.Insect_Parameters_Initialize();
	insect_parameters.Insect_Parameters_Nondimensionalize();
	
	
	for(int i=0;i<3;i++) {
//		lastbodycentre[i]=8.0;
		CoM[i]=0;  // in flying frame
		Wing_Root[i] = -insect_parameters.Nondimensional_CoM[i];
	}
	
	for(int i=0;i<3;i++) {insect->momentum[i]=0;}
	//input  local Inertia
	for(int i=0;i<3;i++) {
	    for(int j=0;j<3;j++) {
		insect->local_inertia[i][j]=insect_parameters.Nondimensional_MOI[i][j];
	    }
	}
	insect->local_inertia[0][0] -= insect_parameters.Nondimensional_Mass
		* (  insect_parameters.Nondimensional_CoM[1]*insect_parameters.Nondimensional_CoM[1]
		   + insect_parameters.Nondimensional_CoM[2]*insect_parameters.Nondimensional_CoM[2] );
	insect->local_inertia[0][1] -= insect_parameters.Nondimensional_Mass 
	    * ( -insect_parameters.Nondimensional_CoM[0]*insect_parameters.Nondimensional_CoM[1] );
	insect->local_inertia[0][2] -= insect_parameters.Nondimensional_Mass
	    * ( -insect_parameters.Nondimensional_CoM[0]*insect_parameters.Nondimensional_CoM[2] );
	insect->local_inertia[1][0] -= insect_parameters.Nondimensional_Mass
	    * ( -insect_parameters.Nondimensional_CoM[1]*insect_parameters.Nondimensional_CoM[0] );
	insect->local_inertia[1][1] -= insect_parameters.Nondimensional_Mass
	    * (  insect_parameters.Nondimensional_CoM[0]*insect_parameters.Nondimensional_CoM[0]
	       + insect_parameters.Nondimensional_CoM[2]*insect_parameters.Nondimensional_CoM[2] );
	insect->local_inertia[1][2] -= insect_parameters.Nondimensional_Mass
	    * ( -insect_parameters.Nondimensional_CoM[1]*insect_parameters.Nondimensional_CoM[2] );
	insect->local_inertia[2][0] -= insect_parameters.Nondimensional_Mass
	    * ( -insect_parameters.Nondimensional_CoM[2]*insect_parameters.Nondimensional_CoM[0] );
	insect->local_inertia[2][1] -= insect_parameters.Nondimensional_Mass
	    * ( -insect_parameters.Nondimensional_CoM[2]*insect_parameters.Nondimensional_CoM[1] );
	insect->local_inertia[2][2] -= insect_parameters.Nondimensional_Mass
	    * (  insect_parameters.Nondimensional_CoM[1]*insect_parameters.Nondimensional_CoM[1]
	       + insect_parameters.Nondimensional_CoM[0]*insect_parameters.Nondimensional_CoM[0] );
    
	for(int i=0;i<3;i++)
	{
		insect->angular_momentum[i] = 0;
	}

// 	Assign wing motion functions
	my_flap_pattern.Set_Flap_Pattern( insect_parameters.Frequency, Wing_Root, bodycentre );
	
	wingplane_1->compute_x     = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wingplane1_X);
	wingplane_1->compute_y     = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wingplane1_Y);
	wingplane_1->compute_z     = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wingplane1_Z);
	wingplane_1->compute_phi   = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wingplane1_Phi);
	wingplane_1->compute_theta = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wingplane1_Theta);
	wingplane_1->compute_psi   = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wingplane1_Psi);
	
	wingplane_2->compute_x     = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wingplane2_X);
	wingplane_2->compute_y     = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wingplane2_Y);
	wingplane_2->compute_z     = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wingplane2_Z);
	wingplane_2->compute_phi   = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wingplane2_Phi);
	wingplane_2->compute_theta = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wingplane2_Theta);
	wingplane_2->compute_psi   = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wingplane2_Psi);
	
	wing_1->compute_x     	   = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wing1_X);
	wing_1->compute_y     	   = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wing1_Y);
	wing_1->compute_z     	   = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wing1_Z);
	wing_1->compute_phi        = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wing1_Phi);
	wing_1->compute_theta      = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wing1_Theta);
	wing_1->compute_psi        = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wing1_Psi);
	
	wing_2->compute_x          = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wing2_X);
	wing_2->compute_y          = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wing2_Y);
	wing_2->compute_z          = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wing2_Z);
	wing_2->compute_phi        = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wing2_Phi);
	wing_2->compute_theta      = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wing2_Theta);
	wing_2->compute_psi        = AttitudeEvaluator( &my_flap_pattern, &FlapPattern :: wing2_Psi);

	wing_1->wingcenter[0] = insect_parameters.Nondimensional_RW_Center[0];
	wing_1->wingcenter[1] = insect_parameters.Nondimensional_RW_Center[1];
	wing_1->wingcenter[2] = insect_parameters.Nondimensional_RW_Center[2];
	wing_2->wingcenter[0] = insect_parameters.Nondimensional_LW_Center[0];
	wing_2->wingcenter[1] = insect_parameters.Nondimensional_LW_Center[1];
	wing_2->wingcenter[2] = insect_parameters.Nondimensional_LW_Center[2];

	wing_1->wingcenterUVW[0] = 0;
	wing_1->wingcenterUVW[1] = 0;
	wing_1->wingcenterUVW[2] = 0;
	wing_2->wingcenterUVW[0] = 0;
	wing_2->wingcenterUVW[1] = 0;
	wing_2->wingcenterUVW[2] = 0;

	wing_1->wingcenterACCE[0] = 0;
	wing_1->wingcenterACCE[1] = 0;
	wing_1->wingcenterACCE[2] = 0;
	wing_2->wingcenterACCE[0] = 0;
	wing_2->wingcenterACCE[1] = 0;
	wing_2->wingcenterACCE[2] = 0;

	for (int i=0; i<3; i++)
	{
		for (int j=0; j<3; j++) 
			{
				wing_1->wing_inertia[i][j] = insect_parameters.Nondimensional_RW_MOI[i][j];
				wing_2->wing_inertia[i][j] = insect_parameters.Nondimensional_LW_MOI[i][j];
			}
    }

    wingplane_1->Time=0;
    wingplane_2->Time=0;
    wing_1->Time=0;
    wing_2->Time=0;
	
	wingplane_1->update_all();
	wingplane_2->update_all();
	wing_1->update_all();
	wing_2->update_all();

	
	lab->obj_number=3;
	insect->obj_number=3;
	wingplane_1->obj_number=1;
	wingplane_2->obj_number=1;
	wing_1->obj_number=1;
	wing_2->obj_number=1;
	
	lab->rigid_body=new Rigid_Obj* [lab->obj_number];
	insect->rigid_body=new Rigid_Obj* [insect->obj_number];
	wingplane_1->rigid_body=new Rigid_Obj* [wingplane_1->obj_number];
	wingplane_2->rigid_body=new Rigid_Obj* [wingplane_2->obj_number];
	wing_1->rigid_body=new Rigid_Obj* [wing_1->obj_number];
	wing_2->rigid_body=new Rigid_Obj* [wing_2->obj_number];
	
	srand((unsigned)time(0)); 
	wing_1->rigid_body[0]=wing_1->get_rigid_body_from_file("../mesh/Hawkmoth-Wing1.dat");
	wing_2->rigid_body[0]=wing_2->get_rigid_body_from_file("../mesh/Hawkmoth-Wing2.dat");
		
	wingplane_1->rigid_body[0]=wingplane_1->get_rigid_body_from_subframe(0,0);
	wingplane_2->rigid_body[0]=wingplane_2->get_rigid_body_from_subframe(0,0);
	
	insect->rigid_body[0]=insect->get_rigid_body_from_file("../mesh/Hawkmoth-Body.dat");
	// insect->rigid_body[0]=insect->get_rigid_body_from_file("../mesh/new-Hawkmoth-rotatebody-cloudthick0.05-mesh0.02.dat");
	for (int i=0; i<insect->rigid_body[0]->POINT_NUMBER; i++) {
		insect->rigid_body[0]->XYZ[0][i] -= insect_parameters.Nondimensional_CoM[0];
		insect->rigid_body[0]->XYZ[1][i] -= insect_parameters.Nondimensional_CoM[1];
		insect->rigid_body[0]->XYZ[2][i] -= insect_parameters.Nondimensional_CoM[2];
	}
	insect->rigid_body[1]=insect->get_rigid_body_from_subframe(0,0);
	insect->rigid_body[2]=insect->get_rigid_body_from_subframe(1,0);
	
	lab->rigid_body[0]=lab->get_rigid_body_from_subframe(0,0);
	lab->rigid_body[1]=lab->get_rigid_body_from_subframe(0,1);
	lab->rigid_body[2]=lab->get_rigid_body_from_subframe(0,2);
	
	load_wing_mass_module(1);

	Point_Meshless=0;
	for(int i=0;i<lab->obj_number;i++) {
		Point_Meshless += lab->rigid_body[i]->POINT_NUMBER;
	}
	
	Re=insect_parameters.Re;
}

void Flapping(double mtime, int it, int fsi_iter, Ref_Frame* lab, Ref_Frame* insect,Ref_Frame* wingplane_1,Ref_Frame* wingplane_2, Ref_Frame* wing_1, Ref_Frame* wing_2)
{
    if (fsi_iter == 1) {
        // Update wing flap pattern in the first FSI iteration only
        my_flap_pattern.Update_Flap_Pattern(mtime, it, bodycentre, insect->angle, Car_Ale, insect->angular_velocity);
    }
	
	wingplane_1->Time=Time;
	wingplane_2->Time=Time;
	wing_1->Time=Time;
	wing_2->Time=Time;
	
	wingplane_1->update_all();
	wingplane_2->update_all();
	wing_1->update_all();
	wing_2->update_all();
	
	//insect->position[0]=8;insect->position[1]=8;insect->position[2]=8;
	insect->velocity[0]=0;insect->velocity[1]=0;insect->velocity[2]=0;
	insect->acceleration[0]=0;insect->acceleration[1]=0;insect->acceleration[2]=0;
	
	wingplane_1->change_ref_frame();
	wingplane_2->change_ref_frame();
	insect->change_ref_frame();
	lab->change_ref_frame();		
}

void Check_If_Open(ifstream &File, char Statement[])
{
	if(!File.is_open())
	{
		cout<<Statement<<" unable to open!";
		exit(0);
	}
}

void Set_Para()
{
    LPlot=0;
    strcpy(Name, "default");
    XL=1;
    YL=1;
    ZL=1;
	IU=0;
	IV=1;
	IW=2;
	IP=3;
    IPoint=10;
    JPoint=10;
    KPoint=10;
    Point_Meshless=0;
	Time_Step=1;
	Time_Current=0;
	Time=0;
	Dt=1e-3;
	Res_PP = 1e-10;
    Max_N_PP = 10;
	ALE=1;
	PM_Iter=4;
	Fp=0.5;
}

void User(const char auxp[])
{
	LPlot=1;
	
	//Auxiliary input
	ifstream glo;
	glo.open(auxp);
	Check_If_Open(glo, "Auxiliary Input file");
	glo >> RECORD;
	glo >> Solver_Choice;
	glo >> TestMode;
	
	glo >> Dt >> Time_Step >> Time_Begin_Save >> Time_Save_Freq >> Time_Plain_Test;

	glo>>CheckPoint_Freq>>Car_Save_Freq>>Square_Save_Freq>>Meshless_Save_Freq>>Body_Save_Freq;

	glo>>ALE>>AD>>CSVD>>CPM>>CLD>>COP;//Calculate SVD, PM, Cal_ClCd, Ouput

	glo>>NSS;//Node Selection Scheme
	
	glo>>GFactor>>Test_Depth>>Eff_Rad>>Drawer_Member;
	glo>>PM_Iter>>Max_N_PP>>Fp;

	glo>>LRead>>Test>>LWrite>>LPlot>>output_redir;

	glo>>KinematicsPattern;
	//General Kinematic Parameters for Symmetrical flapping - Dickinson's Model
	glo>>DKS_Tr>>StrokeAngle>>PitchAngle;
	glo>>Aux_Phi0>>Aux_Theta0>>Aux_Psi0;
	glo>>GlobalInt;
	glo>>FSI_Eps_Position>>FSI_Eps_Angle>>FSI_Eps_Momentum>>FSI_Eps_Angular_Momentum;
	glo.close();


	readp>>IPoint>>JPoint>>KPoint;
	

	IJK_Pressure_Ref=KPoint*JPoint*2+2*JPoint+5;
//	Ch_Num=Re;
	//cout<<"Re="<<Re<<endl;  
	cout<<"Cartesian Grid: "<<IPoint<<"x"<<JPoint<<"x"<<KPoint<<endl;
}

bool Init(char chpname[], Ref_Frame* lab,Ref_Frame* insect,Ref_Frame* wingplane_1,Ref_Frame* wingplane_2,Ref_Frame* wing_1,Ref_Frame* wing_2)
{
    int ijk;
    bool chp_flag = 0;
    
	double chp_Re, chp_Freq;
	int chp_I, chp_J, chp_K, chp_All, chp_Meshless, chp_Iter, chp_Time;
	char *chpDataFile = new char [64];
	char *chpInfoFile = new char [64];
	
	ifstream chpread;
	chpread.open(chpname);
	if(chpread.is_open())//If there is any checkpoint
	{
		chpread>>chp_Iter>>chp_Time>>chp_Re>>chp_Freq;
		chpread>>chp_I>>chp_J>>chp_K>>chp_All>>chp_Meshless;
		chpread>>chpDataFile;
		chpread>>chpInfoFile;
		
		if(fabs(chp_Freq - insect_parameters.Frequency) > 1.0e-10)
		{
			cout<<"Check point FREQUENCY does not match current case. Job terminated."<<endl;
			exit(0);
		}
		else if(chp_I != IPoint || chp_J != JPoint || chp_K != KPoint || chp_All != Point_All || chp_Meshless != Point_Meshless)
		{
			cout<<"Check point MESH does not match current case. Job terminated."<<endl;
			exit(0);
		}
		else
		{
			Time_Current = chp_Iter;
			Time = chp_Time;
			chp_flag = 1;
		}
	}
	else
	{
		chp_flag = 0;
	}
	chpread.close();
	
    //Generate Grid
    Grid();
	
	if(chp_flag)
	{
		int err = Read_Check_Point(chpDataFile, chpInfoFile);
		if(err)
		{
			cout<<"Check point file unable to open!"<<endl;
			exit(0);
		}
		
		Re = chp_Re;
		
		wingplane_1->Time=Time;
		wingplane_2->Time=Time;
		wing_1->Time=Time;
		wing_2->Time=Time;
		
		wingplane_1->update_all();
		wingplane_2->update_all();
		wing_1->update_all();
		wing_2->update_all();
		
		//insect->position[0]=8;insect->position[1]=8;insect->position[2]=8;
		insect->velocity[0]=0;insect->velocity[1]=0;insect->velocity[2]=0;
		insect->acceleration[0]=0;insect->acceleration[1]=0;insect->acceleration[2]=0;
	
		wingplane_1->change_ref_frame();
		wingplane_2->change_ref_frame();
		insect->change_ref_frame();
		lab->change_ref_frame();
		
    	Get_Meshless();
    	
        #pragma omp parallel for
        for(ijk=0;ijk<Point_All;ijk++)
        {
            Ustar[ijk]=0;
			Vstar[ijk]=0;
			Wstar[ijk]=0;
			U_Old[ijk]=U[ijk];
            V_Old[ijk]=V[ijk];
            W_Old[ijk]=W[ijk];
            P_Old[ijk]=P[ijk];
        }
	    #pragma omp parallel for
	    for(ijk=0;ijk<Point_Car;ijk++)
	    {
		    GType1[ijk]=GType2[ijk];
		    if (GType2[ijk]==4) GType1[ijk]=3;
		    //if (GType2[ijk]!=2) GType2[ijk]=1;
	    }
	    
		if(LPlot) 
        {
            cout<<"The calculation was restarted from the ";
            cout<<Time_Current<<"th time step, the current time is: "<<Time<<endl;
        }
        if(LWrite) 
        {
            Info<<"The calculation was restarted from the ";
            Info<<Time_Current<<"th time step, the current time is: "<<Time<<endl;
        }
	}
    else
	{
		if(LPlot) 
        {
            cout<<"New Simulation "<<endl;
			cout<<"Generate Mesh..."<<endl;
        }
		if(LWrite) 
        {
            Info<<"New Simulation "<<endl;
			Info<<"Generate Mesh..."<<endl;
        }
        
		Get_Meshless();
		
		//Initial flow field
		if(LPlot) cout<<"Initial Flow field..";
		#pragma omp parallel for
        for(ijk=0;ijk<Point_All;ijk++)
        {
            U[ijk]=0;
            V[ijk]=0;
            W[ijk]=0;
            P[ijk]=0;
            Ustar[ijk]=0;
			Vstar[ijk]=0;
			Wstar[ijk]=0;
			U_Old[ijk]=0;
            V_Old[ijk]=0;
            W_Old[ijk]=0;
            P_Old[ijk]=0;
        }		
		if(ALE==1)
		{
            #pragma omp parallel for
			for(ijk=0;ijk<Point_Meshless;ijk++)
		    {        
				X_Meshless_Old[ijk]=X_Meshless[ijk];
				Y_Meshless_Old[ijk]=Y_Meshless[ijk];
				Z_Meshless_Old[ijk]=Z_Meshless[ijk];
			    U_Ale_Old[ijk]=U_Ale[ijk];
		        V_Ale_Old[ijk]=V_Ale[ijk];
		        W_Ale_Old[ijk]=W_Ale[ijk];		    
			}
		}
		
		if(LPlot) cout<<".Done!"<<endl;		
		
        //Output(Test);
        Test=0;
        Time_Current=0;	
	}
	
	delete chpDataFile;
	delete chpInfoFile;
	
	return chp_flag;
}

void Motion(double Time, int it, int fsi_iter)
{
	Flapping(Time, it, fsi_iter, &lab, &insect, &wingplane_1, &wingplane_2, &wing_1, &wing_2);
	
	for(int i=0;i<lab.obj_number;i++)
	{
		for(int j=0;j<lab.rigid_body[i]->POINT_NUMBER;j++)
		{
			
			MNx[j+Rigid_Offset[i]]=lab.rigid_body[i]->OUTER_NORMAL_VECTOR[0][j];
			MNy[j+Rigid_Offset[i]]=lab.rigid_body[i]->OUTER_NORMAL_VECTOR[1][j];
			MNz[j+Rigid_Offset[i]]=lab.rigid_body[i]->OUTER_NORMAL_VECTOR[2][j];
			
			X_Meshless[j+Rigid_Offset[i]]=lab.rigid_body[i]->XYZ[0][j];
			Y_Meshless[j+Rigid_Offset[i]]=lab.rigid_body[i]->XYZ[1][j];
			Z_Meshless[j+Rigid_Offset[i]]=lab.rigid_body[i]->XYZ[2][j];

			U_Ale[j+Rigid_Offset[i]]=lab.rigid_body[i]->UVW[0][j];
			V_Ale[j+Rigid_Offset[i]]=lab.rigid_body[i]->UVW[1][j];
			W_Ale[j+Rigid_Offset[i]]=lab.rigid_body[i]->UVW[2][j];
			
			ACCX_Ale[j+Rigid_Offset[i]]=lab.rigid_body[i]->ACC[0][j];
			ACCY_Ale[j+Rigid_Offset[i]]=lab.rigid_body[i]->ACC[1][j];
			ACCZ_Ale[j+Rigid_Offset[i]]=lab.rigid_body[i]->ACC[2][j];
		}
	}
}

void Cal_CdCl()
{
	int i,j,ijk,ip,io;
	double Fpx,Fpy,Fpz,Fvx,Fvy,Fvz;
	double tmpx,tmpy,tmpz,tmpvx,tmpvy,tmpvz,tmppx,tmppy,tmppz;
	double dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz;
	
	Fpx=0;
	Fpy=0;
	Fpz=0;
	Fvx=0;
	Fvy=0;
	Fvz=0;

	insect.power=0;
	for(i=0;i<3;i++)
	{
		insect.force[i]=0; insect.torque[i]=0;
		bodyforce[i]=0; bodytorque[i]=0;
		wing1force[i]=0; wing1torque[i]=0;
		wing2force[i]=0; wing2torque[i]=0;
	}

	for(i=0;i<Total_Body_Nodes;i++)
	{

		ijk=BNI[i];
		ip=ijk-Point_Car;

		dudx=0;
		dudy=0;
		dudz=0;
		dvdx=0;
		dvdy=0;
		dvdz=0;
		dwdx=0;
		dwdy=0;
		dwdz=0;
	 
		for(io=0;io<NB;io++)
		{
			j=Cld_List[ip].Nb_Points[io];
			dudx=dudx+Cld_List[ip].Csvd[0][io]*(U[j]-U[ijk]);
			dvdx=dvdx+Cld_List[ip].Csvd[0][io]*(V[j]-V[ijk]);
			dwdx=dwdx+Cld_List[ip].Csvd[0][io]*(W[j]-W[ijk]);

			dudy=dudy+Cld_List[ip].Csvd[1][io]*(U[j]-U[ijk]);
			dvdy=dvdy+Cld_List[ip].Csvd[1][io]*(V[j]-V[ijk]);
			dwdy=dwdy+Cld_List[ip].Csvd[1][io]*(W[j]-W[ijk]);

			dudz=dudz+Cld_List[ip].Csvd[2][io]*(U[j]-U[ijk]);
			dvdz=dvdz+Cld_List[ip].Csvd[2][io]*(V[j]-V[ijk]);
			dwdz=dwdz+Cld_List[ip].Csvd[2][io]*(W[j]-W[ijk]);
		}

		tmppx=-P[ijk]*MNx[ip]*Surf_Ele_Area[i];
		tmppy=-P[ijk]*MNy[ip]*Surf_Ele_Area[i];
		tmppz=-P[ijk]*MNz[ip]*Surf_Ele_Area[i];
		Fpx=tmppx+Fpx;
		Fpy=tmppy+Fpy;
		Fpz=tmppz+Fpz;
		tmpvx=((2.0*dudx/Re)*MNx[ip]
			+(dudy+dvdx)*MNy[ip]/Re
			+(dwdx+dudz)*MNz[ip]/Re)*Surf_Ele_Area[i];
		tmpvy=((2.0*dvdy/Re)*MNy[ip]
			+(dudy+dvdx)*MNx[ip]/Re
			+(dwdy+dvdz)*MNz[ip]/Re)*Surf_Ele_Area[i];
		tmpvz=((2.0*dwdz/Re)*MNz[ip]
			+(dudz+dwdx)*MNx[ip]/Re
			+(dvdz+dwdy)*MNy[ip]/Re)*Surf_Ele_Area[i];
		Fvx=tmpvx+Fvx;
		Fvy=tmpvy+Fvy;
		Fvz=tmpvz+Fvz;
		
		tmpx=( (-P[ijk]+2.0*dudx/Re)*MNx[ip]
			   +(dudy+dvdx)*MNy[ip]/Re
			   +(dwdx+dudz)*MNz[ip]/Re )*Surf_Ele_Area[i];
		tmpy=( (-P[ijk]+2.0*dvdy/Re)*MNy[ip]
			   +(dudy+dvdx)*MNx[ip]/Re
			   +(dwdy+dvdz)*MNz[ip]/Re )*Surf_Ele_Area[i];
		tmpz=( (-P[ijk]+2.0*dwdz/Re)*MNz[ip]
			   +(dudz+dwdx)*MNx[ip]/Re
			   +(dvdz+dwdy)*MNy[ip]/Re )*Surf_Ele_Area[i];

		insect.force[0]=insect.force[0]+tmpx;
		// insect.force[0]=0;
		insect.force[1]=insect.force[1]+tmpy;
		// insect.force[1]=0;
		insect.force[2]=insect.force[2]+tmpz;
		// insect.force[2]=insect_parameters.Nondimensional_Total_Mass*insect_parameters.Nondimensional_Gravity;
		insect.torque[0]=insect.torque[0]+tmpz*(Y[ijk]-Total_Center_G[1])-tmpy*(Z[ijk]-Total_Center_G[2]);
		// insect.torque[0]=0;
		insect.torque[1]=insect.torque[1]+(tmpx*(Z[ijk]-Total_Center_G[2])-tmpz*(X[ijk]-Total_Center_G[0]));
		// insect.torque[1]=0;
		insect.torque[2]=insect.torque[2]+(tmpy*(X[ijk]-Total_Center_G[0])-tmpx*(Y[ijk]-Total_Center_G[1]));
		// insect.torque[2]=0;
		insect.power=insect.power+tmpx*U[ijk]+tmpy*V[ijk]+tmpz*W[ijk];
		
		// Compute force of each part
		if(Body_Offset[0]<=i && i<Body_Offset[1])
		{
			bodyforce[0]=bodyforce[0]+tmpx;
			bodyforce[1]=bodyforce[1]+tmpy;
			bodyforce[2]=bodyforce[2]+tmpz;
			bodytorque[0]=bodytorque[0]+tmpz*(Y[ijk]-Total_Center_G[1])-tmpy*(Z[ijk]-Total_Center_G[2]);
			bodytorque[1]=bodytorque[1]+tmpx*(Z[ijk]-Total_Center_G[2])-tmpz*(X[ijk]-Total_Center_G[0]);
			bodytorque[2]=bodytorque[2]+tmpy*(X[ijk]-Total_Center_G[0])-tmpx*(Y[ijk]-Total_Center_G[1]);
		}
		else if(Body_Offset[1]<=i && i<Body_Offset[2])
		{
			wing1force[0]=wing1force[0]+tmpx;
			wing1force[1]=wing1force[1]+tmpy;
			wing1force[2]=wing1force[2]+tmpz;
			wing1torque[0]=wing1torque[0]+tmpz*(Y[ijk]-Total_Center_G[1])-tmpy*(Z[ijk]-Total_Center_G[2]);
			wing1torque[1]=wing1torque[1]+tmpx*(Z[ijk]-Total_Center_G[2])-tmpz*(X[ijk]-Total_Center_G[0]);
			wing1torque[2]=wing1torque[2]+tmpy*(X[ijk]-Total_Center_G[0])-tmpx*(Y[ijk]-Total_Center_G[1]);
		}
		else if(Body_Offset[2]<=i && i<Total_Body_Nodes)
		{
			wing2force[0]=wing2force[0]+tmpx;
			wing2force[1]=wing2force[1]+tmpy;
			wing2force[2]=wing2force[2]+tmpz;
			wing2torque[0]=wing2torque[0]+tmpz*(Y[ijk]-Total_Center_G[1])-tmpy*(Z[ijk]-Total_Center_G[2]);
			wing2torque[1]=wing2torque[1]+tmpx*(Z[ijk]-Total_Center_G[2])-tmpz*(X[ijk]-Total_Center_G[0]);
			wing2torque[2]=wing2torque[2]+tmpy*(X[ijk]-Total_Center_G[0])-tmpx*(Y[ijk]-Total_Center_G[1]);
		}
		else
		{
			cout<<"*****Ghost point exist in Cal_CdCl (i="<<i<<" Max="<< Total_Body_Nodes <<")*****"<<endl;
		}
	}
	insect.power=insect.power*insect_parameters.AirDensity*pow(insect_parameters.Dimensional_WingLength,5)*pow(insect_parameters.Frequency,3);
	//     insect.power=insect.power*1.25*pow(2.39*1e-3,5)*pow(Frequency,3);
	insect.energy=insect.energy+insect.power*Dt/insect_parameters.Frequency;
	//    insect.energy=insect.energy+insect.power*Dt/Frequency;

	insect.force[2]=insect.force[2]-insect_parameters.Nondimensional_Total_Mass*insect_parameters.Nondimensional_Gravity;

/*
	CoM_Global[0]=insect.orientation[0][0]*CoM[0]+insect.orientation[0][1]*CoM[1]+insect.orientation[0][2]*CoM[2];
	CoM_Global[1]=insect.orientation[1][0]*CoM[0]+insect.orientation[1][1]*CoM[1]+insect.orientation[1][2]*CoM[2];
	CoM_Global[2]=insect.orientation[2][0]*CoM[0]+insect.orientation[2][1]*CoM[1]+insect.orientation[2][2]*CoM[2];
*/
	//insect.torque[0]=insect.torque[0]-insect_parameters.Nondimensional_Mass*insect_parameters.Nondimensional_Gravity*CoM_Global[1];
	//insect.torque[1]=insect.torque[1]+insect_parameters.Nondimensional_Mass*insect_parameters.Nondimensional_Gravity*CoM_Global[0];
}

unsigned checkCPU(const unsigned set)
{
    unsigned used = set;
    
    if (used == 0)
    {
		cout<<"Please provide no. of cpu"<<endl;
		used = 1;
	}
	
    unsigned sys = omp_get_num_procs();
    cout << "There are " << sys << " CPUs available.\t"
         << "User pre-set CPU number is: " << set << endl;
    if (used>sys)
    {
		cout << "Reset CPU number to: " <<sys;
		used=sys;
    }
    else
    {
		cout << "You can use up to " << sys << " CPUs next time!";
    }
    
    cout << "\tUsed procs:" << used << endl;
    
    return used;
}

void updatePrescribedStructureValues(Ref_Frame& insect, const double mass, const double Time)
{     
    double omega = 1.0/1.0;   // T = 1/omega;
	double final_vel = -0.05;
	
	for(int i=0;i<3;i++) {
	    insect.position[i] = insect.position_old[i];
		insect.velocity[i] = 0.0;
		insect.acceleration[i] = 0.0;
	}
	
	if (Time < 1.0/omega/4.0) {
	    insect.velocity[0] = final_vel*sin(2*PI*omega*Time);
	    insect.acceleration[0] = 2*PI*omega*final_vel*cos(2*PI*omega*Time);
	}
	else {
	    insect.velocity[0] = final_vel;
	    insect.acceleration[0] = 0.0;
	}
    
	for(int i=0;i<3;i++)
	{
		insect.momentum[i] = insect.velocity[i]*mass;
	}
    
    for(int i=0;i<3;i++) {
	    insect.angle[i] = insect.angle_old[i];
	    insect.angular_momentum[i] = 0.0;
	    insect.angular_velocity[i] = 0.0;
	    insect.angular_acceleration[i] = 0.0;
    }
}

int main(int argc, char *argv[])
{
	//argv[0] = NULL, argv[1] = cart, argv[2] = auxp, argv[3] = chp, argv[4] = No_Cpu, argv[5] = f
    clock_t cpu_time_begin, cpu_time_end;
    time_t  loc_time_begin, loc_time_end;
    Timer timer_01;
    char *sptime;
    
    int it, ijk, io;  
    readp.open(argv[1]); 
    Check_If_Open(readp, "Cartesian Grid Input file");
    No_Cpu = atoi(argv[4]);
    Processor=argv[5];
    
    No_Cpu = checkCPU(No_Cpu);
    omp_set_num_threads(No_Cpu);
    
    cpu_time_begin=clock();
    loc_time_begin=time(NULL);
    
    Set_Para();
    User(argv[2]);

    Initialize_Ref_Frame(&lab,&insect,&wingplane_1,&wingplane_2,&wing_1,&wing_2);
    mallocGlobal();
	
    sptime=ctime(&loc_time_begin);
    sptime[strlen(sptime)-1]=0;
    if(LWrite)
    {
        strcpy(Open_Name, Name);
        strcat(Open_Name, "_ifo.dat");
        Info.open(Open_Name);
        Info<<"The Program Begin at: "<<sptime<<endl;
    }
    if(LPlot)
    {
        cout<<"The Program Begin at: "<<sptime<<endl;
        if(Solver_Choice==1) /* BICGSTAB */ cout<<"BICGSTAB Solver"<<endl;
        if(Solver_Choice==2) /* GMRES */ cout<<"GMRES Solver"<<endl;
        if(Solver_Choice==3) /* BICGSTAB+GMRES */ cout<<"Mixed of BICGSTAB and GMRES Solver"<<endl;
    }
	
    bool is_new = Init(argv[3], &lab, &insect, &wingplane_1, &wingplane_2, &wing_1, &wing_2);
	if (Processor == "GPU") Initialize_GPU();
	
    timer_01.Start(); cout<<"Updating Meshless Points Information ... "<<endl;
    updateMeshSystem(lab, 1, Processor, is_new);
    timer_01.Stop(); cout<<"  Finished!  Time Used for updateMeshSystem is: "<<timer_01.Elapsed()<<" ms"<<endl;
    
#   pragma omp parallel for
    for (ijk=0; ijk<Point_All; ijk++) GType1[ijk] = GType2[ijk];
        
	calBoundaryUVW();
    
    Time_Start=Time_Current+1;
    Time_End=Time_Start+Time_Step;

    total_CoM(1);
    Cal_CdCl();

	if(LWrite) 
	{
		strcpy(Open_Name, Name);
		strcat(Open_Name, "_");
		sprintf(number,"%d", int(Ch_Num));
		strcat(Open_Name, number);
		strcat(Open_Name, "_resd.dat");

		Conv.open(Open_Name);
	}

    if(LPlot) cout<<"Projection Method Begins at "<<sptime<<endl;
	
    outforce.open("force-record.dat");
    outpv.open("pv-record.dat");
    trajectory.open("wingtip-trajectory.dat"); int wingtipindex=23884;
	ofstream outcontroller("controller-record.dat");
	ofstream outpartforce("part_force-record.dat");
	// ofstream outMOI("wingMOI.dat");
	// ofstream outVec("VecinB.dat");
	// ofstream outBMI("BMI.dat");

    for(int s=0;s<Point_Meshless;s++)
    {
	if(s==wingtipindex)
	{
	    showindex[s]=1;
	}
	else
	{
	    showindex[s]=0;
	}
    }

	int PM_IterT;

	for(it=Time_Start; it<Time_End; it++)
    {
	    //update time	
		Time=Time+Dt;
		cout<<'\n'<<"Calculation at time Step="<<it<<" Time="<<Time<<endl;
		cout<<"Current flapping frequency is "<<my_flap_pattern.freq<<" ."<<endl;
		cout<<"Current Reynolds number is "<<Re<<" ."<<endl;
		
	    #pragma omp parallel for
	    for(ijk=0;ijk<Point_Meshless;ijk++)
	    {

		    X_Meshless_Old[ijk]=X_Meshless[ijk];
		    Y_Meshless_Old[ijk]=Y_Meshless[ijk];
		    Z_Meshless_Old[ijk]=Z_Meshless[ijk];
		    U_Ale_Old[ijk]=U_Ale[ijk];
		    V_Ale_Old[ijk]=V_Ale[ijk];
		    W_Ale_Old[ijk]=W_Ale[ijk];
		    ACCX_Ale_Old[ijk]=ACCX_Ale[ijk];
		    ACCY_Ale_Old[ijk]=ACCY_Ale[ijk];
		    ACCZ_Ale_Old[ijk]=ACCZ_Ale[ijk];
	    }
	    #pragma omp parallel for
	    for(ijk=0;ijk<Point_Car;ijk++)
	    {
		    GType1[ijk]=GType2[ijk];
		    if (GType2[ijk]==4) GType1[ijk]=3;
		    //if (GType2[ijk]!=2) GType2[ijk]=1;
	    }
	    
	    insect.copy_new2old();
        for(io=0;io<3;io++) 
        {
            Car_Ale_Old[io]=Car_Ale[io];
            bodycentre_old[io]=bodycentre[io];
            Car_ACC_Old[io]=Car_ACC[io];
        }
        
	    computeSourceContributionOld();
	    
		Initial_Guess();
		
		FSI_Iter=0;
		PM_IterT = PM_Iter;

		do
		{
			FSI=1;
			FSI_Iter++;

			if(LPlot) cout<<" The "<<FSI_Iter<<"th FSI Iteration :"<<endl;
			
			// updatePrescribedStructureValues(insect, insect_parameters.Nondimensional_Mass, Time);

			for(io=0;io<3;io++)
			{
			    Car_ACC[io]=insect.acceleration[io];
			    Car_Ale[io]=insect.velocity[io];
			    //bodycentre_tmp[io]=bodycentre[io];
			    bodycentre[io]=bodycentre_old[io]+Dt*Car_Ale[io];
			}

			Motion(Time, it, FSI_Iter);
			
			if (FSI_Iter < 6)
			{
				timer_01.Start();
				updateMeshSystem(lab, FSI_Iter, Processor);
				timer_01.Stop(); cout<<"  Time Used for updateMeshSystem is: "<<timer_01.Elapsed()<<" ms"<<endl;
			}
		    
		    if(LPlot)
		    {
		    cout<<" Residual History U,         V,        W,        P"<<endl;
		    }
		    
            //Fractional Step/Projection Method
            if (Processor == "GPU")
            {
                doProjectionMethod_GPU(it, PM_IterT);
            }
            else
            {
                doProjectionMethod(it, PM_IterT);
            }
			
			Cal_CdCl();
		
			Solve_Structure_Problem();
			
			for(io=0;io<3;io++)
			{
			    Car_Ale[io]=insect.velocity_tmp[io];
			    bodycentre_tmp[io]=bodycentre_old[io]+Dt*Car_Ale[io];
			}

			//add function which compute the relaxation factor   Compute_Relaxation_Factor(FSI_Iter); 
			RF[0]=1.0;RF[1]=1.0;RF[2]=1.0;RF[3]=1.0;
//			cout<<"Relaxation Factor are: "<<RF[0]<<"   "<<RF[1]<<"   "<<RF[2]<<"   "<<RF[3]<<endl;
			
			if(FSI==1) Update_Structrue_value();
			
			FSI=Check_FSI_REL();			
			if(FSI_Iter==FSI_Max) FSI=0;
			
			PM_IterT = PM_IterT-1;
			if (PM_IterT < 4)
			{
				PM_IterT = 3;
			}
		} while (FSI==1);
		
	    //C2M_Object->Del_OldPoint();
	    // Fresh_Node();
#       pragma omp parallel for
	    for (ijk=0; ijk<Point_All; ijk++)
            for (int iv=0; iv<NF_MAX; iv++) 
                F2[iv][ijk] = F0[iv][ijk]; 
	    
	    
		if(LPlot)
		{
		    cout<<"---------------------------------------------"<<endl;
			cout<<"The real position is:  "<<bodycentre[0]<<"  "<<bodycentre[1]<<"  "<<bodycentre[2]<<"  "<<endl;
			cout<<"The real velocity is:  "<<Car_Ale[0]<<"  "<<Car_Ale[1]<<"  "<<Car_Ale[2]<<endl;
			cout<<"The real acceleration is:  "<<Car_ACC[0]<<"  "<<Car_ACC[1]<<"  "<<Car_ACC[2]<<endl;
			cout<<"---------------------------------------------"<<endl;
			cout<<"Current flapping frequency is "<<insect_parameters.Frequency<<" ."<<endl;
			cout<<"Current Reynolds number is "<<Re<<" ."<<endl;
			cout<<"Try to modify Phi from "<<my_flap_pattern.Angle_Previous_Phi<<" to "
				<<my_flap_pattern.Angle_Next_Phi<<". Now the angle is "<<wingplane_1.angle[0]<<" ."<<endl;
			cout<<"Try to modify Theta from "<<my_flap_pattern.Angle_Previous_Theta<<" to "
				<<my_flap_pattern.Angle_Next_Theta<<". Now the anlge is "<<wingplane_1.angle[1]<<" ."<<endl;
			cout<<"Try to modify Psi from "<<my_flap_pattern.Angle_Previous_Psi<<" to "
				<<my_flap_pattern.Angle_Next_Psi<<". Now the anlge is "<<wingplane_1.angle[2]<<" ."<<endl;
			cout<<"---------------------------------------------"<<endl;
			
			cout<<" Insect Force:                Fx=      "
			<<insect.force[0]<<" Fy=      "<<insect.force[1]<<" Fz=      "<<insect.force[2]<<endl;
			cout<<" Insect Torque:               Tx=      "
			<<insect.torque[0]<<" Ty=      "<<insect.torque[1]<<" Tz=      "<<insect.torque[2]<<endl;
			cout<<" Insect Position:             Xobj_Cen="
			<<insect.position[0]<<" Yobj_Cen="<<insect.position[1]<<" Zobj_Cen="<<insect.position[2]<<endl;
			cout<<" Insect Angle:                Phi="
				<<insect.angle[0]<<" Theta="<<insect.angle[1]<<" Psi="<<insect.angle[2]<<endl;
			cout<<" Insect Translation Velocity: Uobj=    "
			<<insect.velocity[0]<<" Vobj=    "<<insect.velocity[1]<<" Wobj=    "<<insect.velocity[2]<<endl;
			cout<<" Insect Rotation Velocity:    Omg_x=   "
			<<insect.angular_velocity[0]<<" Omg_y=   "<<insect.angular_velocity[1]<<" Omg_z=   "<<insect.angular_velocity[2]<<endl;
		}
		
		for(int i=0;i<3;i++)
		{
			Total_Center_Trajectory[i]=bodycentre[i]-R_BtoC[i];
		}

/* 		double IniinB[3][3], O_tran[3][3], half[3][3];
		Transpose(insect.orientation,O_tran);
		Multiply(insect.orientation,body_inertia_effective,half);
		Multiply(half,O_tran,IniinB);

		double omega_sum_rw[3], omega_sum_lw[3], temp_rw1[3], temp_lw1[3], temp_rw2[3], temp_lw2[3];
		double Rw_plus_Rpc[3], Lw_plus_Rpc[3], temp_rr[3], temp_lr[3], temp_rl[3], temp_ll[3], mass_ratio;
		double body_induce_l[3], body_induce_r[3];
		
		for (int i=0; i<3; i++)
		{
			omega_sum_rw[i] = omega_rw[i]+omega_rwp[i];
			omega_sum_lw[i] = omega_lw[i]+omega_lwp[i];
			Rw_plus_Rpc[i] = R_rw[i]+R_PtoC_rw[i];
			Lw_plus_Rpc[i] = R_lw[i]+R_PtoC_lw[i];
		}

		cross_product(omega_sum_rw, R_rw, temp_rw1);    // temp_rw1 = (omega_rw + omega_rwp) x R_rw
		cross_product(R_PtoC_rw, temp_rw1, temp_rw2);   // temp_rw2 = R_PtoC_rw x temp_rw1
		cross_product(omega_sum_lw, R_lw, temp_lw1);    // temp_lw1 = (omega_lw + omega_lwp) x R_lw
		cross_product(R_PtoC_lw, temp_lw1, temp_lw2);   // temp_lw2 = R_PtoC_lw x temp_lw1
		cross_product(Rw_plus_Rpc, temp_rw1, temp_rr);  // temp_rr = (R_rw + R_PtoC_rw) x [(omega_rw + omega_rwp) x R_rw]
		cross_product(Lw_plus_Rpc, temp_lw1, temp_ll);  // temp_ll = (R_lw + R_PtoC_lw) x [(omega_lw + omega_lwp) x R_lw]
		cross_product(Rw_plus_Rpc, temp_lw1, temp_rl);  // temp_rl = (R_rw + R_PtoC_rw) x [(omega_lw + omega_lwp) x R_lw]
		cross_product(Lw_plus_Rpc, temp_rw1, temp_lr);  // temp_lr = (R_lw + R_PtoC_lw) x [(omega_rw + omega_rwp) x R_rw]
		cross_product(R_BtoC, temp_rw1, body_induce_r); // body_induce_r = R_BtoC x [(omega_rw + omega_rwp) x R_rw]
		cross_product(R_BtoC, temp_lw1, body_induce_l); // body_induce_l = R_BtoC x [(omega_lw + omega_lwp) x R_lw] */
	

		outforce<<Time<<" "<<insect.force[0]<<" "<<insect.force[1]<<" "<<insect.force[2]<<" "
				<<insect.torque[0]<<" "<<insect.torque[1]<<" "<<insect.torque[2]<<" "
				<<insect.power<<" "<<insect.energy<<endl;
		outpv<<Time<<" "<<bodycentre[0]<<"  "<<bodycentre[1]<<"  "<<bodycentre[2]<<"  "
				<<Car_Ale[0]<<"  "<<Car_Ale[1]<<"  "<<Car_Ale[2]<<"  "
				<<insect.angle[0]<<"  "<<insect.angle[1]<<"  "<<insect.angle[2]<<"  "
				<<wingplane_1.angle[0]<<"  "<<wingplane_1.angle[1]<<"  "<<wingplane_1.angle[2]<<"  "
				<<Total_Center_Trajectory[0]<<"  "<<Total_Center_Trajectory[1]<<"  "<<Total_Center_Trajectory[2]<<endl;
		trajectory<<Time<<" "<<X[wingtipindex+Point_Car]<<" "
		        <<Y[wingtipindex+Point_Car]<<" "
		        <<Z[wingtipindex+Point_Car]<<" "
		        <<insect.rigid_body[1]->XYZ[0][wingtipindex-23789]<<" "
		        <<insect.rigid_body[1]->XYZ[1][wingtipindex-23789]<<" "
		        <<insect.rigid_body[1]->XYZ[2][wingtipindex-23789]<<endl;
		outcontroller<<my_flap_pattern.total_cyc_time<<" "
		        <<wing_1.angle[0]<<" "<<wing_1.angle[1]<<" "<<wing_1.angle[2]<<" "
				<<wing_2.angle[0]<<" "<<wing_2.angle[1]<<" "<<my_flap_pattern.Angle_Current_Psi_Delta<<" "
				<<my_flap_pattern.freq<<" "
				<<my_flap_pattern.Angle_Current_Stroke_Delta<<" "
				<<my_flap_pattern.Angle_Current_AoA_Delta<<endl;
		outpartforce<<Time<<" "<<wing1force[0]<<" "<<wing1force[1]<<" "<<wing1force[2]<<" "
		        <<wing1torque[0]<<" "<<wing1torque[1]<<" "<<wing1torque[2]<<" "
				<<wing2force[0]<<" "<<wing2force[1]<<" "<<wing2force[2]<<" "
				<<wing2torque[0]<<" "<<wing2torque[1]<<" "<<wing2torque[2]<<" "
				<<bodyforce[0]<<" "<<bodyforce[1]<<" "<<bodyforce[2]<<endl;
/* 		outMOI<<Time<<" "<<I_rw_B[0][0]<<" "<<I_rw_B[0][1]<<" "<<I_rw_B[0][2]
					<<" "<<I_rw_B[1][0]<<" "<<I_rw_B[1][1]<<" "<<I_rw_B[1][2]
					<<" "<<I_rw_B[2][0]<<" "<<I_rw_B[2][1]<<" "<<I_rw_B[2][2]
					<<" "<<I_lw_B[0][0]<<" "<<I_lw_B[0][1]<<" "<<I_lw_B[0][2]
					<<" "<<I_lw_B[1][0]<<" "<<I_lw_B[1][1]<<" "<<I_lw_B[1][2]
					<<" "<<I_lw_B[2][0]<<" "<<I_lw_B[2][1]<<" "<<I_lw_B[2][2]
					<<" "<<body_inertia[0][0]<<" "<<body_inertia[0][1]<<" "<<body_inertia[0][2]
					<<" "<<body_inertia[1][0]<<" "<<body_inertia[1][1]<<" "<<body_inertia[1][2]
					<<" "<<body_inertia[2][0]<<" "<<body_inertia[2][1]<<" "<<body_inertia[2][2]
					<<" "<<IniinB[0][0]<<" "<<IniinB[0][1]<<" "<<IniinB[0][2]
					<<" "<<IniinB[1][0]<<" "<<IniinB[1][1]<<" "<<IniinB[1][2]
					<<" "<<IniinB[2][0]<<" "<<IniinB[2][1]<<" "<<IniinB[2][2]
					<<" "<<Rpc_op_Rpc_r[0][0]<<" "<<Rpc_op_Rpc_l[0][0]
					<<" "<<Rw_op_Rpc_r[0][0]<<" "<<Rw_op_Rpc_l[0][0]
					<<" "<<Rpc_op_Rw_r[0][0]<<" "<<Rpc_op_Rw_l[0][0]<<endl;
		outVec<<Time<<" "<<Total_Center_B[0]<<" "<<Total_Center_B[1]<<" "<<Total_Center_B[2]
					<<" "<<R_rw[0]<<" "<<R_rw[1]<<" "<<R_rw[2]
					<<" "<<R_lw[0]<<" "<<R_lw[1]<<" "<<R_lw[2]
					<<" "<<R_PtoC_rw[0]<<" "<<R_PtoC_rw[1]<<" "<<R_PtoC_rw[2]
					<<" "<<R_PtoC_lw[0]<<" "<<R_PtoC_lw[1]<<" "<<R_PtoC_lw[2]
					<<" "<<omega_rw[0]<<" "<<omega_rw[1]<<" "<<omega_rw[2]
					<<" "<<omega_lw[0]<<" "<<omega_lw[1]<<" "<<omega_lw[2]
					<<" "<<omega_rwp[0]<<" "<<omega_rwp[1]<<" "<<omega_rwp[2]
					<<" "<<wingplane_2.angular_velocity[0]<<" "<<wingplane_2.angular_velocity[1]<<" "<<wingplane_2.angular_velocity[2]
					<<" "<<insect.angular_velocity[0]<<" "<<insect.angular_velocity[1]<<" "<<insect.angular_velocity[2]
					<<" "<<AM_BMI[0]<<" "<<AM_BMI[1]<<" "<<AM_BMI[2]<<endl;
		outBMI<<Time<<" "<<temp_rw2[0]<<" "<<temp_lw2[0]
					<<" "<<temp_rr[0]<<" "<<temp_rl[0]<<" "<<temp_lr[0]<<" "<<temp_ll[0]
					<<" "<<body_induce_r[0]<<" "<<body_induce_l[0]<<endl; */

		if(RECORD) Write_Result_PLT(it);
		// if(it == Time_Start)
		// {
		// 	Write_Result_PLT_Cartesian(it,1);
		// }
		Write_Check_Point(it);
		//Frame_tranformation(it);
	
	//	Check();
	//	if(COP) Output_D(it, argv[3]);
    }
    outforce.close(); outpv.close(); trajectory.close();
	outcontroller.close();
	outpartforce.close();
	
    ofstream frequencyrecord;
    frequencyrecord.open("frequency-record.dat");
    for(int s=0;s<my_flap_pattern.Z_controller.record.size();s++) {
	    frequencyrecord<<s<<" "<<my_flap_pattern.Z_controller.record[s]<<endl;
    }
    
    if (Processor == "GPU") Finalize_GPU();
    mfreeGlobal();
    
    cpu_time_end=clock();
    loc_time_end=time(NULL);
    sptime=ctime(&loc_time_end);
    sptime[strlen(sptime)-1]=0;
    if (LPlot) 
    {
        cout<<"The Program End at: "<<sptime<<endl;
        cout<<"CPU Time: "<<(cpu_time_end-cpu_time_begin)/CLOCKS_PER_SEC<<" s"<<endl;
        cout<<"Wall Clock: "<<loc_time_end-loc_time_begin<<" s"<<endl;
    }
    if(LWrite)
    {
        Info<<"The Program End at: "<<sptime<<endl;
        Info<<"CPU Time: "<<(cpu_time_end-cpu_time_begin)/CLOCKS_PER_SEC<<" s"<<endl;
        Info<<"Wall Clock: "<<loc_time_end-loc_time_begin<<" s"<<endl;
        Info.close();
    }
}


void Force_Estimate()
{
	double wRc[3][3],wRc_old[3][3],omega[3][3],omega_old[3][3],O_tran[3][3],temp[3][3],Temp_DetA;
	
	for(int i=0;i<3;i++)
	{
		insect.position[i]=insect.position_old[i]+0.5*Dt*(insect.velocity[i]+insect.velocity_old[i]);
	}
	omega[0][0]=0;                              omega[0][1]=-insect.angular_velocity[2];     omega[0][2]=insect.angular_velocity[1];
	omega[1][0]=insect.angular_velocity[2];     omega[1][1]=0;                               omega[1][2]=-insect.angular_velocity[0];
	omega[2][0]=-insect.angular_velocity[1];    omega[2][1]=insect.angular_velocity[0];      omega[2][2]=0;
	
	omega_old[0][0]=0;                                  omega_old[0][1]=-insect.angular_velocity_old[2];     omega_old[0][2]=insect.angular_velocity_old[1];
	omega_old[1][0]=insect.angular_velocity_old[2];     omega_old[1][1]=0;                                   omega_old[1][2]=-insect.angular_velocity_old[0];
	omega_old[2][0]=-insect.angular_velocity_old[1];    omega_old[2][1]=insect.angular_velocity_old[0];      omega_old[2][2]=0;
	
	Multiply(omega,insect.orientation,wRc);
	Multiply(omega_old,insect.orientation_old,wRc_old);
	for(int i=0;i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
			insect.orientation[i][j]=insect.orientation_old[i][j]+0.5*Dt*(wRc[i][j]+wRc_old[i][j]);
		}
	}
	//orthonormalization for Rc
	for(int i=0;i<3;i++)
	{
	    Temp_DetA=sqrt(insect.orientation[0][i]*insect.orientation[0][i]
					  +insect.orientation[1][i]*insect.orientation[1][i]
					  +insect.orientation[2][i]*insect.orientation[2][i]);
	    for(int j=0;j<3;j++)
	    {
		insect.orientation[j][i]=insect.orientation[j][i]/Temp_DetA;
	    }

	}

	for(int i=0;i<3;i++)
	{
		insect.momentum[i]=insect.momentum_old[i]+0.5*Dt*(insect.force[i]+insect.force_old[i]);
		insect.velocity[i]=insect.momentum[i]/insect_parameters.Nondimensional_Mass;
	}
	
	for(int i=0;i<3;i++)
	{
		insect.angular_momentum[i]=insect.angular_momentum_old[i]+0.5*Dt*(insect.torque[i]+insect.torque_old[i]);
	}
	Transpose(insect.orientation,O_tran);
	Multiply(insect.orientation,insect.local_inertia,temp);
	Multiply(temp,O_tran,insect.inertia);
	Inverse(insect.inertia,temp);
	for(int i=0;i<3;i++)
	{
		insect.angular_velocity[i]=temp[i][0]*insect.angular_momentum[0]+temp[i][1]*insect.angular_momentum[1]+temp[i][2]*insect.angular_momentum[2];
	}
	
}

void Initial_Guess()
{
    insect.velocity_old[0]=0;insect.velocity_old[1]=0;insect.velocity_old[2]=0;
    double temp[3][3];
    
    for(int i=0;i<3;i++) 
    {
    	insect.position[i]=insect.position_old[i]+Dt*insect.velocity_old[i];
		insect.momentum[i]=insect.momentum_old[i]+Dt*insect.force_old[i];
		Total_Center_Velocity[i]=insect.momentum[i]/insect_parameters.Nondimensional_Total_Mass;
		Total_Center_Acce[i]=insect.force[i]/insect_parameters.Nondimensional_Total_Mass;
    }
    
    insect.angle[0] = insect.angle_old[0] + Dt * 
                ( cos(insect.angle_old[0])*tan(insect.angle_old[1])*insect.angular_velocity_old[0]
                + sin(insect.angle_old[0])*tan(insect.angle_old[1])*insect.angular_velocity_old[1]
                + insect.angular_velocity_old[2] );
    insect.angle[1] = insect.angle_old[1] + Dt *
                ( -sin(insect.angle_old[0])*insect.angular_velocity_old[0]
                + cos(insect.angle_old[0])*insect.angular_velocity_old[1] );
    insect.angle[2] = insect.angle_old[2] + Dt * 
                ( cos(insect.angle_old[0])/cos(insect.angle_old[1])*insect.angular_velocity_old[0]
                + sin(insect.angle_old[0])/cos(insect.angle_old[1])*insect.angular_velocity_old[1] );
    insect.compute_orientation();

    load_wing_mass_module(1);
    
    for(int i=0;i<3;i++) {
		insect.angular_momentum[i]=insect.angular_momentum_old[i]+Dt*insect.torque_old[i];
    }
 
    Inverse(body_inertia_effective,temp);

    // cout<<"body_inertia_effective is:  "<<body_inertia_effective[0][0]<<"  "<<insect.inertia[0][0]<<endl;

    for(int i=0;i<3;i++)
    {
		insect.angular_velocity[i]=temp[i][0]*(insect.angular_momentum[0]-AM_BMI[0])
		                          +temp[i][1]*(insect.angular_momentum[1]-AM_BMI[1])
		                          +temp[i][2]*(insect.angular_momentum[2]-AM_BMI[2]);
    }

    // double testAV;
    // testAV = -AM_BMI[0]/body_inertia_effective[0][0];
    // double VecinB[3], IniinB[3][3], O_tran[3][3], half[3][3];
	// for(int i=0;i<3;i++)
	// {
	// 	VecinB[i] = insect.orientation[0][i]*AM_BMI[0]+insect.orientation[1][i]*AM_BMI[1]
	// 			+insect.orientation[2][i]*AM_BMI[2];
	// }
	// Transpose(insect.orientation,O_tran);
	// Multiply(insect.orientation,body_inertia_effective,half);
	// Multiply(half,O_tran,IniinB);
    // cout<<"insect.anglar_velocity is:  "<<setprecision(16)<<insect.angular_velocity[0]<<"  "<<testAV<<"  "<<endl;
	// cout<<"AM_BMI in body frame is:  "<<setprecision(16)<<VecinB[0]<<"  "<<VecinB[1]<<"  "<<VecinB[2]<<"  "<<endl;
	// cout<<"MOI in body frame is:  "<<setprecision(16)<<IniinB[0][0]<<"  "<<IniinB[0][1]<<"  "<<IniinB[0][2]<<"  "<<endl;
	// cout<<"MOI in body frame is:  "<<setprecision(16)<<IniinB[1][0]<<"  "<<IniinB[1][1]<<"  "<<IniinB[1][2]<<"  "<<endl;
	// cout<<"MOI in body frame is:  "<<setprecision(16)<<IniinB[2][0]<<"  "<<IniinB[2][1]<<"  "<<IniinB[2][2]<<"  "<<endl;

  //   Omega[0][0]=0;Omega[0][1]=-insect.angular_velocity[2];Omega[0][2]=insect.angular_velocity[1];
  //   Omega[1][0]=insect.angular_velocity[2];Omega[1][1]=0;Omega[1][2]=-insect.angular_velocity[0];
  //   Omega[2][0]=-insect.angular_velocity[1];Omega[2][1]=insect.angular_velocity[0];Omega[2][2]=0;
    
  //   for(int i=0;i<3;i++)
  //   {
		// t_wL_dBMI[i]=insect.torque[i]
		// 	        -Omega[i][0]*(insect.angular_momentum[0]-AM_BMI[0])
		// 	        -Omega[i][1]*(insect.angular_momentum[1]-AM_BMI[1])
		// 	        -Omega[i][2]*(insect.angular_momentum[2]-AM_BMI[2])
		// 	        -d_AM_BMI[i];
  //   }

 //    cross_product(insect.angular_velocity, R_BtoC, wxR_BtoC);

 //    double omega_sum_rw[3], omega_sum_lw[3], temp_rw[3], temp_lw[3];

	// for (int i=0; i<3; i++)
	// {
	// 	omega_sum_rw[i] = omega_rw[i]+omega_rwp[i];
	// 	omega_sum_lw[i] = omega_lw[i]+omega_lwp[i];
	// }
	// cross_product(omega_sum_rw, R_rw, temp_rw);
	// cross_product(omega_sum_lw, R_lw, temp_lw);
      
    // cout<<"R_BtoC is:  "<<R_BtoC[0]<<"  "<<R_BtoC[1]<<"  "<<R_BtoC[2]<<"  "<<endl;  

    for(int i=0;i<3;i++)
    {
		insect.angular_acceleration[i]=(insect.angular_velocity[i]-insect.angular_velocity_old[i])/Dt;
		//insect.angular_acceleration[i]=temp[i][0]*t_wL_dBMI[0]+temp[i][1]*t_wL_dBMI[1]+temp[i][2]*t_wL_dBMI[2];
		// insect.velocity[i]=1/insect_parameters.Nondimensional_Total_Mass*
		// 				   (insect.momentum[i]-insect_parameters.Nondimensional_One_Wing_Mass*(temp_rw[i]+temp_lw[i]))
		// 				  +wxR_BtoC[i];
    }

    // cout<<"insect.velocity is:  "<<insect.velocity[0]<<"  "<<insect.velocity[1]<<"  "<<insect.velocity[2]<<"  "<<endl; 

    // cross_product(insect.angular_velocity, wxR_BtoC, wxwxR_BtoC);
    // cross_product(insect.angular_acceleration, R_BtoC, alphaxR_BtoC);

    wing_center_UVW_ACCE(1);
    double RW_center_ACCE_Ab[3], LW_center_ACCE_Ab[3], RW_center_UVW_Vb[3], LW_center_UVW_Vb[3];

    for(int i=0;i<3;i++)
    {
    	RW_center_ACCE_Ab[i] = RW_center_ACCE[i]-insect.acceleration[i];
    	LW_center_ACCE_Ab[i] = LW_center_ACCE[i]-insect.acceleration[i];
    	RW_center_UVW_Vb[i] = RW_center_UVW[i]-insect.velocity[i];
    	LW_center_UVW_Vb[i] = LW_center_UVW[i]-insect.velocity[i];
		insect.acceleration[i] = (insect.force[i]-insect_parameters.Nondimensional_One_Wing_Mass*(RW_center_ACCE_Ab[i]+LW_center_ACCE_Ab[i]))/
								 insect_parameters.Nondimensional_Total_Mass;

		insect.velocity[i]=1/insect_parameters.Nondimensional_Total_Mass*
						   (insect.momentum[i]-insect_parameters.Nondimensional_One_Wing_Mass*(RW_center_UVW_Vb[i]+LW_center_UVW_Vb[i]));
	   	// insect.acceleration[i] = (insect.velocity[i]-insect.velocity_old[i])/Dt;
    }
}

// solve insect.angle_tmp, insect.angular_velocity_tmp, insect.velocity_tmp
void Solve_Structure_Problem()
{
	double temp[3][3];
	// double wxR_BtoC[3];
	
	for(int i=0;i<3;i++)
	{
		insect.position_tmp[i]=insect.position_old[i]+0.5*Dt*(insect.velocity[i]+insect.velocity_old[i]);
		bodycentre_tmp[i]=bodycentre_old[i]+0.5*Dt*(Car_Ale_Old[i]+Car_Ale[i]);
		insect.momentum_tmp[i]=insect.momentum_old[i]+0.5*Dt*(insect.force[i]+insect.force_old[i]);
	}

	insect.angle_tmp[0] = insect.angle_old[0] + 0.5 * Dt * 
                    ( cos(insect.angle_old[0])*tan(insect.angle_old[1])*insect.angular_velocity_old[0]
                    + sin(insect.angle_old[0])*tan(insect.angle_old[1])*insect.angular_velocity_old[1]
                    + insect.angular_velocity_old[2]
                    + cos(insect.angle[0])*tan(insect.angle[1])*insect.angular_velocity[0]
                    + sin(insect.angle[0])*tan(insect.angle[1])*insect.angular_velocity[1]
                    + insect.angular_velocity[2] );
	insect.angle_tmp[1] = insect.angle_old[1] + 0.5 * Dt * 
                    ( -sin(insect.angle_old[0])*insect.angular_velocity_old[0]
                    + cos(insect.angle_old[0])*insect.angular_velocity_old[1]
                    - sin(insect.angle[0])*insect.angular_velocity[0]
                    + cos(insect.angle[0])*insect.angular_velocity[1] );
	insect.angle_tmp[2] = insect.angle_old[2] + 0.5 * Dt *
                    ( cos(insect.angle_old[0])/cos(insect.angle_old[1])*insect.angular_velocity_old[0]
                    + sin(insect.angle_old[0])/cos(insect.angle_old[1])*insect.angular_velocity_old[1]
                    + cos(insect.angle[0])/cos(insect.angle[1])*insect.angular_velocity[0]
                    + sin(insect.angle[0])/cos(insect.angle[1])*insect.angular_velocity[1] );

    // cout<<"insect.angle_tmp is:  "<<insect.angle_tmp[0]<<"  "<<insect.angle_tmp[1]<<"  "<<insect.angle_tmp[2]<<"  "<<endl;

	insect.compute_orientation_tmp();

	load_wing_mass_module(2);

	for(int i=0;i<3;i++)
	{
		insect.angular_momentum_tmp[i]=insect.angular_momentum_old[i]+0.5*Dt*(insect.torque[i]+insect.torque_old[i]);
	}

	Inverse(body_inertia_effective,temp);

    for(int i=0;i<3;i++)
    {
		insect.angular_velocity_tmp[i]=temp[i][0]*(insect.angular_momentum_tmp[0]-AM_BMI[0])
		                              +temp[i][1]*(insect.angular_momentum_tmp[1]-AM_BMI[1])
		                              +temp[i][2]*(insect.angular_momentum_tmp[2]-AM_BMI[2]);
    }

 //    cross_product(insect.angular_velocity_tmp, R_BtoC, wxR_BtoC);   

 //    double omega_sum_rw[3], omega_sum_lw[3], temp_rw[3], temp_lw[3];

	// for (int i=0; i<3; i++)
	// {
	// 	omega_sum_rw[i] = omega_rw[i]+omega_rwp[i];
	// 	omega_sum_lw[i] = omega_lw[i]+omega_lwp[i];
	// }
	// cross_product(omega_sum_rw, R_rw, temp_rw);
	// cross_product(omega_sum_lw, R_lw, temp_lw);  

 //    for(int i=0;i<3;i++)
 //    {
	// 	insect.velocity_tmp[i]=1/insect_parameters.Nondimensional_Total_Mass*
	// 					   (insect.momentum_tmp[i]-insect_parameters.Nondimensional_One_Wing_Mass*(temp_rw[i]+temp_lw[i]))
	// 					  +wxR_BtoC[i];
 //    }
  //   double remain[3], R_rw_inG[3], R_lw_inG[3], cro_r[3], cro_l[3], RW_test[3], LW_test[3], isthatequal[3], isthatequal2[3], RCinB[3], RCinBinG[3];
  //   go_superframe(insect.orientation_tmp, R_rw_inB, R_rw_inG);
  //   go_superframe(insect.orientation_tmp, R_lw_inB, R_lw_inG);
  //   cross_product(insect.angular_velocity_tmp, R_rw_inG, cro_r);
  //   cross_product(insect.angular_velocity_tmp, R_lw_inG, cro_l);
    

  //   for(int i=0;i<3;i++)
  //   {
		// remain[i]=-insect_parameters.Nondimensional_Total_Mass*wxR_BtoC[i]
		// 				   +insect_parameters.Nondimensional_One_Wing_Mass*(temp_rw[i]+temp_lw[i]);
	 //    RW_test[i]=temp_rw[i]+cro_r[i];
	 //    LW_test[i]=temp_lw[i]+cro_l[i];
	 //    isthatequal[i] = insect_parameters.Nondimensional_One_Wing_Mass*(cro_r[i]+cro_l[i]);
	 //    isthatequal2[i] = -insect_parameters.Nondimensional_Total_Mass*wxR_BtoC[i];
	 //    RCinB[i] = insect_parameters.Nondimensional_One_Wing_Mass/insect_parameters.Nondimensional_Total_Mass*(R_rw_inB[i]+R_lw_inB[i]);
	 //    go_superframe(insect.orientation_tmp, RCinB, RCinBinG);
  //   }
  //   cout<<"RCinBinG is:  "<<RCinBinG[0]<<"  "<<RCinBinG[1]<<"  "<<RCinBinG[2]<<"  "<<endl;
  //   cout<<"R_BtoC is:  "<<R_BtoC[0]<<"  "<<R_BtoC[1]<<"  "<<R_BtoC[2]<<"  "<<endl;
    // cout<<"isthatequal is:  "<<isthatequal[0]<<"  "<<isthatequal[1]<<"  "<<isthatequal[2]<<"  "<<endl;
    // cout<<"isthatequal2 is:  "<<isthatequal2[0]<<"  "<<isthatequal2[1]<<"  "<<isthatequal2[2]<<"  "<<endl;
    // cout<<"remain is:  "<<remain[0]<<"  "<<remain[1]<<"  "<<remain[2]<<"  "<<endl;
    // cout<<"insect.velocity_tmp is:  "<<insect.velocity_tmp[0]<<"  "<<insect.velocity_tmp[1]<<"  "<<insect.velocity_tmp[2]<<"  "<<endl; 

    wing_center_UVW_ACCE(2);
    double RW_center_UVW_Vb[3], LW_center_UVW_Vb[3];
    for(int i=0;i<3;i++)
    {
    	RW_center_UVW_Vb[i] = RW_center_UVW[i]-insect.velocity_tmp[i];
    	LW_center_UVW_Vb[i] = LW_center_UVW[i]-insect.velocity_tmp[i];
		insect.velocity_tmp[i]=1/insect_parameters.Nondimensional_Total_Mass*
						   (insect.momentum_tmp[i]-insect_parameters.Nondimensional_One_Wing_Mass*(RW_center_UVW_Vb[i]+LW_center_UVW_Vb[i]));
    }
  //   for(int i=0;i<3;i++)
  //   {
		// remain[i]=insect_parameters.Nondimensional_One_Wing_Mass*(RW_center_UVW_Vb[i]+LW_center_UVW_Vb[i]);
  //   }
    // cout<<"remain is:  "<<remain[0]<<"  "<<remain[1]<<"  "<<remain[2]<<"  "<<endl;
    // cout<<"insect.velocity_tmp is:  "<<insect.velocity_tmp[0]<<"  "<<insect.velocity_tmp[1]<<"  "<<insect.velocity_tmp[2]<<"  "<<endl;
    // cout<<"RW_test is:  "<<RW_test[0]<<"  "<<RW_test[1]<<"  "<<RW_test[2]<<"  "<<endl;
    // cout<<"LW_test is:  "<<LW_test[0]<<"  "<<LW_test[1]<<"  "<<LW_test[2]<<"  "<<endl;
    // cout<<"RW_center_UVW_Vb is:  "<<RW_center_UVW_Vb[0]<<"  "<<RW_center_UVW_Vb[1]<<"  "<<RW_center_UVW_Vb[2]<<"  "<<endl;
    // cout<<"LW_center_UVW_Vb is:  "<<LW_center_UVW_Vb[0]<<"  "<<LW_center_UVW_Vb[1]<<"  "<<LW_center_UVW_Vb[2]<<"  "<<endl;
}

// solve insect.angle, insect.angular_velocity, insect.angular_acceleration, insect.velocity, insect.acceleration
void Update_Structrue_value()
{
    double temp[3][3];
    // double wxR_BtoC[3], wxwxR_BtoC[3], alphaxR_BtoC[3];
    
    for(int i=0;i<3;i++)
    {
		insect.position[i]=(1-RF[0])*insect.position[i]+RF[0]*insect.position_tmp[i];
		bodycentre[i]=(1-RF[0])*bodycentre[i]+RF[0]*bodycentre_tmp[i];

		insect.momentum[i]=(1-RF[2])*insect.momentum[i]+RF[2]*insect.momentum_tmp[i];
		Total_Center_Velocity[i]=insect.momentum[i]/insect_parameters.Nondimensional_Total_Mass;
		Total_Center_Acce[i]=insect.force[i]/insect_parameters.Nondimensional_Total_Mass;
    }
    
    for(int i=0;i<3;i++)
    {
		insect.angle[i]=(1-RF[1])*insect.angle[i]+RF[1]*insect.angle_tmp[i];
    }
    // cout<<"insect.angle is:  "<<setprecision(16)<<insect.angle[0]<<"  "<<insect.angle[1]<<"  "<<insect.angle[2]<<"  "<<endl;

    insect.compute_orientation();

    load_wing_mass_module(1);
    
    for(int i=0;i<3;i++)
    {
		insect.angular_momentum[i]=(1-RF[3])*insect.angular_momentum[i]+RF[3]*insect.angular_momentum_tmp[i];
    }

    Inverse(body_inertia_effective,temp);

    for(int i=0;i<3;i++)
    {
		insect.angular_velocity[i]=temp[i][0]*(insect.angular_momentum[0]-AM_BMI[0])
		                          +temp[i][1]*(insect.angular_momentum[1]-AM_BMI[1])
		                          +temp[i][2]*(insect.angular_momentum[2]-AM_BMI[2]);
    }

    // double testAV;
    // testAV = -AM_BMI[0]/body_inertia_effective[0][0];
    // cout<<"insect.anglar_velocity is:  "<<setprecision(16)<<insect.angular_velocity[0]<<"  "<<testAV<<"  "<<endl;
	// double AVinB[3];
	// for(int i=0;i<3;i++)
	// {
	// 	AVinB[i] = insect.orientation[0][i]*insect.angular_velocity[0]+insect.orientation[1][i]*insect.angular_velocity[1]
	// 			+insect.orientation[2][i]*insect.angular_velocity[2];
	// }
	// cout<<"insect.anglar_velocity is:  "<<setprecision(16)<<insect.angular_velocity[0]<<"  "<<insect.angular_velocity[1]<<"  "<<insect.angular_velocity[2]<<"  "<<endl;
	// cout<<"insect.anglar_velocity in body frame is:  "<<setprecision(16)<<AVinB[0]<<"  "<<AVinB[1]<<"  "<<AVinB[2]<<"  "<<endl;

	// double VecinB[3], IniinB[3][3], O_tran[3][3], half[3][3];
	// for(int i=0;i<3;i++)
	// {
	// 	VecinB[i] = insect.orientation[0][i]*AM_BMI[0]+insect.orientation[1][i]*AM_BMI[1]
	// 			+insect.orientation[2][i]*AM_BMI[2];
	// }
	// Transpose(insect.orientation,O_tran);
	// Multiply(insect.orientation,body_inertia_effective,half);
	// Multiply(half,O_tran,IniinB);
    // cout<<"insect.anglar_velocity is:  "<<setprecision(16)<<insect.angular_velocity[0]<<"  "<<insect.angular_velocity[1]<<"  "<<insect.angular_velocity[2]<<"  "<<endl;
	// cout<<"AM_BMI in body frame is:  "<<setprecision(16)<<VecinB[0]<<"  "<<VecinB[1]<<"  "<<VecinB[2]<<"  "<<endl;
	// cout<<"MOI in body frame is:  "<<setprecision(16)<<IniinB[0][0]<<"  "<<IniinB[0][1]<<"  "<<IniinB[0][2]<<"  "<<endl;
	// cout<<"MOI in body frame is:  "<<setprecision(16)<<IniinB[1][0]<<"  "<<IniinB[1][1]<<"  "<<IniinB[1][2]<<"  "<<endl;
	// cout<<"MOI in body frame is:  "<<setprecision(16)<<IniinB[2][0]<<"  "<<IniinB[2][1]<<"  "<<IniinB[2][2]<<"  "<<endl;

    // Omega[0][0]=0;Omega[0][1]=-insect.angular_velocity[2];Omega[0][2]=insect.angular_velocity[1];
    // Omega[1][0]=insect.angular_velocity[2];Omega[1][1]=0;Omega[1][2]=-insect.angular_velocity[0];
    // Omega[2][0]=-insect.angular_velocity[1];Omega[2][1]=insect.angular_velocity[0];Omega[2][2]=0;

  //   for(int i=0;i<3;i++)
  //   {
		// t_wL_dBMI[i]=insect.torque[i]
		// 	        -Omega[i][0]*(insect.angular_momentum[0]-AM_BMI[0])
		// 	        -Omega[i][1]*(insect.angular_momentum[1]-AM_BMI[1])
		// 	        -Omega[i][2]*(insect.angular_momentum[2]-AM_BMI[2])
		// 	        -d_AM_BMI[i];
  //   }

 //    cross_product(insect.angular_velocity, R_BtoC, wxR_BtoC);     

 //    double omega_sum_rw[3], omega_sum_lw[3], temp_rw[3], temp_lw[3];

	// for (int i=0; i<3; i++)
	// {
	// 	omega_sum_rw[i] = omega_rw[i]+omega_rwp[i];
	// 	omega_sum_lw[i] = omega_lw[i]+omega_lwp[i];
	// }
	// cross_product(omega_sum_rw, R_rw, temp_rw);
	// cross_product(omega_sum_lw, R_lw, temp_lw);
      
    for(int i=0;i<3;i++)
    {
		insect.angular_acceleration[i]=(insect.angular_velocity[i]-insect.angular_velocity_old[i])/Dt;
		// insect.angular_acceleration[i]=temp[i][0]*t_wL_dBMI[0]+temp[i][1]*t_wL_dBMI[1]+temp[i][2]*t_wL_dBMI[2];
		// insect.velocity[i]=1/insect_parameters.Nondimensional_Total_Mass*
		// 				   (insect.momentum[i]-insect_parameters.Nondimensional_One_Wing_Mass*(temp_rw[i]+temp_lw[i]))
		// 				  +wxR_BtoC[i];
    }   

    // cross_product(insect.angular_velocity, wxR_BtoC, wxwxR_BtoC);
    // cross_product(insect.angular_acceleration, R_BtoC, alphaxR_BtoC);

    wing_center_UVW_ACCE(1);
	double RW_center_ACCE_Ab[3], LW_center_ACCE_Ab[3], RW_center_UVW_Vb[3], LW_center_UVW_Vb[3];

    for(int i=0;i<3;i++)
    {
    	RW_center_ACCE_Ab[i] = RW_center_ACCE[i]-insect.acceleration[i];
    	LW_center_ACCE_Ab[i] = LW_center_ACCE[i]-insect.acceleration[i];
    	RW_center_UVW_Vb[i] = RW_center_UVW[i]-insect.velocity[i];
    	LW_center_UVW_Vb[i] = LW_center_UVW[i]-insect.velocity[i];
		insect.acceleration[i] = (insect.force[i]-insect_parameters.Nondimensional_One_Wing_Mass*(RW_center_ACCE_Ab[i]+LW_center_ACCE_Ab[i]))/
								 insect_parameters.Nondimensional_Total_Mass;
		insect.velocity[i]=1/insect_parameters.Nondimensional_Total_Mass*
						   (insect.momentum[i]-insect_parameters.Nondimensional_One_Wing_Mass*(RW_center_UVW_Vb[i]+LW_center_UVW_Vb[i]));
	   	// insect.acceleration[i] = (insect.velocity[i]-insect.velocity_old[i])/Dt;
    }
}

int Check_FSI_REL()
{
  
/*  
    if(fabs(insect.position_tmp[0]-insect.position[0])>FSI_Eps*insect.position[0]||
       fabs(insect.position_tmp[1]-insect.position[1])>FSI_Eps*insect.position[1]||
       fabs(insect.position_tmp[2]-insect.position[2])>FSI_Eps*insect.position[2]) return 1;
*/

    if(fabs(bodycentre[0]-bodycentre_tmp[0])>FSI_Eps*fabs(bodycentre[0])||
       fabs(bodycentre[1]-bodycentre_tmp[1])>FSI_Eps*fabs(bodycentre[1])||
       fabs(bodycentre[2]-bodycentre_tmp[2])>FSI_Eps*fabs(bodycentre[2])) return 1;


    if(fabs(insect.angle_tmp[0]-insect.angle[0])>FSI_Eps*fabs(insect.angle[0])||
       fabs(insect.angle_tmp[1]-insect.angle[1])>FSI_Eps*fabs(insect.angle[1])||
       fabs(insect.angle_tmp[2]-insect.angle[2])>FSI_Eps*fabs(insect.angle[2]))  return 1;


    if(fabs(insect.momentum_tmp[0]-insect.momentum[0])>FSI_Eps*fabs(insect.momentum[0])||
       fabs(insect.momentum_tmp[1]-insect.momentum[1])>FSI_Eps*fabs(insect.momentum[1])||
       fabs(insect.momentum_tmp[2]-insect.momentum[2])>FSI_Eps*fabs(insect.momentum[2]))  return 1;
    
    if(fabs(insect.angular_momentum_tmp[0]-insect.angular_momentum[0])>FSI_Eps*fabs(insect.angular_momentum[0])||
       fabs(insect.angular_momentum_tmp[1]-insect.angular_momentum[1])>FSI_Eps*fabs(insect.angular_momentum[1])||
       fabs(insect.angular_momentum_tmp[2]-insect.angular_momentum[2])>FSI_Eps*fabs(insect.angular_momentum[2]))  return 1;
	   
    if(Res[IP]/sqrt(Point_All)>0.08) return 1;
    
    return 0;
}

int Check_FSI_ABS()
{
  
/*  
    if(fabs(insect.position_tmp[0]-insect.position[0])>FSI_Eps_Position||
       fabs(insect.position_tmp[1]-insect.position[1])>FSI_Eps_Position||
       fabs(insect.position_tmp[2]-insect.position[2])>FSI_Eps_Position) return 1;
*/

    if(fabs(bodycentre[0]-bodycentre_tmp[0])>FSI_Eps_Position||
       fabs(bodycentre[1]-bodycentre_tmp[1])>FSI_Eps_Position||
       fabs(bodycentre[2]-bodycentre_tmp[2])>FSI_Eps_Position) return 1;
    
    if(fabs(insect.angle_tmp[0]-insect.angle[0])>FSI_Eps_Angle||
       fabs(insect.angle_tmp[1]-insect.angle[1])>FSI_Eps_Angle||
       fabs(insect.angle_tmp[2]-insect.angle[2])>FSI_Eps_Angle)  return 1;
    
    if(fabs(insect.momentum_tmp[0]-insect.momentum[0])>FSI_Eps_Momentum||
       fabs(insect.momentum_tmp[1]-insect.momentum[1])>FSI_Eps_Momentum||
       fabs(insect.momentum_tmp[2]-insect.momentum[2])>FSI_Eps_Momentum)  return 1;
    
    if(fabs(insect.angular_momentum_tmp[0]-insect.angular_momentum[0])>FSI_Eps_Angular_Momentum||
       fabs(insect.angular_momentum_tmp[1]-insect.angular_momentum[1])>FSI_Eps_Angular_Momentum||
       fabs(insect.angular_momentum_tmp[2]-insect.angular_momentum[2])>FSI_Eps_Angular_Momentum)  return 1;
    
    return 0;
}

double RF_Recursion(double lastrf, double *rold, double *rnew)
{
    double temp[3],s,result;
    temp[0]=rnew[0]-rold[0];temp[1]=rnew[1]-rold[1];temp[2]=rnew[2]-rold[2];
    s=temp[0]*temp[0]+temp[1]*temp[1]+temp[2]*temp[2];
    if(s<1e-15) {cout<<"Warning, the norm of residual is too small in function RF_Recursion."<<endl; return 0;}
    result=(-lastrf)*(rold[0]*temp[0]+rold[1]*temp[1]+rold[2]*temp[2])/s;
    if(result>1)
    {
	return 1;
    }
    else if(result<0)
    {
	return 0;
    }
    else
    {
	return result;
    }
}

void Compute_Relaxation_Factor(int FSI_Iter)
{
    if(FSI_Iter==1)
    {
	for(int j=0;j<3;j++)
	{
	    //RF_Residual[0][j]=insect.position_tmp[j]-insect.position[j];
	    RF_Residual[0][j]=bodycentre_tmp[j]-bodycentre[j];
	    RF_Residual[1][j]=insect.angle_tmp[j]-insect.angle[j];
	    RF_Residual[2][j]=insect.momentum_tmp[j]-insect.momentum[j];
	    RF_Residual[3][j]=insect.angular_momentum_tmp[j]-insect.angular_momentum[j];
	}
	for(int i=0;i<4;i++)
	{	  
	    RF[i]=MAX(RF_Old[i],RF_Max); 
	}
    }
    else
    {
	for(int j=0;j<3;j++)
	{
	    //RF_Residual[0][j]=insect.position_tmp[j]-insect.position[j];
	    RF_Residual[0][j]=bodycentre_tmp[j]-bodycentre[j];
	    RF_Residual[1][j]=insect.angle_tmp[j]-insect.angle[j];
	    RF_Residual[2][j]=insect.momentum_tmp[j]-insect.momentum[j];
	    RF_Residual[3][j]=insect.angular_momentum_tmp[j]-insect.angular_momentum[j];
	}
	
	for(int i=0;i<4;i++)
	{	  
	    RF[i]=RF_Recursion(RF_Old[i],RF_Residual_Old[i],RF_Residual[i]);   
	}
    }
    
    for(int i=0;i<4;i++)
    {
	for(int j=0;j<3;j++)
	{
	    RF_Residual_Old[i][j]=RF_Residual[i][j];

	}
	RF_Old[i]=RF[i];
    }
    
}

void Div(int Num_Seg, double X0, double X1, double Exp1, int offs, double *XPT, double *Ff)
{
    double length, Size_First_Seg;
	double Dl, Sl;
	double *Size_Seg, *FD;
	int    i;
	length = X1-X0;
	if(fabs(Exp1-1.0)<0.0001) 
	    Size_First_Seg = length/double(Num_Seg);
	else 
	    Size_First_Seg=length*(1-Exp1)/(1-pow(Exp1, Num_Seg));
	
	Size_Seg= new double [Num_Seg+1];
	FD= new double [Num_Seg+1];
	Dl=1.0/(length+1e-20);
	Size_Seg[0]=Size_First_Seg;
	FD[0]=0;
	Sl=0;

	for(i=1;i<Num_Seg;i++)
	{
        Size_Seg[i]=Size_Seg[i-1]*Exp1;
		Sl=Sl+Size_Seg[i-1];
		FD[i]=Sl*Dl;
	}
	FD[Num_Seg]=1.0;
	//calculate coordinates of grid points for straight lines
	for(i=0; i<Num_Seg+1; i++) XPT[i+offs]=X0+FD[i]*(X1-X0);
	//for(i=0; i<Num_Seg+1; i++) cout<<XPT[i+offs]<<endl;
	for(i=1; i<Num_Seg+1; i++) Ff[i+offs]=XPT[i+offs]-XPT[i+offs-1];
	delete []FD;
	delete []Size_Seg;
}

//void Memory_Allocate()
//{
//    int ico, iv, i, io;
//	

//    Point_Car=IPoint*JPoint*KPoint;
//    Point_All=Point_Car+Point_Meshless;

//    GType1 = new char [Point_All];
//    GType2 = new char [Point_All];
//    GTypeT = new char [Point_All];
//    IIndex = new int [IPoint];
//    JIndex = new int [JPoint];

//	for(ico=0;ico<3;ico++) MeshAll_Cord[ico] = new double [Point_All];
//    X = MeshAll_Cord[0];
//    Y = MeshAll_Cord[1];
//    Z = MeshAll_Cord[2];
//	Fcx = new double [IPoint];
//    Fcy = new double [JPoint];
//	Fcz = new double [KPoint];
//	X_Meshless = & MeshAll_Cord[0][Point_Car];
//	Y_Meshless = & MeshAll_Cord[1][Point_Car];
//	Z_Meshless = & MeshAll_Cord[2][Point_Car];
//    
//    Cord[0] = new CoordMember [IPoint];
//    Cord[1] = new CoordMember [JPoint];
//    Cord[2] = new CoordMember [KPoint];
//    
//    MNx = new double [Point_Meshless]; 
//	MNy = new double [Point_Meshless];
//	MNz = new double [Point_Meshless];
//	
//	ACCX_Ale=new double [Point_Meshless];
//	ACCY_Ale=new double [Point_Meshless];
//	ACCZ_Ale=new double [Point_Meshless];
//	
//	ACCX_Ale_Old=new double [Point_Meshless];
//	ACCY_Ale_Old=new double [Point_Meshless];
//	ACCZ_Ale_Old=new double [Point_Meshless];
//	
//	PointCategory = new int [Point_Meshless];
//	
//	Csvd = new double** [Point_Meshless];
//	Csvd_Old = new double** [Point_Meshless];
//	
//	showindex=new double [Point_Meshless];
//	
//	for(i=0;i<Point_Meshless;i++)
//	{
//	    Csvd[i] = new double* [6];
//		Csvd_Old[i] = new double* [6];
//	}
//	for(i=0;i<Point_Meshless;i++)
//	    for(ico=0;ico<6;ico++)
//        {
//		    Csvd[i][ico] = new double [NB];
//			Csvd_Old[i][ico] = new double [NB];
//		}	
//        
//	Nb_Points = new int* [Point_Meshless];
//	Nb_Points_Old = new int* [Point_Meshless];
//	
//	for(io=0;io<Point_Meshless;io++)
//	{
//		Nb_Points[io] = new int [NB];
//		Nb_Points_Old[io] = new int [NB];
//	}
//	
//	for(iv=0;iv<NF_MAX;iv++)
//	{
//        F0[iv] = new double [Point_All];
//		F1[iv] = new double [Point_All];
//		F2[iv] = new double [Point_All];
//	}	
//	
//	U=F0[0];
//	V=F0[1];
//	W=F0[2];
//	P=F0[3];
//	Ustar=F1[0];
//    Vstar=F1[1];	
//    Wstar=F1[2];	
//    So=F1[3];
//	U_Old=F2[0];
//    V_Old=F2[1];	
//    W_Old=F2[2];	
//    P_Old=F2[3];
//    So_U = new double [Point_All];
//	So_V = new double [Point_All];
//	So_W = new double [Point_All];
//	
//	if(ALE == 1)
//	{
//		X_Meshless_Old = new double [Point_Meshless];
//        Y_Meshless_Old = new double [Point_Meshless];
//        Z_Meshless_Old = new double [Point_Meshless];
//		U_Ale = new double [Point_Meshless];
//		V_Ale = new double [Point_Meshless];
//		W_Ale = new double [Point_Meshless];
//		U_Ale_Old = new double [Point_Meshless];
//		V_Ale_Old = new double [Point_Meshless];
//		W_Ale_Old = new double [Point_Meshless];
//	}
//	
//    if (Rigid_Object_Number>0)
//	{
//		Rigid_Offset = new int [Rigid_Object_Number];
//		Body_Offset  = new int [Rigid_Object_Number];
//		Outer_Offset = new int [Rigid_Object_Number];
//	}
//}

//void Memory_Free()
//{
//    int ico, iv, i;
// 
//    delete []GType1;
//	delete []GType2;
//	delete []GTypeT;
//	delete []IIndex;
//	delete []JIndex;

//	for(ico=0;ico<3;ico++) delete []MeshAll_Cord[ico];
//	for(ico=0;ico<3;ico++) delete []Cord[ico];
//	
//    delete []MNx; 
//	delete []MNy; 
//	delete []MNz; 
//	delete []Fcx;
//	delete []Fcy;
//	delete []Fcz;

//	for(iv=0;iv<NF_MAX;iv++)
//	{
//        delete []F0[iv];
//		delete []F1[iv];
//		delete []F2[iv];
//	}	
//	for(i=0;i<Point_Meshless;i++)
//	    for(ico=0;ico<6;ico++)
//        {
//		    delete []Csvd[i][ico];
//			delete []Csvd_Old[i][ico];
//		}

//	for(i=0;i<Point_Meshless;i++)
//	{
//	    delete []Csvd[i];
//		delete []Csvd_Old[i];
//	}		

//	delete []Csvd;
//	delete []Csvd_Old;

//	for(ico=0;ico<Point_Meshless;ico++)
//	{
//	    delete []Nb_Points[ico] ;
//		delete []Nb_Points_Old[ico];
//	}

//	delete []Nb_Points;
//	delete []Nb_Points_Old;
//	
//	delete []PointCategory;

//	if(ALE == 1)
//	{
//		delete []X_Meshless_Old;
//        delete []Y_Meshless_Old;
//        delete []Z_Meshless_Old;
//		delete []U_Ale;
//		delete []V_Ale;
//		delete []W_Ale;
//		delete []U_Ale_Old;
//		delete []V_Ale_Old;
//		delete []W_Ale_Old;
//	}
//}

void Grid()
{
    int i, j, k, ijk, Ns;
	double *Xs, *Yw, *Zb;
	double X0, X1, Exp1;
    double tmp;
	void Div(int, double, double, double, int, double*, double*);
	int offset, Nums;	
	Xs = new double [IPoint];
	Yw = new double [JPoint];
    Zb = new double [KPoint];		
	if (LPlot) 
    {
	    cout<<"   Generate Cartesian Grid...";
	}
	offset=0;
	readp>>Ns;
	readp>>X0;
	
	Fcx[0]=0;
    for(int i=0;i<Ns;i++)
	{
		readp>>X1>>Nums>>Exp1;
		Div(Nums, X0, X1, Exp1, offset, Xs, Fcx);
		X0=X1;
		offset+=Nums;
	}	
	offset=offset+1;
	if(offset!=IPoint) 
	{
	    cout<<endl<<"error! check the grid in X direction"<<endl;
		mfreeGlobal();
		exit(0);
	}
	
	offset=0;
	readp>>Ns;
	readp>>X0;
	Fcy[0]=0;
    for(int i=0;i<Ns;i++)
	{
		readp>>X1>>Nums>>Exp1;
		Div(Nums, X0, X1, Exp1, offset, Yw, Fcy);
		X0=X1;
		offset+=Nums;
	}
	offset=offset+1;
	if(offset!=JPoint) 
	{
	    cout<<endl<<"error! check the grid in Y direction"<<endl;
		mfreeGlobal();
		exit(0);
	}	

	offset=0;
	readp>>Ns;
	readp>>X0;
	Fcz[0]=0;
    for(int i=0;i<Ns;i++)
	{
		readp>>X1>>Nums>>Exp1;
		Div(Nums, X0, X1, Exp1, offset, Zb, Fcz);
		X0=X1;
		offset+=Nums;
	}
	offset=offset+1;
	if(offset!=KPoint) 
	{
	    cout<<endl<<"error! check the grid in Z direction"<<endl;
		mfreeGlobal();
		exit(0);
	}

	readp>>Isq>>Jsq>>Ksq>>NumIsq>>NumJsq>>NumKsq;
	readp>>Xsq>>Ysq>>Zsq>>XLsq>>YLsq>>ZLsq;

	readp.close();
	
	Deltx=XLsq/double(NumIsq);
	tmp=YLsq/double(NumJsq);
	if(fabs(Deltx-tmp)>1e-6) 
	{
	    cout<<endl<<"error, check square region"<<endl;
		mfreeGlobal();

		exit(0);
	}
	tmp=ZLsq/double(NumKsq);
	if(fabs(Deltx-tmp)>1e-6) 
	{
	    cout<<endl<<"error, check square region"<<endl;
		mfreeGlobal();
		exit(0);
	}
	
    sq_box.idx[0] = Isq;    sq_box.idx[1] = Jsq;    sq_box.idx[2] = Ksq;
    sq_box.dim[0] = NumIsq; sq_box.dim[1] = NumJsq; sq_box.dim[2] = NumKsq;
    sq_box.pos[0] = Xsq;    sq_box.pos[1] = Ysq;    sq_box.pos[2] = Zsq;
    sq_box.len[0] = XLsq;   sq_box.len[1] = YLsq;   sq_box.len[2] = ZLsq;
	sq_box.delta = Deltx;
	
	#pragma omp parallel for
    for(j=0;j<JPoint;j++) JIndex[j]=j*KPoint;
	
    #pragma omp parallel for
    for(i=0;i<IPoint;i++) IIndex[i]=i*KPoint*JPoint;
	
	#pragma omp parallel for private(j,k,ijk)
    for (i=0; i<IPoint; i++)
    for (j=0; j<JPoint; j++)
    for (k=0; k<KPoint; k++)
    {
        ijk=IIndex[i]+JIndex[j]+k;
        X[ijk]=Xs[i];
        Y[ijk]=Yw[j];
        Z[ijk]=Zb[k];
        
        if((i==0)||(j==0)||(k==0)||(i==(IPoint-1))||(j==(JPoint-1))||(k==(KPoint-1)))
        {
            if(   ((i==0||i==IPoint-1)&&(j==0)&&(0<=k)&&(k<=KPoint-1))||
               ((i==0||i==IPoint-1)&&(j==JPoint-1)&&(0<=k)&&(k<=KPoint-1))||
               ((i==0||i==IPoint-1)&&(1<=j)&&(j<=JPoint-2)&&(k==0))||
               ((i==0||i==IPoint-1)&&(1<=j)&&(j<=JPoint-2)&&(k==KPoint-1))||
               ((1<=i)&&(i<=IPoint-2)&&(j==0)&&(k==0))||
               ((1<=i)&&(i<=IPoint-2)&&(j==0)&&(k==KPoint-1))||
               ((1<=i)&&(i<=IPoint-2)&&(j==JPoint-1)&&(k==0))||
               ((1<=i)&&(i<=IPoint-2)&&(j==JPoint-1)&&(k==KPoint-1))	)
            {
                GType1[ijk] = GType::MUTE;
                GType2[ijk] = GType::MUTE;
            }
            else
            {
                GType1[ijk] = GType::OUTFLOW;
                GType2[ijk] = GType::OUTFLOW;
            }
        }
//#       ifdef UPWIND
//        else if ((i==1)||(j==1)||(k==1)||(i==(IPoint-2))||j==((JPoint-2))||k==((KPoint-2)))
//        {
//            GType1[ijk] = GType::LAYER_2;
//            GType2[ijk] = GType::LAYER_2;
//        }
//#       endif
        else
        {
            GType1[ijk] = GType::_7POINT;
            GType2[ijk] = GType::_7POINT;	
        }
    }
	
	Get_1DCoord_Info(Cord[0], IPoint, JPoint*KPoint, Xs);
	Get_1DCoord_Info(Cord[1], JPoint, KPoint, Yw);
	Get_1DCoord_Info(Cord[2], KPoint, 1, Zb);
	
    if (LPlot) 
    {
	    cout<<"Done!"<<endl;
	}
	delete []Xs;
	delete []Yw;
	delete []Zb;
    
//	Get_Meshless();
}

void Get_1DCoord_Info(CoordMember* coord, const int dim, const int offset, const double* Xs)
{
    for (int i=0; i<dim; i++)
    {
        coord[i].pos = Xs[i];
        unsigned int nb_id[3];
        double D1, D2;
        
        nb_id[0] = i;
        
        if (i == 0)

        {
            nb_id[1] = i + 1;
            nb_id[2] = i + 2;
        }
        else if (i == dim-1)
        {
            nb_id[1] = i - 1;
            nb_id[2] = i - 2;
        }
        else
        {
            nb_id[1] = i - 1;
            nb_id[2] = i + 1;
        }
        
        coord[i].offset[0] = nb_id[0] * offset;
        coord[i].offset[1] = nb_id[1] * offset;
        coord[i].offset[2] = nb_id[2] * offset;
        D1 = Xs[nb_id[1]] - Xs[nb_id[0]];
        D2 = Xs[nb_id[2]] - Xs[nb_id[0]];
        
        if (i == 0 || i == dim-1)
        {
            // coefficients used to compute bounday values, derived from (n.dudx = 0)
            coord[i].coef[0][0] = 0;
            coord[i].coef[0][1] = D2*D2 / ( D2*D2 - D1*D1 );
            coord[i].coef[0][2] = - D1*D1 / ( D2*D2 - D1*D1 );
        }
        
        // central difference coefficient for 1st order derivatives
        coord[i].coef[1][0] = -(D1 + D2 ) / D1 / D2;
        coord[i].coef[1][1] = D2 / D1 / (D2 - D1);
        coord[i].coef[1][2] = -D1 / D2 / (D2 - D1);
        
        // central difference coefficient for 2nd order derivatives
        coord[i].coef[2][0] = 2.0 / (D1 * D2);
        coord[i].coef[2][1] = -2.0 / D1 / (D2 - D1);
        coord[i].coef[2][2] = 2.0 / D2 / (D2 - D1);
    }
    
#ifdef  UPWIND
    for (int i=0; i<dim; i++)
    {
        unsigned int uw_id[2][3];
        double D1, D2;
        
        uw_id[0][0] = i;
        uw_id[1][0] = i;
        
        if (i == 0)
        {
            uw_id[0][1] = i + 1;
            uw_id[0][2] = i + 2;
            uw_id[1][1] = i + 1;
            uw_id[1][2] = i + 2;
        }
        else if (i == 1)
        {
            uw_id[0][1] = i - 1;
            uw_id[0][2] = i + 1;
            uw_id[1][1] = i + 1;
            uw_id[1][2] = i + 2;
        }
        else if (i == dim-1)
        {
            uw_id[0][1] = i - 1;
            uw_id[0][2] = i - 2;
            uw_id[1][1] = i - 1;
            uw_id[1][2] = i - 2;
        }
        else if (i == dim-2)
        {
            uw_id[0][1] = i - 1;
            uw_id[0][2] = i - 2;
            uw_id[1][1] = i + 1;
            uw_id[1][2] = i - 1;
        }
        else
        {
            uw_id[0][1] = i - 1;
            uw_id[0][2] = i - 2;
            uw_id[1][1] = i + 1;
            uw_id[1][2] = i + 2;
        }
        
        // upwind coefficient for 1st order derivatives when U >= 0        
        coord[i].offset_upwind[0][0] = uw_id[0][0] * offset;
        coord[i].offset_upwind[0][1] = uw_id[0][1] * offset;
        coord[i].offset_upwind[0][2] = uw_id[0][2] * offset;
        
        D1 = Xs[uw_id[0][1]] - Xs[uw_id[0][0]];
        D2 = Xs[uw_id[0][2]] - Xs[uw_id[0][0]];
        
        coord[i].coef_upwind[0][0] = -(D1 + D2) / D1 / D2;
        coord[i].coef_upwind[0][1] = D2 / D1 / (D2 - D1);
        coord[i].coef_upwind[0][2] = -D1 / D2 / (D2 - D1);
        
        // upwind coefficient for 1st order derivatives when U < 0        
        coord[i].offset_upwind[1][0] = uw_id[1][0] * offset;
        coord[i].offset_upwind[1][1] = uw_id[1][1] * offset;
        coord[i].offset_upwind[1][2] = uw_id[1][2] * offset;
        
        D1 = Xs[uw_id[1][1]] - Xs[uw_id[1][0]];
        D2 = Xs[uw_id[1][2]] - Xs[uw_id[1][0]];
        
        coord[i].coef_upwind[1][0] = -(D1 + D2) / D1 / D2;
        coord[i].coef_upwind[1][1] = D2 / D1 / (D2 - D1);
        coord[i].coef_upwind[1][2] = -D1 / D2 / (D2 - D1); 
    }
#endif
}

void Get_Meshless()
{
    if(LPlot) cout<<"   Generate Meshless Points...";
    
	int bcount,ocount;
	
	Point_Meshless=0;
	Total_Body_Nodes=0;
	Total_Outer_Nodes=0;
	
	for(int i=0;i<lab.obj_number;i++)
	{
		Rigid_Offset[i]=Point_Meshless;
		Body_Offset[i]=Total_Body_Nodes;
		Outer_Offset[i]=Total_Outer_Nodes;
		
		Point_Meshless += lab.rigid_body[i]->POINT_NUMBER;
		Total_Body_Nodes += lab.rigid_body[i]->INNER_POINT_NUMBER;
		Total_Outer_Nodes += lab.rigid_body[i]->OUTER_POINT_NUMBER;
	}
	
	BNI = new int [Total_Body_Nodes];
	ONI = new int [Total_Outer_Nodes];
	Surf_Ele_Area = new double [Total_Body_Nodes];
	
	bcount=0;ocount=0;
	
    for(int i=0;i<lab.obj_number;i++)
    {
		for(int j=0;j<lab.rigid_body[i]->POINT_NUMBER;j++)
		{
			X_Meshless[j+Rigid_Offset[i]]=lab.rigid_body[i]->XYZ[0][j];
			Y_Meshless[j+Rigid_Offset[i]]=lab.rigid_body[i]->XYZ[1][j];
			Z_Meshless[j+Rigid_Offset[i]]=lab.rigid_body[i]->XYZ[2][j];
			
			U_Ale[j+Rigid_Offset[i]]=lab.rigid_body[i]->UVW[0][j];
			V_Ale[j+Rigid_Offset[i]]=lab.rigid_body[i]->UVW[1][j];
			W_Ale[j+Rigid_Offset[i]]=lab.rigid_body[i]->UVW[2][j];
			
			ACCX_Ale[j+Rigid_Offset[i]]=lab.rigid_body[i]->ACC[0][j];
			ACCY_Ale[j+Rigid_Offset[i]]=lab.rigid_body[i]->ACC[1][j];
			ACCZ_Ale[j+Rigid_Offset[i]]=lab.rigid_body[i]->ACC[2][j];

			MNx[j+Rigid_Offset[i]]=lab.rigid_body[i]->OUTER_NORMAL_VECTOR[0][j];
			MNy[j+Rigid_Offset[i]]=lab.rigid_body[i]->OUTER_NORMAL_VECTOR[1][j];
			MNz[j+Rigid_Offset[i]]=lab.rigid_body[i]->OUTER_NORMAL_VECTOR[2][j];
			
			PointCategory[j+Rigid_Offset[i]]=i;
			
			if(lab.rigid_body[i]->INNERMARK[j]==1)
			{
				BNI[bcount]=j+Rigid_Offset[i];
				Surf_Ele_Area[bcount]=lab.rigid_body[i]->AREA[j];
				bcount++;
			}
			if(lab.rigid_body[i]->OUTERMARK[j]==2)
			{
				ONI[ocount]=j+Rigid_Offset[i];
				ocount++;
			}
			
		}
    }
    
    memset(Cld_List, 0, Point_Meshless*sizeof(MeshlessMember));
    
	for(int ijk=0;ijk<Point_Meshless;ijk++)
    {
        GType1[Point_Car+ijk]=3;
        GType2[Point_Car+ijk]=3;
        Cld_List[ijk].Meshless_Ind = ijk + Point_Car;
    }
    
    int i;
    //#pragma omp parallel for private(i)
    for(int ijk=0;ijk<Total_Body_Nodes;ijk++)
    {
        i=BNI[ijk]+Point_Car;
        BNI[ijk]=i;
        //cout<<Surf_Ele_Area[ijk]<<endl;
        //cout<<endl<<BNI[ijk];
        GType1[i]=5;
        GType2[i]=5;
    }
    //#pragma omp parallel for
    for(int ijk=0;ijk<Total_Outer_Nodes;ijk++) ONI[ijk]=ONI[ijk]+Point_Car;
    
    if(LPlot) cout<<"Done!"<<endl;
    if(LPlot) cout<<"Finish Generate Mesh"<<endl;
}





