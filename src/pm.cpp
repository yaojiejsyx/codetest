#include <iostream>
#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <vector>

using namespace std;

#include "../common/pm.h"
#include "../common/timer.h"
#include "../common/mesh_type.h"
#include "../common/write_result.h"

#define  NF_MAX 4

extern int    Point_All, Point_Car, Point_Meshless, IPoint, JPoint, KPoint, Total_Body_Nodes, AD, ALE, LPlot;
extern double Dt, Re, AD_Alfa, AD_Beta, Deltx;

extern char   *GType1, *GType2;
extern int    *IIndex, *JIndex, *BNI;
//extern double *Fcx, *Fcy, *Fcz;
extern double *F0[NF_MAX], *F1[NF_MAX], *F2[NF_MAX];
extern double *P, *U, *V, *W;
extern double *P_Old, *U_Old, *V_Old, *W_Old;
//extern double *DPX, *DPY, *DPZ, *DUX, *DUY, *DUZ;
extern double *So, *So_U, *So_V, *So_W, *Ustar, *Vstar, *Wstar;
extern double *MNx, *MNy, *MNz;

extern double Car_Ale[3], Car_ACC[3], *U_Ale, *V_Ale, *W_Ale, *ACCX_Ale, *ACCY_Ale, *ACCZ_Ale;
extern double Car_Ale_Old[3], *U_Ale_Old, *V_Ale_Old, *W_Ale_Old;

extern vector<MeshlessMember> C2M_List;
extern MeshlessMember* Cld_List;
//extern vector<MeshlessMember> C2M_List_Old;
//extern MeshlessMember* Cld_List_Old;
extern CoordMember* Cord[3];

extern double Res[NF_MAX];
extern int    IU, IV, IW, IP;

//void Memory_Free();

void computeSourceContributionNew(void);
void computeSource(void);
void updateVelocity(void);	
void calBoundaryUstar(void);
int  prepareCooMatrix(unsigned int* count_mark);
void setCooMatrixForCartNodes(double* coo_value, unsigned int* coo_row_idx, unsigned int* coo_col_idx, const unsigned int* count_mark);
void setCooMatrixForMLNodes(double* coo_value, unsigned int* coo_row_idx, unsigned int* coo_col_idx, const unsigned int* count_mark);

template <size_t DIM>
inline void Compute_Convection_Term(double res[DIM], double dudx[][DIM], double U_loc[DIM])
{
	// convection part
	// (U[i]-U_Ale[ip]-Car_Ale[0])*dudx + (V[i]-U_Ale[ip]-Car_Ale[1])*dudy + (W[i]-U_Ale[ip]-Car_Ale[2])*dudz);
	for (int dim=0; dim<DIM; dim++) 
	{
	    res[dim] = 0;
        for (int dim_j=0; dim_j<DIM; dim_j++) 
        {
            res[dim] += U_loc[dim_j] * dudx[dim][dim_j];
        }
    }
}

template <size_t DIM>
void Compute_Diffusion_Term(double res[DIM], double dudx2[][DIM], double dpdx[DIM], double dudx[][DIM], double Re, double Deltx, int type)
{
    // diffusion part
    
    double tmpx[3] = {0};
    double nu_sgs = 1.0e-10;
    
    // Using global variables: Re, AD, AD_alfa, AD_Beta, Deltx
    
    // Compute artificial diffusion and LES viscousity
#   ifdef USE_ART_DIFF
    for (int dim=0; dim<DIM; dim++) 
    {
        tmpx[dim] = AD_ALPHA*fabs(dpdx[dim]);
        if (tmpx[dim] > AD_BETA) tmpx[dim] = AD_BETA;
        tmpx[dim] = tmpx[dim]*Deltx*Deltx;
    }
#   endif //USE_ART_DIFF
            
#   ifdef USE_LES
    double S_mag = 0;
	
    for (int ii = 0; ii < DIM; ii++)
    for (int jj = 0; jj < DIM; jj++)
        S_mag += (dudx[ii][jj] + dudx[jj][ii])
	           * (dudx[ii][jj] + dudx[jj][ii]);
    
    S_mag *= 0.5;
    
    if (type == 1)
        nu_sgs = (LES_Cs*Deltx)*(LES_Cs*Deltx) * sqrt(S_mag);
    else
        nu_sgs = 0;
#   endif //USE_LES
    
	// (dudx2+dudy2+dudz2)*(1/Re+tmpx+nu_sgs)
	for (int dim=0; dim<DIM; dim++) 
	{
        res[dim] = 0;
        for (int dim_j=0; dim_j<DIM; dim_j++) 
        {
            res[dim] += dudx2[dim][dim_j];
        }
        res[dim] *= (1.0/Re + tmpx[dim] + nu_sgs);
    }
}

void computeSourceContributionOld()
{
    void calCarOld(int i, int j, int k);
    void calMLOld(MeshlessMember*, int);
    
//    memset(So_U, 0, Point_All*sizeof(double));
//    memset(So_V, 0, Point_All*sizeof(double));
//    memset(So_W, 0, Point_All*sizeof(double));
    
#   pragma omp parallel for
    for (int i=0; i<IPoint; i++) 
    for (int j=0; j<JPoint; j++) 
    for (int k=0; k<KPoint; k++) 
    {
        int ijk = IIndex[i]+JIndex[j]+k;
        if ( GType1[ijk] == 1 ) 
        {
            calCarOld(i, j, k);
        }
        else
        {
            So_U[ijk] = So_V[ijk] = So_W[ijk] = 0;
        }
    }
    
#   pragma omp parallel for
    for (int s=0; s<Point_Meshless; s++) 
    {
        int ijk = Cld_List[s].Meshless_Ind;
        if (GType1[ijk] == 3)
        {
            calMLOld(&Cld_List[s], 1);
        }
    }

#   pragma omp parallel for
    for (int s=0; s<C2M_List.size(); s++)
    {
        int ijk = C2M_List[s].Meshless_Ind;
	    if (GType2[ijk] != 0 && GType1[ijk] == 3)
	    {
	        calMLOld(&C2M_List[s], 2);
        }
    }
}

void calCarOld(int i, int j, int k)
{
    // index range: 1 ~ IPoint-2
    
    int ijk;
//        double dudxv[3][3] = { { dudx, dudy, dudz }, 
//                               { dvdx, dvdy, dvdz },
//                               { dwdx, dwdy, dwdz } };
    double dpdx[3]     = {0};
    double dudx[3][3]  = {0};
    double dudx2[3][3] = {0};
    double conv[3], diff[3];
    
    int id, ijk_w, ijk_e;
    double coef_p, coef_w, coef_e;
    
    ijk = IIndex[i] + JIndex[j] + k;
    
    for (int dim=0; dim<3; dim++)
    {
        switch (dim)
        {
          case 0:
            id = i; break;
          case 1:
            id = j; break;
          case 2:
            id = k; break;
          default:
            printf("dimension error!\n");
            continue;
        }
        
        ijk_w = ijk - Cord[dim][id].offset[0] + Cord[dim][id].offset[1];
        ijk_e = ijk - Cord[dim][id].offset[0] + Cord[dim][id].offset[2];
        
        // 1st order difference coefficient
        coef_w = Cord[dim][id].coef[1][1];
        coef_p = Cord[dim][id].coef[1][0];
        coef_e = Cord[dim][id].coef[1][2];
        
        dpdx[dim] = P_Old[ijk_w]*coef_w + P_Old[ijk]*coef_p + P_Old[ijk_e]*coef_e;
        
        dudx[0][dim] = U_Old[ijk_w]*coef_w + U_Old[ijk]*coef_p + U_Old[ijk_e]*coef_e;
        dudx[1][dim] = V_Old[ijk_w]*coef_w + V_Old[ijk]*coef_p + V_Old[ijk_e]*coef_e;
        dudx[2][dim] = W_Old[ijk_w]*coef_w + W_Old[ijk]*coef_p + W_Old[ijk_e]*coef_e;
        
        // 2st order difference coefficient
        coef_w = Cord[dim][id].coef[2][1];
        coef_p = Cord[dim][id].coef[2][0];
        coef_e = Cord[dim][id].coef[2][2];
        
        dudx2[0][dim] = U_Old[ijk_w]*coef_w + U_Old[ijk]*coef_p + U_Old[ijk_e]*coef_e;
        dudx2[1][dim] = V_Old[ijk_w]*coef_w + V_Old[ijk]*coef_p + V_Old[ijk_e]*coef_e;
        dudx2[2][dim] = W_Old[ijk_w]*coef_w + W_Old[ijk]*coef_p + W_Old[ijk_e]*coef_e;
    }
    
#ifdef UPWIND
    double vel = 0, coef_uw[3] = {0};
    int ijk_uw[3] = {0};
    if ((i==1)||(j==1)||(k==1)||(i==(IPoint-2))||j==((JPoint-2))||k==((KPoint-2)))
    {}
    else
    {
    for (int dim=0; dim<3; dim++)
    {
        switch (dim)
        {
          case 0:
            id = i; vel = U_Old[ijk]; break;
          case 1:
            id = j; vel = V_Old[ijk]; break;
          case 2:
            id = k; vel = W_Old[ijk]; break;
          default:
            printf("dimension error!\n");
            continue;
        }
        
        if (vel >= 0)
        {
            for (int i_nb = 0; i_nb<3; i_nb++)
            {
                ijk_uw[i_nb] = ijk - Cord[dim][id].offset_upwind[0][0] + Cord[dim][id].offset_upwind[0][i_nb];
                coef_uw[i_nb] = Cord[dim][id].coef_upwind[0][i_nb];
            }
        }
        else
        {
            for (int i_nb = 0; i_nb<3; i_nb++)
            {
                ijk_uw[i_nb] = ijk - Cord[dim][id].offset_upwind[1][0] + Cord[dim][id].offset_upwind[1][i_nb];
                coef_uw[i_nb] = Cord[dim][id].coef_upwind[1][i_nb];
            }
        }
        
        dudx[0][dim] = U_Old[ijk_uw[0]]*coef_uw[0] + U_Old[ijk_uw[1]]*coef_uw[1] + U_Old[ijk_uw[2]]*coef_uw[2];
        dudx[1][dim] = V_Old[ijk_uw[0]]*coef_uw[0] + V_Old[ijk_uw[1]]*coef_uw[1] + V_Old[ijk_uw[2]]*coef_uw[2];
        dudx[2][dim] = W_Old[ijk_uw[0]]*coef_uw[0] + W_Old[ijk_uw[1]]*coef_uw[1] + W_Old[ijk_uw[2]]*coef_uw[2];
    }
    }
#endif
    
	// currently ALE = TRUE
	double U_loc[3] = { U_Old[ijk]-Car_Ale_Old[0],
	                    V_Old[ijk]-Car_Ale_Old[1],
	                    W_Old[ijk]-Car_Ale_Old[2] };
    Compute_Convection_Term <3> (conv, dudx, U_loc);
	
    Compute_Diffusion_Term  <3> (diff, dudx2, dpdx, dudx, Re, Deltx, 1);

    So_U[ijk] = 0.5*Dt*(diff[0] - conv[0] - dpdx[0]) + U_Old[ijk];
    So_V[ijk] = 0.5*Dt*(diff[1] - conv[1] - dpdx[1]) + V_Old[ijk];
    So_W[ijk] = 0.5*Dt*(diff[2] - conv[2] - dpdx[2]) + W_Old[ijk];
}

void calMLOld(MeshlessMember* Current_Meshless_Member, int mls_type)
{
	int i, j;
//        double dudxv[3][3] = { { dudx, dudy, dudz }, 
//                               { dvdx, dvdy, dvdz },
//                               { dwdx, dwdy, dwdz } };
	double dpdx[3]     = {0};
	double dudx[3][3]  = {0};
	double dudx2[3][3] = {0};
    double conv[3], diff[3];
	
	i = Current_Meshless_Member->Meshless_Ind;

    for (int dim=0; dim<3; dim++) 
    {
	    for (int io=0; io<NB; io++) 
	    {
            j = Current_Meshless_Member->Nb_Points[io];
            
            dpdx[dim] += Current_Meshless_Member->Csvd[dim][io]*(P_Old[j]-P_Old[i]);
            
            dudx[0][dim] += Current_Meshless_Member->Csvd[dim][io]*(U_Old[j]-U_Old[i]);
            dudx[1][dim] += Current_Meshless_Member->Csvd[dim][io]*(V_Old[j]-V_Old[i]);
            dudx[2][dim] += Current_Meshless_Member->Csvd[dim][io]*(W_Old[j]-W_Old[i]);
            
            dudx2[0][dim] += Current_Meshless_Member->Csvd[dim+3][io]*(U_Old[j]-U_Old[i]);
            dudx2[1][dim] += Current_Meshless_Member->Csvd[dim+3][io]*(V_Old[j]-V_Old[i]);
            dudx2[2][dim] += Current_Meshless_Member->Csvd[dim+3][io]*(W_Old[j]-W_Old[i]);
        }
	}
    
	// currently ALE = TRUE
	double U_loc[3] = {0};
    switch (mls_type)
    {
      case 1:
        U_loc[0] = U_Old[i] - U_Ale_Old[i - Point_Car] - Car_Ale_Old[0];
        U_loc[1] = V_Old[i] - V_Ale_Old[i - Point_Car] - Car_Ale_Old[1];
        U_loc[2] = W_Old[i] - W_Ale_Old[i - Point_Car] - Car_Ale_Old[2];
        break;
      case 2:
        U_loc[0] = U_Old[i] - Car_Ale_Old[0];
        U_loc[1] = V_Old[i] - Car_Ale_Old[1];
        U_loc[2] = W_Old[i] - Car_Ale_Old[2];
        break;
    }
    Compute_Convection_Term <3> (conv, dudx, U_loc);
	
    Compute_Diffusion_Term  <3> (diff, dudx2, dpdx, dudx, Re, Deltx, 1);

//  So_U[i]=U_Old[i]+0.5*Dt*((dudx2+dudy2+dudz2)*(1/Re+tmpx+nu_sgs)-(U_Old[i]-Car_Ale_Old[0])*dudx-(V_Old[i]-Car_Ale_Old[1])*dudy-(W_Old[i]-Car_Ale_Old[2])*dudz-dpdx);
    So_U[i] = 0.5*Dt*(diff[0] - conv[0] - dpdx[0]) + U_Old[i];
    So_V[i] = 0.5*Dt*(diff[1] - conv[1] - dpdx[1]) + V_Old[i];
    So_W[i] = 0.5*Dt*(diff[2] - conv[2] - dpdx[2]) + W_Old[i];
}

void doProjectionMethod0(int it, const int PM_Iter)
{
	int  ijk, iv;
	
//	memset(So, 0, Point_All*sizeof(double));
	Timer pre_timer; pre_timer.Start();
	
    int nnz = 0;
    unsigned int* count_mark = NULL;
    unsigned int* coo_row_idx = NULL;
	unsigned int* coo_col_idx = NULL;
	double*       coo_value = NULL;
    
    count_mark = new unsigned int [Point_All+1];
    
    nnz = prepareCooMatrix(count_mark);
    
    coo_value   = new double [nnz];
    coo_row_idx = new unsigned int [nnz];
    coo_col_idx = new unsigned int [nnz];
    
    setCooMatrixForCartNodes(coo_value, coo_row_idx, coo_col_idx, count_mark);
    setCooMatrixForMLNodes(coo_value, coo_row_idx, coo_col_idx, count_mark);
    
    pre_timer.Stop();  cout << "prep time: " << pre_timer.Elapsed() << "ms | " << endl;
    
	for (int ifs=0; ifs<PM_Iter; ifs++)
	{
	    Timer pm_timer, bicg_timer;
	    pm_timer.Start();
	    
		computeSourceContributionNew();
		
		calBoundaryUstar();
        
        computeSource();
        
        double eps = 10;
        int maxit  = 5;
        int info   = 0;
        bicg_timer.Start();
        Res[IP] = bicgstab_solver(Point_All, P, So, count_mark, 
                nnz, coo_value, coo_row_idx, coo_col_idx, maxit, eps, info);
	    bicg_timer.Stop();  cout << "bicg time: " << bicg_timer.Elapsed() << "ms | ";
        
        updateVelocity();
	    
		pm_timer.Stop();
		if(LPlot) cout<<"PM iteration time: "<<pm_timer.Elapsed()<<"ms | ";
        if(LPlot) cout<<"Res_P: "<<Res[IP]/sqrt(Point_All)<<" "<<endl;
        
		if (Res[IU]>1e16 || Res[IV]>1e16 || Res[IW]>1e16 || Res[IP]>1e16)
        {
            cout<<"Divergence!!! Code is terminated"<<endl;
//			Memory_Free();
            exit(0);
        }
	}
	
	delete[] coo_row_idx; coo_row_idx = NULL;
    delete[] coo_col_idx; coo_col_idx = NULL;
    delete[] coo_value;   coo_value = NULL;
    delete[] count_mark;  count_mark = NULL;
}

void doProjectionMethod(int it, const int PM_Iter)
{
    void cmpPoissonEq(int n, double* xi, double* Axi);
    
	int  ijk, iv;
	
//	memset(So, 0, Point_All*sizeof(double));
	
	for (int ifs=0; ifs<PM_Iter; ifs++)
	{
	    Timer pm_timer, bicg_timer;
	    pm_timer.Start();
	    
		computeSourceContributionNew();
		
		calBoundaryUstar();
        
        computeSource();
        
        double eps = 10;
        int maxit  = 5;
        int info   = 0;
        bicg_timer.Start();
        Res[IP] = bicgstab_solver2(Point_All, &cmpPoissonEq, P, So, maxit, eps, info);
	    bicg_timer.Stop();  cout << "bicg time: " << bicg_timer.Elapsed() << "ms | ";
        
        updateVelocity();
	    
		pm_timer.Stop();
		if(LPlot) cout<<"PM iteration time: "<<pm_timer.Elapsed()<<"ms | ";
        if(LPlot) cout<<"Res_P: "<<Res[IP]/sqrt(Point_All)<<" "<<endl;
        
		if (Res[IU]>1e16 || Res[IV]>1e16 || Res[IW]>1e16 || Res[IP]>1e16)
        {
            Write_Result_DIV(it);
            cout<<"Divergence!!! Code is terminated"<<endl;
//			Memory_Free();
            exit(0);
        }
	}
}

void calBoundaryUstar()
{
    int i, j, k, ijk,ip;	
	#pragma omp parallel for private(k, ijk)
	for(j=1;j<JPoint-1;j++)
	{
	    for(k=1;k<KPoint-1;k++)
		{
		    //West
			ijk=IIndex[0]+JIndex[j]+k;
			Ustar[ijk]=U[ijk];
			Vstar[ijk]=V[ijk];
			Wstar[ijk]=W[ijk];
			
			//East
			ijk=IIndex[IPoint-1]+JIndex[j]+k;
			Ustar[ijk]=U[ijk];
			Vstar[ijk]=V[ijk];
			Wstar[ijk]=W[ijk];
		}
	}
	#pragma omp parallel for private(k, ijk)
	for(i=1;i<IPoint-1;i++)
	{
	    for(k=1;k<KPoint-1;k++)
		{
		    //South
			ijk=IIndex[i]+JIndex[0]+k;
			Ustar[ijk]=U[ijk];
			Vstar[ijk]=V[ijk];
			Wstar[ijk]=W[ijk];
			
			//North
			ijk=IIndex[i]+JIndex[JPoint-1]+k;
			Ustar[ijk]=U[ijk];
			Vstar[ijk]=V[ijk];
			Wstar[ijk]=W[ijk];
		}
	}
	#pragma omp parallel for private(j, ijk)
	for(i=1;i<IPoint-1;i++)
	{
	    for(j=1;j<JPoint-1;j++)
		{
		    //Bottom
			ijk=IIndex[i]+JIndex[j]+0;
			Ustar[ijk]=U[ijk];
			Vstar[ijk]=V[ijk];
			Wstar[ijk]=W[ijk];
			
			//Top
			ijk=IIndex[i]+JIndex[j]+KPoint-1;
			Ustar[ijk]=U[ijk];
			Vstar[ijk]=V[ijk];
			Wstar[ijk]=W[ijk];
		}
	}
   
	
	#pragma omp parallel for private (i, ip)
	for(ijk=0;ijk<Total_Body_Nodes;ijk++)
	{
		i=BNI[ijk];
		ip=i-Point_Car;
		Ustar[i]=U_Ale[ip]+Car_Ale[0]-0.5*Dt*(ACCX_Ale[ip]+Car_ACC[0]);
		Vstar[i]=V_Ale[ip]+Car_Ale[1]-0.5*Dt*(ACCY_Ale[ip]+Car_ACC[1]);
		Wstar[i]=W_Ale[ip]+Car_Ale[2]-0.5*Dt*(ACCZ_Ale[ip]+Car_ACC[2]);

	}
	
}

void computeSourceContributionNew()
{
    void calCarNew(int i, int j, int k);
    void calMLNew(MeshlessMember*, int);
    
#   pragma omp parallel for
    for (int i=1; i<IPoint-1; i++) 
    for (int j=1; j<JPoint-1; j++) 
    for (int k=1; k<KPoint-1; k++) 
    {
        int ijk = IIndex[i]+JIndex[j]+k;
        if ( GType2[ijk] == 1 ) 
        {
            calCarNew(i, j, k);
        }
        else if (GType2[ijk] == GType::INACTIVE)
        {
            U[ijk] = V[ijk] = W[ijk] = 0;
            P[ijk] = 0;
        }
    }
    
#   pragma omp parallel for
    for (int s=0; s<Point_Meshless; s++) 
    {
        int ijk = Cld_List[s].Meshless_Ind;
	    if (GType2[ijk] == 3)
	    {
	        calMLNew(&Cld_List[s], 1);
        }
        else if (GType2[ijk] == GType::INACTIVE)
        {
            U[ijk] = V[ijk] = W[ijk] = 0;
            P[ijk] = 0;
        }
    }

#   pragma omp parallel for
    for (int s=0; s<C2M_List.size(); s++)
    {
        int ijk = C2M_List[s].Meshless_Ind;
	    if (GType2[ijk] == 3)
	    {
	        calMLNew(&C2M_List[s], 2);
        }
        else if (GType2[ijk] == GType::INACTIVE)
        {
            U[ijk] = V[ijk] = W[ijk] = 0;
            P[ijk] = 0;
        }
    }
}

void calCarNew(int i, int j, int k)
{
    // index range: 1 ~ IPoint-2
    
    int ijk;
//        double dudxv[3][3] = { { dudx, dudy, dudz }, 
//                               { dvdx, dvdy, dvdz },
//                               { dwdx, dwdy, dwdz } };
    double dpdx[3]     = {0};
    double dudx[3][3]  = {0};
    double dudx2[3][3] = {0};
    double conv[3], diff[3];
    
    int id, ijk_w, ijk_e;
    double coef_p, coef_w, coef_e;
    
    ijk = IIndex[i] + JIndex[j] + k;
    
    for (int dim=0; dim<3; dim++)
    {
        switch (dim)
        {
          case 0:
            id = i; break;
          case 1:
            id = j; break;
          case 2:
            id = k; break;
          default:
            printf("dimension error!\n");
            continue;
        }
        
        ijk_w = ijk - Cord[dim][id].offset[0] + Cord[dim][id].offset[1];
        ijk_e = ijk - Cord[dim][id].offset[0] + Cord[dim][id].offset[2];
        
        // 1st order difference coefficient
        coef_w = Cord[dim][id].coef[1][1];
        coef_p = Cord[dim][id].coef[1][0];
        coef_e = Cord[dim][id].coef[1][2];
        
        dpdx[dim] = P[ijk_w]*coef_w + P[ijk]*coef_p + P[ijk_e]*coef_e;
        
        dudx[0][dim] = U[ijk_w]*coef_w + U[ijk]*coef_p + U[ijk_e]*coef_e;
        dudx[1][dim] = V[ijk_w]*coef_w + V[ijk]*coef_p + V[ijk_e]*coef_e;
        dudx[2][dim] = W[ijk_w]*coef_w + W[ijk]*coef_p + W[ijk_e]*coef_e;
        
        // 2st order difference coefficient
        coef_w = Cord[dim][id].coef[2][1];
        coef_p = Cord[dim][id].coef[2][0];
        coef_e = Cord[dim][id].coef[2][2];
        
        dudx2[0][dim] = U[ijk_w]*coef_w + U[ijk]*coef_p + U[ijk_e]*coef_e;
        dudx2[1][dim] = V[ijk_w]*coef_w + V[ijk]*coef_p + V[ijk_e]*coef_e;
        dudx2[2][dim] = W[ijk_w]*coef_w + W[ijk]*coef_p + W[ijk_e]*coef_e;
    }
    
//#ifdef UPWIND
//    double vel = 0, coef_uw[3] = {0};
//    int ijk_uw[3] = {0};
//    if ((i==1)||(j==1)||(k==1)||(i==(IPoint-2))||j==((JPoint-2))||k==((KPoint-2)))
//    {}
//    else
//    {
//    for (int dim=0; dim<3; dim++)
//    {
//        switch (dim)
//        {
//          case 0:
//            id = i; vel = U[ijk]; break;
//          case 1:
//            id = j; vel = V[ijk]; break;
//          case 2:
//            id = k; vel = W[ijk]; break;
//          default:
//            printf("dimension error!\n");
//            continue;
//        }
//        
//        if (vel >= 0)
//        {
//            for (int i_nb = 0; i_nb<3; i_nb++)
//            {
//                ijk_uw[i_nb] = ijk - Cord[dim][id].offset_upwind[0][0] + Cord[dim][id].offset_upwind[0][i_nb];
//                coef_uw[i_nb] = Cord[dim][id].coef_upwind[0][i_nb];
//            }
//        }
//        else
//        {
//            for (int i_nb = 0; i_nb<3; i_nb++)
//            {
//                ijk_uw[i_nb] = ijk - Cord[dim][id].offset_upwind[1][0] + Cord[dim][id].offset_upwind[1][i_nb];
//                coef_uw[i_nb] = Cord[dim][id].coef_upwind[1][i_nb];
//            }
//        }
//        
//        dudx[0][dim] = U[ijk_uw[0]]*coef_uw[0] + U[ijk_uw[1]]*coef_uw[1] + U[ijk_uw[2]]*coef_uw[2];
//        dudx[1][dim] = V[ijk_uw[0]]*coef_uw[0] + V[ijk_uw[1]]*coef_uw[1] + V[ijk_uw[2]]*coef_uw[2];
//        dudx[2][dim] = W[ijk_uw[0]]*coef_uw[0] + W[ijk_uw[1]]*coef_uw[1] + W[ijk_uw[2]]*coef_uw[2];
//    }
//    }
//#endif
    
	// currently ALE = TRUE
	double U_loc[3] = { U[ijk] - Car_Ale[0],
	                    V[ijk] - Car_Ale[1],
	                    W[ijk] - Car_Ale[2] };
    Compute_Convection_Term <3> (conv, dudx, U_loc);
	
    Compute_Diffusion_Term  <3> (diff, dudx2, dpdx, dudx, Re, Deltx, 1);

//		Ustar[i]=0.5*Dt* ( (dudx2+dudy2+dudz2)*(1/Re+tmpx+nu_sgs)
//						 - (U[i]-U_Ale[ip]-Car_Ale[0])*dudx
//						 - (V[i]-V_Ale[ip]-Car_Ale[1])*dudy
//						 - (W[i]-W_Ale[ip]-Car_Ale[2])*dudz )
//						 + So_U[i];
    Ustar[ijk] = 0.5*Dt*(diff[0] - conv[0]) + So_U[ijk];
    Vstar[ijk] = 0.5*Dt*(diff[1] - conv[1]) + So_V[ijk];
    Wstar[ijk] = 0.5*Dt*(diff[2] - conv[2]) + So_W[ijk];
}

void calMLNew(MeshlessMember* Current_Meshless_Member, int mls_type)
{
	int i, j;
//        double dudxv[3][3] = { { dudx, dudy, dudz }, 
//                               { dvdx, dvdy, dvdz },
//                               { dwdx, dwdy, dwdz } };
	double dpdx[3]     = {0};
	double dudx[3][3]  = {0};
	double dudx2[3][3] = {0};
    double conv[3], diff[3];
	
	i = Current_Meshless_Member->Meshless_Ind;

    for (int dim=0; dim<3; dim++) 
    {
	    for (int io=0; io<NB; io++) 
	    {
            j = Current_Meshless_Member->Nb_Points[io];
            
            dpdx[dim] += Current_Meshless_Member->Csvd[dim][io]*(P[j]-P[i]);
            
            dudx[0][dim] += Current_Meshless_Member->Csvd[dim][io]*(U[j]-U[i]);
            dudx[1][dim] += Current_Meshless_Member->Csvd[dim][io]*(V[j]-V[i]);
            dudx[2][dim] += Current_Meshless_Member->Csvd[dim][io]*(W[j]-W[i]);
            
            dudx2[0][dim] += Current_Meshless_Member->Csvd[dim+3][io]*(U[j]-U[i]);
            dudx2[1][dim] += Current_Meshless_Member->Csvd[dim+3][io]*(V[j]-V[i]);
            dudx2[2][dim] += Current_Meshless_Member->Csvd[dim+3][io]*(W[j]-W[i]);
        }
	}
    
	// currently ALE = TRUE
	double U_loc[3] = {0};
    switch (mls_type)
    {
      case 1:
        U_loc[0] = U[i] - U_Ale[i - Point_Car] - Car_Ale[0];
        U_loc[1] = V[i] - V_Ale[i - Point_Car] - Car_Ale[1];
        U_loc[2] = W[i] - W_Ale[i - Point_Car] - Car_Ale[2];
        break;
      case 2:
        U_loc[0] = U[i] - Car_Ale[0];
        U_loc[1] = V[i] - Car_Ale[1];
        U_loc[2] = W[i] - Car_Ale[2];
        break;
    }
    Compute_Convection_Term <3> (conv, dudx, U_loc);
	
    Compute_Diffusion_Term  <3> (diff, dudx2, dpdx, dudx, Re, Deltx, 1);

//  Ustar[i]=0.5*Dt*((dudx2+dudy2+dudz2)*(1/Re+tmpx+nu_sgs)-(U[i]-Car_Ale[0])*dudx-(V[i]-Car_Ale[1])*dudy-(W[i]-Car_Ale[2])*dudz)+So_U[i];
    Ustar[i] = 0.5*Dt*(diff[0] - conv[0]) + So_U[i];
    Vstar[i] = 0.5*Dt*(diff[1] - conv[1]) + So_V[i];
    Wstar[i] = 0.5*Dt*(diff[2] - conv[2]) + So_W[i];
}

void computeSource()
{
#   pragma omp parallel for
    for (int i=0; i<IPoint; i++)
    for (int j=0; j<JPoint; j++)
    for (int k=0; k<KPoint; k++)
    {
        int ijk = IIndex[i] + JIndex[j] + k;
        
        if (GType2[ijk] == GType::_7POINT)
        {
            int ijk_w, ijk_e;
            
            So[ijk] = 0;
            
            ijk_w = ijk - Cord[0][i].offset[0] + Cord[0][i].offset[1];
            ijk_e = ijk - Cord[0][i].offset[0] + Cord[0][i].offset[2];
            So[ijk] += Ustar[ijk_w] * Cord[0][i].coef[1][1]
                     + Ustar[ijk]   * Cord[0][i].coef[1][0]
                     + Ustar[ijk_e] * Cord[0][i].coef[1][2];
            
            ijk_w = ijk - Cord[1][j].offset[0] + Cord[1][j].offset[1];
            ijk_e = ijk - Cord[1][j].offset[0] + Cord[1][j].offset[2];
            So[ijk] += Vstar[ijk_w] * Cord[1][j].coef[1][1]
                     + Vstar[ijk]   * Cord[1][j].coef[1][0]
                     + Vstar[ijk_e] * Cord[1][j].coef[1][2];
            
            ijk_w = ijk - Cord[2][k].offset[0] + Cord[2][k].offset[1];
            ijk_e = ijk - Cord[2][k].offset[0] + Cord[2][k].offset[2];
            So[ijk] += Wstar[ijk_w] * Cord[2][k].coef[1][1]
                     + Wstar[ijk]   * Cord[2][k].coef[1][0]
                     + Wstar[ijk_e] * Cord[2][k].coef[1][2];
            
            So[ijk] = So[ijk] * (2.0/Dt);
        }
        else
        {
            So[ijk] = 0;
        }
    }
    
#   pragma omp parallel for
	for (int s=0; s<Point_Meshless; s++)
	{
		int ijk = Cld_List[s].Meshless_Ind;
		
		if (GType2[ijk] == GType::_GFDSVD)
		{
		    So[ijk] = 0;
			for (int io=0; io<NB; io++)
			{
				int j = Cld_List[s].Nb_Points[io];

				So[ijk] += Cld_List[s].Csvd[0][io] * (Ustar[j] - Ustar[ijk])
			                + Cld_List[s].Csvd[1][io] * (Vstar[j] - Vstar[ijk])
			                + Cld_List[s].Csvd[2][io] * (Wstar[j] - Wstar[ijk]);
            }
            
            So[ijk] = So[ijk] * (2.0/Dt);
        }
		else if (GType2[ijk] == GType::WALL)
		{
            So[ijk] = -(MNx[s] * (ACCX_Ale[s] + Car_ACC[0])
                        + MNy[s] * (ACCY_Ale[s] + Car_ACC[1])
                        + MNz[s] * (ACCZ_Ale[s] + Car_ACC[2]) );
		}
		else
		{
		    So[ijk] = 0;
			cout << "Wrong type in meshless point!" << endl;
		}
	}
	
#   pragma omp parallel for
    for (int s=0; s<C2M_List.size(); s++)
    {
        int ijk = C2M_List[s].Meshless_Ind;
        
        if (GType2[ijk] == GType::MOBILIZED)
        {
            So[ijk] = 0;
            for (int io=0; io<NB; io++)
            {
                int j = C2M_List[s].Nb_Points[io];
                
                So[ijk] += C2M_List[s].Csvd[0][io] * (Ustar[j] - Ustar[ijk])
                            + C2M_List[s].Csvd[1][io] * (Vstar[j] - Vstar[ijk])
                            + C2M_List[s].Csvd[2][io] * (Wstar[j] - Wstar[ijk]);
            }
            
            So[ijk] = So[ijk] * (2.0/Dt);
        }
        else
        {
            So[ijk] = 0;
        }
    }
}

void updateVelocity()
{
#   pragma omp parallel for
    for (int i=0; i<IPoint; i++)
    for (int j=0; j<JPoint; j++)
    for (int k=0; k<KPoint; k++)
    {
        int ijk=IIndex[i]+JIndex[j]+k;
        
        if (GType2[ijk] == GType::_7POINT) 
        {
        	double dpdx[3] = {0};
            int id, id_w, id_e, ijk_w, ijk_e;
            double coef_p, coef_w, coef_e;
            
            for (int dim=0; dim<3; dim++)
            {
                switch (dim)
                {
                  case 0:
                    id = i; break;
                  case 1:
                    id = j; break;
                  case 2:
                    id = k; break;
                  default:
                    cout << "dimension error!" << endl;
                }
                
                id_w = id - 1;
                id_e = id + 1;
                
                ijk_w = ijk - Cord[dim][id].offset[0] + Cord[dim][id].offset[1];
                ijk_e = ijk - Cord[dim][id].offset[0] + Cord[dim][id].offset[2];
                
                // 1st order difference coefficient
                coef_p = Cord[dim][id].coef[1][0];
                coef_w = Cord[dim][id].coef[1][1];
                coef_e = Cord[dim][id].coef[1][2];
                
                dpdx[dim] = P[ijk_w]*coef_w + P[ijk]*coef_p + P[ijk_e]*coef_e;
            }
            
            U[ijk] = Ustar[ijk] - 0.5*Dt*dpdx[0];
            V[ijk] = Vstar[ijk] - 0.5*Dt*dpdx[1];
            W[ijk] = Wstar[ijk] - 0.5*Dt*dpdx[2];
        }
    }
    
#   pragma omp parallel for
	for (int ip=0; ip<Point_Meshless; ip++)
	{
		int i = Cld_List[ip].Meshless_Ind;
		int j;
		double dpdx, dpdy, dpdz, dps;
				
		if (GType2[i] == 3)
		{
			dpdx=0;
			dpdy=0;
			dpdz=0;
			
			for (int io=0; io<NB; io++)
			{
				j =  Cld_List[ip].Nb_Points[io];
				
				dps = (P[j] - P[i]);
				
				dpdx +=  Cld_List[ip].Csvd[0][io]*dps;
				dpdy +=  Cld_List[ip].Csvd[1][io]*dps;
				dpdz +=  Cld_List[ip].Csvd[2][io]*dps;
			}
			
			U[i]=Ustar[i]-0.5*Dt*dpdx;
			V[i]=Vstar[i]-0.5*Dt*dpdy;
			W[i]=Wstar[i]-0.5*Dt*dpdz;
		}
	}
    
#   pragma omp parallel for
    for (int s=0; s<C2M_List.size(); s++)
    {
        int i, j;
	    double dpdx, dpdy, dpdz, dps;
	    
	    i = C2M_List[s].Meshless_Ind;
	    
        if (GType2[i] == 3)
        {
	        dpdx=0;
	        dpdy=0;
	        dpdz=0;
	        		
	        for (int io=0; io<NB; io++)
	        {
		        j = C2M_List[s].Nb_Points[io];
                
		        dps = (P[j]-P[i]);
		        dpdx += C2M_List[s].Csvd[0][io]*dps;
		        dpdy += C2M_List[s].Csvd[1][io]*dps;
		        dpdz += C2M_List[s].Csvd[2][io]*dps;
	        }
	        
	        U[i]=Ustar[i]-0.5*Dt*dpdx;
	        V[i]=Vstar[i]-0.5*Dt*dpdy;
	        W[i]=Wstar[i]-0.5*Dt*dpdz;
        }
    }
    
    calBoundaryUVW();
}

void reset(double* FI, int range)
{
    int istart, iend;
	if(range==0)
    {
	    istart=0;
		iend=Point_All;
    }
	
	if(range==1)
	{
	    istart=0;
		iend=Point_Car;
	}
	if(range==2)
	{

	    istart=Point_Car;
		iend=Point_All;
	}
	if(range==3)
	{
	    istart=0;
		iend=Point_Meshless;
	}
	//#pragma omp parallel for
	for(int ijk=istart;ijk<iend;ijk++) FI[ijk]=0;
}

void residual()
{
    int iv, ijk;
	double temp;
	for(iv=0;iv<NF_MAX;iv++) 
	{
	    if(iv!=IP) Res[iv]=0;
	}
	
    for(iv=0;iv<NF_MAX;iv++)
	{
	    temp=0;
		if(iv!=IP)
		{
		    //#pragma omp parallel for shared (iv) reduction(+: temp)
		    for(ijk=0;ijk<Point_All;ijk++)	    	    
            {
		        temp=temp+(F2[iv][ijk]-F0[iv][ijk])*(F2[iv][ijk]-F0[iv][ijk]);
            }
			Res[iv]=temp;
		}
	}
	
    for(iv=0;iv<NF_MAX;iv++) 
	{
	    if(iv!=IP) Res[iv]=sqrt(Res[iv]/double(Point_All));
	}
}

void calBoundaryUVW()
{
    int i, j, k, ijk, ijk_1, ijk_2, ip;
#   pragma omp parallel for private (j, ijk, ijk_1, ijk_2)
    for(i=1;i<IPoint-1;i++)
	{
	    for(j=1;j<JPoint-1;j++)
	    {
			ijk   = IIndex[i]+JIndex[j]+0;
			ijk_1 = IIndex[i]+JIndex[j]+1;
		    ijk_2 = IIndex[i]+JIndex[j]+2;
		    U[ijk] = Cord[2][0].coef[0][1] * U[ijk_1]
                   + Cord[2][0].coef[0][2] * U[ijk_2];
            V[ijk] = Cord[2][0].coef[0][1] * V[ijk_1]
                   + Cord[2][0].coef[0][2] * V[ijk_2];
            W[ijk] = Cord[2][0].coef[0][1] * W[ijk_1]
                   + Cord[2][0].coef[0][2] * W[ijk_2];

		    ijk   = IIndex[i]+JIndex[j]+KPoint-1;
			ijk_1 = IIndex[i]+JIndex[j]+KPoint-2;
		    ijk_2 = IIndex[i]+JIndex[j]+KPoint-3;
            U[ijk] = Cord[2][KPoint-1].coef[0][1] * U[ijk_1]
                   + Cord[2][KPoint-1].coef[0][2] * U[ijk_2];
            V[ijk] = Cord[2][KPoint-1].coef[0][1] * V[ijk_1]
                   + Cord[2][KPoint-1].coef[0][2] * V[ijk_2];
            W[ijk] = Cord[2][KPoint-1].coef[0][1] * W[ijk_1]
                   + Cord[2][KPoint-1].coef[0][2] * W[ijk_2];
	    }
	}
#   pragma omp parallel for private (k, ijk, ijk_1, ijk_2)
	for(i=1;i<IPoint-1;i++)
	{
		for(k=1;k<KPoint-1;k++)
	    {
		    ijk   = IIndex[i]+JIndex[0]+k;           
            ijk_1 = IIndex[i]+JIndex[1]+k;
            ijk_2 = IIndex[i]+JIndex[2]+k;
            U[ijk] = Cord[1][0].coef[0][1] * U[ijk_1]
                   + Cord[1][0].coef[0][2] * U[ijk_2];
            V[ijk] = Cord[1][0].coef[0][1] * V[ijk_1]
                   + Cord[1][0].coef[0][2] * V[ijk_2];
            W[ijk] = Cord[1][0].coef[0][1] * W[ijk_1]
                   + Cord[1][0].coef[0][2] * W[ijk_2];

			ijk   = IIndex[i]+JIndex[JPoint-1]+k;
            ijk_1 = IIndex[i]+JIndex[JPoint-2]+k;
            ijk_2 = IIndex[i]+JIndex[JPoint-3]+k;				
            U[ijk] = Cord[1][JPoint-1].coef[0][1] * U[ijk_1]
                   + Cord[1][JPoint-1].coef[0][2] * U[ijk_2];
            V[ijk] = Cord[1][JPoint-1].coef[0][1] * V[ijk_1]
                   + Cord[1][JPoint-1].coef[0][2] * V[ijk_2];
            W[ijk] = Cord[1][JPoint-1].coef[0][1] * W[ijk_1]
                   + Cord[1][JPoint-1].coef[0][2] * W[ijk_2];
	    }	
	}	
#   pragma omp parallel for private (k, ijk, ijk_1, ijk_2)
	for(j=1;j<JPoint-1;j++)
	{
		for(k=1;k<KPoint-1;k++)
		{
			ijk   = IIndex[0]+JIndex[j]+k;
			ijk_1 = IIndex[1]+JIndex[j]+k;
			ijk_2 = IIndex[2]+JIndex[j]+k;
            U[ijk] = Cord[0][0].coef[0][1] * U[ijk_1]
                   + Cord[0][0].coef[0][2] * U[ijk_2];
            V[ijk] = Cord[0][0].coef[0][1] * V[ijk_1]
                   + Cord[0][0].coef[0][2] * V[ijk_2];
            W[ijk] = Cord[0][0].coef[0][1] * W[ijk_1]
                   + Cord[0][0].coef[0][2] * W[ijk_2];
            
			ijk   = IIndex[IPoint-1]+JIndex[j]+k;
			ijk_1 = IIndex[IPoint-2]+JIndex[j]+k;
			ijk_2 = IIndex[IPoint-3]+JIndex[j]+k;
            U[ijk] = Cord[0][IPoint-1].coef[0][1] * U[ijk_1]
                   + Cord[0][IPoint-1].coef[0][2] * U[ijk_2];
            V[ijk] = Cord[0][IPoint-1].coef[0][1] * V[ijk_1]
                   + Cord[0][IPoint-1].coef[0][2] * V[ijk_2];
            W[ijk] = Cord[0][IPoint-1].coef[0][1] * W[ijk_1]
                   + Cord[0][IPoint-1].coef[0][2] * W[ijk_2];
            		
		}
	}
	if(ALE==1)
    {	
#       pragma omp parallel for private (i, ip)
		for(ijk=0;ijk<Total_Body_Nodes;ijk++)
		{
			i=BNI[ijk];
			ip=i-Point_Car;
			U[i]=U_Ale[ip]+Car_Ale[0];
			V[i]=V_Ale[ip]+Car_Ale[1];
			W[i]=W_Ale[ip]+Car_Ale[2];
		}
    }
}


int prepareCooMatrix(unsigned int* count_mark)
{
    int count=0;
    
    for (int ijk=0; ijk<Point_Car; ijk++)
    {
        if (GType2[ijk] == GType::_7POINT)
        {
            count_mark[ijk] = count;
            count = count + 7;
        }
        else if (GType2[ijk] == GType::OUTFLOW)
        {  
            count_mark[ijk] = count;
            count = count + 3;
        }
        else if (GType2[ijk] == GType::MOBILIZED)
        {
            count_mark[ijk] = count;
            count = count + (NB + 1);
        }
        else if (GType2[ijk] == GType::INACTIVE || GType2[ijk] == GType::NEWBIE
                || GType2[ijk] == GType::MUTE)
        {
            count_mark[ijk] = count;
            count = count + 1;
        }
        else
        {
            cout << "Mistake in function prepareCooMatrix." << endl;
        }
    }
    
    for (int ip=0; ip<Point_Meshless; ip++)
    {
        int ijk = ip + Point_Car;
        count_mark[ijk] = count;
        count = count + (NB + 1);
    }
    count_mark[Point_All] = count;
    
    return count;
}

void setCooMatrixForCartNodes(double* coo_value, unsigned int* coo_row_idx, unsigned int* coo_col_idx, const unsigned int* count_mark)
{
#   pragma omp parallel for
    for (int i=0; i<IPoint; i++)
	for (int j=0; j<JPoint; j++)
    for (int k=0; k<KPoint; k++)
    {
		int ijk = IIndex[i] + JIndex[j] + k;
		int tp = count_mark[ijk];
		
		if (GType2[ijk] == GType::_7POINT)
		{
	        // central point
		    coo_row_idx[tp] = ijk;
		    coo_col_idx[tp] = ijk;
		    coo_value  [tp] = Cord[0][i].coef[2][0] 
		                    + Cord[1][j].coef[2][0] 
		                    + Cord[2][k].coef[2][0];
		    
		    // west point
		    coo_row_idx[tp + 1] = ijk;
		    coo_col_idx[tp + 1] = IIndex[i-1] + JIndex[j] + k;
		    coo_value  [tp + 1] = Cord[0][i].coef[2][1]; 
		    
		    // east point
		    coo_row_idx[tp + 2] = ijk;
		    coo_col_idx[tp + 2] = IIndex[i+1] + JIndex[j] + k;
		    coo_value  [tp + 2] = Cord[0][i].coef[2][2]; 
		    
		    // south point
		    coo_row_idx[tp + 3] = ijk;
		    coo_col_idx[tp + 3] = IIndex[i] + JIndex[j-1] + k;
		    coo_value  [tp + 3] = Cord[1][j].coef[2][1];
		    
		    // north point
		    coo_row_idx[tp + 4] = ijk;
		    coo_col_idx[tp + 4] = IIndex[i] + JIndex[j+1] + k;
		    coo_value  [tp + 4] = Cord[1][j].coef[2][2]; 
		    
		    // lower point
		    coo_row_idx[tp + 5] = ijk;
		    coo_col_idx[tp + 5] = IIndex[i] + JIndex[j] + k-1;
		    coo_value  [tp + 5] = Cord[2][k].coef[2][1]; 
		    
		    // upper point
		    coo_row_idx[tp + 6] = ijk;
		    coo_col_idx[tp + 6] = IIndex[i] + JIndex[j] + k+1;
		    coo_value  [tp + 6] = Cord[2][k].coef[2][2]; 
		}
		else if (GType2[ijk] == GType::INACTIVE || GType2[ijk] == GType::NEWBIE
                || GType2[ijk] == GType::MUTE)
		{
		    coo_row_idx[tp] = ijk;
		    coo_col_idx[tp] = ijk;
		    coo_value  [tp] = 1.0;
		}
		else if (GType2[ijk] == GType::OUTFLOW)
		{
		    int dim, id;
		    double norm;
		    
	        if ((i == 0) || (i == IPoint-1))
            {
                dim = 0;
                id = i;
            }
            else if ((j == 0) || (j == JPoint-1))
            {
                dim = 1;
                id = j;
            }
            else if ((k == 0) || (k == KPoint-1))
            {
                dim = 2;
                id = k;
            }
            else
            {
                cout << "Mistake in function setCooMatrixForCartNodes." << endl;
                continue;
            }
            
            if (id == 0)
            {
                norm = 1.0;
            }
            else
            {
                norm = -1.0;
            }
            
	        // central point
            coo_row_idx[tp] = ijk;
            coo_col_idx[tp] = ijk;
            coo_value  [tp] = norm * Cord[dim][id].coef[1][0];
            
            // first neighbour point
            coo_row_idx[tp + 1] = ijk;
            coo_col_idx[tp + 1] = ijk - Cord[dim][id].offset[0] + Cord[dim][id].offset[1];
            coo_value  [tp + 1] = norm * Cord[dim][id].coef[1][1];
            
            // second neighbour point
            coo_row_idx[tp + 2] = ijk;
            coo_col_idx[tp + 2] = ijk - Cord[dim][id].offset[0] + Cord[dim][id].offset[2];
            coo_value  [tp + 2] = norm * Cord[dim][id].coef[1][2];
		}
		else
		{
		    continue;
		}
    }
}

void setCooMatrixForMLNodes(double* coo_value, unsigned int* coo_row_idx, unsigned int* coo_col_idx, const unsigned int* count_mark)
{
#   pragma omp parallel for
    for (int it=0; it<Point_Meshless; it++)
    {
        int ijk = Cld_List[it].Meshless_Ind;
        int tp = count_mark[ijk];
        
        double aps = 0, dpx = 0;
        int j;
        
        if (GType2[ijk] == GType::_GFDSVD)
        {
            for (int io=0; io<NB; io++)
            { 
                j = Cld_List[it].Nb_Points[io];
                
                dpx = Cld_List[it].Csvd[3][io]
                    + Cld_List[it].Csvd[4][io]
                    + Cld_List[it].Csvd[5][io];
                
                aps += dpx;
                
    		    coo_row_idx[tp] = ijk;
		        coo_col_idx[tp] = j;
		        coo_value  [tp] = dpx;
		        
                tp++;
            }
            
		    coo_row_idx[tp] = ijk;
	        coo_col_idx[tp] = ijk;
	        coo_value  [tp] = -aps;
        }
        else if (GType2[ijk] == GType::WALL)
        {
            for (int io=0; io<NB; io++)
            { 
                j = Cld_List[it].Nb_Points[io];
                
                dpx = MNx[it]*Cld_List[it].Csvd[0][io]
                    + MNy[it]*Cld_List[it].Csvd[1][io]
                    + MNz[it]*Cld_List[it].Csvd[2][io];
                
                aps += dpx;
                
		        coo_row_idx[tp] = ijk;
	            coo_col_idx[tp] = j;
	            coo_value  [tp] = dpx;
	            
                tp++;
            }
            
	        coo_row_idx[tp] = ijk;
            coo_col_idx[tp] = ijk;
            coo_value  [tp] = -aps;
        }
    }
    
#   pragma omp parallel for
    for (int it=0; it<C2M_List.size(); it++)
    {
        int ijk = C2M_List[it].Meshless_Ind;
        int tp = count_mark[ijk];
        
        if (GType2[ijk] == GType::MOBILIZED)
        {
            double aps = 0, dpx = 0;
            int j;
            
            for (int io=0; io<NB; io++)
            { 
                j = C2M_List[it].Nb_Points[io];
                
                dpx = C2M_List[it].Csvd[3][io]
                    + C2M_List[it].Csvd[4][io]
                    + C2M_List[it].Csvd[5][io];
                
                aps += dpx;
                
    		    coo_row_idx[tp] = ijk;
		        coo_col_idx[tp] = j;
		        coo_value  [tp] = dpx;
		        
                tp++;
            }
            
		    coo_row_idx[tp] = ijk;
	        coo_col_idx[tp] = ijk;
	        coo_value  [tp] = -aps;
        }
    }
}


void cmpPoissonEqForCartNodes(double* Axi, const double* xi, const unsigned i, const unsigned j, const unsigned k)
{
    if (i>=IPoint || j>=JPoint || k>=KPoint)
    {
        cout << " PoissonERR: index exceeded grid range " << endl;
        return;
    }
    
    int ijk = IIndex[i] + JIndex[j] + k;
    if (GType2[ijk] == 1)
    {
        int ijk_w = IIndex[i-1] + JIndex[j] + k;
        int ijk_e = IIndex[i+1] + JIndex[j] + k;
        int ijk_s = IIndex[i] + JIndex[j-1] + k;
        int ijk_n = IIndex[i] + JIndex[j+1] + k;
        
        Axi[ijk] = (Cord[0][i].coef[2][0] + Cord[1][j].coef[2][0] + Cord[2][k].coef[2][0]) * xi[ijk]
                  + Cord[0][i].coef[2][1] * xi[ijk_w] // west point
                  + Cord[0][i].coef[2][2] * xi[ijk_e] // east point
                  + Cord[1][j].coef[2][1] * xi[ijk_s] // south point
                  + Cord[1][j].coef[2][2] * xi[ijk_n] // north point
                  + Cord[2][k].coef[2][1] * xi[ijk-1] // lower point
                  + Cord[2][k].coef[2][2] * xi[ijk+1]; // upper point
                
    }
    else if (GType2[ijk]==0 || GType2[ijk]==4 || GType2[ijk]==6)
    {
        Axi[ijk] = xi[ijk];
    }
    else if (GType2[ijk] == 2)
    {
	    int id;
	    double norm;
	    CoordMember* current_coord = NULL;
	    
        if ((i == 0) || (i == IPoint-1))
        {
            id = i;
            current_coord = Cord[0];
        }
        else if ((j == 0) || (j == JPoint-1))
        {
            id = j;
            current_coord = Cord[1];
        }
        else if ((k == 0) || (k == KPoint-1))
        {
            id = k;
            current_coord = Cord[2];
        }
        else
        {
            printf("Mistake in function cmpPoissonEqForCartNodes for outflow boundaries.\n");
            return;
        }
        
        if (id == 0)
        {
            norm = 1.0;
        }
        else
        {
            norm = -1.0;
        }
        
        int ijk_1 = ijk - current_coord[id].offset[0] + current_coord[id].offset[1];
        int ijk_2 = ijk - current_coord[id].offset[0] + current_coord[id].offset[2];
        Axi[ijk] = norm * current_coord[id].coef[1][0] * xi[ijk]
                 + norm * current_coord[id].coef[1][1] * xi[ijk_1]
                 + norm * current_coord[id].coef[1][2] * xi[ijk_2];
    }
    else 
    {
        return;
    }
}

void cmpPoissonEqForMLNodes(double* Axi, const double* xi, const MeshlessMember* mls_member, unsigned id)
{
	int ijk, j, io;
    double dpx, apx, dps;
	
    ijk = mls_member->Meshless_Ind;
	
    if (GType2[ijk] == GType::_GFDSVD || GType2[ijk] == GType::MOBILIZED)
    {
        apx = 0;
        dps = 0;
        
        for (io=0; io<NB; io++) 
        {
	        j = mls_member->Nb_Points[io];
	        dpx = mls_member->Csvd[3][io]
	            + mls_member->Csvd[4][io]
	            + mls_member->Csvd[5][io];
		
		    apx += dpx;
		    
		    dps += dpx * xi[j];
	    }
	    
        Axi[ijk] = -apx*xi[ijk] + dps;
    }
    else if (GType2[ijk] == GType::WALL)
    {
        apx = 0;
        dps = 0;
        
        for (io=0; io<NB; io++)
        { 
            j = mls_member->Nb_Points[io];
            
            dpx = MNx[id]*mls_member->Csvd[0][io]
                + MNy[id]*mls_member->Csvd[1][io]
                + MNz[id]*mls_member->Csvd[2][io];
            
            apx += dpx;
            
            dps += dpx * xi[j];
        }
        
        Axi[ijk] = -apx*xi[ijk] + dps;
    }
    else if (GType2[ijk]==GType::INACTIVE || GType2[ijk]==GType::NEWBIE)
    {
        Axi[ijk] = xi[ijk];
    }
    else
    {
        return;
    }
}

void cmpPoissonEq(int n, double* xi, double* Axi)
{
    if (n != Point_All) 
    {
        printf("Mistake in function cmpPoissonEq.\n");
    }
    
#   pragma omp parallel for
    for (unsigned i=0; i<IPoint; i++)
    for (unsigned j=0; j<JPoint; j++)
    for (unsigned k=0; k<KPoint; k++)
    {
        cmpPoissonEqForCartNodes(Axi, xi, i, j, k);
    }
    
#   pragma omp parallel for
    for (unsigned i=0; i<Point_Meshless; i++)
    {
        cmpPoissonEqForMLNodes(Axi, xi, &Cld_List[i], i);
    }
    
#   pragma omp parallel for
    for (unsigned i=0; i<C2M_List.size(); i++)
    {
        cmpPoissonEqForMLNodes(Axi, xi, &C2M_List[i], 0);
    }
}


