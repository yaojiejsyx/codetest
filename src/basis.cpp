#include <math.h>
#include <iostream>
#include <stdio.h>
#ifdef _OPENMP
#	include <omp.h>
#endif

using namespace std;

#define  PI 3.1415926535897932

double Det3(double a0[], double a1[], double a2[])
{
	return(a0[0]*(a2[2]*a1[1]-a2[1]*a1[2]) - a1[0]*(a2[2]*a0[1]-a2[1]*a0[2]) + a2[0]*(a1[2]*a0[1]-a1[1]*a0[2]));
}

double Distance(double x1, double y1, double z1, double x2, double y2, double z2)
{
	return sqrt(pow(x1-x2,2)+pow(y1-y2,2)+pow(z1-z2,2));
}


void Inverse(double a[3][3],double b[3][3])
{
	double detA;

	detA=a[0][0]*a[1][1]*a[2][2]-a[0][0]*a[1][2]*a[2][1]-a[0][1]*a[1][0]*a[2][2]
      +a[0][1]*a[1][2]*a[2][0]+a[0][2]*a[1][0]*a[2][1]-a[0][2]*a[1][1]*a[2][0];

    if(fabs(detA)<1e-20) 
	{
		cout<<"Ill Matrix for body inertia, can not find its inversion!"<<endl;
//		exit(0);
	}
	b[0][0]=(a[1][1]*a[2][2]-a[1][2]*a[2][1])/detA;
	b[0][1]=(a[0][2]*a[2][1]-a[0][1]*a[2][2])/detA;
	b[0][2]=(a[0][1]*a[1][2]-a[0][2]*a[1][1])/detA;
	
	b[1][0]=(a[1][2]*a[2][0]-a[1][0]*a[2][2])/detA;
	b[1][1]=(a[0][0]*a[2][2]-a[0][2]*a[2][0])/detA;
	b[1][2]=(a[0][2]*a[1][0]-a[0][0]*a[1][2])/detA;
	
	b[2][0]=(a[1][0]*a[2][1]-a[1][1]*a[2][0])/detA;
	b[2][1]=(a[0][1]*a[2][0]-a[0][0]*a[2][1])/detA;
	b[2][2]=(a[0][0]*a[1][1]-a[0][1]*a[1][0])/detA;

}

void Transpose(double a[3][3],double b[3][3])
{
	
    int i,j;
#	pragma omp parallel for private(j)
	for(i=0;i<3;i++)
		for(j=0;j<3;j++)
			b[i][j]=a[j][i];
}

void transpose(double **a, double **b, int m, int n)
{
	int i,j;
#	pragma omp parallel for private(j)
	for(i=0;i<m;i++)
	{
		for(j=0;j<n;j++)
		{
			b[j][i]=a[i][j];
		}
	}
}

void matrix_multiply(double **A, double **B, double **C, int m, int n, int s)
{
#	pragma omp parallel for
	for(int i=0;i<m;i++)
	{
		for(int j=0;j<s;j++)
		{
			C[i][j]=0;
			for(int l=0;l<n;l++)
			{
				C[i][j]=C[i][j]+A[i][l]*B[l][j];
			}
		}
	}
}


void Multiply(double a[3][3],double b[3][3],double c[3][3])
{
	int i,j;
	for(i=0;i<3;i++)
		for(j=0;j<3;j++)
		{
			c[i][j]= a[i][0]*b[0][j]+a[i][1]*b[1][j]+a[i][2]*b[2][j];
		}	
}

double DetA(double a[3][3])
{
	double detA;

	detA=a[0][0]*a[1][1]*a[2][2]-a[0][0]*a[1][2]*a[2][1]-a[0][1]*a[1][0]*a[2][2]
       +a[0][1]*a[1][2]*a[2][0]+a[0][2]*a[1][0]*a[2][1]-a[0][2]*a[1][1]*a[2][0];
	return detA;
}

int gauss(double **a,double *b,int n) 
{ 
	int *js; 
	int l,k,i,j,is; 
	double d,t; 
 	js=new int[n]; 
	l=1;
	for (k=0;k<=n-2;k++) {
		d=0.0; 
		for (i=k;i<=n-1;i++) 
		for (j=k;j<=n-1;j++) {
			t=fabs(*(*(a+i)+j));    
			if (t>d) { d=t; js[k]=j; is=i;} 
		} 
		if (d+1.0==1.0) 
			l=0; 
		else {
			if (js[k]!=k) 
				for (i=0;i<=n-1;i++) {  
				t=*(*(a+i)+k);*(*(a+i)+k)=*(*(a+i)+js[k]) ; *(*(a+i)+js[k])=t; 
				} 
			if (is!=k) { 
				for (j=k;j<=n-1;j++) {
				t=*(*(a+k)+j) ; *(*(a+k)+j)=*(*(a+is)+j); *(*(a+is)+j)=t; 
				} 
				t=b[k]; b[k]=b[is]; b[is]=t; 
			} 
		} 
		if (l==0) {
			delete[] js; printf("fail\n"); 
			return(0); 
		} 
		d=*(*(a+k)+k); 
		for (j=k+1;j<=n-1;j++) { 
			*(*(a+k)+j)=*(*(a+k)+j)/d;
		} 
		b[k]=b[k]/d; 
		for (i=k+1;i<=n-1;i++) {
			for (j=k+1;j<=n-1;j++) { 
				*(*(a+i)+j)=*(*(a+i)+j)-*(*(a+i)+k)**(*(a+k)+j); 
			} 
			b[i]=b[i]-*(*(a+i)+k)*b[k]; 
		} 
	}

	d=*(*(a+n-1)+n-1);
	if (fabs(d)+1.0==1.0) {
		delete[] js; printf("fail\n"); 
		return(0); 
	} 
	b[n-1]=b[n-1]/d; 
	for (i=n-2;i>=0;i--) {
		t=0.0; 
		for (j=i+1;j<=n-1;j++) 
			t=t+*(*(a+i)+j)*b[j]; 
		b[i]=b[i]-t; 
	} 
	js[n-1]=n-1; 
	for (k=n-1;k>=0;k--) 
		if (js[k]!=k) 
		{ t=b[k]; b[k]=b[js[k]]; b[js[k]]=t;} 
	delete(js); 
	return(1); 
} 


int dcinv(double **a,int nn)
{ 
	int n=nn;
	int *is,*js,i,j,k;
	double d,p;
	is=new int[n];
	js=new int[n];
	for (k=0; k<=n-1; k++)
	{ 
	  d=0.0;
	  for (i=k; i<=n-1; i++)
	  {
	   for (j=k; j<=n-1; j++)
	   { 
		p=fabs(*(*(a+i)+j));
		if (p>d) 
		{ 
		 d=p; 
		 is[k]=i; 
		 js[k]=j;
		}
	   }
	  }
	  if (d+1.0==1.0)
	  {
	   delete []is;
	   delete []js;
	   printf("err***not inv\n");
	   return(0);
	  }
	  if (is[k]!=k)
	  {
	   for (j=0; j<=n-1; j++)
	   { 
		p=*(*(a+k)+j);*(*(a+k)+j)=*(*(a+is[k])+j);*(*(a+is[k])+j)=p;
	   }
	  }
	  if (js[k]!=k)
	  {
	   for (i=0; i<=n-1; i++)
	   { 
		p=*(*(a+i)+k);*(*(a+i)+k)=*(*(a+i)+js[k]);*(*(a+i)+js[k])=p;
	   }
	  }
	  *(*(a+k)+k)=1.0/(*(*(a+k)+k));
	  for (j=0; j<=n-1; j++)
	  {
	   if (j!=k)
	   { 
	   *(*(a+k)+j)=*(*(a+k)+j)**(*(a+k)+k);
	   }
	  }
	  for (i=0; i<=n-1; i++)
	  {
	   if (i!=k)
	   {
		for (j=0; j<=n-1; j++)
		{
		 if (j!=k)
		 { 
			*(*(a+i)+j)=*(*(a+i)+j)-*(*(a+i)+k)**(*(a+k)+j);
		 }
		}
	   }
	  }
	  for (i=0; i<=n-1; i++)
	  {
	   if (i!=k)
	   {    
			*(*(a+i)+k)=-*(*(a+i)+k)**(*(a+k)+k);
	   }   
	  } 
	}
	for (k=n-1; k>=0; k--)
	{ 
	  if (js[k]!=k)
	  {
	   for (j=0; j<=n-1; j++)
	   { 
		p=*(*(a+k)+j); *(*(a+k)+j)=*(*(a+js[k])+j); *(*(a+js[k])+j)=p;
	   }
	  }
	  if (is[k]!=k)
	  {
	   for (i=0; i<=n-1; i++)
	   { 
		p=*(*(a+i)+k); *(*(a+i)+k)=*(*(a+i)+is[k]); *(*(a+i)+is[k])=p;
	   }
	  }
	}
	delete []is;
	delete []js;
	return(1);
}


void QuickSort (double* a, int* b, int lo, int hi)
{
//  lo is the lower index, hi is the upper index
//  of the region of array a that is to be sorted
    int i=lo, j=hi, ijk;
    double x=a[(lo+hi)/2];
	double h;

    //  partition
    do
    {    
        while (a[i]<x) i++; 
        while (a[j]>x) j--;
        if (i<=j)
        {
            h=a[i];
            ijk=b[i];			
			a[i]=a[j];
            b[i]=b[j];			
			a[j]=h;
			b[j]=ijk;
            i++; 
			j--;
        }
    } while (i<=j);

    //  recursion
    if (lo<j) QuickSort(a, b, lo, j);
    if (i<hi) QuickSort(a, b, i, hi);
}

void cross_product(double a[3], double b[3], double c[3])
{
	double tmp[3];
	tmp[0] = a[1]*b[2]-a[2]*b[1];
	tmp[1] = a[2]*b[0]-a[0]*b[2];
	tmp[2] = a[0]*b[1]-a[1]*b[0];
	c[0] = tmp[0];
	c[1] = tmp[1];
	c[2] = tmp[2];
}

template <typename T>
int sgn(T val) 
{
    return (T(0) < val) - (val < T(0));
}
template int sgn<double>(double);
template int sgn<int>(int val);

