#include <iostream>
#include <math.h>
#include <stdio.h>

#include <cublas.h>
#include <cusparse.h>
#include <cuda_runtime.h>

//#include "../common/mesh_type.h"
#include "../common/gpu_helper.h"

using namespace std;

// double bicgstab_solver_GPU
// (
    // int n, int nnz, int* cooRowIndex, int* cooColIndex, double* cooVal,
    // double* x_device, double* b_device,
    // int maxit, double tol,int info
// )
// {
    // cusparseHandle_t sparse_handle=0;
    // cusparseMatDescr_t sparse_descr=0;

    // int *csrRowPtr;
    // double *temp;
    // cudaMalloc((void**)&csrRowPtr, (n+1)*sizeof(int));
    // cudaMalloc((void**)&temp, 5*n*sizeof(double));
    // cudaMemset(temp, 0, 5*n*sizeof(temp[0]));
    
    // cusparseCreate(&sparse_handle);
    // cusparseCreateMatDescr(&sparse_descr);
    // cusparseSetMatType(sparse_descr, CUSPARSE_MATRIX_TYPE_GENERAL);
    // cusparseSetMatIndexBase(sparse_descr, CUSPARSE_INDEX_BASE_ZERO);
    // cusparseXcoo2csr(sparse_handle, cooRowIndex, nnz, n, csrRowPtr, CUSPARSE_INDEX_BASE_ZERO);

    // int        iflag = 1, icount = 0;
    // double     delta0, deltai;
    // double     bet, roi;
    // double     roim1 = 1.0, al = 1.0, wi = 1.0;

    // cusparseDcsrmv(sparse_handle, CUSPARSE_OPERATION_NON_TRANSPOSE, n, n, -1.0, sparse_descr,
                   // cooVal, csrRowPtr, cooColIndex, &x_device[0], 1.0, &b_device[0]);
    // cublasDcopy(n, b_device, 1, &temp[0], 1);
    // delta0 = cublasDnrm2(n, b_device, 1);
    // if (info == 1) cout << "initial error is " << delta0 << endl;
	
    // if (delta0 < tol)
    // {  
	    // deltai = delta0;
    // }
    // else
    // {
	    // while (iflag != 0 && icount < maxit)
	    // {
	        // icount += 1;
	        // roi = cublasDdot(n, &temp[0], 1, b_device, 1);
	        // bet = (roi/roim1) * (al/wi);
	        // cublasDaxpy(n, (-1)*wi, &temp[n], 1, &temp[2*n], 1);
	        // cublasDcopy(n, b_device, 1, &temp[4*n], 1);
	        // cublasDaxpy(n, bet, &temp[2*n], 1, &temp[4*n], 1);
	        // cublasDcopy(n, &temp[4*n], 1, &temp[2*n], 1);
	        // cusparseDcsrmv(sparse_handle, CUSPARSE_OPERATION_NON_TRANSPOSE, n, n, 1.0, sparse_descr,
	                       // cooVal, csrRowPtr, cooColIndex, &temp[2*n], 0.0, &temp[n]);
	        // al = roi/cublasDdot(n, &temp[0], 1, &temp[n], 1);
	        // cublasDaxpy(n, (-1)*al, &temp[n], 1, b_device, 1);
	        // cusparseDcsrmv(sparse_handle, CUSPARSE_OPERATION_NON_TRANSPOSE, n, n, 1.0, sparse_descr,
	                       // cooVal, csrRowPtr, cooColIndex, b_device, 0.0, &temp[3*n]);
	        // wi = cublasDdot(n, &temp[3*n], 1, b_device, 1) / cublasDdot(n, &temp[3*n], 1, &temp[3*n], 1);
	        // cublasDaxpy(n, al, &temp[2*n], 1, x_device, 1);
	        // cublasDaxpy(n, wi, b_device, 1, x_device, 1);
	        // cublasDaxpy(n, (-1)*wi, &temp[3*n], 1, b_device, 1);
	        // deltai = cublasDnrm2(n, b_device, 1);
	        
	        // if (info == 1) cout << "The " << icount << "th's error is " << deltai << endl;
			
	        // if (deltai < tol) 
	            // iflag = 0;
	        // else 
	            // roim1 = roi;
	    // }
    // }
    // if (info == 1) cout << "GPU | Bicgstab iteration: " << icount << " | ";
    
    // cusparseDestroyMatDescr(sparse_descr);
    // cusparseDestroy(sparse_handle);
    
    // cudaFree(csrRowPtr);
    // cudaFree(temp);
    
    // cudaDeviceSynchronize(); getLastCudaError("bicgstab_solver");
    
    // return deltai;
// }


double bicgstab_solver2_GPU
(
    int n, void (*matv)(int, double* xi, double* Axi),
    double* x_device, double* b_device, int maxit, double eps, int info,
    double* d_tmp1, double* d_tmp2, double* d_tmp3, double* d_tmp4
)
{
    int        iflag = 1, icount = 0;
    double     delta0, deltai;
    double     bet, roi;
    double     roim1 = 1.0, al = 1.0, wi = 1.0;
    
    double *ri, *t, *vi, *pi;
    ri = d_tmp1; cudaMemset(ri,  0, n*sizeof(double));
    t  = d_tmp2; cudaMemset(t,   0, n*sizeof(double));
    vi = d_tmp3; cudaMemset(vi,  0, n*sizeof(double));
    pi = d_tmp4; cudaMemset(pi,  0, n*sizeof(double));
    cudaDeviceSynchronize(); getLastCudaError("bicgstab_solver: malloc error");
    
    matv(n, x_device, t);
    cublasDaxpy(n, -1, t, 1, b_device, 1);
    cublasDcopy(n, b_device, 1, ri, 1);
    delta0 = cublasDnrm2(n, ri, 1);
    
    if (info == 1) cout << "initial error is " << delta0 << endl;
    if (delta0 < eps)
    {  
        deltai = delta0;
    }
    else
    {
        while (iflag != 0 && icount < maxit)
	    {
	        icount++;
	        roi = cublasDdot(n, b_device, 1, ri, 1);
	        bet = (roi/roim1) * (al/wi);
	        
	        // pi[] = ri[] + bet*(pi[] - wi*vi[])
	        cublasDaxpy(n, -1.0*wi, vi, 1, pi, 1);
	        cublasDaxpy(n, 1.0/bet, ri, 1, pi, 1);
	        cublasDscal(n, bet, pi, 1);
	        
	        // vi[] = A[][] * pi[];
	        matv(n, pi, vi);
	        
	        // al = roi / ( roc[] * vi[] );
	        al = roi/cublasDdot(n, b_device, 1, vi, 1);
	        
	        // s[] = ri[] - vi[] * al;   
	        cublasDaxpy(n, -1.0*al, vi, 1, ri, 1);
	        
	        // t[] = A[][] * s[];
	        matv(n, ri, t);
	        
	        // wi = ( t[] * s[] ) / ( t[] * t[] );
	        wi = cublasDdot(n, t, 1, ri, 1) / cublasDdot(n, t, 1, t, 1);
	        
	        // xi[] += pi[] * al + s[] * wi;
	        cublasDaxpy(n, wi, ri, 1, x_device, 1);
	        cublasDaxpy(n, al, pi, 1, x_device, 1);
	        
	        // ri[] = s[] - t[] * wi;
	        cublasDaxpy(n, -1.0*wi, t, 1, ri, 1);
	        
	        deltai = cublasDnrm2(n, ri, 1);
	        
	        if (info == 1) cout << "The " << icount << "th's error is " << deltai << endl;
	        
	        if (deltai < eps)
	        {
		        iflag = 0;
	        }
	        else
	        {
		        roim1 = roi;
	        }
	    }
    }
    if (info == 1) cout << "GPU | Bicgstab iteration: " << icount << " | ";
    
    cudaDeviceSynchronize(); getLastCudaError("bicgstab_solver");
    
//    cudaFree(ri);
//    cudaFree(t);
//    cudaFree(vi);
//    cudaFree(pi);
    return deltai;
}

