#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

#include "../common/frame_structure.h"
#include "../common/flap_pattern.h"

extern const double Time, Re;
extern const int IPoint, JPoint, KPoint, Point_All, Point_Car, Point_Meshless;

extern double *X, *Y, *Z, *U, *V, *W, *P;
extern char *GType2;

extern double bodycentre[3], lastbodycentre[3];
extern double Car_Ale[3], Car_ACC[3];

extern Ref_Frame insect;
extern FlapPattern my_flap_pattern;

extern int Time_Step;

void Write_Check_Point_Data(char file_name[]) {
	ofstream out_data;
	out_data.open(file_name);
	
	int ijk;
	for(ijk=0;ijk<Point_All;ijk++) {
		out_data<<X[ijk]<<" "<<Y[ijk]<<" "<<Z[ijk]<<" "
				<<U[ijk]<<" "<<V[ijk]<<" "<<W[ijk]<<" "
				<<P[ijk]<<" "<<int(GType2[ijk])<<endl;
	}

	out_data.close();
}

void Write_Check_Point_Information(char file_name[]) {
	ofstream out_info;
	out_info.open(file_name);
	
	// flight dynamics
	out_info<<bodycentre[0]<<" "<<bodycentre[1]<<" "<<bodycentre[2]<<" "
			<<Car_Ale[0]<<" "<<Car_Ale[1]<<" "<<Car_Ale[2]<<" "
			<<Car_ACC[0]<<" "<<Car_ACC[1]<<" "<<Car_ACC[2]<<endl;
	
	out_info<<insect.position[0]<<" "<<insect.position[1]<<" "<<insect.position[2]<<" "
			<<insect.velocity[0]<<" "<<insect.velocity[1]<<" "<<insect.velocity[2]<<" "
			<<insect.acceleration[0]<<" "<<insect.acceleration[1]<<" "<<insect.acceleration[2]<<endl;
	out_info<<insect.angle[0]<<" "<<insect.angle[1]<<" "<<insect.angle[2]<<" "
			<<insect.angular_velocity[0]<<" "
			<<insect.angular_velocity[1]<<" "
			<<insect.angular_velocity[2]<<" "
			<<insect.angular_acceleration[0]<<" "
			<<insect.angular_acceleration[1]<<" "
			<<insect.angular_acceleration[2]<<endl;
	out_info<<insect.force[0]<<" "<<insect.force[1]<<" "<<insect.force[2]<<" "
			<<insect.torque[0]<<" "<<insect.torque[1]<<" "<<insect.torque[2]<<endl;
	out_info<<insect.momentum[0]<<" "<<insect.momentum[1]<<" "<<insect.momentum[2]<<" "
			<<insect.angular_momentum[0]<<" "
			<<insect.angular_momentum[1]<<" "
			<<insect.angular_momentum[2]<<endl;
			
	for(int i=0;i<3;i++)
		for(int j=0;j<3;j++)
			out_info<<insect.orientation[i][j]<<" ";
	out_info<<endl;
	
	for(int i=0;i<3;i++)
		for(int j=0;j<3;j++)
			out_info<<insect.inertia[i][j]<<" ";
	out_info<<endl;
	
	// controller
	int err = my_flap_pattern.Record_Flap_Pattern(out_info);
	
	out_info<<endl;
	out_info.close();
}

int Write_Check_Point(int iter) {
	int freq = 7500;
	if((iter%freq!=0) && (iter!=Time_Step)) return 1;
	
	char *chp_name = new char [64];
	char *file_name = new char [64];
	char char_buffer[32];
	
	// v0.1 first checkpoint file edition
	// v0.2 add AoA angle controller for longitudinal control, record only last 50 outputs of controllers
	strcpy(chp_name, "chp_v0.2_");
	sprintf(char_buffer, "%d", int(iter/500));
	strcat(chp_name, char_buffer);
	
	ofstream out;
	out.open(chp_name);
	
	out<<iter<<" "<<Time<<" "<<Re<<" "<<my_flap_pattern.freq_global<<endl;
	out<<IPoint<<" "<<JPoint<<" "<<KPoint<<" "<<Point_All<<" "<<Point_Meshless<<endl;
	
	strcpy(file_name, chp_name);
	strcat(file_name, ".data");
	out<<file_name<<endl;
	Write_Check_Point_Data(file_name);
	
	// Auxiliary data and information
	strcpy(file_name, chp_name);
	strcat(file_name, ".info");
	out<<file_name<<endl;
	Write_Check_Point_Information(file_name);
	
	out.close();
	delete file_name;
	delete chp_name;
	return 0;
}

int Read_Check_Point_Data(char file_name[]) {
	ifstream in_data;
	in_data.open(file_name);

	if(!in_data.is_open()) {
		return 1;
	}
	
	int ijk; double tmp;
	for(ijk=0;ijk<Point_All;ijk++) {
	    int tmp_t;
		in_data>>tmp>>tmp>>tmp>>U[ijk]>>V[ijk]>>W[ijk]>>P[ijk]>>tmp_t;
		GType2[ijk] = tmp_t;
	}
	
	in_data.close();
	return 0;
}

int Read_Check_Point_Information(char file_name[]) {
	ifstream in_info;
	in_info.open(file_name);

	if(!in_info.is_open()) {
		return 1;
	}
	
	// flight dynamics
	in_info>>bodycentre[0]>>bodycentre[1]>>bodycentre[2]
		   >>Car_Ale[0]>>Car_Ale[1]>>Car_Ale[2]
		   >>Car_ACC[0]>>Car_ACC[1]>>Car_ACC[2];
	
	in_info>>insect.position[0]>>insect.position[1]>>insect.position[2]
		   >>insect.velocity[0]>>insect.velocity[1]>>insect.velocity[2]
		   >>insect.acceleration[0]>>insect.acceleration[1]>>insect.acceleration[2];
	in_info>>insect.angle[0]>>insect.angle[1]>>insect.angle[2]
		   >>insect.angular_velocity[0]>>insect.angular_velocity[1]>>insect.angular_velocity[2]
		   >>insect.angular_acceleration[0]>>insect.angular_acceleration[1]>>insect.angular_acceleration[2];
	in_info>>insect.force[0]>>insect.force[1]>>insect.force[2]
	       >>insect.torque[0]>>insect.torque[1]>>insect.torque[2];
	in_info>>insect.momentum[0]>>insect.momentum[1]>>insect.momentum[2]
		   >>insect.angular_momentum[0]>>insect.angular_momentum[1]>>insect.angular_momentum[2];
	
	for(int i=0;i<3;i++)
		for(int j=0;j<3;j++)
			in_info>>insect.orientation[i][j];
	
	for(int i=0;i<3;i++)
		for(int j=0;j<3;j++)
			in_info>>insect.inertia[i][j];
	
	// controller
	int err = my_flap_pattern.Load_Flap_Pattern(in_info);
	
	in_info.close();
	return err;
}

int Read_Check_Point(char data_name[], char info_name[]) {
	int err;
	err = Read_Check_Point_Data(data_name);
	err = Read_Check_Point_Information(info_name);
	
	return err;
}
