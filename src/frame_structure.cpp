#include <iostream>
#include <math.h>
#include <fstream>

using namespace std;

#include "../common/basis.h"
#include "../common/frame_structure.h"

#define  PI 3.1415926535897932

extern int Point_All;

double Ref_Frame :: compute_angular_velocity1(double Time)
{
    return -compute_theta(Time,1)*sin( compute_phi(Time,0) ) + compute_psi(Time,1)*cos( compute_theta(Time,0) )*cos( compute_phi(Time,0) ) ;
}
  
double Ref_Frame :: compute_angular_velocity2(double Time)
{
    return compute_theta(Time,1)*cos( compute_phi(Time,0) ) + compute_psi(Time,1)*cos( compute_theta(Time,0) )*sin( compute_phi(Time,0) ) ;
}

double Ref_Frame :: compute_angular_velocity3(double Time)
{
    return compute_phi(Time,1) - compute_psi(Time,1)*sin( compute_theta(Time,0) ) ;
}

double Ref_Frame :: compute_angular_acceleration1(double Time)
{
    return (  - compute_theta(Time,2) * sin( compute_phi(Time,0) )
    		  - compute_theta(Time,1) * compute_phi(Time,1) * cos( compute_phi(Time,0) )
              + compute_psi(Time,2) * cos( compute_theta(Time,0) ) * cos( compute_phi(Time,0) )
              - compute_psi(Time,1) * compute_theta(Time,1) * sin( compute_theta(Time,0) )*cos( compute_phi(Time,0) )
              - compute_psi(Time,1) * compute_phi(Time,1) * cos( compute_theta(Time,0) )*sin( compute_phi(Time,0))  );
}

double Ref_Frame :: compute_angular_acceleration2(double Time)
{
    return (  compute_theta(Time,2) * cos( compute_phi(Time,0) )
              - compute_theta(Time,1) * compute_phi(Time,1) * sin( compute_phi(Time,0) )
              + compute_psi(Time,2) * cos(compute_theta(Time,0)) * sin( compute_phi(Time,0) )
              - compute_psi(Time,1) * compute_theta(Time,1) * sin( compute_theta(Time,0) )*sin( compute_phi(Time,0) )
              + compute_psi(Time,1) * compute_phi(Time,1) * cos( compute_theta(Time,0) )*cos( compute_phi(Time,0) )  );
}

double Ref_Frame :: compute_angular_acceleration3(double Time)
{
    return (  compute_phi(Time,2)
              - compute_psi(Time,1) * compute_theta(Time,1) * cos( compute_theta(Time,0) )
              - compute_psi(Time,2) * sin( compute_theta(Time,0) )  );
}

void Ref_Frame :: update_all()
{
    position[0]=compute_x(Time,0);
    position[1]=compute_y(Time,0);
    position[2]=compute_z(Time,0);
    velocity[0]=compute_x(Time,1);
    velocity[1]=compute_y(Time,1);
    velocity[2]=compute_z(Time,1);
    acceleration[0]=compute_x(Time,2);
    acceleration[1]=compute_y(Time,2);
    acceleration[2]=compute_z(Time,2);
    
    angle[0]=compute_phi(Time,0);
    angle[1]=compute_theta(Time,0);
    angle[2]=compute_psi(Time,0);
    angular_velocity[0]=compute_angular_velocity1(Time);
    angular_velocity[1]=compute_angular_velocity2(Time);
    angular_velocity[2]=compute_angular_velocity3(Time);
    compute_orientation();
    angular_acceleration[0]=compute_angular_acceleration1(Time);
    angular_acceleration[1]=compute_angular_acceleration2(Time);
    angular_acceleration[2]=compute_angular_acceleration3(Time);
}


void Ref_Frame :: copy_new2old()
{
    for(int i=0;i<3;i++)
    {
	position_old[i]             =    position[i];
	velocity_old[i]             =    velocity[i];
	acceleration_old[i]         =    acceleration[i];
	angle_old[i]                =    angle[i];
	angular_velocity_old[i]     =    angular_velocity[i];
	angular_acceleration_old[i] =    angular_acceleration[i];
	force_old[i]                =    force[i];
	torque_old[i]               =    torque[i];
	momentum_old[i]             =    momentum[i];
	angular_momentum_old[i]     =    angular_momentum[i];
	for(int j=0;j<3;j++)
	{
	    orientation_old[i][j]   =    orientation[i][j];
	    inertia_old[i][j]       =    inertia[i][j];
	}
    }
}

void Ref_Frame :: compute_orientation_tmp()
{
	double Phi,Theta,Psi;
	Phi=angle_tmp[0];Theta=angle_tmp[1];Psi=angle_tmp[2];
	orientation_tmp[0][0] = cos(Phi)*cos(Theta);
	orientation_tmp[0][1] = cos(Phi)*sin(Theta)*sin(Psi) - sin(Phi)*cos(Psi);
	orientation_tmp[0][2] = cos(Phi)*sin(Theta)*cos(Psi) + sin(Phi)*sin(Psi);
	orientation_tmp[1][0] = sin(Phi)*cos(Theta);
	orientation_tmp[1][1] = sin(Phi)*sin(Theta)*sin(Psi) + cos(Phi)*cos(Psi);
	orientation_tmp[1][2] = sin(Phi)*sin(Theta)*cos(Psi) - cos(Phi)*sin(Psi);
	orientation_tmp[2][0] = -sin(Theta);
	orientation_tmp[2][1] = cos(Theta)*sin(Psi);
	orientation_tmp[2][2] = cos(Theta)*cos(Psi);
}

void Ref_Frame :: compute_orientation()
{
	double Phi,Theta,Psi;
	Phi=angle[0];Theta=angle[1];Psi=angle[2];
	orientation[0][0] = cos(Phi)*cos(Theta);
	orientation[0][1] = cos(Phi)*sin(Theta)*sin(Psi) - sin(Phi)*cos(Psi);
	orientation[0][2] = cos(Phi)*sin(Theta)*cos(Psi) + sin(Phi)*sin(Psi);
	orientation[1][0] = sin(Phi)*cos(Theta);
	orientation[1][1] = sin(Phi)*sin(Theta)*sin(Psi) + cos(Phi)*cos(Psi);
	orientation[1][2] = sin(Phi)*sin(Theta)*cos(Psi) - cos(Phi)*sin(Psi);
	orientation[2][0] = -sin(Theta);
	orientation[2][1] = cos(Theta)*sin(Psi);
	orientation[2][2] = cos(Theta)*cos(Psi);
}

void Ref_Frame :: memory_allocate_for_rigid_body(Rigid_Obj* temp)
{
	for (int i=0; i<3; i++)
	{
		temp->XYZ[i] = new double [temp->POINT_NUMBER];
		temp->UVW[i] = new double [temp->POINT_NUMBER];
		temp->ACC[i] = new double [temp->POINT_NUMBER];
		temp->OUTER_NORMAL_VECTOR[i] = new double [temp->POINT_NUMBER];
		temp->TRIANGLE[i] = new int [temp->TRIANGLE_NUMBER];	
	}
	
	if(temp->POINTMODE==true)
	{
	    for(int i=0; i<4; i++)
	    {
		temp->TETRAHEDRON[i] = new int [temp->TETRAHEDRON_NUMBER];
	    }
	}
	
	temp->AREA = new double [temp->POINT_NUMBER];
	temp->INNERMARK = new int [temp->POINT_NUMBER];
	temp->OUTERMARK = new int [temp->POINT_NUMBER];
}


Rigid_Obj* Ref_Frame :: get_rigid_body_from_file(char filename[])
{
	Rigid_Obj *temp = new Rigid_Obj;
	ifstream in;
	in.open(filename);
	in>>temp->IDENTITY;
	in>>temp->LEVEL;
	in>>temp->POINTMODE;
	in>>temp->POINT_NUMBER>>temp->TETRAHEDRON_NUMBER>>temp->TRIANGLE_NUMBER;
	in>>temp->INNER_POINT_NUMBER>>temp->OUTER_POINT_NUMBER;

	memory_allocate_for_rigid_body(temp);
	
	for (int i=0; i<temp->POINT_NUMBER; i++)
	{
		in>>temp->XYZ[0][i]>>temp->XYZ[1][i]>>temp->XYZ[2][i];
		in>>temp->OUTER_NORMAL_VECTOR[0][i]>>temp->OUTER_NORMAL_VECTOR[1][i]>>temp->OUTER_NORMAL_VECTOR[2][i];
		in>>temp->INNERMARK[i]>>temp->OUTERMARK[i];
		in>>temp->AREA[i];
		temp->UVW[0][i]=0;temp->UVW[1][i]=0;temp->UVW[2][i]=0;
		temp->ACC[0][i]=0;temp->ACC[1][i]=0;temp->ACC[2][i]=0;
	}
	
	for(int j=0; j<temp->TETRAHEDRON_NUMBER; j++)
	{
		in>>temp->TETRAHEDRON[0][j]>>temp->TETRAHEDRON[1][j]>>temp->TETRAHEDRON[2][j]>>temp->TETRAHEDRON[3][j];
	}
	
	
	for (int j=0; j<temp->TRIANGLE_NUMBER; j++)
	{
		in>>temp->TRIANGLE[0][j]>>temp->TRIANGLE[1][j]>>temp->TRIANGLE[2][j];
	}
	in.close();
	
	cout<<"Point is: "<<temp->POINT_NUMBER<<endl;
	
	return temp;
}
	
Rigid_Obj* Ref_Frame :: get_rigid_body_from_subframe(int no_fra,int no_obj)
{
	if(subframe_number==0){cout<<"There is no subframe!"<<endl;}
	if(no_fra<0||no_fra>=subframe_number){cout<<"The range of no_fra is wrong!"<<endl;}
	if(no_obj<0||no_obj>=sub[no_fra]->obj_number){cout<<"The range of no_obj is wrong!"<<endl;}
	Rigid_Obj *temp=new Rigid_Obj;
	temp->IDENTITY=sub[no_fra]->rigid_body[no_obj]->IDENTITY;
	temp->LEVEL=sub[no_fra]->rigid_body[no_obj]->LEVEL-1;
	temp->POINTMODE=sub[no_fra]->rigid_body[no_obj]->POINTMODE;
	temp->POINT_NUMBER=sub[no_fra]->rigid_body[no_obj]->POINT_NUMBER;
	temp->TETRAHEDRON_NUMBER=sub[no_fra]->rigid_body[no_obj]->TETRAHEDRON_NUMBER;
	temp->TRIANGLE_NUMBER=sub[no_fra]->rigid_body[no_obj]->TRIANGLE_NUMBER;
	temp->INNER_POINT_NUMBER=sub[no_fra]->rigid_body[no_obj]->INNER_POINT_NUMBER;
	temp->OUTER_POINT_NUMBER=sub[no_fra]->rigid_body[no_obj]->OUTER_POINT_NUMBER;
	memory_allocate_for_rigid_body(temp);
	
	double T_t[3][3],Omega[3][3],Omega_T[3][3],Omega_Omega_T[3][3],dOmega[3][3],dOmega_T[3][3];

	T_t[0][0]=sub[no_fra]->orientation[0][0];
	T_t[0][1]=sub[no_fra]->orientation[0][1];
	T_t[0][2]=sub[no_fra]->orientation[0][2];
	T_t[1][0]=sub[no_fra]->orientation[1][0];
	T_t[1][1]=sub[no_fra]->orientation[1][1];
	T_t[1][2]=sub[no_fra]->orientation[1][2];
	T_t[2][0]=sub[no_fra]->orientation[2][0];
	T_t[2][1]=sub[no_fra]->orientation[2][1];
	T_t[2][2]=sub[no_fra]->orientation[2][2];

	Omega[0][0]=0;
	Omega[0][1]=-sub[no_fra]->angular_velocity[2];
	Omega[0][2]=sub[no_fra]->angular_velocity[1];
	Omega[1][0]=sub[no_fra]->angular_velocity[2];
	Omega[1][1]=0;
	Omega[1][2]=-sub[no_fra]->angular_velocity[0];
	Omega[2][0]=-sub[no_fra]->angular_velocity[1]; 
	Omega[2][1]=sub[no_fra]->angular_velocity[0];   
	Omega[2][2]=0;
	
	Multiply(Omega,T_t,Omega_T);
	Multiply(Omega,Omega_T,Omega_Omega_T);
	
	dOmega[0][0]=0;
	dOmega[0][1]=-sub[no_fra]->angular_acceleration[2];  
	dOmega[0][2]=sub[no_fra]->angular_acceleration[1];
	dOmega[1][0]=sub[no_fra]->angular_acceleration[2];  
	dOmega[1][1]=0;                                      
	dOmega[1][2]=-sub[no_fra]->angular_acceleration[0];
	dOmega[2][0]=-sub[no_fra]->angular_acceleration[1]; 
	dOmega[2][1]=sub[no_fra]->angular_acceleration[0];
	dOmega[2][2]=0;
			
	Multiply(dOmega,T_t,dOmega_T);
	
	for (int s=0; s<temp->POINT_NUMBER; s++)
	{
		temp->XYZ[0][s] = sub[no_fra]->position[0]
                        + T_t[0][0]*sub[no_fra]->rigid_body[no_obj]->XYZ[0][s]
                        + T_t[0][1]*sub[no_fra]->rigid_body[no_obj]->XYZ[1][s]
                        + T_t[0][2]*sub[no_fra]->rigid_body[no_obj]->XYZ[2][s];
		temp->XYZ[1][s] = sub[no_fra]->position[1]
                        + T_t[1][0]*sub[no_fra]->rigid_body[no_obj]->XYZ[0][s]
                        + T_t[1][1]*sub[no_fra]->rigid_body[no_obj]->XYZ[1][s]
                        + T_t[1][2]*sub[no_fra]->rigid_body[no_obj]->XYZ[2][s];
		temp->XYZ[2][s] = sub[no_fra]->position[2]
                        + T_t[2][0]*sub[no_fra]->rigid_body[no_obj]->XYZ[0][s]
                        + T_t[2][1]*sub[no_fra]->rigid_body[no_obj]->XYZ[1][s]
                        + T_t[2][2]*sub[no_fra]->rigid_body[no_obj]->XYZ[2][s];
		
		temp->UVW[0][s] = sub[no_fra]->velocity[0]
                        + T_t[0][0]*sub[no_fra]->rigid_body[no_obj]->UVW[0][s]
                        + T_t[0][1]*sub[no_fra]->rigid_body[no_obj]->UVW[1][s]
                        + T_t[0][2]*sub[no_fra]->rigid_body[no_obj]->UVW[2][s]
                        + Omega_T[0][0]*sub[no_fra]->rigid_body[no_obj]->XYZ[0][s]
                        + Omega_T[0][1]*sub[no_fra]->rigid_body[no_obj]->XYZ[1][s]
                        + Omega_T[0][2]*sub[no_fra]->rigid_body[no_obj]->XYZ[2][s];
		temp->UVW[1][s] = sub[no_fra]->velocity[1]
                        + T_t[1][0]*sub[no_fra]->rigid_body[no_obj]->UVW[0][s]
                        + T_t[1][1]*sub[no_fra]->rigid_body[no_obj]->UVW[1][s]
                        + T_t[1][2]*sub[no_fra]->rigid_body[no_obj]->UVW[2][s]
                        + Omega_T[1][0]*sub[no_fra]->rigid_body[no_obj]->XYZ[0][s]
                        + Omega_T[1][1]*sub[no_fra]->rigid_body[no_obj]->XYZ[1][s]
                        + Omega_T[1][2]*sub[no_fra]->rigid_body[no_obj]->XYZ[2][s];
		temp->UVW[2][s] = sub[no_fra]->velocity[2]
                        + T_t[2][0]*sub[no_fra]->rigid_body[no_obj]->UVW[0][s]
                        + T_t[2][1]*sub[no_fra]->rigid_body[no_obj]->UVW[1][s]
                        + T_t[2][2]*sub[no_fra]->rigid_body[no_obj]->UVW[2][s]
                        + Omega_T[2][0]*sub[no_fra]->rigid_body[no_obj]->XYZ[0][s]
                        + Omega_T[2][1]*sub[no_fra]->rigid_body[no_obj]->XYZ[1][s]
                        + Omega_T[2][2]*sub[no_fra]->rigid_body[no_obj]->XYZ[2][s];
		
		temp->ACC[0][s] = sub[no_fra]->acceleration[0]
                        + T_t[0][0]*sub[no_fra]->rigid_body[no_obj]->ACC[0][s]
                        + T_t[0][1]*sub[no_fra]->rigid_body[no_obj]->ACC[1][s]
                        + T_t[0][2]*sub[no_fra]->rigid_body[no_obj]->ACC[2][s]
                        + 2*Omega_T[0][0]*sub[no_fra]->rigid_body[no_obj]->UVW[0][s]
                        + 2*Omega_T[0][1]*sub[no_fra]->rigid_body[no_obj]->UVW[1][s]
                        + 2*Omega_T[0][2]*sub[no_fra]->rigid_body[no_obj]->UVW[2][s]
                        + (Omega_Omega_T[0][0]+dOmega_T[0][0])*sub[no_fra]->rigid_body[no_obj]->XYZ[0][s]
                        + (Omega_Omega_T[0][1]+dOmega_T[0][1])*sub[no_fra]->rigid_body[no_obj]->XYZ[1][s]
                        + (Omega_Omega_T[0][2]+dOmega_T[0][2])*sub[no_fra]->rigid_body[no_obj]->XYZ[2][s];	    
		temp->ACC[1][s] = sub[no_fra]->acceleration[1]
                        + T_t[1][0]*sub[no_fra]->rigid_body[no_obj]->ACC[0][s]
                        + T_t[1][1]*sub[no_fra]->rigid_body[no_obj]->ACC[1][s]
                        + T_t[1][2]*sub[no_fra]->rigid_body[no_obj]->ACC[2][s]
                        + 2*Omega_T[1][0]*sub[no_fra]->rigid_body[no_obj]->UVW[0][s]
                        + 2*Omega_T[1][1]*sub[no_fra]->rigid_body[no_obj]->UVW[1][s]
                        + 2*Omega_T[1][2]*sub[no_fra]->rigid_body[no_obj]->UVW[2][s]
                        + (Omega_Omega_T[1][0]+dOmega_T[1][0])*sub[no_fra]->rigid_body[no_obj]->XYZ[0][s]
                        + (Omega_Omega_T[1][1]+dOmega_T[1][1])*sub[no_fra]->rigid_body[no_obj]->XYZ[1][s]
                        + (Omega_Omega_T[1][2]+dOmega_T[1][2])*sub[no_fra]->rigid_body[no_obj]->XYZ[2][s];
		temp->ACC[2][s] = sub[no_fra]->acceleration[2]
                        + T_t[2][0]*sub[no_fra]->rigid_body[no_obj]->ACC[0][s]
                        + T_t[2][1]*sub[no_fra]->rigid_body[no_obj]->ACC[1][s]
                        + T_t[2][2]*sub[no_fra]->rigid_body[no_obj]->ACC[2][s]
                        + 2*Omega_T[2][0]*sub[no_fra]->rigid_body[no_obj]->UVW[0][s]
                        + 2*Omega_T[2][1]*sub[no_fra]->rigid_body[no_obj]->UVW[1][s]
                        + 2*Omega_T[2][2]*sub[no_fra]->rigid_body[no_obj]->UVW[2][s]
                        + (Omega_Omega_T[2][0]+dOmega_T[2][0])*sub[no_fra]->rigid_body[no_obj]->XYZ[0][s]
                        + (Omega_Omega_T[2][1]+dOmega_T[2][1])*sub[no_fra]->rigid_body[no_obj]->XYZ[1][s]
                        + (Omega_Omega_T[2][2]+dOmega_T[2][2])*sub[no_fra]->rigid_body[no_obj]->XYZ[2][s];
		
		temp->OUTER_NORMAL_VECTOR[0][s] = 
                          T_t[0][0]*sub[no_fra]->rigid_body[no_obj]->OUTER_NORMAL_VECTOR[0][s]
                        + T_t[0][1]*sub[no_fra]->rigid_body[no_obj]->OUTER_NORMAL_VECTOR[1][s]
                        + T_t[0][2]*sub[no_fra]->rigid_body[no_obj]->OUTER_NORMAL_VECTOR[2][s];
		temp->OUTER_NORMAL_VECTOR[1][s] = 
                          T_t[1][0]*sub[no_fra]->rigid_body[no_obj]->OUTER_NORMAL_VECTOR[0][s]
                        + T_t[1][1]*sub[no_fra]->rigid_body[no_obj]->OUTER_NORMAL_VECTOR[1][s]
                        + T_t[1][2]*sub[no_fra]->rigid_body[no_obj]->OUTER_NORMAL_VECTOR[2][s];
		temp->OUTER_NORMAL_VECTOR[2][s] = 
                          T_t[2][0]*sub[no_fra]->rigid_body[no_obj]->OUTER_NORMAL_VECTOR[0][s]
                        + T_t[2][1]*sub[no_fra]->rigid_body[no_obj]->OUTER_NORMAL_VECTOR[1][s]
                        + T_t[2][2]*sub[no_fra]->rigid_body[no_obj]->OUTER_NORMAL_VECTOR[2][s];
		
		temp->AREA[s]=sub[no_fra]->rigid_body[no_obj]->AREA[s];
		
		temp->INNERMARK[s]=sub[no_fra]->rigid_body[no_obj]->INNERMARK[s];
		temp->OUTERMARK[s]=sub[no_fra]->rigid_body[no_obj]->OUTERMARK[s];
	}
	
	
	for (int j=0; j<temp->TETRAHEDRON_NUMBER; j++)
	{
		temp->TETRAHEDRON[0][j]=sub[no_fra]->rigid_body[no_obj]->TETRAHEDRON[0][j];
		temp->TETRAHEDRON[1][j]=sub[no_fra]->rigid_body[no_obj]->TETRAHEDRON[1][j];
		temp->TETRAHEDRON[2][j]=sub[no_fra]->rigid_body[no_obj]->TETRAHEDRON[2][j];
		temp->TETRAHEDRON[3][j]=sub[no_fra]->rigid_body[no_obj]->TETRAHEDRON[3][j];
	}
	
	for (int j=0; j<temp->TRIANGLE_NUMBER; j++)
	{
		temp->TRIANGLE[0][j]=sub[no_fra]->rigid_body[no_obj]->TRIANGLE[0][j];
		temp->TRIANGLE[1][j]=sub[no_fra]->rigid_body[no_obj]->TRIANGLE[1][j];
		temp->TRIANGLE[2][j]=sub[no_fra]->rigid_body[no_obj]->TRIANGLE[2][j];
	}
	
	
	return temp;
}

void Ref_Frame :: change_ref_frame()
{
    if(subframe_number==0){cout<<"No sub frame exist!"<<endl;}
    if(obj_number==0){cout<<"No object exist in current frame!"<<endl;}
    double T_t[3][3],Omega[3][3],Omega_T[3][3],Omega_Omega_T[3][3],dOmega[3][3],dOmega_T[3][3];
	
    for(int k=0;k<obj_number;k++)
    {
	for(int i=0;i<subframe_number;i++)
	{
	    if(sub[i]->obj_number==0){cout<<"No object exist in sub frame!"<<endl;continue;}
	
	    T_t[0][0]=sub[i]->orientation[0][0];
	    T_t[0][1]=sub[i]->orientation[0][1];
	    T_t[0][2]=sub[i]->orientation[0][2];
	    T_t[1][0]=sub[i]->orientation[1][0];
	    T_t[1][1]=sub[i]->orientation[1][1];
	    T_t[1][2]=sub[i]->orientation[1][2];
	    T_t[2][0]=sub[i]->orientation[2][0];
	    T_t[2][1]=sub[i]->orientation[2][1];
	    T_t[2][2]=sub[i]->orientation[2][2];
			
	    Omega[0][0]=0;
	    Omega[0][1]=-sub[i]->angular_velocity[2];  
	    Omega[0][2]=sub[i]->angular_velocity[1];
	    Omega[1][0]=sub[i]->angular_velocity[2];  
	    Omega[1][1]=0;
	    Omega[1][2]=-sub[i]->angular_velocity[0];
	    Omega[2][0]=-sub[i]->angular_velocity[1]; 
	    Omega[2][1]=sub[i]->angular_velocity[0];   
	    Omega[2][2]=0;
			
	    Multiply(Omega,T_t,Omega_T);
			
	    Multiply(Omega,Omega_T,Omega_Omega_T);
			
	    dOmega[0][0]=0;
	    dOmega[0][1]=-sub[i]->angular_acceleration[2];  
	    dOmega[0][2]=sub[i]->angular_acceleration[1];
	    dOmega[1][0]=sub[i]->angular_acceleration[2];  
	    dOmega[1][1]=0;
	    dOmega[1][2]=-sub[i]->angular_acceleration[0];
	    dOmega[2][0]=-sub[i]->angular_acceleration[1]; 
	    dOmega[2][1]=sub[i]->angular_acceleration[0];
	    dOmega[2][2]=0;
			
	    Multiply(dOmega,T_t,dOmega_T);
			
	    for(int j=0;j<sub[i]->obj_number;j++)
	    {
		if((rigid_body[k]->IDENTITY==sub[i]->rigid_body[j]->IDENTITY)&&(rigid_body[k]->LEVEL==sub[i]->rigid_body[j]->LEVEL-1))
		{
		    for(int s=0;s<rigid_body[k]->POINT_NUMBER;s++)
		    {
			rigid_body[k]->XYZ[0][s] = sub[i]->position[0]
                        + T_t[0][0]*sub[i]->rigid_body[j]->XYZ[0][s]
                        + T_t[0][1]*sub[i]->rigid_body[j]->XYZ[1][s]
                        + T_t[0][2]*sub[i]->rigid_body[j]->XYZ[2][s];
			rigid_body[k]->XYZ[1][s] = sub[i]->position[1]
                        + T_t[1][0]*sub[i]->rigid_body[j]->XYZ[0][s]
                        + T_t[1][1]*sub[i]->rigid_body[j]->XYZ[1][s]
                        + T_t[1][2]*sub[i]->rigid_body[j]->XYZ[2][s];
			rigid_body[k]->XYZ[2][s] = sub[i]->position[2]
                        + T_t[2][0]*sub[i]->rigid_body[j]->XYZ[0][s]
                        + T_t[2][1]*sub[i]->rigid_body[j]->XYZ[1][s]
                        + T_t[2][2]*sub[i]->rigid_body[j]->XYZ[2][s];
						
			rigid_body[k]->UVW[0][s] = sub[i]->velocity[0]
                        + T_t[0][0]*sub[i]->rigid_body[j]->UVW[0][s]
                        + T_t[0][1]*sub[i]->rigid_body[j]->UVW[1][s]
                        + T_t[0][2]*sub[i]->rigid_body[j]->UVW[2][s]
                        + Omega_T[0][0]*sub[i]->rigid_body[j]->XYZ[0][s]
                        + Omega_T[0][1]*sub[i]->rigid_body[j]->XYZ[1][s]
                        + Omega_T[0][2]*sub[i]->rigid_body[j]->XYZ[2][s];
			rigid_body[k]->UVW[1][s] = sub[i]->velocity[1]
                        + T_t[1][0]*sub[i]->rigid_body[j]->UVW[0][s]
                        + T_t[1][1]*sub[i]->rigid_body[j]->UVW[1][s]
                        + T_t[1][2]*sub[i]->rigid_body[j]->UVW[2][s]
                        + Omega_T[1][0]*sub[i]->rigid_body[j]->XYZ[0][s]
                        + Omega_T[1][1]*sub[i]->rigid_body[j]->XYZ[1][s]
                        + Omega_T[1][2]*sub[i]->rigid_body[j]->XYZ[2][s];
			rigid_body[k]->UVW[2][s] = sub[i]->velocity[2]
                        + T_t[2][0]*sub[i]->rigid_body[j]->UVW[0][s]
                        + T_t[2][1]*sub[i]->rigid_body[j]->UVW[1][s]
                        + T_t[2][2]*sub[i]->rigid_body[j]->UVW[2][s]
                        + Omega_T[2][0]*sub[i]->rigid_body[j]->XYZ[0][s]
                        + Omega_T[2][1]*sub[i]->rigid_body[j]->XYZ[1][s]
                        + Omega_T[2][2]*sub[i]->rigid_body[j]->XYZ[2][s];
						
			rigid_body[k]->ACC[0][s] = sub[i]->acceleration[0]
                        + T_t[0][0]*sub[i]->rigid_body[j]->ACC[0][s]
                        + T_t[0][1]*sub[i]->rigid_body[j]->ACC[1][s]
                        + T_t[0][2]*sub[i]->rigid_body[j]->ACC[2][s]
                        + 2*Omega_T[0][0]*sub[i]->rigid_body[j]->UVW[0][s]
                        + 2*Omega_T[0][1]*sub[i]->rigid_body[j]->UVW[1][s]
                        + 2*Omega_T[0][2]*sub[i]->rigid_body[j]->UVW[2][s]
                        + (Omega_Omega_T[0][0]+dOmega_T[0][0])*sub[i]->rigid_body[j]->XYZ[0][s]
                        + (Omega_Omega_T[0][1]+dOmega_T[0][1])*sub[i]->rigid_body[j]->XYZ[1][s]
                        + (Omega_Omega_T[0][2]+dOmega_T[0][2])*sub[i]->rigid_body[j]->XYZ[2][s];
			rigid_body[k]->ACC[1][s] = sub[i]->acceleration[1]
                        +T_t[1][0]*sub[i]->rigid_body[j]->ACC[0][s]
                        +T_t[1][1]*sub[i]->rigid_body[j]->ACC[1][s]
                        +T_t[1][2]*sub[i]->rigid_body[j]->ACC[2][s]
                        +2*Omega_T[1][0]*sub[i]->rigid_body[j]->UVW[0][s]
                        +2*Omega_T[1][1]*sub[i]->rigid_body[j]->UVW[1][s]
                        +2*Omega_T[1][2]*sub[i]->rigid_body[j]->UVW[2][s]
                        +(Omega_Omega_T[1][0]+dOmega_T[1][0])*sub[i]->rigid_body[j]->XYZ[0][s]
                        +(Omega_Omega_T[1][1]+dOmega_T[1][1])*sub[i]->rigid_body[j]->XYZ[1][s]
                        +(Omega_Omega_T[1][2]+dOmega_T[1][2])*sub[i]->rigid_body[j]->XYZ[2][s];
			rigid_body[k]->ACC[2][s]=sub[i]->acceleration[2]
                        +T_t[2][0]*sub[i]->rigid_body[j]->ACC[0][s]
                        +T_t[2][1]*sub[i]->rigid_body[j]->ACC[1][s]
                        +T_t[2][2]*sub[i]->rigid_body[j]->ACC[2][s]
                        +2*Omega_T[2][0]*sub[i]->rigid_body[j]->UVW[0][s]
                        +2*Omega_T[2][1]*sub[i]->rigid_body[j]->UVW[1][s]
                        +2*Omega_T[2][2]*sub[i]->rigid_body[j]->UVW[2][s]
                        +(Omega_Omega_T[2][0]+dOmega_T[2][0])*sub[i]->rigid_body[j]->XYZ[0][s]
                        +(Omega_Omega_T[2][1]+dOmega_T[2][1])*sub[i]->rigid_body[j]->XYZ[1][s]
                        +(Omega_Omega_T[2][2]+dOmega_T[2][2])*sub[i]->rigid_body[j]->XYZ[2][s];
													  
			rigid_body[k]->OUTER_NORMAL_VECTOR[0][s] = 
                          T_t[0][0]*sub[i]->rigid_body[j]->OUTER_NORMAL_VECTOR[0][s]
                        + T_t[0][1]*sub[i]->rigid_body[j]->OUTER_NORMAL_VECTOR[1][s]
                        + T_t[0][2]*sub[i]->rigid_body[j]->OUTER_NORMAL_VECTOR[2][s];
			rigid_body[k]->OUTER_NORMAL_VECTOR[1][s] = 
                          T_t[1][0]*sub[i]->rigid_body[j]->OUTER_NORMAL_VECTOR[0][s]
                        + T_t[1][1]*sub[i]->rigid_body[j]->OUTER_NORMAL_VECTOR[1][s]
                        + T_t[1][2]*sub[i]->rigid_body[j]->OUTER_NORMAL_VECTOR[2][s];
			rigid_body[k]->OUTER_NORMAL_VECTOR[2][s] = 
			              T_t[2][0]*sub[i]->rigid_body[j]->OUTER_NORMAL_VECTOR[0][s]
			            + T_t[2][1]*sub[i]->rigid_body[j]->OUTER_NORMAL_VECTOR[1][s]
			            + T_t[2][2]*sub[i]->rigid_body[j]->OUTER_NORMAL_VECTOR[2][s];

		    }
		}
	    }
	}
    }
}

void inverse_change_ref_frame(Ref_Frame *sour,double *sour_X,double *sour_Y,double *sour_Z,double *sour_U,double *sour_V,double *sour_W,
			      Ref_Frame *dest,double *dest_X,double *dest_Y,double *dest_Z,double *dest_U,double *dest_V,double *dest_W)
{
    if(dest->super!=sour) {cout<<"The input two Frames don't match !"<<endl; return;}
    
    double T_t[3][3],T[3][3],Omega[3][3],Omega_T[3][3],T_Omega_T[3][3];
    T_t[0][0]=dest->orientation[0][0];
    T_t[0][1]=dest->orientation[0][1];
    T_t[0][2]=dest->orientation[0][2];
    T_t[1][0]=dest->orientation[1][0];
    T_t[1][1]=dest->orientation[1][1];
    T_t[1][2]=dest->orientation[1][2];
    T_t[2][0]=dest->orientation[2][0];
    T_t[2][1]=dest->orientation[2][1];
    T_t[2][2]=dest->orientation[2][2];
    
    Transpose(T_t,T);
    
    Omega[0][0]=0;                          Omega[0][1]=-dest->angular_velocity[2];  Omega[0][2]=dest->angular_velocity[1];
    Omega[1][0]=dest->angular_velocity[2];  Omega[1][1]=0;                           Omega[1][2]=-dest->angular_velocity[0];
    Omega[2][0]=-dest->angular_velocity[1]; Omega[2][1]=dest->angular_velocity[0];   Omega[2][2]=0;
    
    Multiply(Omega,T_t,Omega_T);
    Multiply(T,Omega_T,T_Omega_T);
    
    for(int i=0;i<Point_All;i++)
    {
	dest_X[i]=T[0][0]*(sour_X[i]-dest->position[0])+T[0][1]*(sour_Y[i]-dest->position[1])+T[0][2]*(sour_Z[i]-dest->position[2]);
	dest_Y[i]=T[1][0]*(sour_X[i]-dest->position[0])+T[1][1]*(sour_Y[i]-dest->position[1])+T[1][2]*(sour_Z[i]-dest->position[2]);
	dest_Z[i]=T[2][0]*(sour_X[i]-dest->position[0])+T[2][1]*(sour_Y[i]-dest->position[1])+T[2][2]*(sour_Z[i]-dest->position[2]);
	
	dest_U[i]=T[0][0]*(sour_U[i]-dest->velocity[0])+T[0][1]*(sour_V[i]-dest->velocity[1])+T[0][2]*(sour_W[i]-dest->velocity[2])-T_Omega_T[0][0]*dest_X[i]-T_Omega_T[0][1]*dest_Y[i]-T_Omega_T[0][2]*dest_Z[i];
	dest_V[i]=T[1][0]*(sour_U[i]-dest->velocity[0])+T[1][1]*(sour_V[i]-dest->velocity[1])+T[1][2]*(sour_W[i]-dest->velocity[2])-T_Omega_T[1][0]*dest_X[i]-T_Omega_T[1][1]*dest_Y[i]-T_Omega_T[1][2]*dest_Z[i];
	dest_W[i]=T[2][0]*(sour_U[i]-dest->velocity[0])+T[2][1]*(sour_V[i]-dest->velocity[1])+T[2][2]*(sour_W[i]-dest->velocity[2])-T_Omega_T[2][0]*dest_X[i]-T_Omega_T[2][1]*dest_Y[i]-T_Omega_T[2][2]*dest_Z[i];
    }
    
}

void Ref_Frame :: get_wing_mass_from_subframe(int sub_id)
{
    if(subframe_number==0){cout<<"No sub frame exist!"<<endl;}
    if(obj_number==0){cout<<"No object exist in current frame!"<<endl;}
    double T_t[3][3], O_tran[3][3], temp[3][3], Omega[3][3], Omega_T[3][3],Omega_Omega_T[3][3],dOmega[3][3],dOmega_T[3][3];
	
    if(sub[sub_id]->obj_number==0){cout<<"No object exist in sub frame!"<<endl;}

	T_t[0][0]=sub[sub_id]->orientation[0][0];
    T_t[0][1]=sub[sub_id]->orientation[0][1];
    T_t[0][2]=sub[sub_id]->orientation[0][2];
    T_t[1][0]=sub[sub_id]->orientation[1][0];
    T_t[1][1]=sub[sub_id]->orientation[1][1];
    T_t[1][2]=sub[sub_id]->orientation[1][2];
    T_t[2][0]=sub[sub_id]->orientation[2][0];
    T_t[2][1]=sub[sub_id]->orientation[2][1];
    T_t[2][2]=sub[sub_id]->orientation[2][2];

    Omega[0][0]=0;
    Omega[0][1]=-sub[sub_id]->angular_velocity[2];  
    Omega[0][2]=sub[sub_id]->angular_velocity[1];
    Omega[1][0]=sub[sub_id]->angular_velocity[2];  
    Omega[1][1]=0;
    Omega[1][2]=-sub[sub_id]->angular_velocity[0];
    Omega[2][0]=-sub[sub_id]->angular_velocity[1]; 
    Omega[2][1]=sub[sub_id]->angular_velocity[0];   
    Omega[2][2]=0;

	Transpose(sub[sub_id]->orientation,O_tran);
	Multiply(T_t,sub[sub_id]->wing_inertia,temp);
	Multiply(temp,O_tran,wing_inertia);

	Multiply(Omega,T_t,Omega_T);
			
    Multiply(Omega,Omega_T,Omega_Omega_T);
		
    dOmega[0][0]=0;
    dOmega[0][1]=-sub[sub_id]->angular_acceleration[2];  
    dOmega[0][2]=sub[sub_id]->angular_acceleration[1];
    dOmega[1][0]=sub[sub_id]->angular_acceleration[2];  
    dOmega[1][1]=0;
    dOmega[1][2]=-sub[sub_id]->angular_acceleration[0];
    dOmega[2][0]=-sub[sub_id]->angular_acceleration[1]; 
    dOmega[2][1]=sub[sub_id]->angular_acceleration[0];
    dOmega[2][2]=0;
		
    Multiply(dOmega,T_t,dOmega_T);

    for (int i=0;i<3;i++)
    {
    	wingcenter[i] = sub[sub_id]->position[i]
	               + T_t[i][0]*sub[sub_id]->wingcenter[0]
                   + T_t[i][1]*sub[sub_id]->wingcenter[1]
                   + T_t[i][2]*sub[sub_id]->wingcenter[2];

        wingcenterUVW[i] = sub[sub_id]->velocity[i]
                + T_t[i][0]*sub[sub_id]->wingcenterUVW[0]
                + T_t[i][1]*sub[sub_id]->wingcenterUVW[1]
                + T_t[i][2]*sub[sub_id]->wingcenterUVW[2]
                + Omega_T[i][0]*sub[sub_id]->wingcenter[0]
                + Omega_T[i][1]*sub[sub_id]->wingcenter[1]
                + Omega_T[i][2]*sub[sub_id]->wingcenter[2];

        wingcenterACCE[i] = sub[sub_id]->acceleration[i]
                + T_t[i][0]*sub[sub_id]->wingcenterACCE[0]
                + T_t[i][1]*sub[sub_id]->wingcenterACCE[1]
                + T_t[i][2]*sub[sub_id]->wingcenterACCE[2]
                + 2*Omega_T[i][0]*sub[sub_id]->wingcenterUVW[0]
                + 2*Omega_T[i][1]*sub[sub_id]->wingcenterUVW[1]
                + 2*Omega_T[i][2]*sub[sub_id]->wingcenterUVW[2]
                + (Omega_Omega_T[i][0]+dOmega_T[i][0])*sub[sub_id]->wingcenter[0]
                + (Omega_Omega_T[i][1]+dOmega_T[i][1])*sub[sub_id]->wingcenter[1]
                + (Omega_Omega_T[i][2]+dOmega_T[i][2])*sub[sub_id]->wingcenter[2];
    }	   		
}

void Ref_Frame :: get_wing_mass_from_subframe_tmp(int sub_id)
{
    if(subframe_number==0){cout<<"No sub frame exist!"<<endl;}
    if(obj_number==0){cout<<"No object exist in current frame!"<<endl;}
    double T_t[3][3], O_tran[3][3], temp[3][3], Omega[3][3], Omega_T[3][3],Omega_Omega_T[3][3],dOmega[3][3],dOmega_T[3][3];
	
    if(sub[sub_id]->obj_number==0){cout<<"No object exist in sub frame!"<<endl;}

	T_t[0][0]=sub[sub_id]->orientation_tmp[0][0];
    T_t[0][1]=sub[sub_id]->orientation_tmp[0][1];
    T_t[0][2]=sub[sub_id]->orientation_tmp[0][2];
    T_t[1][0]=sub[sub_id]->orientation_tmp[1][0];
    T_t[1][1]=sub[sub_id]->orientation_tmp[1][1];
    T_t[1][2]=sub[sub_id]->orientation_tmp[1][2];
    T_t[2][0]=sub[sub_id]->orientation_tmp[2][0];
    T_t[2][1]=sub[sub_id]->orientation_tmp[2][1];
    T_t[2][2]=sub[sub_id]->orientation_tmp[2][2];

    Omega[0][0]=0;
    Omega[0][1]=-sub[sub_id]->angular_velocity_tmp[2];  
    Omega[0][2]=sub[sub_id]->angular_velocity_tmp[1];
    Omega[1][0]=sub[sub_id]->angular_velocity_tmp[2];  
    Omega[1][1]=0;
    Omega[1][2]=-sub[sub_id]->angular_velocity_tmp[0];
    Omega[2][0]=-sub[sub_id]->angular_velocity_tmp[1]; 
    Omega[2][1]=sub[sub_id]->angular_velocity_tmp[0];   
    Omega[2][2]=0;

	Transpose(sub[sub_id]->orientation_tmp,O_tran);
	Multiply(T_t,sub[sub_id]->wing_inertia,temp);
	Multiply(temp,O_tran,wing_inertia);

	Multiply(Omega,T_t,Omega_T);
			
    Multiply(Omega,Omega_T,Omega_Omega_T);
		
    dOmega[0][0]=0;
    dOmega[0][1]=-sub[sub_id]->angular_acceleration[2];  
    dOmega[0][2]=sub[sub_id]->angular_acceleration[1];
    dOmega[1][0]=sub[sub_id]->angular_acceleration[2];  
    dOmega[1][1]=0;
    dOmega[1][2]=-sub[sub_id]->angular_acceleration[0];
    dOmega[2][0]=-sub[sub_id]->angular_acceleration[1]; 
    dOmega[2][1]=sub[sub_id]->angular_acceleration[0];
    dOmega[2][2]=0;
		
    Multiply(dOmega,T_t,dOmega_T);

    for (int i=0;i<3;i++)
    {
    	wingcenter[i] = sub[sub_id]->position[i]
	               + T_t[i][0]*sub[sub_id]->wingcenter[0]
                   + T_t[i][1]*sub[sub_id]->wingcenter[1]
                   + T_t[i][2]*sub[sub_id]->wingcenter[2];

        wingcenterUVW[i] = sub[sub_id]->velocity_tmp[i]
                + T_t[i][0]*sub[sub_id]->wingcenterUVW[0]
                + T_t[i][1]*sub[sub_id]->wingcenterUVW[1]
                + T_t[i][2]*sub[sub_id]->wingcenterUVW[2]
                + Omega_T[i][0]*sub[sub_id]->wingcenter[0]
                + Omega_T[i][1]*sub[sub_id]->wingcenter[1]
                + Omega_T[i][2]*sub[sub_id]->wingcenter[2];

        wingcenterACCE[i] = sub[sub_id]->acceleration[i]
                + T_t[i][0]*sub[sub_id]->wingcenterACCE[0]
                + T_t[i][1]*sub[sub_id]->wingcenterACCE[1]
                + T_t[i][2]*sub[sub_id]->wingcenterACCE[2]
                + 2*Omega_T[i][0]*sub[sub_id]->wingcenterUVW[0]
                + 2*Omega_T[i][1]*sub[sub_id]->wingcenterUVW[1]
                + 2*Omega_T[i][2]*sub[sub_id]->wingcenterUVW[2]
                + (Omega_Omega_T[i][0]+dOmega_T[i][0])*sub[sub_id]->wingcenter[0]
                + (Omega_Omega_T[i][1]+dOmega_T[i][1])*sub[sub_id]->wingcenter[1]
                + (Omega_Omega_T[i][2]+dOmega_T[i][2])*sub[sub_id]->wingcenter[2];
    }	   		
}
